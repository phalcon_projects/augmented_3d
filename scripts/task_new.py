# -*- coding: utf-8 -*-
from pytube import YouTube
import datetime
import MySQLdb
from datetime import timedelta
from celery import Celery
from celery.task import periodic_task
import sys
import time
#app = Celery('tasks', backend='amqp', broker='amqp://localhost//')

#@periodic_task(run_every = timedelta(seconds=10))
def loader():
    db = MySQLdb.connect(host = 'localhost', user='augmented3d', passwd = 'pA1nM1x68NEOO02w', db="augmented3d", use_unicode=True)
    cursor = db.cursor()
    sql = """Select id, object_id, user_id, link From video_loader Where status='New' Limit 1"""
    cursor.execute(sql)
    data = cursor.fetchall()
    if data:
        for rec in data:
            _id, _object_id, user_id, link = rec
    	#try:	#try:
            cursor.execute("""UPDATE video_loader SET status = 'Loading', object_id = '%(_object_id)s' WHERE id = '%(_id)s'"""%{"_object_id": _object_id,"_id": _id})
        db.commit()

        filename = ( '{:%Y%m%d%H%M%S}'.format(datetime.datetime.now()))
        target = YouTube(link)
        target.set_filename(filename)
		#print(target.get_videos())

        video = target.get('mp4', '360p')
        print 'Loading...'

        video.download('../public/files/'+str(user_id)+'/objects')
        cursor.execute("""UPDATE video_loader SET status = 'Loaded', filename =%(filename)s WHERE id = '%(_id)s'"""%{"_id": _id, "filename":filename})
        cursor.execute("""UPDATE objects SET type = 'video/mp4', filename =%(filename)s WHERE id = '%(_object_id)s'"""%{"_object_id": _object_id, "filename":filename})
        db.commit()
        #except:
           #  cursor.execute("""UPDATE video_loader SET status = 'Error' WHERE id = '%(_id)s'"""%{"_id": _id})
             #db.commit()

             #print 'Errors found'
           
            
		#except:
		#	print "Unsuported format"
    else:
        print "Waiting"
       
        #pass
    db.close()
    return "Working...."

loader()


