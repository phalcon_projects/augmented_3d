/* global __dirname */
"use strict";

/*
 run as server and receives queue control commands
 +filename --- add to convertation queue. return status.
 -filename --- remove from queue and delete status record.  return status.
 ?filename --- return status.
file status:
"not found" - if no such file in queue
"wait" - file waits in queue
"processing" - convertation in progress
 <number> - ffmpeg result code
 */

const net = require('net');
const fs = require('fs');
const path = require('path');
const spawn = require('child_process').spawn;
//let clientSocket = null;
const idle_timeout = 10000;
let queue = [];
let file_status = {};
let idle_shutdown_timeout;

//let run_log = fs.createWriteStream('run_log' + (new Date().getTime()) + '.txt');
function wlog(text) {
	console.log(text);
	//run_log.write(text + "\n");
}
//run_log.write("");

function stop_server() {
//	run_log.close();
	server.close();
	process.exit();
}

process.chdir(path.join(__dirname, '/../public/')); // '/../public/files/objects/'

const server = net.createServer((socket) => {
	wlog('Client connected.');
	
	socket.on('data', (data) => {
		wlog('incoming data: ' + data);
		let filename = data.toString().trim();
		let command = filename.substr(0,1);
		filename = filename.substr(1);
		wlog('cmd: ' + command + ',filename: ' + filename);

		if (filename === '!server shutdown!') {
			wlog("server shutdown requested");
			stop_server();
			return;
		}

		let status = get_file_status(filename);
		if (typeof status == 'undefined')
			status = "not found";

		switch (command) {
			case "+":
				if (status === "not found")
					add_to_queue(filename);
				break;
			case "-":
				forget_file(filename);
				break;
			case "?":
				break;
			return;
		}

		socket.write(String(status));
	});
	socket.on('end', () => {
		wlog('Client disconnected.');
	});
});
server.on('error', (err) => { throw err; });
server.listen(8124, () => { wlog('Server bound.'); });

wait_and_shutdown(60000); // wait a minute and shutdown if no activity

// setTimeout(() => {
// 	wlog("server close by timeout");
// 	stop_server();
// }, 30000);


function get_file_status(filename) {
	return file_status[filename];
}

function add_to_queue(filename) {
	stop_shutdown_timeout();

	if (typeof file_status[filename] === 'undefined') {
		file_status[filename] = "wait";
		queue.push(filename);
	}

	if (queue.length == 1) {
		convert_next_in_queue();
	}

	return file_status[filename];
}

function forget_file(filename) {
	delete file_status[filename];
	let index = queue.indexOf(filename);
	if (index !== -1)
		queue.splice(index, 1);
}


function convert_next_in_queue() {
	console.log("CALL:convert_next_in_queue");
	console.log("queue", queue);
	if (!queue.length) {
		wlog("no more files in queue. stop server.");
		wait_and_shutdown();
		return;
	}

	stop_shutdown_timeout();

	let filename = queue.shift();
	file_status[filename] = "processing";

	wlog("processing file [" + filename + "]");

	let out_filename = filename + '.mp4';
	if (fs.existsSync(out_filename)) {
		fs.unlinkSync(out_filename);
	}

	let log = fs.createWriteStream(out_filename + '_log.txt');
	const ffmpeg = spawn('ffmpeg', ['-i', filename, out_filename]);

	ffmpeg.stdout.on('data', (data) => { log.write(data); });
	ffmpeg.stderr.on('data', (data) => { log.write(data); });

	ffmpeg.on('close', (code) => { // Завершення кодування відео.
		file_status[filename] = code;
		wlog('ffmpeg process exited with code ' + code + '.');
		log.write('ffmpeg process exited with code ' + code + '.\n');
		log.write('[' + code + ']'); // for quick check the code from php
		log.close();

		wlog("run convert_next_in_queue");
		convert_next_in_queue();
	});
}

function wait_and_shutdown(timeout) {
	if (typeof timeout !== 'number')
		timeout = idle_timeout;

	wlog("server will shutdown in " + timeout/1000 + "seconds");
	idle_shutdown_timeout = setTimeout(() => {
		stop_server();
	}, timeout);
}

function stop_shutdown_timeout() {
	clearTimeout(idle_shutdown_timeout);
	idle_shutdown_timeout = 0;
	wlog("shutdown timer stopped");
}