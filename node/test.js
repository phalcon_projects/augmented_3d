/* global __dirname */
"use strict";

const net = require('net');
const fs = require('fs');
const path = require('path');
const spawn = require('child_process').spawn;
//let clientSocket = null;

const idle_timeout = 10000;
let idle_shutdown_timeout;

let working_dir = process.argv[2];
let infile = process.argv[3];
let outfile = process.argv[4];

function wlog(text) {
	console.log(text);
}

process.chdir(working_dir);

let log = fs.createWriteStream('_log.txt');
const wkhtmltopdf = spawn('/usr/local/bin/wkhtmltopdf.sh', [infile, outfile]);

wkhtmltopdf.stdout.on('data', (data) => { log.write(data); });
wkhtmltopdf.stderr.on('data', (data) => { log.write(data); });

wkhtmltopdf.on('close', (code) => {
	wlog('wkhtmltopdf process exited with code ' + code + '.');
	log.write('wkhtmltopdf process exited with code ' + code + '.\n');
	log.write('[' + code + ']'); // for quick check the code from php
	log.close();
	process.exit();
});

//process.exit();
wait_and_shutdown(20000); // wait a minute and shutdown if no activity

function wait_and_shutdown(timeout) {
    if (typeof timeout !== 'number')
        timeout = idle_timeout;

    wlog("server will shutdown in " + timeout/1000 + "seconds");
    idle_shutdown_timeout = setTimeout(() => {
        process.exit();
}, timeout);
}
