<?php

error_reporting(E_ALL);

use Phalcon\Mvc\Application;

try {
    define('APP_PATH', realpath('..'));

    /**
     * Read the configuration
     */
    require_once APP_PATH . '/app/library/Config/IniRaw.php';
    $config = new Config\IniRaw(APP_PATH . '/app/config/config.ini', INI_SCANNER_RAW);

    if (is_readable(APP_PATH . '/app/config/config.ini.dev')) {
        $override = new Config\IniRaw(APP_PATH . '/app/config/config.ini.dev', INI_SCANNER_RAW);
        $config->merge($override);
    }

    /**
     * Auto-loader configuration
     */
    require APP_PATH . '/app/config/loader.php';

    $application = new Application(new Services($config));

    // NGINX - PHP-FPM already set PATH_INFO variable to handle route
    $serverValue = null;

    switch ($config->server->type) {
        case 'apache':
            $serverValue = !empty($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : null;
            break;
        case 'nginx':
            if (!empty($_GET['_url'])) {
                $serverValue = $_GET['_url'];
            } else {
                $serverValue = !empty($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : null;
            }
            break;
    }

    echo $application->handle($serverValue)->getContent();
} catch (Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
