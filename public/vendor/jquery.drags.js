(function($) {
	$.fn.drags = function(opt) {
		var $el;
		opt = $.extend({handle:"", cursor:"move", margins: 0}, opt);

		if(opt.handle === "") {
			$el = this;
		} else {
			$el = this.find(opt.handle);
		}

		var container = this.parent();

		var z_idx,
			drg_h, drg_w,
			pos_y, pos_x;
		var parent_mousemove, end_drag;
		var $drag = this;

		parent_mousemove = function(e) {
			var cont_h = container.outerHeight(); // should i get it just once?
			var cont_w = container.outerWidth();

			var rel_x = e.pageX - container.offset().left; // should i get it just once?
			var rel_y = e.pageY - container.offset().top;
			
			var x = rel_x + pos_x - drg_w;
			var y = rel_y + pos_y - drg_h;

			if (x < opt.margins) x = opt.margins;
			if (y < opt.margins) y = opt.margins;
			if (x + drg_w > (cont_w - opt.margins)) x = cont_w - opt.margins - drg_w;
			if (y + drg_h > (cont_h - opt.margins)) y = cont_h - opt.margins - drg_h;

			var pos = {};
			if (x <= cont_w/2) {
				pos['left'] = x;
				pos['right'] = 'auto';
			} else {
				pos['left'] = 'auto';
				pos['right'] = cont_w - (x + drg_w);
			}

			if (y <= cont_h/2) {
				pos['top'] = y;
				pos['bottom'] = 'auto';
			} else {
				pos['top'] = 'auto';
				pos['bottom'] = cont_h - (y + drg_h);
			}

			$drag.css(pos);
		};

		end_drag = function() { // end drag
			container.off("mousemove", parent_mousemove);
			$drag.off("mouseup", end_drag);

			$drag.removeClass('draggable').css('z-index', z_idx);
		};

		return $el.css('cursor', opt.cursor).on("mousedown", function(e) {
			if (e.which !== 1)
				return;

			$drag.addClass('draggable');
			if(opt.handle !== "") {
				$(this).addClass('active-handle');
			}

			z_idx = $drag.css('z-index');
			drg_h = $drag.outerHeight();
			drg_w = $drag.outerWidth();
			pos_y = $drag.offset().top + drg_h - e.pageY;
			pos_x = $drag.offset().left + drg_w - e.pageX;

			$drag.one("mouseup", end_drag);
			$(document).one("mouseup", end_drag);

			$drag.css('z-index', 1000);
			container.on("mousemove", parent_mousemove);


			//$drag.parent().one('mouseleave', function(){
			//
			//})

			e.preventDefault(); // disable selection
		}).on("mouseup", function(e) {
			if (e.which !== 1)
				return;

			if(opt.handle === "") {
				$(this).removeClass('draggable');
			} else {
				$(this).removeClass('active-handle').parent().removeClass('draggable');
			}
			end_drag();
		});

	}
})(jQuery);
