/**
 * based on TransformControls.js by arodic (https://github.com/arodic)
 */
( function () {

	'use strict';

	var GizmoMaterial = function ( parameters ) {

		THREE.MeshBasicMaterial.call( this );

		this.depthTest = false;
		this.depthWrite = false;
		this.side = THREE.FrontSide;
		this.transparent = true;

		this.setValues( parameters );

		this.oldColor = this.color.clone();
		this.oldOpacity = this.opacity;

		this.highlight = function( highlighted ) {

			if ( highlighted ) {

				this.color.setRGB( 1, 1, 0 );
				this.opacity = 1;

			} else {

				this.color.copy( this.oldColor );
				this.opacity = this.oldOpacity;

			}

		};

	};

	GizmoMaterial.prototype = Object.create( THREE.MeshBasicMaterial.prototype );
	GizmoMaterial.prototype.constructor = GizmoMaterial;


	var GizmoLineMaterial = function ( parameters ) {

		THREE.LineBasicMaterial.call( this );

		this.depthTest = false;
		this.depthWrite = false;
		this.transparent = true;
		this.linewidth = 1;

		this.setValues( parameters );

		this.oldColor = this.color.clone();
		this.oldOpacity = this.opacity;

		this.highlight = function( highlighted ) {

			if ( highlighted ) {

				this.color.setRGB( 1, 1, 0 );
				this.opacity = 1;

			} else {

				this.color.copy( this.oldColor );
				this.opacity = this.oldOpacity;

			}

		};

	};

	GizmoLineMaterial.prototype = Object.create( THREE.LineBasicMaterial.prototype );
	GizmoLineMaterial.prototype.constructor = GizmoLineMaterial;

	THREE.ScaleControl2 = function ( camera, scene, domElement ) {

		THREE.Object3D.call( this );

		domElement = ( domElement !== undefined ) ? domElement : document;

		this.object = undefined;
		this.visible = false;
		this.activeHandle = null;

		this.debug = false;

		var _handles_stack = [];
		var handles = {bottom:[], upper:[]};
		var pivot = new THREE.Object3D();
		scene.add(pivot);

		var _plane = new THREE.Plane();
		var _intersection = new THREE.Vector3();
		var bbox = new THREE.Box3();

		// Create a basic rectangle geometry
		var planeGeometry = new THREE.PlaneGeometry(60, 60);
		// Create mesh with the geometry
		var planeMaterial = new THREE.MeshLambertMaterial({color: 0xffff00, side: THREE.DoubleSide});
		var dispPlane = new THREE.Mesh(planeGeometry, planeMaterial);

		if (this.debug) {
			scene.add(dispPlane);
		}



		var scope = this;

		var _dragging = false;

		var changeEvent = { type: "change" };
		var mouseDownEvent = { type: "mouseDown" };
		var mouseUpEvent = { type: "mouseUp" };

		var ray = new THREE.Raycaster();
		var pointerVector = new THREE.Vector2();

		var offset = new THREE.Vector3();

		var eye = new THREE.Vector3();

		var tempMatrix = new THREE.Matrix4();

		var anchorPosition = new THREE.Vector3();
		var oldRadius;

		var worldPosition = new THREE.Vector3();
		var worldRotation = new THREE.Euler();
		var camPosition = new THREE.Vector3();
		var camRotation = new THREE.Euler();

		function init() {
			generate_ui();
			scope.setHandleSizes(2);
		}

		domElement.addEventListener( "mousedown", onPointerDown, false );
		domElement.addEventListener( "touchstart", onPointerDown, false );

		domElement.addEventListener( "mousemove", onPointerHover, false );
		domElement.addEventListener( "touchmove", onPointerHover, false );

		domElement.addEventListener( "mousemove", onPointerMove, false );
		domElement.addEventListener( "touchmove", onPointerMove, false );

		domElement.addEventListener( "mouseup", onPointerUp, false );
		domElement.addEventListener( "mouseout", onPointerUp, false );
		domElement.addEventListener( "touchend", onPointerUp, false );
		domElement.addEventListener( "touchcancel", onPointerUp, false );
		domElement.addEventListener( "touchleave", onPointerUp, false );

		this.dispose = function () {

			domElement.removeEventListener( "mousedown", onPointerDown );
			domElement.removeEventListener( "touchstart", onPointerDown );

			domElement.removeEventListener( "mousemove", onPointerHover );
			domElement.removeEventListener( "touchmove", onPointerHover );

			domElement.removeEventListener( "mousemove", onPointerMove );
			domElement.removeEventListener( "touchmove", onPointerMove );

			domElement.removeEventListener( "mouseup", onPointerUp );
			domElement.removeEventListener( "mouseout", onPointerUp );
			domElement.removeEventListener( "touchend", onPointerUp );
			domElement.removeEventListener( "touchcancel", onPointerUp );
			domElement.removeEventListener( "touchleave", onPointerUp );

		};

		function generate_ui() {
			var geometry = new THREE.SphereBufferGeometry(1,6,6);
			var i;

			for (i=0;i<4;i++) {
				handles.bottom[i] = new THREE.Mesh(geometry, new GizmoMaterial({ color: 0x00ff00, visible:true }));
				handles.upper[i] = new THREE.Mesh(geometry, new GizmoMaterial({ color: 0x00ff00, visible:true }));

				handles.bottom[i].name = "bottom" + i;
				handles.upper[i].name = "upper" + i;

				_handles_stack.push(handles.bottom[i]);
				_handles_stack.push(handles.upper[i]);
			}

			if (scope.debug) {
				scope.checkPoint = new THREE.Mesh(geometry, new GizmoMaterial({color: 0x0000ff, visible: true}));
				scope.checkPoint2 = new THREE.Mesh(geometry, new GizmoMaterial({color: 0xff0000, visible: true}));
			}

			for (i=0;i<4;i++) {
				// pre-assing opposite handles
				var opposite_index = (i + 2) % 4;
				handles.bottom[i].anchor = handles.upper[opposite_index];
				handles.upper[opposite_index].anchor = handles.bottom[i];
			}

		}

		this.setCamera = function(new_camera) {
			camera = new_camera;
		};

		this.setHandleSizes = function(size) {
			size = Math.max(size, 1);

			for (var i=0;i<_handles_stack.length;i++) {
				_handles_stack[i].scale.set(size,size,size);
			}
		};

		function highlight_handle() {
			var highlight;
			for (var i=0;i<_handles_stack.length;i++) {
				highlight = scope.activeHandle && ( _handles_stack[i] === scope.activeHandle || _handles_stack[i] === scope.activeHandle.anchor );

				_handles_stack[i].material.highlight(highlight);
			}
		}


		this.attach = function ( object ) {

			scope.object = object;
			scope.visible = true;
			scope.update();

			// add handles to scene
			for (var i=0;i<_handles_stack.length;i++) {
				scene.add(_handles_stack[i]);
			}

			if (scope.debug) {
				scene.add(scope.checkPoint);
				scene.add(scope.checkPoint2);
			}
            scope.dispatchEvent( changeEvent );
		};

		this.detach = function () {

			scope.object = undefined;
			scope.visible = false;
			scope.activeHandle = null;

			// add handles to scene
			for (var i=0;i<_handles_stack.length;i++) {
				scene.remove(_handles_stack[i]);
			}

			if (scope.debug) {
				scene.remove(scope.checkPoint);
				scene.remove(scope.checkPoint2);
			}
		};

		this.update = function () {

			if ( scope.object === undefined ) return;

			scope.object.updateMatrixWorld();
			worldPosition.setFromMatrixPosition( scope.object.matrixWorld );
			worldRotation.setFromRotationMatrix( tempMatrix.extractRotation( scope.object.matrixWorld ) );

			camera.updateMatrixWorld();
			camPosition.setFromMatrixPosition( camera.matrixWorld );
			camRotation.setFromRotationMatrix( tempMatrix.extractRotation( camera.matrixWorld ) );

			//scale = worldPosition.distanceTo( camPosition ) / 6 * scope.size;
			this.position.copy( worldPosition );

			if ( camera instanceof THREE.PerspectiveCamera ) {

				eye.copy( camPosition ).sub( worldPosition ).normalize();

			} else if ( camera instanceof THREE.OrthographicCamera ) {

				eye.copy( camPosition ).normalize();

			}

			// reposition and show handles
			bbox.setFromObject(scope.object);

			var pos = new THREE.Vector3();
			// bottom
			pos.y = bbox.min.y;

			pos.x = bbox.min.x;
			pos.z = bbox.min.z;
			handles.bottom[0].position.copy(pos);

			pos.x = bbox.max.x;
			pos.z = bbox.min.z;
			handles.bottom[1].position.copy(pos);

			pos.x = bbox.max.x;
			pos.z = bbox.max.z;
			handles.bottom[2].position.copy(pos);

			pos.x = bbox.min.x;
			pos.z = bbox.max.z;
			handles.bottom[3].position.copy(pos);


			// upper
			pos.y = bbox.max.y;

			pos.x = bbox.min.x;
			pos.z = bbox.min.z;
			handles.upper[0].position.copy(pos);

			pos.x = bbox.max.x;
			pos.z = bbox.min.z;
			handles.upper[1].position.copy(pos);

			pos.x = bbox.max.x;
			pos.z = bbox.max.z;
			handles.upper[2].position.copy(pos);

			pos.x = bbox.min.x;
			pos.z = bbox.max.z;
			handles.upper[3].position.copy(pos);

			highlight_handle();
		};

		function nearestPointOnRay(ray_point1, ray_point2, source) {
			var targetPoint = ray_point2.clone().sub(ray_point1).normalize();

			var lp = source.clone().sub(ray_point2);
			var lambda = targetPoint.dot(lp);
			targetPoint.multiplyScalar(lambda);
			targetPoint.add(ray_point2);

			return targetPoint;
		}

		function onPointerHover( event ) {

			if ( scope.object === undefined || _dragging === true || ( event.button !== undefined && event.button !== 0 ) ) return;

			var pointer = event.changedTouches ? event.changedTouches[ 0 ] : event;

			var intersect = intersectObjects( pointer, _handles_stack );

			var handle = null;

			if ( intersect ) {

				handle = intersect.object;

				event.preventDefault();

				//domElement.style.cursor = 'pointer';
			} else {
				//domElement.style.cursor = 'auto';
			}

			if ( scope.activeHandle !== handle ) {

				if (handle) {
					var targetPoint = nearestPointOnRay(handle.position, handle.anchor.position, camera.position);

					var thrd = new THREE.Vector3();
					thrd.crossVectors ( targetPoint.clone().sub(handle.anchor.position), targetPoint.clone().sub(camera.position) );
					thrd.add(targetPoint);

					_plane.setFromCoplanarPoints(handle.anchor.position, targetPoint, thrd);

					if (scope.debug) {
						var geometry = new THREE.Geometry();
						geometry.vertices.push(handle.anchor.position.clone());
						geometry.vertices.push(targetPoint.clone());
						geometry.vertices.push(camera.position.clone());
						geometry.vertices.push(targetPoint.clone());
						geometry.vertices.push(thrd.clone().multiplyScalar(2));
						// Note that lines are drawn between each consecutive pair of vertices, but not between the first and last (the line is not closed.)

						// Now that we have points for two lines and a material, we can put them together to form a line.
						var line = new THREE.Line(geometry, new GizmoLineMaterial({color: 0x00ffff}));
						scene.add(line);

						// Align the geometry to the plane
						var coplanarPoint = _plane.coplanarPoint();
						var focalPoint = new THREE.Vector3().copy(coplanarPoint).add(_plane.normal);
						dispPlane.position.set(coplanarPoint.x, coplanarPoint.y, coplanarPoint.z);
						dispPlane.lookAt(focalPoint);
					}
				}

				scope.activeHandle = handle;
				scope.update();
				scope.dispatchEvent( changeEvent );

			}
            scope.dispatchEvent( changeEvent );

		}

		function onPointerDown( event ) {

			if ( scope.object === undefined || _dragging === true || ( event.button !== undefined && event.button !== 0 ) ) return;

			var pointer = event.changedTouches ? event.changedTouches[ 0 ] : event;

			if ( pointer.button === 0 || pointer.button === undefined ) {

				var intersect = intersectObjects( pointer, _handles_stack );

				if ( intersect ) {

					event.preventDefault();
					event.stopPropagation();

					scope.dispatchEvent( mouseDownEvent );

					scope.activeHandle = intersect.object;



					scope.update();


					if ( ray.ray.intersectPlane( _plane, _intersection ) ) {

						offset.copy( _intersection ).sub( scope.activeHandle.position );

					}

					scope.grabObject();
				}

			}

			_dragging = true;

		}

		scope.grabObject = function() {
            anchorPosition.copy(scope.activeHandle.anchor.position);
            oldRadius = anchorPosition.distanceTo(scope.activeHandle.position);

            pivot.scale.set( 1,1,1 );
            pivot.position.copy( anchorPosition );
            pivot.updateMatrixWorld();
            THREE.SceneUtils.attach( scope.object, scene, pivot );
		};

        scope.releaseObject = function() {
            pivot.updateMatrixWorld();
            scope.object.updateMatrixWorld(); // if not done by the renderer
            THREE.SceneUtils.detach( scope.object, pivot, scene );
        };

        scope._scaleObject = function(scale){
            pivot.scale.set(scale, scale, scale);
		};

		function onPointerMove( event ) {
			if ( scope.object === undefined || scope.activeHandle === null || _dragging === false || ( event.button !== undefined && event.button !== 0 ) ) return;

			event.preventDefault();
			event.stopPropagation();

			var pointer = event.changedTouches ? event.changedTouches[ 0 ] : event;
			var rect = domElement.getBoundingClientRect();

			pointerVector.x = ( (pointer.clientX - rect.left) / rect.width ) * 2 - 1;
			pointerVector.y = - ( (pointer.clientY - rect.top) / rect.height ) * 2 + 1;

			ray.setFromCamera( pointerVector, camera );

			if ( ray.ray.intersectPlane( _plane, _intersection ) ) {

				var targetPoint = _intersection.sub( offset );
				var targetPoint2 = nearestPointOnRay(scope.activeHandle.position, anchorPosition, targetPoint);

				if (scope.debug) {
					scope.checkPoint.position.copy(targetPoint);
					scope.checkPoint2.position.copy(targetPoint2);
				}

				var radius = anchorPosition.distanceTo(targetPoint2);
				var changeCoeff = radius / oldRadius;

				scope._scaleObject(changeCoeff);

			} else {
				// console.log("NOT intersectPlane");
			}

			scope.update();
			scope.dispatchEvent( changeEvent );
			scope.dispatchEvent( { type: "objectChange", object: scope.object, pivot: pivot } );

		}

		function onPointerUp( event ) {

			event.preventDefault(); // Prevent MouseEvent on mobile

			if ( event.button !== undefined && event.button !== 0 ) return;

			if ( _dragging && ( scope.activeHandle !== null ) ) {

				scope.releaseObject();

				scope.dispatchEvent( mouseUpEvent );

			}

			_dragging = false;

			if ( 'TouchEvent' in window && event instanceof TouchEvent ) {

				// Force "rollover"

				scope.activeHandle = null;
				scope.update();
				scope.dispatchEvent( changeEvent );

			} else {

				onPointerHover( event );

			}

		}

		function intersectObjects( pointer, objects ) {

			var rect = domElement.getBoundingClientRect();
			var x = ( pointer.clientX - rect.left ) / rect.width;
			var y = ( pointer.clientY - rect.top ) / rect.height;

			pointerVector.set( ( x * 2 ) - 1, - ( y * 2 ) + 1 );
			ray.setFromCamera( pointerVector, camera );

			var intersections = ray.intersectObjects( objects, true );
			return intersections[ 0 ] ? intersections[ 0 ] : false;

		}

		init();
	};

	THREE.ScaleControl2.prototype = Object.create( THREE.Object3D.prototype );
	THREE.ScaleControl2.prototype.constructor = THREE.ScaleControl2;

}() );
