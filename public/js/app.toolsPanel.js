(function (app) {
    'use strict';

    $('#settings-bar-toggle').on('click', function (event) {
        event.preventDefault();

        $('body').toggleClass('right-sidebar-hidden').trigger('viewport-update');
    });

    $('body').bind('click', function (e) {
        if ($(e.target).attr('data-toggle') !== 'popover' && $('.popover').length !== 0) {
            $('[data-toggle="popover"]').popover('hide');
        }
    });

    app.initUploadSelect = function () {
        app.loadObjectDialog(true);
    };

    app.showUploads = function () {
        $('#modal-object-library').modal('show');
    };

    $('.main-sidebar').hide();

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target;
        e.relatedTarget;
    });

    var $modalAddYoutubeLink = $('#modal-add-youtube-link');
    var $modalImportYouTubeVideo = $('#modal-import-youtube-video');
    var $modalImportYouTubeVideoError = $('#modal-import-youtube-video-error');
    var $modalVideoLinkAdd = $('#modal-video-link-add');

    $('.action.youtube-link-preview').on('click', function (event) {
        event.preventDefault();

        var accessToken = $('#youtube-at').html();

        if (accessToken === undefined || !accessToken || accessToken === '') {
            //return;
        }

        var link = $('input[name="video-youtube-link"]').val();
        var youTubeAPIKey = 'AIzaSyCcYzJHz4YF0V5Hx3XHnn87GwogIAp9ko0';
        var youTubeAPIURL = 'https://www.googleapis.com/youtube/v3/videos';
        var youtubeVideoId;

        if (link.indexOf('watch?v=') !== -1) {
            youtubeVideoId = link.split('watch?v=').pop();
            if (youtubeVideoId.indexOf('&') !== -1) {
                youtubeVideoId = youtubeVideoId.substr(0, youtubeVideoId.indexOf('&'));
            }
        } else if (link.indexOf('youtu.be') !== -1) {
            youtubeVideoId = link.split('/').pop();
        }

        if (youtubeVideoId && youtubeVideoId.length > 0) {
            $.ajax({
                url: youTubeAPIURL,
                dataType: 'jsonp',
                data: {
                    id: youtubeVideoId,
                    part: 'contentDetails,snippet',
                    //access_token: accessToken,
                    key: youTubeAPIKey
                },
                success: function (data) {
                    if (data && !data.error && data.items && data.items.length > 0) {
                        if (/*data.items[0].fileDetails && */data.items[0].contentDetails && data.items[0].snippet) {
                            var duration = moment.duration(data.items[0].contentDetails.duration);
                            var title = data.items[0].snippet.title;
                            var videoWidth = 'unknown';
                            var videoHeight = 'unknown';
                            var thumbnail = '';

                            var videoResolutionType = null;

                            if (data.items[0].snippet.thumbnails.hasOwnProperty('maxres')) {
                                videoResolutionType = 'maxres';
                            } else if (data.items[0].snippet.thumbnails.hasOwnProperty('high')) {
                                videoResolutionType = 'high';
                            } else if (data.items[0].snippet.thumbnails.hasOwnProperty('medium')) {
                                videoResolutionType = 'medium';
                            } else if (data.items[0].snippet.thumbnails.hasOwnProperty('standard')) {
                                videoResolutionType = 'standard';
                            } else if (data.items[0].snippet.thumbnails.hasOwnProperty('default')) {
                                videoResolutionType = 'default';
                            }

                            if (videoResolutionType.length) {
                                thumbnail = data.items[0].snippet.thumbnails[videoResolutionType].url;
                                videoWidth = data.items[0].snippet.thumbnails[videoResolutionType].width;
                                videoHeight = data.items[0].snippet.thumbnails[videoResolutionType].height;
                            }

                            if (data.items[0].fileDetails && data.items[0].fileDetails.videoStreams) {
                                videoWidth = data.items[0].fileDetails.videoStreams.widthPixels;
                                videoHeight = data.items[0].fileDetails.videoStreams.heightPixels;
                            }

                            var linkParams = {
                                link: link,
                                title: title,
                                thumb: thumbnail,
                                width: videoWidth,
                                height: videoHeight
                            };

                            $modalImportYouTubeVideo.find('.modal-title').html(title);
                            $modalImportYouTubeVideo.find('.video-thumbnail').attr('src', thumbnail);
                            $modalImportYouTubeVideo.find('.video-duration').html(duration.humanize());
                            $modalImportYouTubeVideo.find('.video-width').html(videoWidth);
                            $modalImportYouTubeVideo.find('.video-height').html(videoHeight);
                            $modalImportYouTubeVideo.find('.action.import').data('link-params', linkParams);

                            $modalAddYoutubeLink.modal('hide');
                            $modalImportYouTubeVideo.modal('show');

                        } else {
                            $modalImportYouTubeVideoError.modal('show');
                        }
                    } else {
                        $modalAddYoutubeLink.find('.link-error').show();
                    }
                },
                error: function (jqXHR, status, error) {
                    console.error(status + '  ' + error);
                }
            });
        } else {
            $modalAddYoutubeLink.find('.link-error').show();
        }
    });

    $('.action.video-link-add').on('click', function (event) {
        event.preventDefault();

        var link = $modalVideoLinkAdd.find('input[name="video-link"]').val();

        $.ajax({
            url: '/video-link/add',
            data: {
                'edition_id': app.editionId(),
                'cover_id': app.coverId(),
                'link': link
            },
            method: 'post'
        }).then(function (response) {
        });

        $modalVideoLinkAdd.modal('hide');
    });

    $modalAddYoutubeLink.find('.link-error').hide();

    $modalAddYoutubeLink.find('#video-youtube-link').on('change', function () {
        $modalAddYoutubeLink.find('.link-error').hide();
    });

    $modalImportYouTubeVideo.find('.action.import').on('click', function () {
        var linkParams = $(this).data('link-params');
        var data = $.extend({}, linkParams, {
            'edition_id': app.editionId(),
            'cover_id': app.coverId(),
        });

        $.ajax({
            url: 'video-link/save',
            data: data,
            method: 'post'
        }).then(function (response) {
            if (typeof response === 'object') {
                if (response.status === 'ok') {
                    showMessage('Video download started', 'ok', 5000);
                    $('#modal-add-youtube-link').modal('hide');

                    app.mediaLibrary.refreshList().then(function () {
                        if (typeof response.lib_object !== 'undefined') {
                            app.addLibraryObject(response.lib_object);
                        }
                    });
                } else {
                    console.error(response);
                    showError(response.errors.join('<br>'));
                }
            } else {
                console.error(response);
                showError(response);
            }
        });
        $modalImportYouTubeVideo.modal('hide');
    });

    /**
     *
     * @returns {ToolsPanel}
     * @constructor
     */
    function ToolsPanel() {
        var self = this;

        self._buttons = {};

        self.init = function () {
            self.panel = $('#tools-panel');
            var button;

            /*panel.append(panel_button('&#xE872;', 'Delete (Del)', function () {
             	if (transform_control && transform_control.object) {
             		delete_object(transform_control.object);
             	}
             }));*/

            var transformModeButtonsSet = new PanelButtonSet();
            button = new PanelButton('move', ['Move', 'Move (M)'], function () {
                app.setMode('translate');
            });
            transformModeButtonsSet.add(button);
            transformModeButtonsSet.setActive(button);

            button = new PanelButton('scale', ['Scale', 'Scale (S)'], function () {
                app.setMode('scale');
            });
            transformModeButtonsSet.add(button);
            self.addButtonSet(transformModeButtonsSet);


            button = new PanelButton('rotation', ['Rotate', 'Rotate (R)'], function () {
                app.setMode('rotate');
            });
            transformModeButtonsSet.add(button);

            $('.scene-action-bundle-generate').on('click', function (event) {
                event.preventDefault();

                var self = $(this);
                self.attr('disabled', true);
                self.html('<img src="/img/tail-spin-small-dark.svg">');

                $.ajax({
                    url: '/index/bundle/',
                    data: {
                        edition_id: app.editionId()
                    },
                    method: 'get'
                }).then(function (response) {
                    if (response.errors === undefined || response.errors.length === 0) {
                        showMessage('Bundle is generated');
                    } else {
                        console.error('Bundle generation error', response.errors);
                        showError('Error bundle generation');
                    }
                }, function (error) {
                    var errorMessage = 'Error bundle generation';
                    if (error.statusText) {
                        errorMessage += '. ' + error.statusText;
                    }
                    showError(errorMessage);
                }).always(function () {
                    self.html('Publish');
                    self.attr('disabled', false);
                });
            });

            $('.scene-action-save').on('click', function (event) {
                event.preventDefault();

                var self = $(this);
                self.attr('disabled', true);
                self.html('<img src="/img/tail-spin-small-dark.svg">');

                app.saveEdition()
                    .then(function (result) {
                        self.html('Save');
                        self.attr('disabled', false);
                    });
            });

            $('.scene-action-undo').on('click', function (event) {
                event.preventDefault();
                app.undo();
            });

            $('.scene-action-redo').on('click', function (event) {
                event.preventDefault();
                app.redo();
            });

            /*button = new PanelButton('back', ['Back', 'Back (Ctrl + Z)'], function () {
                app.undo();
            });
            button.disable();
            self.addButton(button);

            button = new PanelButton('forward', 'Forward', function () {
                app.redo();
            });
            button.disable();
            self.addButton(button);

            button = new PanelButton('screenshot', 'Screenshot', function () {
                app.makeLibraryThumbnail();
            });
            button.hide();
            self.addButton(button);

            button = new PanelButton('save', ['Save', 'Save (Ctrl + S)'], function () {
                app.saveEdition();
            });
            button.disable();
            self.addButton(button);

            button = new PanelButton('snapshot', 'Snapshot', function () {
                app.getBundleDialog();
            });
            button.hide();
            self.addButton(button);

            button = new PanelButton('upload', 'Upload', function () {
                app.load_object_dialog(true);
            });
            button.disable();
            self.addButton(button);*/

            button = new PanelButton('add', 'Add', function () {
                $('#modal-add-resource').modal('show');
            });
            self.addButton(button);
            var popoverContent = '<div class="block-content">' +
                '             <button type="button" id="action-add-local-file" class="btn btn-secondary"' +
                '                     onclick="app.initUploadSelect()">' +
                '                 <img src="/img/scene/icons/popup-button-local-file.svg">\n' +
                '             </button>\n' +
                '             <label>Local file</label>\n' +
                '         </div>\n' +
                '         <div class="block-content">\n' +
                '             <button type="button" id="action-add-upload" class="btn btn-secondary" data-dismiss="modal"' +
                '                     onclick="app.showUploads()">\n' +
                '                 <img src="/img/scene/icons/popup-button-upload.svg">\n' +
                '             </button>\n' +
                '             <label>Uploaded</label>\n' +
                '         </div>\n' +
                '         <div class="block-content">\n' +
                '             <button click="popoverHide();" type="button" id="action-video-link-add" class="btn btn-secondary" data-toggle="modal" data-target="#modal-video-link-add">\n' +
                '                 <img src="/img/scene/icons/video-link.png">\n' +
                '             </button>\n' +
                '             <label>Video link</label>\n' +
                '         </div>';

            if (app.config().hasYouTubeDownloadAccess) {
                popoverContent += '<div class="block-content">' +
                    '<button click="popoverHide();" type="button" id="action-add-youtube-link" class="btn btn-secondary" data-toggle="modal" data-target="#modal-add-youtube-link">' +
                    '<img src="/img/scene/icons/popup-button-youtube.svg">' +
                    '</button>' +
                    '<label>Youtube</label>' +
                    '</div>';
            }
            $('#tools-panel').find('.action-add').popover({
                html: true,
                /*template: '<div class="popover add-object" role="tooltip">' +
                    '<div class="arrow"></div>' +
                    '<div class="popover-body"></div>' +
                    '</div>',*/
                content: popoverContent
            });

            /*panel.append(panel_button('&#xE0C8;', 'Transform space', function () {
            	var space = transform_control.space === 'local' ? 'world' : 'local';
            	console.log('edit mode: space - ', space);
            	transform_control.setSpace(space);

            	if (space === 'local') {
            		$(this).addClass('active');
            	} else {
            		$(this).removeClass('active');
            	}
            }));

            buttons.snap_button = panel_button('&#xE3EC;', 'Snap (G)', function () {
            	toggle_snap();
            });
            panel.append(buttons.snap_button);*/
        };

        self.enable = function (buttonName) {
            if (self._buttons[buttonName])
                self._buttons[buttonName].enable();
        };
        self.disable = function (buttonName) {
            if (self._buttons[buttonName])
                self._buttons[buttonName].disable();
        };
        self.show = function (buttonName) {
            if (self._buttons[buttonName])
                self._buttons[buttonName].show();
        };
        self.hide = function (buttonName) {
            if (self._buttons[buttonName])
                self._buttons[buttonName].hide();
        };
        self.addButtonSet = function (buttonSet) {
            var buttons = buttonSet.buttons();
            for (var i = 0; i < buttons.length; i++) {
                self.regButton(buttons[i]);
            }
            self.panel.append(buttonSet.dom());
            buttonSet.setParent(self);
        };

        self.addButton = function (button) {
            self.regButton(button);
            button.appendTo(self.panel);
        };

        self.regButton = function (button) {
            self._buttons[button.name()] = button;
        };

        return this;
    }

    function PanelButtonSet() {
        var self = this;
        var _dom = $("<span>");
        self._buttons = [];

        self.setParent = function (parent) {
            self._parent = parent;
        };

        self.buttons = function () {
            return self._buttons;
        };

        /**
         * @param {PanelButton} button
         */
        self.add = function (button) {
            button.appendTo(_dom);

            button.dom().on('click', function () {
                if (!button.disabled()) {
                    self.setActive(button);
                }
            });

            self._buttons.push(button);

            if (self._parent) {
                self._parent.regButton(button);
            }
        };

        /**
         * @param {PanelButton} button
         */
        self.setActive = function (button) {
            for (var i = 0; i < self._buttons.length; i++) {
                if (self._buttons[i] !== button) {
                    self._buttons[i].dom().removeClass('active');
                }
            }
            button.dom().addClass('active');
        };

        self.dom = function () {
            return _dom;
        };

        return self;
    }

    function PanelButton(name, title, callback) {
        var self = this;
        var _name = name;
        var _dom;
        var disabled = false;
        var _label = title, _title = title;
        if (typeof title === 'object') {
            _label = title[0];
            _title = title[1];
        }

        self.name = function () {
            return _name;
        };
        self.dom = function () {
            return _dom;
        };
        self.appendTo = function (jqueryElement) {
            jqueryElement.append(self.dom());
        };
        self.hide = function () {
            _dom.hide();
        };
        self.show = function () {
            _dom.show();
        };
        self.disabled = function () {
            return disabled;
        };
        self.disable = function () {
            disabled = true;
            _dom.addClass('disabled');
        };
        self.enable = function () {
            disabled = false;
            _dom.removeClass('disabled');
        };

        var iconName = name;
        _dom = $('<div>').addClass('panel-button').attr('title', _title);
        var iconContainer = $('<div>').addClass('button-icon');
        var iconTemplate = '';

        switch (iconName) {
            case 'add':
                iconTemplate = 'plus';
                break;
            case 'upload':
                iconTemplate = 'upload';
                break;
            case 'move':
                iconTemplate = '<div class="action-icon move"></div>';
                break;
            case 'rotation':
                iconTemplate = '<div class="action-icon rotate"></div>';
                break;
            case 'scale':
                iconTemplate = '<div class="action-icon scale"></div>';
                break;
            case 'back':
                iconTemplate = 'reply';
                break;
            case 'forward':
                iconTemplate = 'share';
                break;
            case 'save':
                iconTemplate = 'download';
                break;
        }

        var icon;
        if (iconName === 'add') {
            icon = $(iconTemplate);

            var button = $('<button class="action action-add button-icon" type="button" data-toggle="popover" data-placement="left"></button>');
            button.appendTo(_dom);
        } else {
            icon = $(iconTemplate);
            icon.attr('aria-hidden', 'true');

            icon.appendTo(iconContainer);
            iconContainer.appendTo(_dom);
        }

        if (typeof callback === 'function') {
            _dom.on("click", function (e) {
                if (!disabled)
                    callback.apply(_dom, e);
            });
        }

        return self;
    }

    app.toolsPanel = new ToolsPanel();

    var dragInitialMouseY;
    var $currentPropertyControl;
    var $currentPropertyInput;
    var initialPropertyValue;
    var property;
    var axis;

    $('.move-x, .move-y, .move-z, .rotate-x, .rotate-y, .rotate-z, .scale-all').on('mousedown', function (event) {
        if (event.which !== 1) {
            $(this).removeClass('action-drag');

            return;
        }

        $(this).addClass('action-drag');
        $currentPropertyControl = $(this);
        property = $currentPropertyControl.data('property');
        axis = $currentPropertyControl.data('axis');
        $currentPropertyInput = $('#settings-' + property + '-' + axis);
        var currentPropertyValue = parseFloat($currentPropertyInput.val());
        initialPropertyValue = isNaN(currentPropertyValue) ? 0 : currentPropertyValue;
        dragInitialMouseY = event.pageY;

        $(window).on('mousemove', onObjectPropertyChange)
            .one('mouseup', function () {
                $currentPropertyInput.trigger('change');
                $(window).unbind('mousemove', onObjectPropertyChange);
            });
    });

    function onObjectPropertyChange(event) {
        if (event.which !== 1) {
            $(window).unbind('mousemove', onObjectPropertyChange);
        } else {
            var delta = dragInitialMouseY - event.pageY;

            if (property === 'scale') {
                delta = delta / 100;
            }

            var value = parseFloat((initialPropertyValue + delta).toFixed(3));
            var selectedObject = app.getSelected();

            if (selectedObject) {
                if (typeof selectedObject[property] === 'object'
                    && (property === 'scale' || typeof selectedObject[property][axis] === 'number')) {
                    $currentPropertyInput.val(value);

                    if (property === 'scale') {
                        selectedObject.scale.set(value, value, value);
                    } else {
                        var newTransformArray = selectedObject[property].toArray();
                        var axisIndexes = {'x': 0, 'y': 1, 'z': 2};
                        var axisIndex = axisIndexes[axis];

                        if (property === 'rotation') {
                            value = THREE.Math.degToRad(value);
                        }

                        newTransformArray[axisIndex] = value;
                        selectedObject[property].fromArray(newTransformArray);
                    }
                } else {
                    console.error('Selected object has no property "' + property + '" or axis "' + axis + '"');
                }

                app.onUpdateSelectedObject();
            }
        }
    }

})(window.app);
