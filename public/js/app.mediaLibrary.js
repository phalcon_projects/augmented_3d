(function (app) {
    'use  strict';

    /**
     *
     * @returns {MediaLibrary}
     * @constructor
     */
    function MediaLibrary() {
        var self = this;
        var listBlock = $('#bar-object-library').find('.library-list');// should be just one list
        var modalListBlock = $('#modal-object-library').find('.library-list');// should be just one list
        var list = listBlock.find('.items');
        var modalList = modalListBlock.find('.items');
        var typeFilter = listBlock.find('.filter-type');
        var nameFilter = listBlock.find('.filter-name');
        var loaded = false;
        var loading = false;
        var _objects = {};

        self.init = function () {
            $('.library-list.multiple-select').on('click', '.object', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                } else {
                    $(this).addClass('selected');
                }
            });
            $('#modal-object-library').find('.objects-add-save').on('click', function () {

                $('#modal-object-library').find('.object.selected').each(function () {
                    app.addLibraryObject($(this).data('row').id);
                    $(this).removeClass('selected');
                });

                $('#modal-object-library').modal('hide');
            });


            list.delegate('.delete', 'click', deleteObjectClick);
            list.delegate('.crop', 'click', onObjectClick);
            list.delegate('.insert', 'click', addToSceneClick);
            //list.delegate('.object', 'click', onObjectClick);

            initFilter();
            self.refreshList();
        };

        var nameFilterValue = '';
        var typeFilterValue = {};

        function initFilter() {
            nameFilter
                .on('focus', function () {
                    startFilterCheck();
                })
                .on('blur', function () {
                    stopFilterCheck();
                });

            var typeFilterIcons = typeFilter.find('i');
            typeFilterIcons.addClass('active').each(function () {
                typeFilterValue[$(this).data('type')] = true;
            });

            typeFilterIcons.on('click', function () {
                var type = $(this).data('type');
                $(this).toggleClass('active');

                typeFilterValue[type] = $(this).hasClass('active');
                applyFilter();
            });

            listBlock.find('.filter-name-block .reset').on('click', function () {
                stopFilterCheck();
                nameFilter.val('');
                nameFilterValue = '';
                applyFilter();
            })
        }

        var filterTimer, filterCheckInterval = 1000;

        function startFilterCheck() {
            stopFilterCheck();

            filterTimer = window.setInterval(function () {
                var filterValue = nameFilter.val();
                if (nameFilterValue != filterValue) {
                    nameFilterValue = filterValue;
                    applyFilter();
                }
            }, filterCheckInterval);
        }

        function stopFilterCheck() {
            window.clearInterval(filterTimer);
        }

        function applyFilter() {
            var reset = listBlock.find('.filter-name-block .reset');
            var listItems = list.find('.object');
            // name_filter_value

            var atLeastOneIsChecked = false;
            for (var type in typeFilterValue) {
                if (typeFilterValue.hasOwnProperty(type)) {
                    if (typeFilterValue[type]) {
                        atLeastOneIsChecked = true;
                        break;
                    }
                }
            }

            if (!nameFilterValue) {
                reset.hide();

                if (!atLeastOneIsChecked) {
                    listItems.show();
                    return;
                }
            } else {
                reset.show();
            }

            var reg = new RegExp(nameFilterValue, 'i');
            listItems.hide();
            listItems.each(function () {
                var row = $(this).data('row');

                if (row) {
                    var matchName = !nameFilterValue || row.object.title.match(reg);
                    var matchType = typeFilterValue[row.object.type.split('/')[0]];
                    if (matchName && matchType)
                        $(this).show();
                }
            });
        }

        self.show = function () {
            tab.addClass('active').siblings().removeClass('active');

            if (!loaded && !loading) {
                self.refreshList();
            }
        };

        self.refreshList = function () {
            self.clear();
            loaded = false;
            loading = true;

            return $.ajax('/library/' + app.editionId()).then(function (data) {
                loaded = true;
                drawList(data.objects);
                initDragDrop();
            }, function (e) {
                loaded = false;
                console.error(e);
            }).always(function () {
                loading = false;
            });
        };

        self.libraryObject = function (libraryObjectId) {
            return _objects[libraryObjectId] ? _objects[libraryObjectId].object : false;
        };

        self.addToScene = function (libraryObjectId) {
            app.addLibraryObject(libraryObjectId);
        };

        self.getObjects = function () {
            return _objects;
        };

        function drawList(objects) {
            for (var i = 0; i < objects.length; i++) {
                self.addObject(objects[i]);
            }
        }

        function addToSceneClick(e) {
            e.stopPropagation();

            var row = $(this).closest('.object').data('row');
            self.addToScene(row.object.id);
        }

        function deleteObjectClick(e) {
            e.stopPropagation();

            // if (confirm("Delete object from library and scenes?")) {
            // 	var row = $(this).closest('.object').data('row');
            // 	self.delete_from_library(row.id);
            // }
            var row = $(this).closest('.object').data('row');
            var message = row.object.title ? 'Delete \'' + row.object.title + '\' from library and scenes?' : 'Delete object from library and scenes?';
            modalConfirm(message, {
                onConfirm: function () {
                    self.deleteFromLibrary(row.id);
                }
            });
        }

        self.resetActive = function () {
            list.find('.object').removeClass('active');
        };

        function onObjectClick(e) {
            e.stopPropagation();

            var ui = $(this).closest('.object');
            ui.siblings().removeClass('active');
            ui.addClass('active');

            var row = ui.data('row');
            app.switchToObject(row.object);
        }

        self.addObject = function (object) {
            var id = object.id;

            if (_objects[id] && _objects[id].ui) {
                _objects[id].ui.show();
                reindexObjects();
                return;
            }

            var ui = buildItemHtml(object);
            ui.appendTo(list);
            var uiModal = ui.clone();
            uiModal.appendTo(modalList);

            _objects[id] = {
                id: id,
                ui: ui,
                uiModal: uiModal,
                object: object
            };

            $().add(ui).add(uiModal).data('type', object.type.split('/')[0]);
            $().add(ui).add(uiModal).data('row', _objects[id]);

            if (app.libraryObjectId() && app.libraryObjectId() == id) {
                ui.addClass('active');
            }

            reindexObjects();
        };

        self.clear = function () {
            _objects = {};
            list.empty();
        };

        var drake;

        function initDragDrop() {
            var box = app.htmlViewport()[0];
            var obj;

            if (drake)
                drake.destroy();

            drake = dragula([list[0], box], {
                copy: true,
                invalid: function (el, handle) {
                    //console.error("invalid", el == box || $(el).closest("#viewport").length > 0);
                    return el == box || $(el).closest('#viewport').length > 0;
                }
            });

            drake.on('drag', function (el, source) {
                if (source == box) {
                    return false;
                }

                obj = el; // on start - store original element to get data('row') later
            });

            drake.on('drop', function (el, target, source, sibling) {
                if (target && target == box) {
                    var row = $(obj).data('row');
                    self.addToScene(row.object.id);
                    el.remove(); // we don't need copy of list item in viewport
                }
            });
        }

        function buildItemHtml(object) {
            var title = object.title;
            var filename = object.filename;

            var ui = $('<div>').addClass('object')
                .data('id', object.id)
                .data('title', title)
                .data('filename', filename);

            var head = $('<div class="info">').appendTo(ui);
            $('<span>').addClass('lib-title').attr('title', title).html(title).appendTo(head);
            $('<span>').addClass('filename').attr('title', filename).html(filename).appendTo(head);
            $('<span>').addClass('type').attr('title', object.type).html('(' + object.type + ')').appendTo(head);

            $('<div class="action select"><img class="icon-not-selected" src="/img/scene/icons/list-item-no-select.png"><img class="icon-selected" src="/img/scene/icons/list-item-selected.png"></div>').attr('title', 'Select').appendTo(ui);
            $('<div class="action delete"><img src="/img/scene/icons/plus.png" style="transform: rotate(45deg);"></div>').attr('title', 'Delete from library').appendTo(ui);
            //$('<div>').attr("title", "Add to scene").addClass("insert").appendTo(ui);
            //$('<div class="action crop"><img src="/img/scene/icons/plus.png"></div>').attr("title", "Open object").appendTo(ui);

            // 90x124
            self.objectThumbnailHtml(object).appendTo(ui);

            return ui;
        }

        self.objectThumbnailHtml = function (object) {
            var thumbBlock = $('<div>');
            // 90x124
            thumbBlock.addClass('image');
            //thumbBlock.css('background-image', 'url("' + app.objectsPath() + object.filename + '")');
            var thumbImg = $('<img />');
            var thumb;
            if (object.thumb) {
                thumb = app.thumbsPath() + object.thumb;
            } else if (object.type.match(/^image\//)) {
                thumb = app.objectsPath() + object.filename;
            } else {
                thumbImg.addClass('svg-icon');
                thumbBlock.addClass('svg-icon');
                if (object.type.match(/^audio\//)) {
                    thumb = '/img/audio-icon.svg';
                } else if (object.type.match(/^video\//)) {
                    thumb = '/img/video-icon.svg';
                } else {
                    thumb = '/img/object-icon.svg';
                }
            }
            thumbImg.attr('src', thumb).appendTo(thumbBlock);

            return thumbBlock;
        };

        function reindexObjects() {
            list.find('.object').not('.hidden').each(function (index) {
                $(this).find('.index').html(index + 1);
            });
        }

        self.deleteFromLibrary = function (libraryObjectId) {
            $.ajax({url: '/library/softDelete', data: {id: libraryObjectId}, method: 'post'}).then(function (data) {
                if (!data.errors) {
                    _objects[libraryObjectId].ui.remove();
                    _objects[libraryObjectId].uiModal.remove();

                    app.deleteObjectsByLibraryId(libraryObjectId);

                    reindexObjects();
                }
                // TODO: error handling!!!
            });
        };

        self.unDelete = function (libraryObjectId) {
            $.ajax({url: '/library/unDelete', data: {id: libraryObjectId}, method: 'post'}).then(function (data) {
                if (!data.errors) {
                    _objects[libraryObjectId].ui.remove();
                    _objects[libraryObjectId].uiModal.remove();

                    app.deleteObjectsByLibraryId(libraryObjectId);

                    reindexObjects();
                }
                // TODO: error handling!!!
            });
        };

        self.saveTitle = function (libraryObjectId, title) {
            $.ajax({url: '/library/save', data: {id: libraryObjectId, title: title}, method: 'post'}).then(function (data) {
                if (!data.errors.length) {
                    _objects[libraryObjectId].object.title = title;
                    _objects[libraryObjectId].ui.find('.lib-title').html(title);
                    _objects[libraryObjectId].uiModal.find('.lib-title').html(title);
                }
                // TODO: error handling!!!
            });
        };

        self.updateThumbnail = function (id, filename) {
            _objects[id].object.thumb = filename;
            var ui = buildItemHtml(_objects[id].object);
            var uiModal = ui.clone();

            _objects[id].ui.replaceWith(ui);
            _objects[id].uiModal.replaceWith( uiModal );
            _objects[id].ui = ui;
            _objects[id].uiModal = uiModal;
            _objects[id].ui.data('row', _objects[id]);
            _objects[id].uiModal.data('row', _objects[id]);
        };

        self.cropAndSave = function (id, filename, thumbData) {
            cropThumbnailDialog(thumbData).then(function (thumbData) {
                storeThumbData(id, filename, thumbData).then(function (data) {
                    if (!data.errors.length) {
                        self.updateThumbnail(id, data.filename);
                    }
                    // TODO: error handling!!!
                });
            });
        };

        function storeThumbData(id, filename, thumbData) {
            // TODO: make with blob upload?
            // uploadDataUrlImage(url_data, filename, cover.data('id')).then(function () {
            // 	var cover_img = cover.find('.image img');
            // 	cover.data('filename', filename);
            // 	cover_img.attr('src', covers_path + filename);
            // });

            return $.ajax({
                url: '/library/thumb',
                data: {id: id, filename: filename, data: thumbData},
                method: 'POST'
            });
        }


        // Crop
        var cropDialog, cropInputs = {};

        function initCropImageDialog() {
            cropDialog = $('#thumbnail-crop-dialog');

            cropDialog.find('button.cancel, .close').on('click', function () {
                var img = cropDialog.find('img.target');
                var promise = img.data('promise');

                img.cropper('destroy');
                cropDialog.hide();
                img.remove();
                app.displayBlocker(false);

                promise.reject();
            });

            cropDialog.find('button.submit').on('click', function () {
                var compressionQuality = 0.8;
                var img = cropDialog.find('img.target');
                var promise = img.data('promise');
                var dataUrl = img.cropper('getCroppedCanvas').toDataURL('image/jpeg', compressionQuality);

                promise.resolve(dataUrl);
                img.cropper('destroy');
                cropDialog.hide();
                img.remove();
                app.displayBlocker(false);

            });

            // precise crop control
            var cropParams = cropDialog.find('.precise form');
            cropInputs.x = cropParams.find('.crop-x input');
            cropInputs.y = cropParams.find('.crop-y input');
            cropInputs.width = cropParams.find('.crop-width input');
            cropInputs.height = cropParams.find('.crop-height input');

            cropParams.on('submit', function (e) {
                e.preventDefault();
                setCropperByInputs();
            });
            cropParams.find('input').on('change', function () {
                setCropperByInputs();
            });


            var reinitTimeout;
            var cropData = null;
            $(window).on('resize', function () {
                if (cropDialog.is(':visible')) {
                    if (reinitTimeout)
                        window.clearTimeout(reinitTimeout);

                    var img = cropDialog.find('img.target');
                    if (!cropData)
                        cropData = img.cropper('getData');
                    img.cropper('destroy');
                    reinitTimeout = window.setTimeout(function () {
                        copperInit(img, cropData);
                        cropData = null;
                    }, 500);
                }
            });
        }

        /**
         * @returns {{x: number, y: number, width: number, height: number, rotate: number, scaleX: number, scaleY: number }}
         */
        function getCropperInputsValues() {
            var data = {x: 0, y: 0, width: 0, height: 0, rotate: 0, scaleX: 1, scaleY: 1};
            for (var key in cropInputs) {
                if (!cropInputs.hasOwnProperty(key))
                    continue;

                var val = parseFloat(cropInputs[key].val());
                if (isNaN(val)) {
                    val = data[key];
                    cropInputs[key].val(val);
                }
                data[key] = val;
            }

            return data;
        }

        function setCropperByInputs() {
            var img = cropDialog.find('img.target');
            var data = getCropperInputsValues(); // inputs data
            img.cropper('setData', data);
        }

        function setInputsByCropper(e) {
            cropInputs.x.val(Math.round(e.x));
            cropInputs.y.val(Math.round(e.y));
            cropInputs.width.val(Math.round(e.width));
            cropInputs.height.val(Math.round(e.height));
        }

        function copperInit(img, data) {
            var params = {
                viewMode: 1,
                responsive: true,
                // dragMode: 'move',
                movable: false,
                zoomable: false,
                background: false,
                highlight: false,
                rotatable: false,
                scalable: false,
                minContainerWidth: 94,
                minContainerHeight: 124,
                aspectRatio: 0.7580645161290323, // 94/124
                crop: function (e) {
                    setInputsByCropper(e);
                }
            };
            if (data) {
                params.data = data;
            }
            img.cropper(params);
        }

        function cropThumbnailDialog(imageData) {

            app.displayBlocker(true);

            if (!cropDialog) {
                initCropImageDialog();
            }
            cropDialog.find('img.target').remove();

            var promise = new $.Deferred();
            var img = $('<img>')
                .addClass('target')
                .data('promise', promise)
                .attr('src', imageData)
                .appendTo(cropDialog.find('.center'))
                .on('load', function () {
                    copperInit(img);
                });

            cropDialog.show();

            return promise.promise();
        }

        this.renderThumbnail = function (libraryObjectId, object) {
            var deferred = new $.Deferred();
            var processing;

            if (object.type.match(/^object\//)) {
                processing = _renderObjectThumb(object);
            } else if (object.type.match(/^video\//)) {
                processing = _renderVideoThumb(object);
            }

            if (processing) {
                processing.then(function (imageData) {
                    var filename = 'thumb.' + libraryObjectId + '_' + (new Date()).getTime();

                    $.ajax({
                        url: 'library/thumb/',
                        method: 'post',
                        data: {id: libraryObjectId, filename: filename, data: imageData}
                    }).then(function (data) {
                        self.updateThumbnail(libraryObjectId, data.filename);
                        deferred.resolve(data.filename);
                    });
                });
            } else {
                console.warn('Unable to render thumbnail. Unsupported object type:', object.type);
                deferred.reject();
            }

            return deferred.promise();
        };

        function _renderVideoThumb(object) {
            var deferred = new $.Deferred();
            var thumbnailWidth = 400;
            var thumbnailHeight = 400;
            var fullFrame = false; // add transparent pixels to match exact thumbnail sizes

            var video = document.createElement('video');
            var frameNumber = 1;

            video.autoplay = true;
            video.preload = 'none';
            video.volume = 0;

            function canplay(e) {
                video.removeEventListener('canplay', canplay, false);
                video.currentTime = frameNumber;
            }

            function seeked(e) {
                video.pause();
                render();
            }

            video.addEventListener('error', failed);
            video.addEventListener('canplay', canplay, false);
            video.addEventListener('seeked', seeked, false);

            video.src = app.objectsPath() + object.filename;

            function render() {
                var w = video.videoWidth;
                var h = video.videoHeight;
                var scaleWidth = thumbnailWidth / w;
                var scaleHeight = thumbnailHeight / h;

                var scale = Math.min(1, scaleWidth, scaleHeight);

                w = Math.round(w * scale);
                h = Math.round(h * scale);

                var canvas = document.createElement('canvas');
                var context = canvas.getContext('2d');

                if (fullFrame) {
                    canvas.width = thumbnailWidth;
                    canvas.height = thumbnailHeight;
                } else {
                    canvas.width = w;
                    canvas.height = h;
                }

                context.drawImage(video, canvas.width / 2 - w / 2, canvas.height / 2 - h / 2, w, h);

                var imageColorRGB = getAverageRGB(video);
                var videoDuration = Math.floor(video.duration);

                if (!isNaN(videoDuration) && video.currentTime === videoDuration
                    || imageColorRGB.r >= 45 || imageColorRGB.g >= 45 || imageColorRGB.b >= 45) {
                    video.removeEventListener('seeked', seeked, false);
                    deferred.resolve(canvas.toDataURL('image/png'));
                } else {
                    ++frameNumber;
                    video.currentTime = frameNumber;
                }
            }

            function getAverageRGB(image) {
                var analyzeCanvas = document.createElement('canvas');
                var analyzeContext = analyzeCanvas.getContext && analyzeCanvas.getContext('2d');
                var blockSize = 5; // visit every 5 pixels
                var defaultRGB = {r: 0, g: 0, b: 0}; // for non-supporting ENVS
                var data;
                var width;
                var height;
                var i = -4;
                var rgb = {r: 0, g: 0, b: 0};
                var count = 0;

                if (!analyzeContext) {
                    console.error('VIDEO FRAME no context');
                    return defaultRGB;
                }

                height = analyzeCanvas.height = image.naturalHeight
                    || image.offsetHeight
                    || image.videoHeight
                    || image.height;
                width = analyzeCanvas.width = image.naturalWidth
                    || image.offsetWidth
                    || image.videoWidth
                    || image.width;

                analyzeContext.drawImage(image, 0, 0);

                try {
                    data = analyzeContext.getImageData(0, 0, width, height);
                } catch (e) {
                    console.error('Canvas image check error: cannot get image data');
                    return defaultRGB;
                }

                while ((i += blockSize * 4) < data.data.length) {
                    ++count;
                    rgb.r += data.data[i];
                    rgb.g += data.data[i + 1];
                    rgb.b += data.data[i + 2];
                }

                // ~~ used to floor values
                rgb.r = ~~(rgb.r / count);
                rgb.g = ~~(rgb.g / count);
                rgb.b = ~~(rgb.b / count);

                return rgb;
            }

            function failed(e) {
                // video playback failed - show a message saying why
                console.error('video playback failed', e.target.error);
                var errorText = 'An unknown error occurred.';
                switch (e.target.error.code) {
                    case e.target.error.MEDIA_ERR_ABORTED:
                        errorText = 'You aborted the video playback.';
                        break;
                    case e.target.error.MEDIA_ERR_NETWORK:
                        errorText = 'A network error caused the video download to fail part-way.';
                        break;
                    case e.target.error.MEDIA_ERR_DECODE:
                        errorText = 'The video playback was aborted due to a corruption problem or because the video used features your browser did not support.';
                        break;
                    case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
                        errorText = 'The video could not be loaded, either because the server or network failed or because the format is not supported.';
                        break;
                    default:
                        break;
                }

                deferred.fail(errorText);
                console.error(errorText);
            }

            return deferred.promise();
        }

        // TODO: move all common objects here (like scene, lights, camera)
        var thumbRenderer, fallbackTexture;
        function _renderObjectThumb(object) {
            var deferred = new $.Deferred();

            var debug = 0;

            app.loadFile(object).then(function (object3d) {
                    // setup scene
                    // 204x269
                    var thumbnailWidth = 300;
                    var thumbnailHeight = 300;
                    var aspect = thumbnailWidth / thumbnailHeight;

                    if (!thumbRenderer) {
                        thumbRenderer = new THREE.WebGLRenderer({
                            antialias: true,
                            preserveDrawingBuffer: true,
                            alpha: true
                        });
                    }
                    thumbRenderer.setClearColor(0xcccccc, 0);
                    thumbRenderer.setPixelRatio(1);
                    thumbRenderer.setSize(thumbnailWidth, thumbnailHeight);

                    var debugImage;
                    if (debug) {
                        $('body').append(thumbRenderer.domElement);
                        $(thumbRenderer.domElement).css({
                            position: 'absolute',
                            border: '1px solid #ff0000',
                            'z-index': 9999,
                            left: 50,
                            top: 50
                        });
                        debugImage = $('<img />').css({
                            position: 'absolute',
                            border: '1px solid #ff0000',
                            'z-index': 9999,
                            left: 400,
                            top: 50
                        }).appendTo('body');
                    }

                    // setup scene, camera and lights
                    var scene = new THREE.Scene();
                    if (debug > 1)
                        scene = app.scene;

                    var camera = new THREE.PerspectiveCamera(60, aspect, 0.002, 1000);
                    camera.position.set(150, 250, 250);
                    camera.lookAt(new THREE.Vector3());

                    var helper;
                    if (debug > 1) {
                        helper = new THREE.CameraHelper(camera);
                        scene.add(helper);
                    } else {
                        var ambient = new THREE.AmbientLight(0x444444);
                        scene.add(ambient);

                        var spotLight = new THREE.SpotLight(0xffffff);
                        spotLight.position.set(-250, 250, 250);
                        scene.add(spotLight);

                        var spotLight2 = new THREE.SpotLight(0x777777);
                        spotLight2.position.set(250, -250, 250);
                        scene.add(spotLight2);
                    }

                    // add object
                    scene.add(object3d);

                    // position camera
                    var bbox = new THREE.Box3();
                    bbox.setFromObject(object3d);
                    var bounds = new THREE.Sphere();
                    bounds.setFromPoints([bbox.min, bbox.max]);

                    if (debug > 1) {
                        var box = new THREE.BoxHelper(object3d, 0xffff00);
                        scene.add(box);

                        var sphere = new THREE.SphereGeometry(bounds.radius);
                        var sphereObject = new THREE.Mesh(sphere, new THREE.MeshBasicMaterial({wireframe: true}));
                        sphereObject.position.copy(bounds.center);
                        scene.add(sphereObject);
                    }

                    var objectSize = bounds.radius;
                    var objectCenter = bounds.center;
                    var dist = camera.position.distanceTo(bounds.center);
                    var pos = camera.position.clone().normalize();

                    var zoom = 1.1; // additional zoom
                    var fov;
                    var adjustFov = false; // adjust camera field of view(fov,zoom) or position
                    if (adjustFov) {
                        fov = 2 * Math.atan( objectSize / ( 2 * dist ) ) * ( 180 / Math.PI ); // in degrees
                        camera.fov = fov / zoom;
                    } else {
                        fov = THREE.Math.degToRad(camera.fov); // Convert camera fov degrees to radians
                        var distance = Math.abs(objectSize / aspect / Math.sin(fov / 2)); // Calculate the camera distance
                        pos.multiplyScalar(distance / zoom);

                        camera.position.copy(pos);
                    }

                    camera.lookAt(objectCenter);
                    camera.updateProjectionMatrix();
                    if (debug > 1 && helper)
                        helper.update();

                    // check all textures are loaded
                    var i, textures = {}, materials = {}, objects = {}, texturesFound = 0;

                    function collectMaterial(m, obj) {
                        if (m.map && m.map.isTexture) {
                            if (!textures[m.map.uuid]) {
                                textures[m.map.uuid] = m.map;

                                if (!materials[m.map.uuid]) {
                                    materials[m.map.uuid] = [];
                                }
                                materials[m.map.uuid].push(m);

                                texturesFound++;
                            }
                        }
                    }

                    object3d.traverse(function (child) {
                        if (child.material) {
                            if (child.material instanceof THREE.Material) {
                                collectMaterial(child.material, child);
                            } else if (typeof child.material.length !== 'undefined') {
                                for (i = 0; i < child.material.length; i++) {
                                    collectMaterial(child.material[i], child);
                                }
                            }
                        }
                    });


                    function render() {
                        scene.updateMatrixWorld();

                        thumbRenderer.render(scene, camera);
                        thumbRenderer.render(scene, camera); // i don't understand why it's not render by first call, two calls are working good

                        if (debugImage)
                            debugImage.attr('src', thumbRenderer.domElement.toDataURL('image/png'));
                        deferred.resolve(thumbRenderer.domElement.toDataURL('image/png'));
                    }

                    if (debug)
                        window.renderAgain = render;

                    var checkInterval;
                    var checkIntervalTimeout;
                    if (texturesFound > 0) {
                        checkInterval = setInterval(checkAllAreLoaded, 100);
                        checkIntervalTimeout = setTimeout(function(){
                            console.warn('Render thumbnail timeout.');

                            clearInterval(checkInterval);
                            if (!fallbackTexture) {
                                fallbackTexture = generateFallbackTexture();
                            }

                            for (var uuid in textures) {
                                if (!textures.hasOwnProperty(uuid))
                                    continue;

                                var image = textures[uuid].image;

                                // if image still not loaded
                                if (!image || image.width === 0) {
                                    // update all materials using this texture
                                    for (var i=0;i<materials[uuid].length;i++) {
                                        materials[uuid][i].map = fallbackTexture;
                                        materials[uuid][i].emissive = new THREE.Color(0,0,0);
                                        materials[uuid][i].needsUpdate = true;
                                    }
                                }
                            }

                            // render with replaced textures
                            render();
                            // deferred.reject();
                        }, 5000); // wait 30seconds to load all images, then fail
                    } else {
                        render();
                    }

                    function checkAllAreLoaded() {
                        var unloaded = 0;
                        for (var uuid in textures) {
                            if (!textures.hasOwnProperty(uuid))
                                continue;

                            var image = textures[uuid].image;

                            if (!image || image.width === 0)
                                unloaded++;
                        }

                        if (unloaded === 0) {
                            clearTimeout(checkIntervalTimeout);
                            clearInterval(checkInterval);
                            render();
                        }
                    }
                },
                // onError
                function (e) {
                    deferred.fail(e);
                },
                // onProgress
                function (e) {
                    deferred.notify(e);
                });

            return deferred.promise();
        }

        function generateFallbackTexture() {
            var canvas = document.createElement('canvas');
            canvas.width = 20;
            canvas.height = 20;
            var context = canvas.getContext('2d');
            context.fillStyle = '#ff00ff';
            context.fillRect(0, 0, canvas.width, canvas.height);

            context.fillStyle = '#ffff00';
            context.fillRect(0, 0, canvas.width/2, canvas.height/2);
            context.fillRect(canvas.width/2, canvas.height/2 , canvas.width, canvas.height);
            context.save();

            var map = new THREE.Texture(canvas);
            map.wrapS = map.wrapT = THREE.RepeatWrapping;
            map.repeat.set(4,4);
            map.needsUpdate = true;
            return map;
        }


        return this;
    }


    app.mediaLibrary = new MediaLibrary();
})(window.app);
