/* globals console: false, modalConfirm: false, showError: false, showMessage: true */
// jshint trailingcomma: false
$(function () {
    'use strict';
    var content = $('.content-wrapper');
    var projectId = $('.editions').data('project_id');

    initEdit();
    initDelete();
    initPagination();

    function loadPage(num) {
        $.ajax({
            url: 'projects/' + projectId,
            data: {page: num},
            target: 'div.editions',
            method: 'get'
        }).then(function (html) {
            $('.editions').replaceWith(html);
        });
    }

    function doAction(action, data) {
        var process = new $.Deferred();

        $.ajax({
            url: 'editions/' + action,
            data: data,
            method: 'post'
        }).then(function (response) {
            if (response.success !== undefined && response.success) {
                // reload active page
                if (response.redirect) {
                    if (response.redirect === 'current') {
                        window.location.reload();
                    } else if (response.redirect === 'active-page') {
                        var activePage = parseInt($('.list-bottom .pagin a.active').text());

                        if (isNaN(activePage) || activePage <= 0)
                            activePage = 1;

                        loadPage(activePage);
                    }
                }

                if (response.redirect !== 'current') {
                    var message = action === 'delete' ? 'Edition is deleted.' : 'Edition is saved.';
                    showMessage(message, 'ok', 5000);
                }

                process.resolve(response);
            } else {
                console.error(response);
                if (response.join)
                    showError(response.join('<br />'));
                else
                    showError(response);

                process.reject(response);
            }
        });

        return process.promise();
    }

    function initEdit() {
        var $modalEditionTitleEdit = $('#modal-edition-title-edit');
        var $modalTitle = $('.modal-edition-title');
        var $saveButton = $('#modal-action-save');
        var $loader = $modalEditionTitleEdit.find('.loader-wrapper');
        var $titleInput = $modalEditionTitleEdit.find('input[name="edition-title"]');
        var $idInput = $modalEditionTitleEdit.find('input[name="edition-id"]');

        $modalEditionTitleEdit.find('form').off('submit').on('submit', function () {
            $saveButton.trigger('click');
        });

        content.on('click', '.list-bottom .add, .editions .edit', function (e) {
            e.preventDefault();
            var title = '';

            var id = $(this).closest('.list-item').data('id');
            if (id) {
                $idInput.val(id);
                title = $(this).closest('.list-item').data('title');
            } else {
                $idInput.val('');
            }

            $modalTitle.html((id ? 'Edit edition' : 'Add edition'));
            $titleInput.val(title);

            $loader.addClass('hidden');
            $modalEditionTitleEdit.modal('show');
            $titleInput.focus();
        });

        $saveButton.off('click').on('click', function () {
            var data = {projectId: projectId};
            var title = $titleInput.val();
            var id = $idInput.val();

            if (!title || title.trim() === '')
                return;

            if (id) {
                data.id = id;
            }
            data.title = title;

            $loader.removeClass('hidden');
            doAction('save', data).then(function () {
                $modalEditionTitleEdit.modal('hide');
            });
        });
    }

    function initDelete() {
        content.on('click', '.editions .delete', function (e) {
            e.preventDefault();
            var self = $(this);

            modalConfirm('Delete edition and all it\'s pages?', {
                onConfirm: function () {
                    doAction('delete', {id: self.data('id')});
                }
            });
        });
    }

    function initPagination() {
        content.on('click', '.list-bottom .pagin a', function (e) {
            e.preventDefault();

            loadPage($(this).text());
        });
    }

    function initPublish() {
        content.on('click', '.editions .publish', function (e) {
            e.preventDefault();
            var $bundleUrlBlock = $(this).closest('.list-item').find('.bundle-url');
            var $urlText = $bundleUrlBlock.find('.bundle-url-text');

            $urlText.text('Generating...');
            $bundleUrlBlock.addClass('visible');

            $.ajax({
                url: '/index/bundle/',
                data: {editionId: $(this).data('id')},
                method: 'get'
            }).then(function (response) {
                if (response.errors === undefined || response.errors.length === 0) {
                    // response.bundle
                    $urlText.text(response.bundle);
                } else {
                    $urlText.text('Generation failed.');
                    console.error(response);
                    if (response.errors.join)
                        showError(response.errors.join('<br />'));
                    else
                        showError(response);
                }
            });


            /*
            doAction('save', {
                id: $(this).data('id'),
                state: ($(this).data('state') === 1 ? '0' : '1')
            });
            */
        });
    }

    function initImportBundle() {
        var projectId = $('.editions').data('project_id');
        if (!projectId) {
            return;
        }

        var bundleUploaderConfig = {
            url: '/index/importBundle?projectId=' + projectId,
            dataType: 'json',
            dropZone: null,
            done: function (e, data) {
                // app.hide_loading_progress();
            },
            progressall: function (e, data) {

            },
            fail: function (e, data) {
                // app.hide_loading_progress();
                showError('unexpected response from server:' + data.textStatus);
            }
        };
        $('#bundleupload').fileupload(bundleUploaderConfig);
    }

});