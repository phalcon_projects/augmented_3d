(function(app){
    'use strict';

    /**
     *
     * @returns {ObjectsList}
     * @constructor
     */
    function ObjectsList() {
        var self = this;
        var list = $('#bar-layers').find('.objects-list');
        var objects = {};

        self.init = function(){
            list.delegate('.delete', 'click', deleteObjectClick);
            list.delegate('.object', 'click', onObjectClick);
            list.delegate('.object', 'mouseenter mouseleave', onObjectRowHover);

            // covers_bar.find('.view3').on('click', function(){
            // 	covers_bar.removeClass('library-list').addClass('short-list').addClass('objects-list');
            // 	$(this).addClass('active').siblings().removeClass('active');
            // });

            self.checkLayersObjects();
        };

        self.checkLayersObjects = function() {
            var $objectList = list.find('.object').not('.hidden');

            if ($objectList.length > 0) {
                $('.library-list-no-object-selected').hide();
            } else {
                $('.library-list-no-object-selected').show();
            }
        };

        self.getObjects = function() {
            var sceneObjects = [];
            for (var id in objects) {
                if (!objects.hasOwnProperty(id))
                    continue;

                if (!objects[id].ui.is('.hidden'))
                    sceneObjects.push(objects[id].object);
            }

            return sceneObjects;
        };

        self.replaceId = function(id, newId) {
            var r = objects[id];
            delete objects[id];
            objects[newId] = r;
            r.id = newId;
        };

        self.addObject = function(sceneObject){
            var id = app.objectId(sceneObject);

            objects[id] = {
                id: id,
                object: sceneObject
            };

            self.updateObject(sceneObject);
        };

        /**
         * draw and redraw object list row in list
         * @param sceneObject
         */
        self.updateObject = function(sceneObject) {
            var id = app.objectId(sceneObject);
            var newUi = buildItemHtml(sceneObject);

            if (objects[id]) {
                if (objects[id].ui) {
                    // replace old html ui
                    objects[id].ui.replaceWith(newUi);
                    objects[id].ui.show();
                }
            } else {
                objects[id] = {
                    id: id,
                    object: sceneObject
                };
            }

            objects[id].ui = newUi;
            objects[id].object = sceneObject;

            newUi.data('row', objects[id]);
            newUi.appendTo(list);

            reindexObjects();
        };

        self.removeObject = function(id){
            if (typeof objects[id] !== 'undefined') {
                objects[id].ui.remove();
                delete objects[id];
            }
            reindexObjects();
        };

        self.hideObject = function(id){
            if (typeof objects[id] !== 'undefined') {
                objects[id].ui.addClass('hidden');
            }
            reindexObjects();
        };

        self.showObject = function(id){
            if (typeof objects[id] !== 'undefined') {
                objects[id].ui.removeClass('hidden');
            }
            reindexObjects();
        };

        self.onObjectSelect = function(id) {
            if (typeof objects[id] !== 'undefined') {
                objects[id].ui.siblings().removeClass('active');
                objects[id].ui.addClass('active');
            } else {
                list.find('.object').removeClass('active');
            }
        };

        self.onObjectHover = function(id) {
            if (typeof objects[id] !== 'undefined') {
                objects[id].ui.siblings().removeClass('hovered');
                objects[id].ui.addClass('hovered');
            } else {
                list.find('.object').removeClass('hovered');
            }
        };

        self.clear = function () {
            objects = {};
            list.empty();
        };

        self.updateTitle = function(id, title) {
            if (typeof objects[id] !== 'undefined') {
                objects[id].ui.find('.title').attr('title', title).html(title);
                objects[id].ui.find('.obj-title').html(title);
            }
        };

        self.updateThumbnail = function(id) {
            if (typeof objects[id] !== 'undefined') {
                var thumbImg = objects[id].ui.find('.image img');
                var thumb = thumbIconPath(objects[id].object.userData.object);

                if (thumb.match(/.svg$/)) {
                    thumbImg.addClass('svg-icon');
                }

                thumbImg.attr('src', thumb);
            }
        };

        function reindexObjects() {
            var $objectList = list.find('.object').not('.hidden');

            $objectList.each(function(index) {
                $(this).find('.index').html(index+1);
            });

            self.checkLayersObjects();
        }

        function deleteObjectClick(e) {
            e.stopPropagation();

            var row = $(this).closest('.object').data('row');
            var message = row.object.userData.object.title? 'Delete \'' + row.object.userData.object.title + '\'?' : 'Delete object?';
            modalConfirm(message, {
                onConfirm: function(){
                    app.deleteObject(row.object);
                }
            });
        }

        function onObjectClick(e) {
            e.stopPropagation();

            var row = $(this).data('row');
            app.onSelect(row.object);
        }

        function onObjectRowHover(e) {
            e.stopPropagation();

            if (e.type === 'mouseleave') {
                app.onHover(null);
            } else {
                var row = $(this).data('row');
                app.onHover(row.object);
            }
        }

        function buildItemHtml(sceneObject) {
            var serverObject = sceneObject.userData.object;

            var title = serverObject.title;
            var filename = serverObject.filename;

            var ui = $('<div>').addClass('object')
                .data('id', serverObject.id)
                .data('title', title)
                .data('filename', filename);

            var head = $('<div class="head">').appendTo(ui);
            $('<span>').addClass('index').appendTo(head);
            var titleSpan = $('<span>').addClass('title').appendTo(head);
            $('<span>').addClass('obj-title').attr('title', title).html(title).appendTo(titleSpan);
            $('<span>').addClass('type').attr('title', serverObject.type).html(' ('  + serverObject.type + ')').appendTo(titleSpan);
            $('<div>').html('<img src="/img/scene/icons/plus.png" style="transform: rotate(45deg);">').addClass('delete').appendTo(ui);
            //$('<div>').addClass('crop').appendTo(ui);

            var imageBlock = $('<div>').addClass('image').appendTo(ui);
            var thumbImg = $('<img />');
            var thumb = thumbIconPath(serverObject);

            if (thumb.match(/.svg$/)) {
                thumbImg.addClass('svg-icon');
            }

            thumbImg.attr('src', thumb).appendTo(imageBlock);

            return ui;
        }

        function thumbIconPath(serverObject) {
            var thumb = serverObject.thumb;
            if (thumb) {
                thumb = app.thumbsPath() + thumb;
            } else {
                if (serverObject.type.match(/^image\//)) {
                    thumb = app.objectsPath() + serverObject.filename;
                } else {
                    if (serverObject.type.match(/^video\//)) {
                        thumb = '/img/video-icon.svg';
                    } else if (serverObject.type.match(/^audio\//)) {
                        thumb = '/img/audio-icon.svg';
                    } else {
                        thumb = '/img/object-icon.svg';
                    }
                }
            }
            return thumb;
        }

        return this;
    }

    app.objectsList = new ObjectsList();
})(window.app);