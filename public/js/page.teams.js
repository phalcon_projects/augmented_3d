'use strict';
$(function () {

    var $teamModal = $('#modal-team-edit');
    var $teamModalTitleInput = $teamModal.find('input[name="title"]');
    var $teamModalSave = $teamModal.find('.modal-actions .action.save');
    var $teamAddModal = $('#modal-team-add');
    var $teamMembersModal = $('#modal-team-members');
    var $teamInviteModal = $('#modal-team-invite');
    var $teamDeleteModal = $('#modal-team-delete-confirm');
    var $inviteEmailErrorNotification = $teamInviteModal.find('.email .error.danger');
    var $teamMemberRemoveModal = $('#modal-team-member-remove-confirm');


    $('.members-detail').on('click', function (e) {
        e.preventDefault();

        $('#modal-team-members-title').html($(this).closest('.list-item.team').data('team-title'));
        $teamMembersModal.find('.list-content').html($('.team-members[data-team-id="'
            + $(this).closest('.list-item.team').data('team-id') + '"]')[0]);
        $teamMembersModal.modal('show');
    });

    $teamInviteModal.find('input[name="email"]').on('keydown', function () {
        if ($(this).val() !== '') {
            $inviteEmailErrorNotification.hide();
        } else {
            $inviteEmailErrorNotification.show();
        }
    });

    $inviteEmailErrorNotification.hide();

    $('#team-add').on('click', function (e) {
        e.preventDefault();

        initModal('create');
    });

    $('.teams .action.edit').on('click', function (e) {
        e.preventDefault();

        initModal(
            'edit',
            $(this).closest('.list-item.team').data('team-id'),
            $(this).closest('.list-item.team').data('team-title')
        );
    });

    $teamModalSave.on('click', function (e) {
        e.preventDefault();

        if ($teamModalTitleInput.length > 0) {
            var teamTitle = $teamModalTitleInput.val();

            if (teamTitle.length > 0) {
                var teamId = parseInt($(this).data('team-id')),
                    url = '',
                    data = $('#team-form').serialize();

                if (!isNaN(teamId)) {
                    url = 'teams/edit';
                    data += '&team_id=' + teamId;
                } else {
                    url = 'teams/create';
                }

                $.ajax({
                    url: url,
                    data: data,
                    method: 'post'
                }).then(function (response) {
                    if (typeof response === 'object') {
                        if (response.status === 'ok') {
                            showMessage(response.message, 'ok', 5000);
                            $teamModal.modal('hide');

                            if (response.redirect) {
                                setTimeout(function () {
                                    window.location = response.redirect;
                                }, 2000);
                            }
                        } else {
                            console.error(response);
                            showError(response.message);
                        }
                    } else {
                        console.error(response);
                        showError(response);
                    }
                });
            }
        }
    });

    $('.teams .action.invite').on('click', function (e) {
        e.preventDefault();
        var teamId = $(this).closest('.list-item.team').data('team-id');
        $('#modal-invite-team-title').html($(this).closest('.list-item.team').data('team-title'));

        $teamInviteModal.find('.modal-actions .action.invite').data('team-id', teamId);
        $teamInviteModal.find('input[name="email"]').val('');
        $teamInviteModal.modal('show');
    });

    $teamInviteModal.find('.modal-actions .action.invite').on('click', function (e) {
        e.preventDefault();

        var self = $(this),
            email = $teamInviteModal.find('input[name="email"]').val(),
            pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

        if ($.trim(email).match(pattern)) {
            $.ajax({
                url: 'teams/invite-user',
                data: {
                    'team_id': self.data('team-id'),
                    'email': email
                },
                method: 'post'
            }).then(function (response) {
                if (typeof response === 'object') {
                    if (response.status === 'ok') {
                        showMessage('Invitation was sent.');
                    } else if (response.status === 'error') {
                        var message = response.message ? response.message : 'Unable to send invitation.';
                        showError(message);
                    }
                }
            });
        } else {
            $inviteEmailErrorNotification.show();
        }

    });

    $('.teams .action.add').on('click', function (e) {
        e.preventDefault();
        var teamId = $(this).closest('.list-item.team').data('team-id');

        $teamAddModal.find('select').niceSelect();
        $('#modal-add-team-title').html($(this).closest('.list-item.team').data('team-title'));

        $.ajax({
            url: 'teams/users-for-add',
            data: {'team_id': teamId},
            method: 'post'
        }).then(function (response) {
            if (typeof response === 'object') {
                if (response.status === 'ok') {
                    if (response.users && response.users.length > 0) {
                        var userSelect = $('#user-for-add');

                        userSelect.find('option').remove();
                        userSelect.append('<option value="" disabled selected>Select user</option>');
                        $.each(response.users, function (id, user) {
                            userSelect.append($('<option></option>', {'value': user.id})
                                .text(user.first_name + ' ' + user.surname
                                    + (user.email !== null ? ' - ' + user.email : '')));
                        });
                        userSelect.niceSelect('update');

                        $teamAddModal.find('.modal-actions .action.add').data('team-id', teamId);
                        $teamAddModal.modal('show');
                    }
                } else {
                    console.error(response);
                    showError(response.message);
                }
            } else {
                console.error(response);
                showError(response);
            }
        });
    });

    $('.teams .action.remove-member').on('click', function (e) {
        e.preventDefault();

        $('#modal-team-title').html($(this).closest('.list-item.team').data('team-title'));
        $teamMemberRemoveModal.find('.modal-actions .action.remove-confirm')
            .data('user-id', $(this).closest('.list-item.team').data('user-id'));
        $teamMemberRemoveModal.find('.modal-actions .action.remove-confirm')
            .data('team-id', $(this).closest('.list-item.team').data('team-id'));
        $teamMemberRemoveModal.modal('show');
    });

    $('.teams .action.delete').on('click', function (e) {
        e.preventDefault();

        $('#modal-delete-team-title').html($(this).closest('.list-item.team').data('team-title'));
        $teamDeleteModal.find('.modal-actions .action.delete-confirm')
            .data('team-id', $(this).closest('.list-item.team').data('team-id'));
        $teamDeleteModal.modal('show');
    });

    $teamMemberRemoveModal.find('.modal-actions .action.remove-confirm').on('click', function (e) {
        e.preventDefault();

        var self = $(this);

        $.ajax({
            url: 'teams/remove-member',
            data: {
                'team_id': self.data('team-id'),
                'user_id': self.data('user-id')
            },
            method: 'post'
        }).then(function (response) {
            if (typeof response === 'object') {
                if (response.status === 'ok') {
                    self.closest('.team-member').remove();
                    showMessage(response.message, 'ok', 5000);
                    $teamModal.modal('hide');

                    if (response.redirect) {
                        setTimeout(function () {
                            window.location = response.redirect;
                        }, 2000);
                    }
                } else {
                    console.error(response);
                    showError(response.message);
                }
            } else {
                console.error(response);
                showError(response);
            }
        });
    });

    $teamDeleteModal.find('.modal-actions .action.delete-confirm').on('click', function (e) {
        e.preventDefault();

        var self = $(this);

        $.ajax({
            url: 'teams/delete',
            data: {
                'team_id': self.data('team-id')
            },
            method: 'post'
        }).then(function (response) {
            if (typeof response === 'object') {
                if (response.status === 'ok') {
                    showMessage(response.message, 'ok', 5000);
                    $teamDeleteModal.modal('hide');

                    if (response.redirect) {
                        setTimeout(function () {
                            window.location = response.redirect;
                        }, 2000);
                    }
                } else {
                    console.error(response);
                    showError(response.message);
                }
            } else {
                console.error(response);
                showError(response);
            }
        });
    });

        $teamAddModal.find('.modal-actions .action.add').on('click', function (e) {
        e.preventDefault();
        var teamId = $(this).data('team-id');

        $.ajax({
            url: 'teams/add-user',
            data: {
                'team_id': teamId,
                'user_id': $teamAddModal.find('select').val()
            },
            method: 'post'
        }).then(function (response) {
            if (typeof response === 'object') {
                if (response.status === 'ok') {
                    showMessage(response.message, 'ok', 5000);
                    $teamAddModal.modal('hide');

                    if (response.redirect) {
                        setTimeout(function () {
                            window.location = response.redirect;
                        }, 2000);
                    }
                } else {
                    console.error(response);
                    showError(response.message);
                }
            } else {
                console.error(response);
                showError(response);
            }
        });
    });

    function initModal(action, teamId, teamTitle) {
        var teamEditModalLabel = $teamModal.find('.modal-title .title');

        switch (action) {
            case 'create':
                $teamModalTitleInput.val('');
                teamEditModalLabel.html('Create team');

                $teamModal.modal('show');
                break;
            case 'edit':
                $teamModalTitleInput.val(teamTitle);
                teamEditModalLabel.html('Edit team');

                $.ajax({
                    url: 'teams/permissions',
                    data: {
                        'team_id': teamId
                    },
                    method: 'post'
                }).then(function (response) {
                    if (typeof response === 'object') {
                        if (response.status === 'ok') {
                            $teamModal.find('input[name="permission_projects_management"]').attr(
                                'checked',
                                response.permissions.projects !== undefined && response.permissions.projects
                            );

                            $teamModal.find('input[name="permission_apps_management"]').attr(
                                'checked',
                                response.permissions.apps !== undefined && response.permissions.apps
                            );

                            $teamModal.find('input[name="permission_editions_management"]').attr(
                                'checked',
                                response.permissions.editions !== undefined && response.permissions.editions
                            );

                            $teamModal.find('input[name="permission_pages_management"]').attr(
                                'checked',
                                response.permissions.pages !== undefined && response.permissions.pages
                            );

                            $teamModalSave.data('team-id', teamId);

                            $teamModal.modal('show');
                        } else {
                            console.error(response);
                            showError(response.message);
                        }
                    } else {
                        console.error(response);
                        showError(response);
                    }
                });
                break;
        }
    }

});
