/* globals THREE: false, FOUR: false, TWEEN: false, PDFJS: false, console: false, Sortable: false, $: false, Stats: false, Mousetrap: false, modalConfirm: false, showError: false, showMessage: true, plyr: false */
// jshint esversion: 6
// jshint trailingcomma: false

// Avoid `console` errors in browsers that lack a console.
(function () {
    'use strict';

    var method;
    var noop = function () {
    };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

function App() {
    'use strict';

    const COVER_STATUS_TMP = 0;
    const COVER_STATUS_NORMAL = 1;
    const COVER_STATUS_DELETED = 2;

    var self = this;
    var app = this; // TODO: move all "self." references to "app."
    /**
     * configuration
     * @typedef {Object} AppConfig
     * @property {AppPaths} paths
     * @property {EditionInfo} edition
     */
    /**
     * @typedef {Object} AppPaths
     * @property {String} objects
     * @property {String} thumbs
     * @property {String} covers
     */
    /**
     * @typedef {Object} EditionInfo
     * @property {number} id
     * @property {String} title
     */
    /**
     * @type {AppConfig}
     */
    var config = {};
    var clock = new THREE.Clock();
    var camera, perspectiveCamera, orthographicCamera, orbitControl, renderer, scene, toolsScene, loadManager;
    var orthoZoom = 2;
    var zoom = 100;
    var targetZoom = zoom;
    var zoomLimits = [15, 350];
    var viewportMode = '3d';
    var imagePlane, coverBoundingBox;
    var raycaster;
    var transformControl, transformMode = 'translate';
    var scaleControl;
    var dragControls;
    var mouse;
    var $contextMenu;
    /**
     * @typedef {Object} SceneObjectOptions
     * @property {{type:string,url:string,video:string}} [action]
     * @property {String} [volume]
     * @property {String} [animation]
     */
    /**
     * @typedef {Object} ObjectDefinition
     * @property {String} id
     * @property {String} cover_id
     * @property {String} object_id
     * @property {String} title
     * @property {String} filename
     * @property {String} thumb
     * @property {String} type
     * @property {number} x
     * @property {number} y
     * @property {number} z
     * @property {number} rx
     * @property {number} ry
     * @property {number} rz
     * @property {number} scale
     * @property {String} deleted
     * @property {SceneObjectOptions} options
     * @property {Object} params
     */
    /**
     * @typedef {Object} CoverDefinition
     * @property {String} id
     * @property {String} edition_id
     * @property {String} title
     * @property {String} filename
     * @property {String} position
     * @property {String} rating
     * @property {String} status
     * @property {String} main
     * @property {Array.<ObjectDefinition>} objects
     */
    /**
     *
     * @type {Array.<CoverDefinition>}
     */
    var covers = [];
    /**
     * @type {Array.<Object3D>}
     */
    var raytraceObjects = []; // to hold all objects for raytrace/mouse pick
    /**
     * @type {Object.<string, Group>}
     */
    var objectsGroups = {}; // to hold all current cover 3d objects groups
    /**
     * @type {Object.<string, Object3D>}
     */
    var placeholderObjects = {}; // scene object id as key
    /**
     * @type {Array.<THREE.AnimationMixer>}
     */
    var mixers = [];
    var animations; // for animation
    var htmlViewport = $('#viewport');
    var editPanel, transformPanel, animationsPanel, viewcube;
    var mediaPanel = {};
    var actionIconMaterial = {};
    var cameraTarget = new THREE.Vector3(0, 30, 0);

    var sceneMode = 'cover'; // cover | object
    var libraryObjectId = 0; // for "object" mode

    var projectId, editionId, coverId;

    var unsavedChanges = false;

    /**
     * @type {Array.<{name: string, cover_id: *, target: *}>}
     */
    var undoStack = [];
    /**
     * @type {Array.<{name: string, cover_id: *, target: *}>}
     */
    var redoStack = [];

    var selected = null;
    var previousSelected = null;
    var newIndex = 0;
    var defaultRotationOrder = 'ZYX';

    self.covers = function () {
        return covers;
    };
    self.htmlViewport = function () {
        return htmlViewport;
    };
    self.objectsPath = function () {
        return config.paths.objects;
    };
    self.thumbsPath = function () {
        return config.paths.thumbs;
    };
    self.coversPath = function () {
        return config.paths.covers;
    };
    self.libraryObjectId = function () {
        return libraryObjectId;
    };
    self.getSelected = function () {
        return selected;
    };
    self.config = function () {
        return config;
    };
    /**
     *
     * @param threejsObject
     * @return {ObjectDefinition|null}
     */
    self.getCoverObject = function(threejsObject) {
        if (threejsObject.userData && threejsObject.userData.object) {
            return threejsObject.userData.object;
        } else {
            return null;
        }
    };
    /**
     *
     * @return {string}
     */
    self.getDefaultRotationOrder = function() {
        return defaultRotationOrder;
    };

    //////////
    //////////
    // helpers
    var bbox2dScaleColor = 0x00ff00;
    var bboxDefaultColor = 0xffff00;
    var selectionBox = new THREE.BoxHelper();
    selectionBox.material.color.set(bboxDefaultColor);

    var highlightBoxDefaultColor = 0x0097d6;
    var highlightBox = new THREE.BoxHelper();
    highlightBox.material.color.set(highlightBoxDefaultColor);

    //////////
    // DEBUG!!!!!!!!!

    var threejsStats;
    if (typeof Stats === 'function') {
        threejsStats = new Stats();
    }
    var renderRequest = true;

    window.aug = {};
    window.aug.raytraceObjects = raytraceObjects;
    window.aug.mixers = mixers;
    window.aug.clock = clock;

    //////////

    // options
    var snapAngle = 45;
    var snapAngle2d = 90;
    var imagePlaneSize = 100;
    var iconPlaneSize = 11;

    var fogColor = 0xcccccc;
    var ambientColor = 0x333333;//0x00ACFF;
    var transformPanelUpdateDelay = 1; //ms
    var imageAnisotropy = 0; // will be auto-detected later
    var imageAnisotropyDesired = 8;

    var iconSize = 20;

    var sceneDistanceLimit = 500;
    var cameraDistanceLimit = sceneDistanceLimit * 2;
    var renderDistanceLimit = cameraDistanceLimit * 2;

    // TODO: move all this to user settings?
    var pauseOnBlur = true;
    var fps = 60;
    var fpsPaused = 1; // limit fps on app.pause
    var fpsCurrent = fps;

    self.showLoader = function () {
        $('.scene-loader').fadeIn();
    };

    self.hideLoader = function () {
        $('.scene-loader').fadeOut();
    };

    self.run = function (_projectId, _editionId, _config) {
        projectId = _projectId;
        editionId = _editionId;

        config = _config;

        initPanelsToggle();

        // 3D
        THREE.crossOrigin = '';

        if (!isWebglAvailable()) {
            self.displayBlocker(true);
            $('<div>').addClass('message-fatal-error').html('Sorry, unable to initialize WebGL.<br />' +
                '<br />' +
                'Please check<br />' +
                '<a target="_blank" href="http://get.webgl.org">http://get.webgl.org</a><br />' +
                'and<br />' +
                '<a target="_blank" href="http://get.webgl.org/troubleshooting">http://get.webgl.org/troubleshooting</a>')
                .appendTo('body');
            return;
        }

        try {
            initRenderer();
        } catch (e) {
            self.fatalError('Sorry, unable to initialize WebGL.<br />' +
                '<br />' +
                'Please check<br />' +
                '<a target="_blank" href="http://get.webgl.org">http://get.webgl.org</a><br />' +
                'and<br />' +
                '<a target="_blank" href="http://get.webgl.org/troubleshooting">http://get.webgl.org/troubleshooting</a>');

            self.sendErrorReport(e);
            console.error('catched', e);
            return;
        }

        try {
            initCamera();
            initMouse();
            initContextMenu();
            initSceneAndLights();
            initEditControls();
            initKeyboard();
            initTrackball();
            initViewcube();
            initViewportLock();
            initViewport3DSwitch();
            initLoadManager();
            initScaleHandlesAutosize();
            initActionsPanel();
            initZoomControl();

            if (pauseOnBlur) {
                $(window).on('focus blur', function (e) {
                    app.pause(e.type === 'blur');
                });
            }

            // debug
            if (threejsStats) {
                htmlViewport.append(threejsStats.dom);
            }

            animate();

            // HTML UI
            self.loadingControl = new self.LoadingControl($('.loading-control'));
            initEditPanel();
            self.mediaLibrary.init();
            self.toolsPanel.init();
            initTransformTab();
            self.coversList.init();
            initUploadObject();
            self.objectsList.init();
            initAnimationsTab();
            initMediaPanels();
            initCheckLeavePageUnsaved();
            initCoverPdfExport();
            initObjectReplaceDialog();
            self.hideLoader();

            var $coversListPage = $('#covers-list-page-number');
            var coversListPagesCount = 1;

            $coversListPage.on('keyup', function (event) {
                if ($(this).val() === '') {
                    self.coversList.setCovers(covers);
                    return;
                }

                if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
                    event.preventDefault();
                    return;
                }

                var currentPageNumber = parseInt($(this).val()),
                    maxPageNumber = parseInt($(this).attr('max'));

                if (!isNaN(maxPageNumber) && currentPageNumber > maxPageNumber || currentPageNumber === 0) {
                    $(this).val('');
                    self.coversList.setCovers(covers);
                }

                var currentCoverId = 1,
                    currentCoverNumber = 1;

                for (var i = 0; i < covers.length; i++) {
                    if (i + 1 === currentPageNumber) {
                        currentCoverId = covers[i].id;
                        currentCoverNumber = i + 1;
                        self.coversList.setCovers([covers[i]]);
                        break;
                    }
                }

                self.switchToCover(currentCoverId);
                $('.cover.active .head .index').html(currentCoverNumber);
            });

            // load covers
            self.loadCovers().then(function () {
                coversListPagesCount = covers.length;
                $coversListPage.attr('max', coversListPagesCount);
                $('.covers-list-count .count-title').html(coversListPagesCount);

                self.coversList.setCovers(covers);
                // switch to selected or first available
                self.switchToCover();
            });

            var $modalExportPDF = $('#modal-export-pdf');
            $modalExportPDF.find('.action.pdf-open').on('click', function () {
                $modalExportPDF.modal('hide');
            });

            var $modalCustomFieldsManage = $('#modal-custom-fields-manage');

            $('.manage-custom-fields').on('click', function (e) {
                e.preventDefault();

                $modalCustomFieldsManage.modal('show');
            });

            $modalCustomFieldsManage.find('.custom-fields-save').on('click', function (e) {
                e.preventDefault();

                if (app.objectId(selected)) {
                    saveCustomFields(selected, $modalCustomFieldsManage.find('textarea[name="custom-fields"]').val());
                }
            });

            $('#settings-bar-content-accordion').hide();
            $('.settings-bar-no-object-selected').show();
        } catch (e) {
            self.fatalError('Sorry, application failed to initialize.');

            self.sendErrorReport(e);
            console.error('catched', e);
        }
    };

    function saveCustomFields(object, customFieldsString) {
        $.ajax({
            url: '/editions/set-custom-fields',
            data: {
                object_id: app.objectId(object),
                custom_fields:  customFieldsString
            },
            method: 'post'
        }).then(function (response) {
            if (response) {
                if (response.errors) {
                    showError('Error saving custom fields');
                } else {
                    if (typeof object.userData.object.options === 'undefined') {
                        object.userData.object.options = {};
                    }
                    object.userData.object.options.custom_fields = JSON.parse(customFieldsString);

                    showMessage('Custom fields are saved', 'ok', 2000);
                }
            } else {
                showError('Error saving custom fields');
            }
        });
        // TODO: error check!
    }

    function updateCustomFields(object) {
        var serverObject = object.userData.object;

        var customFieldsValue = (serverObject.options && serverObject.options.custom_fields)? JSON.stringify(serverObject.options.custom_fields) : '';
        $('#modal-custom-fields-manage').find('.custom-fields').val(customFieldsValue);
    }

    self.loadCovers = function () {
        return $.ajax({
            url: 'covers/',
            data: {editionId: editionId},
            dataType: 'json'
        }).then(function (_covers) {
            // TODO: error check
            covers = _covers;
            window.__covers = covers;
        });
    };

    self.repositionCover = function (oldIndex, newIndex) {
        if (newIndex >= covers.length) {
            var k = newIndex - covers.length;
            while ((k--) + 1) {
                covers.push(undefined);
            }
        }
        covers.splice(newIndex, 0, covers.splice(oldIndex, 1)[0]);

        for (var i = 0; i < covers.length; i++) {
            covers[i].position = i;
        }

        app.setUnsavedState(true);
    };

    self.deleteCover = function (coverId) {
        var i;
        var mainCoverDeleted = false;
        for (i = 0; i < covers.length; i++) {
            if (covers[i].id == coverId) {
                covers[i].status = COVER_STATUS_DELETED;
                mainCoverDeleted = String(covers[i].main) === '1';
                covers[i].main = '0';

                self.coversList.setDeleted(coverId, true);
                $('.covers-list-count .count-title').html($('.covers-list .cover:not(.template, .deleted)').length);

                var action = {
                    name: 'deleteCover',
                    coverId: coverId,
                    target: coverId
                };
                self.addToUndo(action, false);
                break;
            }
        }

        if (mainCoverDeleted) {
            for (i = 0; i < covers.length; i++) {
                if (covers[i].status != COVER_STATUS_DELETED) {
                    covers[i].main = '1';
                    self.coversList.setCoverMain(covers[i].id);
                    break;
                }
            }
        }

        app.setUnsavedState(true);
    };

    self.addUploadedCovers = function (uploadedCovers) {
        var newCovers = [];

        for (var i = 0; i < uploadedCovers.length; i++) {
            var updCover = uploadedCovers[i];
            var ownCover = getCover(updCover.id);

            if (ownCover) {
                // update
                ownCover.filename = updCover.filename;
                ownCover.rating = updCover.rating;

                // update ui
                self.coversList.updateCoverHtml(updCover);
            } else {
                // add
                updCover.objects = [];
                updCover.position = covers.length + i;
                if (covers.length === 0) {
                    updCover.main = 1;
                }
                covers.push(updCover);
                // add ui;
                self.coversList.addCover(updCover);

                newCovers.push(updCover);
            }
        }

        if (newCovers.length) {
            var action = {
                name: 'add_covers',
                target: newCovers
            };
            self.addToUndo(action, false);
        }
    };

    self.sendErrorReport = function (e) {
        var report = {};
        report.exception = e.toString();
        if (e.stack)
            report.stack = e.stack.toString();
        $.ajax({url: '/', data: report, type: 'POST'});
    };

    self.fatalError = function (message) {
        self.displayBlocker(true);
        $('<div>').addClass('message-fatal-error').html(message)
            .appendTo('body');
    };

    self.alert = function (message) {
        var def = new $.Deferred();

        self.displayBlocker(true);

        var main = $('<div>').addClass('message-alert');
        var messageBlock = $('<div>' + message + '</div>');
        var buttonsBlock = $('<div class=\'buttons\'></div>');

        var button = $('<div>').addClass('button centered');

        main.append(messageBlock);
        main.append(buttonsBlock);
        main.appendTo('body');

        return def.promise();
    };

    self.isPDFExporting = false;

    function initCoverPdfExport() {
        $('.scene-action-pdf-export').on('click', function () {
            if (self.isPDFExporting) {
                return false;
            }

            self.isPDFExporting = true;
            $(this).html('<img src="/img/tail-spin-small-dark.svg">').attr('disabled', true);
            self.exportCoverToPdf();
        });
    }

    function forceDownload(uri, name) {
        var link = document.createElement('a');
        link.download = name;
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        // delete link;
    }

    function openLink(uri) {
        var link = document.createElement('a');
        link.target = '_blank';
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        // delete link;
    }

    self.exportCoverToPdf = function () {
        var coversRendered = new $.Deferred();

        // html render size
        var sceneImageSize = 800; // px
        var debug = 0;

        var templateLoaded = $.ajax('/editions/pdfTemplate');

        var coverIndex = 0;
        var coversResults = [];
        var renderNextCover = function () {
            app.renderCover(covers[coverIndex], sceneImageSize).then(function (img) {
                coversResults.push(img);
                coverIndex++;

                if (coverIndex < covers.length) {
                    renderNextCover();
                } else {
                    coversRendered.resolve();
                }
            });
        };
        renderNextCover();

        templateLoaded.fail(function(error){
            var errorMessage = 'Pdf export failed.';
            if (error.statusText) {
                errorMessage += ' ' + error.statusText;
            }
            showError(errorMessage);
        });


        $.when(templateLoaded, coversRendered).then(function (templateLoadResult) {
            var template = $('<div>').html(templateLoadResult[0]);
            var logoImage = template.find('.logo-image');
            logoImage.attr('src', location.origin + logoImage.attr('src'));

            var pages = [];
            for (var i = 0; i < coversResults.length; i++) {
                var res = coversResults[i];
                var scale = sceneImageSize / (res.radius * 2);

                if (i !== 0) {
                    template.find('style').remove();
                }
                if (i === coversResults.length - 1) {
                    template.find('.page-export-view').addClass('last');
                }
                template.find('.page-image').attr('src', location.origin + '/' + app.coversPath() + covers[i].filename);
                template.find('.page-count .number').text(i + 1);
                template.find('.page-scene-screen img.main').attr('src', res.image);
                if (typeof res.image2 !== 'undefined') {
                    template.find('.page-scene-screen img.underground').attr('src', res.image2);
                } else {
                    template.find('.page-scene-screen img.underground').remove();
                }
                template.find('.page-scene-screen .cover-shadow').css({
                    left: (sceneImageSize / 2) - (res.cover.width / 2) * scale + 1,
                    top: (sceneImageSize / 2) - (res.cover.height / 2) * scale + 1,
                    width: res.cover.width * scale - 2,
                    height: res.cover.height * scale - 2
                }).attr('src', location.origin + '/' + app.coversPath() + covers[i].filename);

                var labelsBox = template.find('.labels-box').empty();
                for (var li = 0; li < res.labels.length; li++) {
                    var x = (sceneImageSize / 2) + res.labels[li].x * scale;
                    var y = (sceneImageSize / 2) + res.labels[li].y * scale;
                    var text = res.labels[li].type;

                    if (text === 'object')
                        text = '3d';
                    var label = $('<div>').text(text);

                    x -= label.width();
                    y -= label.height();

                    label.css({
                        left: x,
                        top: y
                    });

                    labelsBox.append(label);
                }

                if (debug) {
                    var div = $('<div>').html(template[0].outerHTML).appendTo($('body')).css({
                        border: '1px solid #ff00ff',
                        position: 'absolute',
                        top: 130,
                        left: 50,
                        width: '85%',
                        height: '65%',
                        overflow: 'auto'
                    });
                }

                pages.push(template[0].outerHTML);
            }

            coversResults = null;

            return $.ajax({
                url: '/editions/pdf', data: {
                    editionId: app.editionId(),
                    images: pages
                }, method: 'post'
            }).then(function (result) {
                if (result.fileLink) {
                    //openLink(result.fileLink);
                    //downloadURI(result.fileLink, result.fileName);
                    var $modalExportPDF = $('#modal-export-pdf');
                    $modalExportPDF.find('.action.pdf-open').attr('href', result.fileLink);
                    $modalExportPDF.modal('show');
                }
            }, function (reason) {
                console.error('upload failed?', reason);
            }).done(function () {

            });
        }).always(function(){
            self.isPDFExporting = false;
            $('.scene-action-pdf-export').html('Export').attr('disabled', false);
        });

    };

    function initCheckLeavePageUnsaved() {
        $(window).bind('beforeunload', function () {
            if (unsavedChanges) {
                return 'You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?';
            }
        });
    }

    function initPanelsToggle() {
        var mainPanel = $('.main-sidebar');
        var rightPanel = $('#right-side-panel');

        mainPanel.find('.toggle-sidebar').click(function (e) {
            e.preventDefault();

            $('body').toggleClass('main-sidebar-hidden').trigger('viewport-update');
        });
        rightPanel.find('.toggle-sidebar').click(function (e) {
            e.preventDefault();

            $('body').toggleClass('right-sidebar-hidden').trigger('viewport-update');
        });


        // autohide after page load
        var autohideTimeout = 5000;

        var leftAutohideTimer = window.setTimeout(function () {
            $('body').toggleClass('main-sidebar-hidden').trigger('viewport-update');
        }, autohideTimeout);
        mainPanel.one('mousemove', function () {
            window.clearTimeout(leftAutohideTimer);
        });

        // var right_autohide_timer = window.setTimeout(function(){
        // 	$("body").addClass("right-sidebar-hidden").trigger('viewport-update');
        // }, autohide_timeout);
        // right_panel.one('mousemove', function(){
        // 	window.clearTimeout(right_autohide_timer);
        // });
    }

    function initLoadManager() {
        // model
        loadManager = new THREE.LoadingManager();
        loadManager.onProgress = function (item, loaded, total) {

            // console.log("load++");
            // // console.log(item, loaded, total);
            // console.log(item);
            // console.log(loaded);
            // console.log(total);
            // console.log("load--");

        };
    }

    function initCamera() {
        var aspect = htmlViewport.width() / htmlViewport.height();
        var orthoHeight = imagePlaneSize * orthoZoom;
        var orthoWidth = orthoHeight * aspect;
        perspectiveCamera = new THREE.PerspectiveCamera(60, aspect, 1, renderDistanceLimit);
        orthographicCamera = new THREE.OrthographicCamera(orthoWidth / -2, orthoWidth / 2, orthoHeight / 2, orthoHeight / -2, 1, renderDistanceLimit);

        perspectiveCamera.position.set(imagePlaneSize, imagePlaneSize, imagePlaneSize).add(cameraTarget);
        perspectiveCamera.lookAt(cameraTarget);

        orthographicCamera.position.set(0, cameraDistanceLimit, 0);
        orthographicCamera.lookAt(new THREE.Vector3());

        camera = viewportMode === '2d' ? orthographicCamera : perspectiveCamera;

        perspectiveCamera.addEventListener('update', function(){
            animate();
        });
    }

    function initMouse() {
        var mouseTarget = renderer.domElement; // document
        mouse = new THREE.Vector2();
        raycaster = new THREE.Raycaster();

        var onDownPosition = new THREE.Vector2();
        var onUpPosition = new THREE.Vector2();
        var mouseIsDown = false;

        function getIntersects(point, objects) {
            mouse.set((point.x * 2) - 1, -(point.y * 2) + 1);
            raycaster.setFromCamera(mouse, camera);
            return raycaster.intersectObjects(objects);
        }

        function getMousePosition(dom, x, y) {
            var rect = dom.getBoundingClientRect();
            return [(x - rect.left) / rect.width, (y - rect.top) / rect.height];
        }

        function handleClick(event) {
            $contextMenu.hide();

            if (onDownPosition.distanceTo(onUpPosition) === 0) {
                var intersects = getIntersects(onUpPosition, raytraceObjects);
                var newSelected = null;

                if (intersects.length > 0) {
                    newSelected = intersects[0].object;
                    newSelected = topmostParentOf(newSelected);
                }

                // selection undo/redo is UNFINISHED
                // if (selected != new_selected) {
                // 	console.log("add to undo");
                // 	self.addToUndo({
                // 		name: "select",
                // 		target: selected
                // 	});
                // }

                self.onSelect(newSelected);

                if (event.button === 2 && selected) {
                    event.preventDefault();

                    // add 1px to prevent immediate system context menu on our context menu
                    $contextMenu.css({left: event.clientX + 1, top: event.clientY + 1});
                    $contextMenu.data('target', selected);
                    $contextMenu.find('.title').text(app.getCoverObject(selected).title);

                    $contextMenu.show();
                }
            }
        }

        function onMouseDown(event) {
            event.preventDefault();

            mouseIsDown = true;

            var array = getMousePosition(renderer.domElement, event.clientX, event.clientY);
            onDownPosition.fromArray(array);

            mouseTarget.addEventListener('mouseup', onMouseUp, false);
        }

        function onMouseUp(event) {
            mouseIsDown = false;

            var array = getMousePosition(renderer.domElement, event.clientX, event.clientY);
            onUpPosition.fromArray(array);

            mouseTarget.removeEventListener('mouseup', onMouseUp, false);

            // Handle different event models
            handleClick(event);
        }

        function onMouseMove(event) {
            //event.preventDefault();

            if (mouseIsDown) {
                // self.onHover(null);
                return;
            }

            var array = getMousePosition(renderer.domElement, event.clientX, event.clientY);
            var position =  new THREE.Vector2();
            position.fromArray(array);

            var intersects = getIntersects(position, raytraceObjects);
            var hoveredObject = null;

            if (intersects.length > 0) {
                hoveredObject = intersects[0].object;
                hoveredObject = topmostParentOf(hoveredObject);
            }

            self.onHover(hoveredObject);
        }

        mouseTarget.addEventListener('mousedown', onMouseDown, false);
        mouseTarget.addEventListener('mousemove', onMouseMove, false);
    }

    function initContextMenu() {
        $contextMenu = $('#context-menu');

        $contextMenu.find('.action.object-replace').on('click', function(event){
            event.preventDefault();

            $contextMenu.hide();

            showObjectReplaceDialog();
        });

        $contextMenu.find('.action.object-delete').on('click', function(event){
            event.preventDefault();

            $contextMenu.hide();

            var message = selected.userData.object.title ? 'Delete \'' + selected.userData.object.title + '\'?' : 'Delete object?';
            modalConfirm(message, {
                onConfirm: function () {
                    self.deleteObject(selected);
                }
            });
        });
    }

    function topmostParentOf(object) {

        if (isTopmost(object)) {
            return object;
        }

        var parent = object.parent;
        while (!isTopmost(parent)) {
            parent = parent.parent;
        }

        return parent;
    }

    function isTopmost(object) {
        return !object.parent || (object.parent instanceof THREE.Scene);
    }

    function initObjectReplaceDialog() {
        var dialog = $('#modal-object-replace');
        var fileInput = $('#object-replace-input');

        fileInput.on('change', function() {
            var libraryId = app.getCoverObject(selected).object_id;
            if (fileInput[0].files.length) {
                uploadFilesToLibrary(fileInput[0].files, libraryId);
            }
        });

        dialog.find('.action.object-replace').click(function(e){
            fileInput.val(null);
            fileInput.trigger('click');

            dialog.modal('hide');
        });
    }

    function showObjectReplaceDialog() {
        var dialog = $('#modal-object-replace');
        var libraryId = app.getCoverObject(selected).object_id;
        var coversList = dialog.find('.object-covers-list');
        var usedCoversBlock = dialog.find('.used-covers');
        usedCoversBlock.hide();
        coversList.empty();

        $.ajax('/library/usage/' + libraryId).then(function(response){
            if (response && response.covers) {

                for (var key in response.covers) {
                    if (!response.covers.hasOwnProperty(key)) {
                        continue;
                    }

                    var row = response.covers[key];
                    var cover = $('<div class="cover">')
                        .data('id', row.id)
                        .data('title', row.title)
                        .data('filename', row.filename);

                    //var head = $('<div class="head">').appendTo(cover);
                    //$('<span>').addClass('index').attr('title', row.title).appendTo(head);
                    //$('<span>').addClass('title').attr('title', row.title).html(row.title).appendTo(head);

                    var linkBlock = $('<a>').appendTo(cover);
                    linkBlock.attr('target', '_blank').attr('href', '/projects/' + app.projectId() + '/' + app.editionId() + '#' + row.id);

                    var imageBlock = $('<span>').addClass('image').appendTo(linkBlock);


                    $('<img src="'+ app.coversPath() + row.filename+'">"').appendTo(imageBlock);

                    coversList.append(cover);
                }
                usedCoversBlock.show();
            } else {
                showError('Failed to get object usage data from server.');
            }
        });

        dialog.modal('show');
    }

    var lastHighlighted;
    self.onHover = function (object) {

        if (!object) {
            highlightBox.visible = false;
        } else if (object !== selected) {
            highlightBox.setFromObject(object);

            highlightBox.update();
            highlightBox.visible = true;
        }

        if (object && sceneMode !== 'object' && typeof object.userData !== 'undefined') {
            self.objectsList.onObjectHover(object.userData.object.id);
        } else {
            self.objectsList.onObjectHover(0);
        }

        // request render only if hovering new object
        if (lastHighlighted !== object) {
            lastHighlighted = object;
            self.requestRender();
        }
    };

    self.onSelect = function (object) {
        if (selected) {
            transformPanelOnInputChange.apply(transformInputs.title[0]);
        }

        previousSelected = selected;
        selected = object;
        //console.log("selected", object);

        // if (selected !== prev_selected) {
        //     hide_action_icon(prev_selected);
        // }


        if (object !== null) {
            $('#settings-bar-content-accordion').show();
            $('.settings-bar-no-object-selected').hide();

            editMode(object, previousSelected);
            if (transformMode === 'translate') {
                enableFreeDrag(object);
            }

        } else {
            $('#settings-bar-content-accordion').hide();
            $('.settings-bar-no-object-selected').show();

            if (transformMode === 'scale') {
                scaleControl.detach(object);
            } else {
                transformControl.detach();
                toolsScene.remove(transformControl);
                disableFreeDrag();
            }

            selectionBox.visible = false;
            editPanel.hide();

            updateMediaPanel('video', previousSelected);
            updateMediaPanel('audio', previousSelected);
        }

        if (object && sceneMode !== 'object' && typeof object.userData !== 'undefined') {
            //console.log("selected id", object.userData.id);

            self.objectsList.onObjectSelect(object.userData.object.id);
        } else {
            self.objectsList.onObjectSelect(0);
        }

        self.requestRender();
    };

    function enableFreeDrag(object) {
        if (dragControls) {
            dragControls.dispose();
        }

        dragControls = new THREE.DragControls([object], camera, renderer.domElement);
        dragControls.addEventListener('drag', function (event) {
            if (typeof transformControl.object === 'undefined') return;
            var object = transformControl.object;

            if (sceneMode === 'cover') {
                _applyLimitsOnTransform(object);
            }
            _applySnapOnMove(object);

            selectionBox.update();

            updateActionIcon(object);
        });
        //dragControls.addEventListener( 'dragstart', function ( event ) { controls.enabled = false; } ); // turn off camera move
        //dragControls.addEventListener( 'dragend', function ( event ) { controls.enabled = true; } );// turn on camera move
    }

    function disableFreeDrag() {
        if (dragControls) {
            dragControls.dispose();
            dragControls = null;
        }
    }

    function editMode(object, previousSelected) {

        selectionBox.setFromObject(object);

        if (transformMode === 'scale' && viewportMode === '2d') {
            selectionBox.material.color.set(bbox2dScaleColor);
        } else {
            selectionBox.material.color.set(bboxDefaultColor);
        }

        selectionBox.update();
        selectionBox.visible = true;

        if (transformMode === 'scale') {
            scaleControl.attach(object);
        } else {
            transformControl.attach(object);
            toolsScene.add(transformControl);
        }

        editPanel.show();

        fillAnimationsList(object);
        updateMediaPanel('video', previousSelected);
        updateMediaPanel('audio', previousSelected);
        updateTransformPanel(object);
        updateCustomFields(object);
        updateActionsPanel();

        updateActionIcon(object);
        showActionIcon(object);
    }

    self.objectId = function (object) {
        return object && object.userData ? object.userData.object.id : null;
    };

    // mark as deleted in database (will be actualy deleted on reload of scene)
    self.deleteObject = function (object, byRedo) {
        if (typeof object === 'undefined' || object === null || sceneMode === 'object')
            return;

        var id = self.objectId(object);
        convertationArray.splice(convertationArray.indexOf(id), 1);

        var action = {
            name: 'delete',
            coverId: coverId,
            target: object
        };
        self.addToUndo(action, byRedo);
        self.objectsList.hideObject(id);

        // delete from scene
        _deleteObject(object);
        // delete from cover
        if (object.userData.object) {
            _deleteServerObjectFromCover(coverId, object.userData.object.id);
        }
    };

    self.deleteObjectsByLibraryId = function (libraryId) {
        var i, removeList = [];
        for (i = 0; i < raytraceObjects.length; i++) {
            if (raytraceObjects[i].userData && raytraceObjects[i].userData.object && raytraceObjects[i].userData.object.object_id === libraryId) {
                removeList.push(raytraceObjects[i]);
            }
        }
        for (i = 0; i < removeList.length; i++) {
            self.objectsList.removeObject(removeList[i].userData.object.id);
            _deleteObject(removeList[i]);
            // delete from cover
            if (removeList[i].userData.object) {
                _deleteServerObjectFromCover(coverId, removeList[i].userData.object.id);
            }
        }
    };

    // delete from scene (do not save anything, just clear)
    function _deleteObject(object, doArrayUpdate) {
        // console.log("_delete_object", object);
        if (typeof object === 'undefined') {
            return;
        }

        _deleteFromScene(object, doArrayUpdate);

        if (object.video) {
            object.video.pause();
            $(object.video.getContainer()).hide();
        }
        if (object.audio) {
            object.audio.pause();
            $(object.audio.getContainer()).hide();
        }

        hideActionIcon(object);
    }

    function _deleteFromScene(object, doArrayUpdate) {
        doArrayUpdate = typeof doArrayUpdate === 'boolean' ? doArrayUpdate : true;

        if (selected && selected === object)
            self.onSelect(null);
        scene.remove(object);

        object.traverse(function (child) {
            var childIndex = raytraceObjects.indexOf(child);

            if (doArrayUpdate && childIndex > -1)
                raytraceObjects.splice(childIndex, 1);
            if (child.mixer) {
                mixers.splice(mixers.indexOf(child.mixer), 1);
            }
        });
        if (doArrayUpdate && raytraceObjects.indexOf(object) > -1) {
            raytraceObjects.splice(raytraceObjects.indexOf(object), 1);
        }
    }

    function _deleteServerObjectFromCover(_coverId, objectId) {
        var coverObjects = getCover(_coverId).objects;
        // var so_index = -1;
        for (var i = 0; i < coverObjects.length; i++) {
            if (coverObjects[i].id === objectId) {
                coverObjects[i].deleted = 1;
                // so_index = i;
                break;
            }
        }
        //
        // if (so_index >= 0)
        // 	scene_objects.splice(so_index, 1);
    }

    /**
     *
     * @param {number} _coverId
     * @return {CoverDefinition}
     */
    function getCover(_coverId) {
        // TODO: optimise this
        for (var i = 0; i < covers.length; i++) {
            if (covers[i].id == _coverId) {
                return covers[i];
            }
        }
    }

    function setSnap(on) {
        if (viewportMode === '2d')
            return;

        if (on) {
            //transform_control.setTranslationSnap(snap);
            transformControl.setRotationSnap(THREE.Math.degToRad(snapAngle));
        } else {
            //transform_control.setTranslationSnap(null);
            transformControl.setRotationSnap(null);
        }
    }

    function toggleSnap() {
        var snap = !snapEnabled();
        setSnap(snap);
    }

    function snapEnabled() {
        return !!transformControl.translationSnap;
    }

    function initEditControls() {
        transformControl = new THREE.TransformControls(camera, renderer.domElement);
        transformControl.uniformScale = true;
        transformControl.scaleCoeff = 0.1;
        transformControl.addEventListener('objectChange', onObjectTransform);
        transformControl.addEventListener('mouseDown', function () {
            if (typeof transformControl.object === 'undefined') return;

            if (dragControls) {
                dragControls.deactivate();
            }
        });
        transformControl.addEventListener('mouseDown', function () {
            if (typeof transformControl.object === 'undefined') return;

            if (dragControls) {
                dragControls.activate();
            }
        });
        transformControl.addEventListener('change', function () {
            app.requestRender();
        });

        scaleControl = new THREE.ScaleControl2(camera, scene, renderer.domElement);
        scaleControl.addEventListener('objectChange', onObjectTransform);
        scaleControl.addEventListener('mouseDown', function () {
            if (typeof transformControl.object === 'undefined') return;

            if (dragControls) {
                dragControls.deactivate();
            }
        });
        scaleControl.addEventListener('mouseDown', function () {
            if (typeof transformControl.object === 'undefined') return;

            if (dragControls) {
                dragControls.activate();
            }
        });
        scaleControl.addEventListener('mouseUp', function () {
            selectionBox.update();
        });
        scaleControl.addEventListener('change', function () {
            self.requestRender();
        });
    }

    function initScaleHandlesAutosize() {
        var maxHandleSize = 10; // size at minimum zoom
        var minZoom = 0.3;
        orbitControl.addEventListener('change', function (e) {
            var size = 2;
            var zoom = orbitControl.getZoom();

            size = Math.min(size / zoom, maxHandleSize);

            scaleControl.setHandleSizes(size);
        });
    }

    var zoomControl;

    function initZoomControl() {
        zoomControl = $('#scene-scale');

        orbitControl.addEventListener('change', function (e) {
            zoom = orbitControl.getZoom() * 100;
            updateZoomControl();
        });


        var wrapper = $('.scene-scale-wrapper');
        var zoomIn = wrapper.find('.zoom-in').closest('.scale-control');
        var zoomOut = wrapper.find('.zoom-out').closest('.scale-control');
        zoomIn.on('click', function () {
            self.zoomIn();
        });
        zoomOut.on('click', function () {
            self.zoomOut();
        });

        zoomControl.on('change', function () {
            var zoom = parseInt($(this).val().replace(/%/, ''));
            if (isNaN(zoom)) {
                // auto-revert to actual zoom
                updateZoomControl();
            } else {
                self.setZoom(zoom);
            }
        });

        //zoom_control.on('mousewheel', function(event){
        zoomControl[0].addEventListener('wheel', function (event) {
            event.preventDefault();
            event.stopPropagation();

            if (event.deltaY < 0) {
                self.setZoom(targetZoom + 10);
            } else {
                self.setZoom(targetZoom - 10);
            }
        }, false);

        htmlViewport.on('dblclick', function () {
            if (viewportMode === '3d') {
                app.setZoom(100);
                viewcube.setView(viewcube.FACES.TOP);
            }
        });

        zoom = orbitControl.getZoom() * 100;
        updateZoomControl();
    }

    function updateZoomControl() {
        zoomControl.val(Math.round(zoom) + '%');
    }

    self.zoomIn = function () {
        self.setZoom(targetZoom + 5);
    };

    self.zoomOut = function () {
        self.setZoom(targetZoom - 5);
    };

    var zoomTweening = false;
    var zoomTween;
    self.setZoom = function (value) {
        var newTargetZoom = Math.max(zoomLimits[0], Math.min(zoomLimits[1], value)); // update global

        if (targetZoom === newTargetZoom) {
            return;
        }

        if (zoomTween) {
            zoomTween.stop();
        }

        targetZoom = newTargetZoom;

        zoomTweening = true;
        var easing = TWEEN.Easing.Cubic.Out; // TWEEN.Easing.Cubic.InOut
        var duration = 400;
        var tweenValues = {zoom: zoom};
        var targetValues = {zoom: targetZoom};

        zoomTween = new TWEEN.Tween(tweenValues);
        zoomTween.to(targetValues, duration);
        zoomTween.easing(easing);
        zoomTween.onComplete(function () {
            zoomTweening = false;
        });
        zoomTween.onUpdate(function () {
            zoom = tweenValues.zoom;
            orbitControl.setZoom(tweenValues.zoom / 100);
            updateZoomControl();
        });

        zoomTween.start();

    };

    var actionTypeSelect, actionUrlInput, actionUrlBlock, urlValidationIndicator, actionVideoBlock;

    function initActionsPanel() {
        actionTypeSelect = $('#settings-action-type');
        actionUrlInput = $('#settings-open-url-link');
        urlValidationIndicator = $('#settings-action').find('.url-validity-state');
        urlValidationIndicator.hide();
        actionUrlBlock = $('.action-url-block');
        actionUrlBlock.hide();
        actionVideoBlock = $('.action-video-block');
        actionVideoBlock.hide();
        var previewBlock = $('.action-video-preview');
        previewBlock.hide().empty();

        var updateActionVideoUi = function () {
            var action = selected.userData.object.options.action;

            previewBlock.empty().hide();
            if (action.video) {
                var libraryObject = app.mediaLibrary.libraryObject(action.video);
                if (libraryObject) {
                    //preview_block.append(app.mediaLibrary.object_thumbnail_html(lib_obj));
                    var video = document.createElement('video');
                    $(video).prop('controls', true);
                    $(video).prop('loop', false);
                    $(video).appendTo(previewBlock);

                    var players = plyr.setup([video], {storage: false});
                    var player = players[0];
                    var playerSourceConfig = {
                        type: 'video',
                        title: libraryObject.title,
                        sources: [{
                            src: self.objectsPath() + libraryObject.filename,
                            type: libraryObject.type
                        }]
                    };
                    if (libraryObject.thumb) {
                        playerSourceConfig.poster = app.thumbsPath() + libraryObject.thumb;
                    }

                    player.source(playerSourceConfig);

                    player.on('loadedmetadata', function (e) {
                        var media = e.detail.plyr.getMedia();
                        var width = media.videoWidth;
                        var height = media.videoHeight;

                        if (!width && !height) {
                            // console.warn("empty video metadata loaded? [" + media.src + "]");
                            return;
                        }

                        previewBlock.show();
                    });

                }
            }

        };

        var setActionVideo = function (libraryId) {
            app.setUnsavedState(true);
            var object = selected.userData.object;
            if (typeof object.options !== 'object') {
                object.options = {};
            }
            if (typeof object.options.action !== 'object') {
                object.options.action = {};
            }

            object.options.action.video = libraryId;

            updateActionVideoUi();
        };

        var onActionChanges = function () {
            if (!selected) {
                return;
            }

            app.setUnsavedState(true);
            var object = selected.userData.object;
            if (typeof object.options !== 'object') {
                object.options = {};
            }
            if (typeof object.options.action !== 'object') {
                object.options.action = {};
            }

            var action = object.options.action;
            if (actionTypeSelect.val()) {
                action.type = actionTypeSelect.val();
                action.url = actionUrlInput.val();

                if (action.type === 'open-url') {
                    actionUrlBlock.show();
                    $('.settings-action-wrapper .action-icon-video').hide();
                    $('.settings-action-wrapper .action-icon-link').show();
                    actionVideoBlock.hide();
                    validateActionsUrl();
                } else if (action.type === 'open-video') {
                    actionUrlBlock.hide();

                    var objectActionVideoPopupToggler = $('.settings-action-wrapper .action-icon-video');

                    $('.settings-action-wrapper .action-icon-link').hide();
                    objectActionVideoPopupToggler.show();
                    actionVideoBlock.show();

                    updateActionVideoUi();

                    objectActionVideoPopupToggler.popover({
                        html: true,
                        class: '2',
                        trigger: 'click',
                        placement: 'left',
                        content: function () {
                            var list = $('<ul>').addClass('video-list');
                            var objects = app.mediaLibrary.getObjects();
                            var videoObjectsCount = 0;
                            var obj, li;

                            for (var id in objects) {
                                if (!objects.hasOwnProperty(id))
                                    continue;

                                obj = objects[id].object;

                                if (obj.type.match(/^video\//)) {
                                    videoObjectsCount++;

                                    li = $('<li>').addClass('video-item').data('id', obj.id);

                                    li.append(app.mediaLibrary.objectThumbnailHtml(obj));
                                    li.append($('<div class="title"></div>').html(obj.title));

                                    list.append(li);
                                }
                            }

                            if (!videoObjectsCount) {
                                list = 'You have no uploaded video';
                            } else {

                                list.find('.video-item').on('click', function () {
                                    objectActionVideoPopupToggler.popover('hide');

                                    setActionVideo($(this).data('id'));
                                });

                            }

                            return list;
                        }
                    });

                }

                showActionIcon(selected);
            } else {
                actionUrlBlock.hide();
                actionVideoBlock.hide();
                $('.settings-action-wrapper .action-icon-video').hide();
                $('.settings-action-wrapper .action-icon-link').show();
                hideActionIcon(selected);
            }

            updateActionIcon(selected);

            app.requestRender();
        };

        actionTypeSelect.on('change', onActionChanges);

        actionUrlInput.on('keyup', function (event) {
            event.preventDefault();

            validateActionsUrl();
            onActionChanges();
        });

        // setup icons materials
        actionIconMaterial['open-video'] = new THREE.MeshBasicMaterial({
            color: 0xffffff,
            map: new THREE.TextureLoader().load('/img/action-open-video.png'),
            side: THREE.DoubleSide,
            // side: THREE.FrontSide,
            transparent: true
        });

        actionIconMaterial['open-url'] = new THREE.MeshBasicMaterial({
            color: 0xffffff,
            map: new THREE.TextureLoader().load('/img/action-open-url.png'),
            side: THREE.DoubleSide,
            // side: THREE.FrontSide,
            transparent: true
        });

        var magFilter = THREE.LinearFilter;
        var minFilter = THREE.LinearFilter;

        actionIconMaterial['open-video'].map.magFilter = magFilter;
        actionIconMaterial['open-video'].map.minFilter = minFilter;
        actionIconMaterial['open-url'].map.magFilter = magFilter;
        actionIconMaterial['open-url'].map.minFilter = minFilter;

        orbitControl.addEventListener('change', function (e) {
            if (selected) {
                updateActionIconSize(selected);
            }
        });
    }

    function validateActionsUrl() {
        var url = actionUrlInput.val();
        if (actionTypeSelect.val() && url) {
            if (validURL(url)) {
                urlValidationIndicator.addClass('valid');
            } else {
                urlValidationIndicator.removeClass('valid');
            }
            urlValidationIndicator.show();
        } else {
            // hide if empty url or action type == 'none' (empty)
            urlValidationIndicator.hide();
        }
    }

    function updateActionsPanel() {
        var object;

        if (selected) {
            object = selected.userData.object;
        }

        if (object && object.options && typeof object.options.action === 'object') {
            actionTypeSelect.val(object.options.action.type);
            actionUrlInput.val(object.options.action.url);
        } else {
            actionTypeSelect.val('');
            actionUrlInput.val('');
        }
        actionTypeSelect.trigger('change');
    }

    // https://stackoverflow.com/users/356635/tom-gullen
    function validURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locater
        return pattern.test(str);
    }

    function onObjectTransform(e) {
        var object;
        if (transformMode === 'scale') {
            object = scaleControl.object;
        } else {
            object = transformControl.object;
        }

        if (typeof object === 'undefined') {
            return;
        }

        // check only on move
        switch (transformMode) {
            case 'translate':
                if (sceneMode === 'cover') {
                    _applyLimitsOnTransform(object);
                }
                _applySnapOnMove(object);
                break;
            case 'scale':
                if (_applySnapOnScale(object, e)) {
                    scaleControl.update();
                }
                break;
        }

        selectionBox.update();
        updateActionIcon(object);
        self.requestRender();
    }

    function checkSnap(point, checkPoint, snapMargin) {
        var _snapMargin = typeof snapMargin === 'undefined' ? 5 : snapMargin;
        return (point > checkPoint - _snapMargin) && (point < checkPoint + _snapMargin);
    }

    function _applyLimitsOnTransform(object) {
        var applied = false;

        // block everything go lower then 1
        if (object.position.y < 0) {
            object.position.y = 0;
            applied = true;
        }

        var center = new THREE.Vector3(0, 1, 0);
        if (object.position.distanceTo(center) > sceneDistanceLimit) {

            var dir = object.position.clone().sub(center).normalize().multiplyScalar(sceneDistanceLimit);
            var limitedPos = center.clone().add(dir);

            object.position.copy(limitedPos);

            applied = true;
        }

        return applied;
    }

    function _applySnapOnMove(object) {
        // snap to edges
        var box = new THREE.Box3();
        var objectBoundingBox = new THREE.Box3();

        var snappedOnX = false;
        var snappedOnZ = false;

        // snap to cover edges
        if (imagePlane) {
            box.setFromObject(imagePlane);
            objectBoundingBox.setFromObject(object); // object bounds

            if (!snappedOnX) {
                if (checkSnap(objectBoundingBox.min.x, box.min.x)) {
                    // snap to left
                    snappedOnX = true;
                    object.position.x = box.min.x + (object.position.x - objectBoundingBox.min.x);
                } else if (checkSnap(objectBoundingBox.max.x, box.max.x)) {
                    // snap to right
                    snappedOnX = true;
                    object.position.x = box.max.x + (object.position.x - objectBoundingBox.max.x);
                }
            }

            if (!snappedOnZ) {
                if (checkSnap(objectBoundingBox.min.z, box.min.z)) {
                    // snap to left
                    snappedOnZ = true;
                    object.position.z = box.min.z + (object.position.z - objectBoundingBox.min.z);
                } else if (checkSnap(objectBoundingBox.max.z, box.max.z)) {
                    // snap to right
                    snappedOnZ = true;
                    object.position.z = box.max.z + (object.position.z - objectBoundingBox.max.z);
                }
            }

        }

        // snap to center
        if (!snappedOnX && checkSnap(object.position.x, 0)) {
            object.position.x = 0;
        }
        if (!snappedOnZ && checkSnap(object.position.z, 0)) {
            object.position.z = 0;
        }
    }

    function _applySnapOnScale(object, event) {
        // snap to edges
        var planeBoundingBox = new THREE.Box3();
        var objectBoundingBox = new THREE.Box3();
        var pivot = event.pivot;

        // snap to cover edges
        if (imagePlane) {
            planeBoundingBox.setFromObject(imagePlane);
            objectBoundingBox.setFromObject(pivot); // object bounds

            var currentSize = {
                x: Math.abs(objectBoundingBox.max.x - objectBoundingBox.min.x),
                z: Math.abs(objectBoundingBox.max.z - objectBoundingBox.min.z)
            };

            var targetScale, targetSize;
            var availableScales = [];

            if (checkSnap(objectBoundingBox.min.x, planeBoundingBox.min.x)) {
                targetSize = Math.abs(planeBoundingBox.min.x - objectBoundingBox.max.x);
                targetScale = targetSize / currentSize.x;

                availableScales.push(targetScale);
            }
            if (checkSnap(objectBoundingBox.max.x, planeBoundingBox.max.x)) {
                targetSize = Math.abs(planeBoundingBox.max.x - objectBoundingBox.min.x);
                targetScale = targetSize / currentSize.x;

                availableScales.push(targetScale);
            }

            if (checkSnap(objectBoundingBox.min.z, planeBoundingBox.min.z)) {
                targetSize = Math.abs(planeBoundingBox.min.z - objectBoundingBox.max.z);
                targetScale = targetSize / currentSize.z;

                availableScales.push(targetScale);
            }
            if (checkSnap(objectBoundingBox.max.z, planeBoundingBox.max.z)) {
                targetSize = Math.abs(planeBoundingBox.max.z - objectBoundingBox.min.z);
                targetScale = targetSize / currentSize.z;

                availableScales.push(targetScale);
            }

            if (availableScales.length) {
                targetScale = Math.min.apply(null, availableScales);

                pivot.scale.multiplyScalar(targetScale);

                return true;
            }
        }
        return false;
    }

    self.getTransformControl = function () {
        return transformControl;
    };

    function initKeyboard() {
// check what keyCode for Delete on Mac and add it here
//Mousetrap.addKeycodes({
//    144: 'numlock'
//});
        Mousetrap.bind(['del', 'backspace'], function () {
            var message = selected.userData.object.title ? 'Delete \'' + selected.userData.object.title + '\'?' : 'Delete object?';
            modalConfirm(message, {
                onConfirm: function () {
                    self.deleteObject(selected);
                }
            });

        });
        // Mousetrap.bind('q', function () { // switch transform space
        // 	var space = transform_control.space === "local" ? "world" : "local";
        // 	transform_control.setSpace(space);
        // });
        // Mousetrap.bind('g', function () { // toggle snap to grid
        // 	toggle_snap();
        // });
        Mousetrap.bind('m', function () { // tool: move
            app.setMode('translate');
        });
        Mousetrap.bind('r', function () { // tool: rotate
            app.setMode('rotate');
        });
        Mousetrap.bind('s', function () { // tool: scale
            app.setMode('scale');
        });
        Mousetrap.bind(['plus', '='], function () { // transform helper bigger
            //transform_control.setSize(transform_control.size + 0.1);
            app.zoomIn();
        });
        Mousetrap.bind(['-', '_'], function () { // transform helper smaller
            //transform_control.setSize(Math.max(transform_control.size - 0.1, 0.1));
            app.zoomOut();
        });
        $(window).on('keydown', function (event) {
            if (event.keyCode === 17)
                setSnap(true);
        }).on('keyup', function (event) {
            if (event.keyCode === 17)
                setSnap(false);
        });

        Mousetrap.bind(['ctrl+s', 'meta+s'], function (e) {
            if (e.preventDefault) {
                e.preventDefault();
            } else {
                // internet explorer
                e.returnValue = false;
            }

            self.saveEdition();
        });

        Mousetrap.bind(['ctrl+z', 'meta+z'], function (e) {
            if (e.preventDefault) {
                e.preventDefault();
            } else {
                // internet explorer
                e.returnValue = false;
            }

            self.undo();
        });
    }

    function initTrackball() {

        orbitControl = new THREE.OrbitControls(perspectiveCamera, renderer.domElement, cameraTarget);
        orbitControl.enableZoom = true;
        orbitControl.enableZoomByMouseWheel = false;
        orbitControl.enablePan = false;
        orbitControl.minDistance = 30;
        orbitControl.maxDistance = cameraDistanceLimit;
        orbitControl.minZoom = zoomLimits[0] / 100;
        orbitControl.maxZoom = zoomLimits[1] / 100;

        orbitControl.addEventListener('change', function (e) {
            self.requestRender();
        });
    }

    self.onUpdateSelectedObject = function () {
        selectionBox.update();

        if (transformMode === 'scale') {
            scaleControl.update();
        }

        self.requestRender();
    };

    function initSceneAndLights() {

        scene = new THREE.Scene();
        toolsScene = new THREE.Scene();
        app.scene = scene; // for debug only

        // Add axes
        // var axes = buildAxes(1000);
        // scene.add(axes);

        selectionBox = new THREE.BoxHelper();
        selectionBox.material.depthTest = false;
        selectionBox.material.transparent = true;
        selectionBox.visible = false;
        toolsScene.add(selectionBox);

        highlightBox.material.depthTest = false;
        highlightBox.material.transparent = true;
        highlightBox.visible = false;
        toolsScene.add(highlightBox);

        var ambient = new THREE.AmbientLight(ambientColor);
        scene.add(ambient);

        var spotLight = new THREE.SpotLight(0xffffff);
        spotLight.position.set(-250, 250, 250);
        scene.add(spotLight);

        var spotLightBack = new THREE.SpotLight(0xffffff, 0.6);
        spotLightBack.position.set(250, 250, -250);
        scene.add(spotLightBack);
    }

    function isWebglAvailable() {
        try {
            var canvas = document.createElement('canvas');
            return !!(window.WebGLRenderingContext && (canvas.getContext('webgl') || canvas.getContext('experimental-webgl')));
        } catch (e) {
            return false;
        }
    }

    function initRenderer() {
        var canvas = !!window.CanvasRenderingContext2D;

        if (isWebglAvailable()) {
            renderer = new THREE.WebGLRenderer({antialias: true, preserveDrawingBuffer: true});
        } else if (canvas) {
            renderer = new THREE.CanvasRenderer();
        } else {
            renderer = new THREE.SoftwareRenderer();
        }
        //renderer = new THREE.WebGLRenderer({antialias: true});
        renderer.setClearColor(fogColor, 1); // Fog //scene.fog.color
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(htmlViewport.width(), htmlViewport.height());

        if (typeof renderer.capabilities !== 'undefined' && typeof renderer.capabilities.getMaxAnisotropy === 'function')
            imageAnisotropy = Math.min(imageAnisotropyDesired, renderer.capabilities.getMaxAnisotropy());

        htmlViewport.append(renderer.domElement);

        $(renderer.domElement).mouseout(function () {
            enableControls(false);
        });
        $(renderer.domElement).mouseover(function () {
            enableControls(true);
        });
        window.addEventListener('resize', onWindowResize, false);
        $('body').on('viewport-update', onWindowResize);

    }

    /**
     *
     * @param {"scale"|"translate"|"rotate"} mode
     */
    self.setMode = function (mode) {
        transformMode = mode;
        if (mode === 'scale') {
            if (selected) {
                transformControl.detach();
                scaleControl.attach(selected);
                disableFreeDrag();
            }
        } else {
            if (selected) {
                scaleControl.detach();
                transformControl.attach(selected);
                enableFreeDrag(selected);
            }
            transformControl.setMode(mode);
        }

        if (transformMode === 'scale' && viewportMode === '2d') {
            selectionBox.material.color.set(bbox2dScaleColor);
        } else {
            selectionBox.material.color.set(bboxDefaultColor);
        }
    };

    function onWindowResize() {
        var w = htmlViewport.width(), h = htmlViewport.height();
        perspectiveCamera.aspect = w / h;
        camera.updateProjectionMatrix();

        var orthoHeight = imagePlaneSize * orthoZoom;
        var orthoWidth = orthoHeight * (w / h);
        orthographicCamera.left = -orthoWidth / 2;
        orthographicCamera.right = orthoWidth / 2;
        orthographicCamera.top = orthoHeight / 2;
        orthographicCamera.bottom = -orthoHeight / 2;

        camera.updateProjectionMatrix();

        renderer.setSize(w, h);
        self.requestRender();
    }

    self.requestRender =function () {
        renderRequest = true;
    };

    function render() {
        renderRequest = false;

        renderer.autoClear = false;
        renderer.clear();
        renderer.render(scene, camera);

        renderer.clearDepth();
        renderer.render(toolsScene, camera);
    }

    var now;
    var then = Date.now();
    var interval = 1000/fpsCurrent;
    var delta;
    function setFps(newFps) {
        fpsCurrent = newFps;
        interval = 1000/fpsCurrent;
        animate();
    }

    function animate() {

        requestAnimationFrame(animate);

        now = Date.now();
        delta = now - then;

        if (delta > interval) {
            // update time stuffs

            /*
            Just `then = now` is not enough.
            Lets say we set fps at 10 which means
            each frame must take 100ms
            Now frame executes in 16ms (60fps) so
            the loop iterates 7 times (16*7 = 112ms) until
            delta > interval === true
            Eventually this lowers down the FPS as
            112*10 = 1120ms (NOT 1000ms).
            So we have to get rid of that extra 12ms
            by subtracting delta (112) % interval (100).
            Hope that makes sense.
            */

            then = now - (delta % interval);

            TWEEN.update();
            viewcube.update();
            if (threejsStats) {
                threejsStats.update();
            }
            if (transformControl.object) {
                transformControl.update();
            }

            var animDelta = clock.getDelta();
            if (mixers.length > 0) {
                for (var i = 0; i < mixers.length; i++) {
                    if (mixers[i].stopped)
                        continue;

                    mixers[i].update(animDelta);
                    self.requestRender();
                }
            }

            // check for currently playing videos
            for (var i = 0; i < scene.children.length; i++) {
                if (typeof scene.children[i].video !== 'undefined') {
                    if (typeof scene.children[i].video.isPaused === 'function' && !scene.children[i].video.isPaused()) {
                        self.requestRender();
                    }
                }
            }


            if (renderRequest) {
                render();
            }
        }
    }

    /**
     * pause all animation, sounds, video
     * @param doPause boolean
     */
    this.pause = function (doPause) {
        // TODO: pause/unpause sounds and video

        if (doPause) {
            //renderer.setPixelRatio(window.devicePixelRatio/4);
            setFps(fpsPaused);
        } else {
            //renderer.setPixelRatio(window.devicePixelRatio);
            setFps(fps);
        }
    };

    function enableControls(state) {
        orbitControl.enabled = state;
    }

    self.loadFromFile = function (file, callback) {
        var reader = new FileReader();
        if (typeof callback === 'function') {
            reader.onload = function (e) {
                callback(e.target.result, e.target);
            };
        }
        reader.readAsBinaryString(file);
    };

    self.loadFromFileUint8Array = function (file, callback) {
        var reader = new FileReader();
        if (typeof callback === 'function') {
            reader.onload = function (e) {
                callback(new Uint8Array(this.result), e.target);
            };
        }
        reader.readAsArrayBuffer(file);
    };

    /**
     *
     * @param {String} src
     */
    self.reloadCoverImage = function (src) {

        self.loadImage(src).then(function (object) {
            if (imagePlane) {
                scene.remove(imagePlane);
                if (coverBoundingBox) {
                    scene.remove(coverBoundingBox);
                }
            }

            imagePlane = object;
            imagePlane.position.set(0, -0.2, 0);
            scene.add(imagePlane);

            coverBoundingBox = new THREE.BoxHelper(imagePlane, 0xAAAAAA);
            scene.add(coverBoundingBox);

            self.requestRender();
        });
    };

    /**
     *
     * @param {array|input} fileInput
     * @param {boolean} addToScene
     * @param {number|string} [objectId]
     * @return JQuery.Promise
     */
    function uploadFile(fileInput, addToScene, objectId) {
        var files = fileInput;
        var url = 'library/upload-object/?editionId=' + app.editionId();

        if (!fileInput) {
            return $.Deferred().reject('no file input to upload').promise();
        }

        if (addToScene) {
            url += '&coverId=' + coverId;
        }
        if (typeof objectId !== 'undefined') {
            url += '&objectId=' + objectId;
        }

        if (fileInput && typeof fileInput.files !== 'undefined') {
            files = fileInput.files;
        }

        return uploadObjectInput.fileupload('send', {
            url: url,
            files: files
        });
    }

    self.sceneMode = function () {
        return sceneMode;
    };

    var _addToScene;
    var uploadObjectInput;
    var fileInput;

    function initUploadObject() {
        uploadObjectInput = $('#upload-object-input');

        var uploadObjectConfig = {
            url: 'library/' + app.editionId(),
            dropZone: null,
            dataType: 'json',
            done: function (e, data) {
                app.hideLoadingProgress();
            },
            progressall: function (e, data) {
                app.displayLoadingProgress(data.loaded, data.total);
            },
            fail: function (e, data) {
                app.hideLoadingProgress();
                showError('unexpected response from server:' + data.textStatus);
            }
        };

        uploadObjectInput.fileupload(uploadObjectConfig);


        fileInput = $('#file-upload-dialog-input');
        fileInput.val(null);

        fileInput.on('change', function () {
            if (!this.files.length) {
                return;
            }

            uploadFilesToLibrary(this.files);
        });


        // var replaceInput = $('#object-replace-input');
        // replaceInput.val(null);
        // replaceInput.on('change', function () {
        //     if (!this.files.length) {
        //         return;
        //     }
        // });


        /* region drag and drop upload */

        var isAdvancedUpload = function () {
            var div = document.createElement('div');
            return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div)) && 'FormData' in window && 'FileReader' in window;
        }();

        if (isAdvancedUpload) {
            $(window).on('dragover dragenter', function () {
                htmlViewport.addClass('is-dragging');
            }).on('dragleave dragend drop', function () {
                htmlViewport.removeClass('is-dragging');
            });

            var droppedFiles = false;

            htmlViewport.on('dragover dragenter', function () {
                htmlViewport.addClass('is-dragover');
            }).on('dragleave dragend drop', function () {
                $(this).removeClass('is-dragover');
            }).on('drop', function (e) {
                droppedFiles = e.originalEvent.dataTransfer.files;

                // if (html_viewport.hasClass('is-uploading')) return false;
                //
                // html_viewport.addClass('is-uploading').removeClass('is-error');

                if (droppedFiles) {
                    _addToScene = true;
                    uploadFilesToLibrary(droppedFiles);
                }

            }).addClass('advanced-upload');
        }

        /* endregion drag and drop upload */
    }

    /**
     *
     * @param inputFiles
     * @param {number|string} [objectId]
     */
    function uploadFilesToLibrary(inputFiles, objectId) {
        var videoLimit = 100 * 1024 * 1024;


        var errors = [];
        for (var i = 0; i < inputFiles.length; i++) {
            var file = inputFiles[i];
            if (file.size > videoLimit) {
                errors.push(file.name + ' - file size should be less or equal to ' + self.humanizeFilesize(videoLimit) + '. This file size is ' + self.humanizeFilesize(file.size) + '.');
            }
        }

        if (errors.length) {
            showError(errors.join('<br>'));
            return;
        }

        var libraryIds = [];
        var promise = new $.Deferred();

        var processUpload = function (files, index) {

            if (index >= files.length) {
                // all are uploaded
                promise.resolve();
                return;
            }

            uploadFile(files[index], _addToScene, objectId).then(function (result) {
                /**
                 * @var {{errors:Array.<String>, objects:Array.<SceneObject>, libraryObjects:Array.<{id:string, filename:string, type:string}>}} result
                 */

                self.hideLoadingProgress();

                if (typeof result === 'string') {
                    try {
                        result = JSON.parse(result);
                    } catch (e) {
                        // console.error("exc", e);
                        window.alert('server json error: ' + e);
                        return;
                    }
                }

                if (typeof result.errors !== 'undefined' && result.errors.length) {
                    console.error(result.errors);
                    window.alert(result.errors.join('/n'));
                }

                if (typeof result.libraryObjects !== 'undefined' && result.libraryObjects.length) {
                    for (var i = 0; i < result.libraryObjects.length; i++) {
                        libraryIds.push(result.libraryObjects[i].id);
                    }
                }

                // if (typeof result.objects !== 'undefined' && result.objects.length) {
                //     var undo_action = {name: "add", target: []};
                //     for (var i = 0; i < result.objects.length; i++) {
                //         self.import_to_scene(result.objects[i]);
                //         undo_action.target.push(result.objects[i]);
                //     }
                //
                //     self.addToUndo(undo_action);
                //
                //     self.run_convertation_checker();
                // }

                processUpload(files, index + 1);
            }, function (error) {
                console.error('upload send error!');
                console.error(error);
                self.hideLoadingProgress();
                promise.reject(error);
            }, function (event) {
                self.displayLoadingProgress(event.loaded, event.total);
            });
        };

        // start
        processUpload(inputFiles, 0);

        promise.then(function(){
            app.mediaLibrary.refreshList().then(function () {

                if (objectId) {
                    app.onMediaObjectUpdate(objectId);
                } else if (libraryIds.length) {
                    app.addLibraryObject(libraryIds);
                }
            });
        });
    }

    self.onMediaObjectUpdate = function(libraryObjectId) {
        var i, objectGroup, objectData;
        var libraryObject = app.mediaLibrary.libraryObject(libraryObjectId);
        var replaceObjects = [];

        for (i = 0; i < raytraceObjects.length; i++) {
            objectData = app.getCoverObject(raytraceObjects[i]);

            if (objectData && objectData.object_id == libraryObjectId) {
                replaceObjects.push(raytraceObjects[i]);
            }
        }

        for (i = 0; i < replaceObjects.length; i++) {
            objectGroup = replaceObjects[i];
            objectData = app.getCoverObject(objectGroup);

            // _deleteObject(objectGroup);
            var libObject = objectGroup.getObjectByName('libraryObject');
            if (!libObject) {
                libObject = objectGroup.getObjectByName('placeholder');
            }
            objectGroup.remove(libObject);

            // remove animation mixers
            libObject.traverse(function (child) {
                var childIndex = raytraceObjects.indexOf(child);

                if (childIndex > -1)
                    raytraceObjects.splice(childIndex, 1);
                if (child.mixer) {
                    mixers.splice(mixers.indexOf(child.mixer), 1);
                }
            });
            if (raytraceObjects.indexOf(libObject) > -1) {
                raytraceObjects.splice(raytraceObjects.indexOf(libObject), 1);
            }

            // stop media
            if (objectGroup.video) {
                objectGroup.video.pause();
                $(objectGroup.video.getContainer()).hide();
            }
            if (objectGroup.audio) {
                objectGroup.audio.pause();
                $(objectGroup.audio.getContainer()).hide();
            }

            // update cover object information

            if (objectData.title === objectData.filename) {
                objectData.title = libraryObject.title;
            }
            objectData.filename = libraryObject.filename;
            objectData.thumb = libraryObject.thumb;
            objectData.type = libraryObject.type;
            objectData.params = libraryObject.params;

            app.loadToScene(objectData);
            //app.importToScene(objectData);
        }

        app.runConvertationChecker();
    };

    self.loadObjectDialog = function (addToScene) {
        fileInput.val(null);

        _addToScene = addToScene;

        fileInput.trigger('click');
    };

    self.emptyScene = function () {
        for (var i = 0; i < raytraceObjects.length; i++) {
            _deleteObject(raytraceObjects[i], false);
        }
        raytraceObjects = [];

        self.objectsList.clear();
        self.requestRender();
    };

    self.importToScene = function (object) {
        setupObject(object);

        if (object.y < 0)
            object.y = 0;

        var objectGroup = new THREE.Group();
        objectGroup.userData.object = object;
        objectGroup.position.fromArray([object.x, object.y, object.z, 'XYZ']);
        objectGroup.rotation.fromArray([object.rx, object.ry, object.rz, defaultRotationOrder]);
        objectGroup.scale.fromArray([object.scale, object.scale, object.scale, 'XYZ']);

        var placeholder = new THREE.Mesh(
            new THREE.SphereBufferGeometry(20, 8, 4),
            new THREE.MeshBasicMaterial({color: 0xffff00})
        );
        placeholder.rotation.order = defaultRotationOrder;

        placeholder.name = 'placeholder';

        //object.placeholder = placeholder;
        placeholderObjects[object.id] = placeholder;

        objectGroup.add(placeholder);
        // scene.add(objectGroup);
        // raytraceObjects.push(objectGroup);
        // raytraceObjects.push(placeholder);
        app.objectsList.addObject(objectGroup);

        objectsGroups[object.id] = objectGroup;

        self.addToScene(objectGroup);

        return app.loadToScene(object);
    };

    function setupObject(obj) {
        var floatTypeAttributes = ['x', 'y', 'z', 'rx', 'ry', 'rz', 'scale'];
        for (var i = 0; i < floatTypeAttributes.length; i++) {
            var key = floatTypeAttributes[i];
            obj[key] = parseFloat(obj[key]);
            if (isNaN(obj[key])) {
                if (key === 'scale')
                    obj[key] = 1;
                else
                    obj[key] = 0;
            }
        }
    }

    var convertationArray = [];
    var convertationCheckerInterval;
    /**
     * @param {number=} _coverId
     */
    self.switchToCover = function (_coverId) {
        // if (scene_mode === 'cover' && unsaved_changes && !confirm("You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?")) {
        // 	return;
        // }

        sceneMode = 'cover';
        libraryObjectId = 0;
        newIndex = 0;
        if (!_coverId) {
            _coverId = parseInt(document.location.hash.slice(1));
            if (isNaN(_coverId)) {
                coverId = 0;
                if (covers.length) {
                    _coverId = covers[0].id;
                }
            }
        }

        if (!_coverId)
            return;

        coverId = _coverId;
        self.coversList.setActive(coverId);

        document.location.hash = coverId;

        convertationArray = [];
        app.stopConvertationChecker();
        app.emptyScene();

        app.mediaLibrary.resetActive();

        app.toolsPanel.enable('upload');
        app.toolsPanel.hide('screenshot');
        // app.toolsPanel.disable('save'); // reset it old state
        app.toolsPanel.show('save');
        app.toolsPanel.show('snapshot');

        for (var i = 0; i < covers.length; i++) {
            if (covers[i].id == coverId) {
                app.loadScene(covers[i]);
                break;
            }
        }

        app.objectsList.checkLayersObjects();
    };

    /**
     *
     * @param {CoverDefinition} cover
     */
    self.loadScene = function (cover) {

        app.reloadCoverImage(self.coversPath() + cover.filename);
        for (var i = 0; i < cover.objects.length; i++) {
            if (cover.objects[i].deleted != 1) {
                app.importToScene(cover.objects[i]);
            }
        }
        app.runConvertationChecker();
    };

    self.switchToObject = function (libraryObject) {
        if (sceneMode === 'cover' && unsavedChanges) {
            modalConfirm('You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?', {
                onConfirm: function() {
                    _switchToObject(libraryObject);
                }
            });
        } else {
            _switchToObject(libraryObject);
        }
    };

    function _switchToObject (libraryObject) {
        sceneMode = 'object';
        libraryObjectId = libraryObject.id;
        document.location.hash = '';

        convertationArray = [];
        self.stopConvertationChecker();
        self.emptyScene();
        scene.remove(imagePlane);

        app.toolsPanel.disable('upload');
        app.toolsPanel.show('screenshot');
        app.toolsPanel.hide('save');
        app.toolsPanel.hide('snapshot');

        var object = $.extend({}, libraryObject);
        self.importToScene(object);
    };

    self.makeLibraryThumbnail = function () {

        var currentObject = selected;
        self.onSelect(null);
        render();
        var thumbData = renderer.domElement.toDataURL();
        self.onSelect(currentObject);

        var filename = 'thumb.' + libraryObjectId + '_' + (new Date()).getTime();

        app.mediaLibrary.cropAndSave(libraryObjectId, filename, thumbData);
    };

    self.getBundleDialog = function () {
        $.ajax({url: '/index/bundle'}).then(function (result) {
            if (result.errors && result.errors.length) {
                showError(result.errors.join('<br>'));
                return;
            } else if (typeof result !== 'object') {
                showError(result);
                return;
            }

            //showMessage("Bundle generated. Press <a href='" +result.bundle+ "'>here</a> to download.");
            document.location = result.bundle;
        });
    };

    self.makeCoverMain = function(coverId) {
        for (var i=0;i<covers.length;i++) {
            covers[i].main = covers[i].id == coverId? '1' : '0';
        }

        self.coversList.setCoverMain(coverId);
    };

    self.stopConvertationChecker = function () {
        window.clearInterval(convertationCheckerInterval);
        convertationCheckerInterval = 0;
    };

    self.runConvertationChecker = function () {

        if (convertationArray.length && !convertationCheckerInterval) {
            convertationCheckerInterval = window.setInterval(function () {

                if (!convertationArray.length) {
                    self.stopConvertationChecker();
                }

                $.ajax({url: '/library/checkConvertation', data: {objects: convertationArray}}).then(function (data) {
                    // TODO: add error handling here! if data is empty or else
                    var finishedIds = Object.keys(data.finished);

                    var id, i, j;
                    var fontSize = 65;
                    var lineHeight = fontSize * 1.3;

                    for (id in data.statuses) {
                        if (!data.statuses.hasOwnProperty(id))
                            continue;

                        if (data.statuses[id] !== 'error')
                            continue;

                        for (j = 0; j < raytraceObjects.length; j++) {
                            if (!raytraceObjects[j].userData.object)
                                continue;

                            if (raytraceObjects[j].userData.object.object_id == id) {
                                raytraceObjects[j].children[0].material.map = self.textTexture({
                                    color: '#ffffff',
                                    backgroundColor: '#ff4615',
                                    fontSize: fontSize,
                                    lineHeight: lineHeight,
                                    width: 1024,
                                    height: 512,
                                    // fit: true,
                                    text: 'Failed to download\n[ ' + raytraceObjects[j].userData.object.filename + ' ]'
                                });
                            }
                        }
                    }


                    if (!finishedIds.length) {
                        // nothing to update
                        return;
                    }

                    // cleanup convertation_array
                    for (i = 0; i < finishedIds.length; i++) {
                        id = finishedIds[i];
                        var convertationIndex = convertationArray.indexOf(id);
                        if (convertationIndex !== -1)
                            convertationArray.splice(convertationIndex, 1);
                    }

                    app.mediaLibrary.refreshList().then(function(){
                        for (var libraryId in data.finished) {
                            if (!data.finished.hasOwnProperty(libraryId))
                                continue;

                            app.onMediaObjectUpdate(libraryId);
                        }

                        app.requestRender();
                    });
                });
            }, 10000);
        }
    };

    /**
     *
     * @param {string|string[]|number|number[]} libraryIds
     */
    self.addLibraryObject = function (libraryIds) {
        if (typeof libraryIds === 'number' || typeof libraryIds === 'string') {
            libraryIds = [libraryIds];
        }

        var newObjects = [];
        for (var i = 0; i < libraryIds.length; i++) {
            var libraryId = libraryIds[i];
            var libraryObject = app.mediaLibrary.libraryObject(libraryId);
            if (!libraryObject) {
                console.error('Failed to find library object by id', libraryId);
                continue;
            }

            /**
             *
             * @type {ObjectDefinition}
             */
            var obj = {};
            obj.id = 'new' + newIndex;
            newIndex++;
            obj.cover_id = coverId;
            obj.object_id = libraryId;
            obj.x = 0;
            obj.y = 0;
            obj.z = 0;
            obj.rx = 0;
            obj.ry = 0;
            obj.rz = 0;
            obj.scale = 1;

            obj.title = libraryObject.title;
            obj.filename = libraryObject.filename;
            obj.thumb = libraryObject.thumb;
            obj.type = libraryObject.type;
            obj.deleted = 0;
            obj.options = {};
            obj.params = libraryObject.params;

            newObjects.push(obj);
            getCover(coverId).objects.push(obj);
            app.importToScene(obj);
        }

        if (newObjects.length) {
            var undoAction = {name: 'add', target: newObjects};
            self.addToUndo(undoAction);
        }
    };

    /**
     *
     * @param {ObjectDefinition} serverObject
     */
    self.loadToScene = function (serverObject) {

        /**
         *
         * @param {Object3D} loadedObject
         */
        var onLoaded = function (loadedObject) {
            self.hideLoadingProgress();
            var objectGroup = objectsGroups[serverObject.id];

            // clean placeholder
            self.removePlaceholder(serverObject.id);

            loadedObject.name = 'libraryObject';

            // add loaded object to group
            objectGroup.add(loadedObject);

            // update info
            self.rescanAnimations(objectGroup);
            self.addChildrenToRaytrace(objectGroup);

            delete objectGroup.userData.placeholder;
            delete objectGroup.video;
            delete objectGroup.audio;

            // adopt video/audio links
            if (loadedObject.userData.placeholder) {
                objectGroup.userData.placeholder = loadedObject.userData.placeholder;
            }
            if (loadedObject.video) {
                objectGroup.video = loadedObject.video;
            }
            if (loadedObject.audio) {
                objectGroup.audio = loadedObject.video;
            }

            // select loaded
            self.onSelect(objectGroup);

            if (serverObject && (serverObject.type.match(/^video\//) || serverObject.type.match(/^object\//)) && !serverObject.thumb) {
                app.mediaLibrary.renderThumbnail(serverObject.object_id, serverObject).then(function(filename){
                    serverObject.thumb = filename;

                    app.objectsList.updateThumbnail(serverObject.id);

                    if (serverObject.type.match(/^video\//) && loadedObject.userData.placeholder && loadedObject.userData.canPlay) {
                        // update placeholder object

                        new THREE.TextureLoader().load(app.thumbsPath() + serverObject.thumb, function(texture){
                            var frontPlaneMaterial = new THREE.MeshBasicMaterial({
                                // color: 0x0000FF,
                                map: texture,
                                side: THREE.FrontSide
                            });
                            var frontPlane = objectGroup.getObjectByName('frontPlane');
                            frontPlane.material = frontPlaneMaterial;

                            objectGroup.userData.thumbnail = true;
                            objectGroup.userData.placeholder = false;
                            objectGroup.video.poster(app.thumbsPath() + serverObject.thumb);

                            app.requestRender();
                        });
                    }
                });
            }
            app.requestRender();
        };
        var onError = function (event) {
            self.hideLoadingProgress();
            var placeholder = placeholderObjects[serverObject.id];

            if (placeholder) {
                placeholderObjects[serverObject.id].material.color = new THREE.Color(1, 0, 0);
                app.requestRender();
            }

            console.error('error', event);
        };
        var onProgress = function (event) {
            var placeholder = placeholderObjects[serverObject.id];

            if (placeholder) {
                var coeff = event.loaded / event.total;
                // lerp red component from 1 to 0, making color change from yellow to green
                placeholder.material.color = new THREE.Color(1 - coeff, 1, 0);
                app.requestRender();
            }

            self.displayLoadingProgress(event.loaded, event.total);


        };

        return self.loadFile(serverObject).then(onLoaded, onError, onProgress);
    };

    self.rescanAnimations = function(objectGroup) {
        objectGroup.traverse(function (child) {
            self.checkAndRegisterAnimMixers(child, objectGroup);
        });
    };

    self.removePlaceholder = function(objectId) {
        var placeholder = placeholderObjects[objectId];

        if (!placeholder) {
            return;
        }

        placeholder.parent.remove(placeholder);
        var placeholderIndex = raytraceObjects.indexOf(placeholder);
        if (placeholderIndex > -1)
            raytraceObjects.splice(placeholderIndex, 1);
        delete placeholderObjects[objectId];
    };

    self.addChildrenToRaytrace = function (objectGroup) {
        objectGroup.traverse(function (child) {
            if (raytraceObjects.indexOf(child) === -1)
                raytraceObjects.push(child);
        });
    };

    self.addToScene = function (objectGroup) {
        var obj = self.getCoverObject(objectGroup);

        self.rescanAnimations(objectGroup);

        self.addChildrenToRaytrace(objectGroup);

        // clean placeholder
        // self.removePlaceholder(obj.id);

        // adjust/restrict y to 1
        if (objectGroup.position.y < 0)
            objectGroup.position.y = 0;

        // add object to scene
        scene.add(objectGroup);

        self.objectsList.updateObject(objectGroup);

        showActionIcon(objectGroup);
        updateActionIcon(objectGroup);
        updateActionIconSize(objectGroup);

        if (obj.type !== 'video/mp4' && obj.type.match(/^video\//)) {
            convertationArray.push(obj.object_id);
            self.runConvertationChecker();
        }
    };

    function showActionIcon(object) {
        if (!(object && object.userData && object.userData.object && object.userData.object.options))
            return;

        var serverObject = object.userData.object;

        if (serverObject.options.action && serverObject.options.action.type) {

            if (!object.userData.actionIcon) {
                var iconPivot = new THREE.Group();
                iconPivot.name = 'action icon';
                var iconPlane = new THREE.Mesh(new THREE.PlaneGeometry(iconPlaneSize, iconPlaneSize), actionIconMaterial.link);
                iconPlane.rotateX(-Math.PI / 2);
                iconPlane.position.set(-iconPlaneSize / 2, 0, -iconPlaneSize / 2);
                iconPivot.add(iconPlane);
                object.userData.actionIcon = iconPivot;
                iconPivot.userData.icon = iconPlane;
            }

            var type = serverObject.options.action.type;
            if (actionIconMaterial[type]) {
                toolsScene.add(object.userData.actionIcon);
                object.userData.actionIcon.visible = true;

                updateActionIcon(object);
            }
            else
                console.error('no material for action ', type, actionIconMaterial);

            app.requestRender();
        }
    }

    function hideActionIcon(object) {
        if (object && object.userData) {
            toolsScene.remove(object.userData.actionIcon);
            app.requestRender();
        }
    }

    function updateActionIconSize(object) {
        if (object && object.userData.actionIcon) {
            var scale = Math.min(100 / zoom, 4);
            for (var i = 0; i < raytraceObjects.length; i++) {
                if (raytraceObjects[i].userData.actionIcon) {
                    raytraceObjects[i].userData.actionIcon.scale.set(scale, scale, scale);
                }
            }
            app.requestRender();
        }
    }

    function updateActionIcon(object) {
        if (object.userData.actionIcon) {
            var type = object.userData.object.options.action.type;
            if (actionIconMaterial[type]) {
                object.userData.actionIcon.userData.icon.material = actionIconMaterial[type];
            }

            var pos = new THREE.Vector3();
            var bbox = new THREE.Box3();
            bbox.setFromObject(object);
            pos.x = bbox.max.x;
            pos.y = bbox.max.y;
            pos.z = bbox.max.z;
            object.userData.actionIcon.position.copy(pos);

            app.requestRender();
        }
    }

    /**
     *
     * @param object
     * @param objectGroup
     */
    self.checkAndRegisterAnimMixers = function (object, objectGroup) {
        var anims;
        if (typeof object.animations !== 'undefined') {
            anims = object.animations;
        }
        else if (typeof object.geometry !== 'undefined') {

            if (typeof object.geometry.animations !== 'undefined') {
                anims = object.geometry.animations;
            } else if (typeof object.geometry.morphAnimations !== 'undefined') {
                anims = object.geometry.morphAnimations;
            }
        }

        if (!anims)
            return;

        object.mixer = new THREE.AnimationMixer(object);
        mixers.push(object.mixer);

        var coverObject = app.getCoverObject(objectGroup);

        if (coverObject.options && typeof coverObject.animation !== 'undefined') {
            objectGroup.currentAnimation = coverObject.options.animation;
        } else {
            objectGroup.currentAnimation = -1;
            for (var i = 0; i < anims.length; i++) {
                if (anims[i].name === 'idle') {
                    objectGroup.currentAnimation = i;
                    break;
                }
            }
        }

        if (objectGroup.currentAnimation > -1) {
            objectGroup.currentAnimation = anims[objectGroup.currentAnimation].name;
            object.mixer.clipAction(objectGroup.currentAnimation).setEffectiveWeight(1).play();
        }
    };

    self.loadFile = function (object, options) {
        var _options = $.extend({}, options);
        var src = self.objectsPath() + object.filename;
        var ext = object.filename.split('.').pop().toLowerCase();
        var promise;

        if (ext === 'obj') {
            promise = self.loadObj(src);
        }
        else if (ext === 'stl') {
            promise = self.loadStl(src);
        }
        else if (ext === 'fbx') {
            promise = self.loadFbx(src);
        }
        else if (ext === 'json') {
            promise = self.loadJson(src);
        }
        else if (object.type.match(/^image\//)) {
            promise = self.loadImage(src, object);
        }
        else if (object.type.match(/^video\//)) {
            if (_options && _options.fakeVideo && object.thumb) {
                promise = self.loadImage(app.thumbsPath() + object.thumb, object);
            } else {
                promise = self.loadVideo(src, object.type, object, _options);
            }
        }
        else if (object.type.match(/^audio\//)) {
            promise = self.loadAudio(src, object.type, object, _options);
        }
        else {
            var process = new $.Deferred();
            console.error('Unsupported file format', ext);
            process.reject('Unsupported file format');
            return process.promise();
        }
        return promise;
    };

    self.loadFbx = function (src) {
        var process = new $.Deferred();
        var loader = new THREE.FBXLoader(loadManager);
        //var material = new THREE.MeshLambertMaterial( { skinning: true, color: Math.random() * 0xffffff } );

        loader.load(src, function (object) {
            // assign random material
            // object.traverse(function (child) {
            // 	if (child instanceof THREE.Mesh) {
            // 		child.material = material;
            // 	}
            // });

            object.traverse(function (child) {
            	if (child.type === 'Line') {
            		child.visible = false;
            	}
            });

            process.resolve(object);
        }, function (xhr) {
            process.notify(xhr);
        }, function (xhr) {
            process.reject(xhr);
        });
        return process.promise();
    };

    self.loadStl = function (src) {
        var process = new $.Deferred();
        var loader = new THREE.STLLoader(loadManager);
        var material;// = new THREE.MeshLambertMaterial({color: Math.random() * 0xffffff});

        loader.load(src, function (geometry) {
            // BufferGeometry

            // detect material
            if (geometry.hasColors) {
                material = new THREE.MeshPhongMaterial({opacity: geometry.alpha, vertexColors: THREE.VertexColors});
            } else {
                material = new THREE.MeshLambertMaterial({color: Math.min(Math.random(), 0.1) * 0xffffff});
            }
            var object = new THREE.Mesh(geometry, material);

            //var object = new THREE.Mesh(geometry, material);
            process.resolve(object);
        }, function (xhr) {
            process.notify(xhr);
        }, function (xhr) {
            process.reject(xhr);
        });

        return process.promise();
    };

    self.loadObj = function (src) {
        var process = new $.Deferred();
        var objLoader = new THREE.OBJLoader(loadManager);
        var filepathParts = src.split('/');
        var filename = filepathParts.pop();
        var filepath = filepathParts.join('/') + '/';
        var mtlFilename = filename.replace(/(.obj)$/, '.mtl');

        objLoader.setPath( filepath );

        var loadObject = function(){
            objLoader.load(filename, function (object) {
                process.resolve(object);
            }, function (xhr) {
                process.notify(xhr);
            }, function (xhr) {
                process.reject(xhr);
            });
        };

        var mtlLoader = new THREE.MTLLoader();
        mtlLoader.setPath( filepath );
        mtlLoader.load( mtlFilename, function( materials ) {
            materials.preload();
            objLoader.setMaterials( materials );
            loadObject();
        }, function(){
        }, function () {
            // console.log('mtl loading error?');
            // load anyway
            loadObject();
        });

        return process.promise();
    };

    self.loadJson = function (src) {
        var process = new $.Deferred();
        var loader = new THREE.JSONLoader(loadManager);

        loader.load(src, function (geometry, materials) {
            materials.forEach(function (material) {

                material.skinning = true;

            });

            var object = new THREE.SkinnedMesh(
                geometry,
                materials
            );

            process.resolve(object);
        }, function (xhr) {
            process.notify(xhr);
        }, function (xhr) {
            process.reject(xhr);
        });

        return process.promise();
    };

    self.loadImage = function (src) {
        var process = new $.Deferred();

        var image = new Image();
        var texture = new THREE.Texture();
        // Connect the image to the Texture
        image.onload = function () {
            var object = new THREE.Group();
            object.image = image;

            var polygonOffset = Math.random() * 0.5;

            var ext = src.split('.').pop();
            var transparent = ext === 'png';

            texture.image = image;
            texture.needsUpdate = true;
            texture.anisotropy = imageAnisotropy;
            // texture.minFilter = THREE.LinearFilter;
            // texture.magFilter = THREE.LinearFilter;
            var frontPlaneMat = new THREE.MeshBasicMaterial({
                color: 0xffffff,
                map: texture,
                // side: THREE.FrontSide,
                transparent: transparent,
                polygonOffset: true,
                polygonOffsetFactor: polygonOffset
            });

            // var plane_width = image.width*px2mm;
            // var plane_height = image.height*px2mm;
            var aspect = image.width / imagePlaneSize;
            var planeWidth = imagePlaneSize;
            var planeHeight = image.height / aspect;

            var frontPlane = new THREE.Mesh(new THREE.PlaneGeometry(planeWidth, planeHeight), frontPlaneMat);
            var rot = -Math.PI / 2;
            frontPlane.rotateX(rot);
            //image_plane.overdraw = true;
            object.add(frontPlane);

            // back
            var backPlaneMat;
            if (transparent) {
                var backTexture = new THREE.Texture();
                backTexture.image = image;
                backTexture.flipY = true;
                backTexture.needsUpdate = true;

                // uniforms
                var uniforms = {
                    color: {type: 'c', value: new THREE.Color(0x888888)},
                    texture: {type: 't', value: backTexture}
                };

                // material
                backPlaneMat = new THREE.ShaderMaterial({
                    side: THREE.BackSide,
                    transparent: true,
                    polygonOffset: true,
                    polygonOffsetFactor: polygonOffset,

                    uniforms: uniforms,
                    vertexShader: document.getElementById('vertex_shader').textContent,
                    fragmentShader: document.getElementById('fragment_shader').textContent
                });
            } else {
                backPlaneMat = new THREE.MeshBasicMaterial({
                    color: 0x888888,
                    side: THREE.BackSide,
                    polygonOffset: true,
                    polygonOffsetFactor: polygonOffset
                });
            }

            var backPlane = new THREE.Mesh(new THREE.PlaneGeometry(planeWidth, planeHeight), backPlaneMat);
            backPlane.rotateX(rot);
            object.add(backPlane);

            //object.position.setY(5);

            process.resolve(object);
        };
        image.src = src;

        return process.promise();
    };

    /**
     * @typedef {Object} textTextureOptions
     * @property {String} text
     * @property {String} [color]
     * @property {String} [backgroundColor]
     * @property {number} [width]
     * @property {number} [height]
     * @property {String} [fontFamily]
     * @property {number} [fontSize]
     * @property {number} [lineHeight]
     * @property {boolean} [fit]
     * @property {number} [magFilter] How the texture is sampled when a texel covers more than one pixel. The default is THREE.LinearFilter
     * @property {number} [minFilter] How the texture is sampled when a texel covers less than one pixel. The default is THREE.LinearMipMapLinearFilter
     */
    /**
     * @param {textTextureOptions} options
     * @return {THREE.Texture}
     */
    self.textTexture = function (options) { // text, font_size, width, height) {
        var defaults = {
            color: '#000000',
            width: 128,
            height: 128,
            // backgroundColor:
            fontFamily: 'Arial',
            fontSize: 16,
            lineHeight: 20
        };
        var i;
        var _options = $.extend({}, defaults, options);
        var texts = _options.text.split('\n');
        // 128
        var canvas = document.createElement('canvas');
        canvas.width = _options.width;
        canvas.height = _options.height;
        var context = canvas.getContext('2d');

        if (_options.backgroundColor) {
            context.fillStyle = _options.backgroundColor;
            context.fillRect(0, 0, canvas.width, canvas.height);
        }

        context.save();
        context.translate(canvas.width / 2, canvas.height / 2 - (texts.length * _options.lineHeight) / 2 + _options.lineHeight / 2);

        context.font = _options.fontSize + 'px ' + _options.fontFamily;
        if (_options.fit) {
            // not sure if all this is correct
            var size = _options.fontSize;
            var checkText = texts[0];
            var maxWidth = context.measureText(texts[0]).width;
            for (i = 1; i < texts.length; i++) {
                var tmpWidth = context.measureText(texts[i]).width;
                if (tmpWidth > maxWidth) {
                    maxWidth = tmpWidth;
                    checkText = texts[i];
                }
            }

            while (context.measureText(checkText).width > canvas.width) {
                size -= 0.5;
                //var textWidth = context.measureText(_options.text).width;
                context.font = size + 'px ' + _options.fontFamily;
            }
        }
        context = canvas.getContext('2d');
        context.textAlign = 'center';
        context.textBaseline = 'middle';
        context.fillStyle = _options.color;

        for (i = 0; i < texts.length; i++) {
            context.fillText(texts[i], 0, i * _options.lineHeight);
        }
        context.restore();

        //$(canvas).appendTo("body").css({position:'absolute', top: 10, left: 10, width: "auto", height: "auto"});


        var texture = new THREE.Texture(canvas);

        if (_options.magFilter) {
            texture.magFilter = _options.magFilter;
        }
        if (_options.minFilter) {
            texture.minFilter = _options.minFilter;
        }

        texture.needsUpdate = true;

        return texture;
    };

    self.loadVideo = function (src, type, sceneObject, options) {
        var process = new $.Deferred();

        var video, canPlay;

        if (type === 'link') {
            canPlay = false;
        } else {
            // create the video element
            video = document.createElement('video');
            canPlay = video.canPlayType(type) !== '';
        }

        var rot = -Math.PI / 2;
        //var elevation = 5;

        var object = new THREE.Group();
        var aspect = 3 / 4;
        var width = imagePlaneSize;
        var height = width * aspect;
        if (sceneObject.params && sceneObject.params.width) {
            width = sceneObject.params.width;
            height = sceneObject.params.height;

            // scale to cover width
            aspect = width / imagePlaneSize;
            width = imagePlaneSize;
            height = height / aspect;
        }

        var fontSize = 65;
        var lineHeight = fontSize * 1.3;
        var filename = src.split('/').pop();
        var frontPlaneMaterial;
        object.userData.placeholder = false;
        object.userData.canPlay = canPlay;

        if (sceneObject.thumb && canPlay) {
            object.userData.thumbnail = true;
            frontPlaneMaterial = new THREE.MeshBasicMaterial({
                // color: 0x0000FF,
                map: new THREE.TextureLoader().load(app.thumbsPath() + sceneObject.thumb),
                side: THREE.FrontSide
            });
        } else {
            var placeholderText = (type === 'link' ? 'Video link' : 'Loading...');

            if (!canPlay) {
                placeholderText = '[ ' + filename + ' ]\nVideo is being converted\nand will be available here shortly';
            }

            frontPlaneMaterial = new THREE.MeshBasicMaterial({
                // color: 0x0000FF,
                map: self.textTexture({
                    color: '#ffffff',
                    backgroundColor: '#ddd',
                    fontSize: fontSize,
                    lineHeight: lineHeight,
                    width: 1024,
                    height: 512,
                    // fit: true,
                    text: placeholderText
                }),
                side: THREE.FrontSide
            });

            object.userData.placeholder = true;
        }
        var frontPlane = new THREE.Mesh(new THREE.PlaneGeometry(width, height), frontPlaneMaterial);
        frontPlane.name = 'frontPlane';
        var backPlaneMaterial = new THREE.MeshBasicMaterial({
            color: 0x888888,
            side: THREE.BackSide
        });
        var backPlane = new THREE.Mesh(new THREE.PlaneGeometry(width, height), backPlaneMaterial);
        frontPlane.rotateX(rot);
        backPlane.rotateX(rot);
        object.add(frontPlane);
        object.add(backPlane);

        // object.position.setY(5);

        process.resolve(object);

        if (canPlay) {
            mediaPanel.video.find('.settings-video >div').first().append(video);

            // video.id = 'video';
            // video.type = ' video/ogg; codecs="theora, vorbis" ';

            $(video).prop('controls', true);
            $(video).prop('loop', true);

            var players = plyr.setup([video], {storage: false});
            object.video = players[0];

            var metadataPromise = new $.Deferred();
            var playPromise = new $.Deferred();

            object.video.on('loadedmetadata', function (e) {
                var media = e.detail.plyr.getMedia();
                var width = media.videoWidth;
                var height = media.videoHeight;

                if (!width && !height) {
                    console.warn('empty video metadata loaded? [' + media.src + ']');
                    return;
                }

                metadataPromise.resolve(e);
            });

            object.video.on('playing', function (e) {
                playPromise.resolve(e);
            });
            object.video.on('seeked', function (e) {
                playPromise.resolve(e);
            });

            $.when(metadataPromise, playPromise).done(function(metaEvent, playEvent){
                var media = metaEvent.detail.plyr.getMedia();
                
                // clear placeholder
                raytraceObjects.splice(raytraceObjects.indexOf(frontPlane), 1);
                raytraceObjects.splice(raytraceObjects.indexOf(backPlane), 1);
                object.remove(frontPlane);
                object.remove(backPlane);

                // setup real
                var texture = new THREE.VideoTexture(media);
                texture.minFilter = THREE.LinearFilter;
                texture.magFilter = THREE.LinearFilter;
                texture.format = THREE.RGBFormat;

                //			texture.image = image;
                texture.needsUpdate = true;
                var frontPlaneMaterial = new THREE.MeshBasicMaterial({
                    color: 0xffffff,
                    map: texture,
                    side: THREE.FrontSide
                });
                var aspect = width / imagePlaneSize;
                frontPlane = new THREE.Mesh(new THREE.PlaneGeometry(imagePlaneSize, height / aspect), frontPlaneMaterial);
                frontPlane.name = 'frontPlane';
                frontPlane.rotateX(rot);
                object.add(frontPlane);

                // back
                var backPlaneMaterial = new THREE.MeshBasicMaterial({
                    color: 0x888888,
                    side: THREE.BackSide
                });
                backPlane = new THREE.Mesh(new THREE.PlaneGeometry(imagePlaneSize, height / aspect), backPlaneMaterial);
                backPlane.rotateX(rot);
                object.add(backPlane);

                raytraceObjects.push(frontPlane);
                raytraceObjects.push(backPlane);

                object.userData.placeholder = false;
            });

            var playerSourceConfig = {
                type: 'video',
                title: '',
                sources: [{
                    src: src,
                    type: type
                }],
                poster: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAC0lEQVQYV2NgAAIAAAUAAarVyFEAAAAASUVORK5CYII=' // empty image
            };
            if (sceneObject.thumb) {
                playerSourceConfig.poster = app.thumbsPath() + sceneObject.thumb;
            }

            object.video.source(playerSourceConfig);

            // setting volume must be after source change
            if (sceneObject.options && sceneObject.options.volume) {
                object.video.setVolume(sceneObject.options.volume * 10);
            } else {
                object.video.setVolume(10);
            }

            object.video.on('volumechange', function () {
                if (!sceneObject.options)
                    sceneObject.options = {};
                sceneObject.options.volume = object.video.getVolume();
                app.setUnsavedState(true);
            });

            // // autostart video
            // if (!_options.plainLoad) {
            //     object.video.play();
            // }

            if (!selected || selected !== object) {
                $(object.video.getContainer()).hide();
            }
        }

        return process.promise();
    };

    var audioObjectMaterial;

    self.loadAudio = function (src, type, sceneObject, options) {
        var process = new $.Deferred();
        var _options = $.extend({}, options);
        // create the video element
        var audio = document.createElement('audio');
        mediaPanel.audio.find('.settings-audio >div').first().append(audio);

        $(audio).prop('controls', true);
        $(audio).prop('loop', true);

        var object = new THREE.Group();

        var geometry = new THREE.SphereGeometry(iconSize / 2);
        var material = new THREE.MeshBasicMaterial({visible: false});
        var sphere = new THREE.Mesh(geometry, material);
        object.add(sphere);

        if (!audioObjectMaterial) {
            var textureLoader = new THREE.TextureLoader();
            var texture = textureLoader.load('img/audio.png');
            audioObjectMaterial = new THREE.SpriteMaterial({map: texture, color: 0xffffff, fog: true});
        }
        var sprite = new THREE.Sprite(audioObjectMaterial);
        sprite.scale.set(iconSize, iconSize, 1);
        object.add(sprite);

        var players = plyr.setup(audio);
        object.audio = players[0];

        object.audio.on('volumechange', function (e, x) {
            if (!sceneObject.options)
                sceneObject.options = {};
            sceneObject.options.volume = object.audio.getVolume();
            app.setUnsavedState(true);
        });

        object.audio.source({
            type: 'audio',
            title: '',
            sources: [{
                src: src,
                type: type
            }]
        });

        if (sceneObject.options && sceneObject.options.volume) {
            object.audio.setVolume(sceneObject.options.volume * 10);
        }

        if (!_options.plainLoad) {
            object.audio.play();
        }

        process.resolve(object);
        return process.promise();
    };

    function onUpdateObjectState(object) {
        var titleChanged = object.userData.titleChanged;
        var data = getObjectSaveData(object);

        // update raw data
        for (var key in data) {
            if (data.hasOwnProperty(key))
                object.userData.object[key] = data[key];
        }

        if (titleChanged)
            app.objectsList.updateTitle(object.userData.object.id, object.userData.object.title);

        app.setUnsavedState(true);


        // $.ajax({url:"/objects", data:data, type:"POST"}).then(function(){
        // 	if (titleChanged) {
        // 		app.objectsList.update_title(object.userData.id, object.userData.object.title);
        // 	}
        // });
    }

    self.setUnsavedState = function (unsaved) {
        unsavedChanges = unsaved;

        if (sceneMode === 'cover') {
            // update button
            if (unsavedChanges)
                self.toolsPanel.enable('save');
            else
                self.toolsPanel.disable('save');
        }
    };

    self.saveEdition = function () {
        if (sceneMode !== 'cover')
            return;

        var process = new $.Deferred();
        var data = {
            covers: JSON.stringify(covers),
            editionId: app.editionId()
        };

        $.ajax({
            url: '/covers/save',
            data: data,
            type: 'POST'
        }).then(function (result) {
            /** @var {{errors:Array.<String>, newObjects:Array.<{coverId: number, tmpId: string, newId: number}>}} result */

            if (typeof result === 'string') {
                showError(result);
                process.reject(result);
                return;
            }

            if (result.errors && result.errors.length) {
                showError(result.errors.join('<br>'));
                process.reject(result);
                return;
            }

            // set server id's to new objects
            if (result.newObjects && result.newObjects.length) {
                result.newObjects.map(function(replacePair) {
                    var coverId = replacePair.coverId;
                    var tmpId = replacePair.tmpId;
                    var newId = replacePair.newId;

                    var cover = getCover(coverId);
                    if (!cover) {
                        console.error('failed to get updated cover', coverId);
                        return;
                    }

                    cover.objects.map(function(object){
                        if (object.id !== tmpId)
                            return;

                        object.id = newId;
                        app.objectsList.replaceId(tmpId, newId);
                    });

                });

                console.log("newObjs", result.newObjects);
            }

            for (var i=0; i<covers.length;i++) {
                if (covers[i].status === null || covers[i].status === COVER_STATUS_TMP) {
                    covers[i].status = COVER_STATUS_NORMAL;
                }
            }

            unsavedChanges = false;
            showMessage('Edition saved', 'success', 3000);
            self.toolsPanel.disable('save');

            process.resolve(result);

        }, function (error) {
            console.error('save failed');
            console.error(error);

            var errorMessage = 'Save error.';
            if (error.statusText) {
                errorMessage += ' ' + error.statusText;
            }
            showMessage(errorMessage, 'error', 3000);

            process.reject(error);
        });

        return process.promise();
    };

    function getObjectSaveData(object) {
        var obj = {};

        obj.id = object.userData.object.id;
        if (object.userData.titleChanged) {
            obj.title = object.userData.object.title;
            obj.titleChanged = true;
        }
        obj.x = object.position.x;
        obj.y = object.position.y;
        obj.z = object.position.z;

        obj.rx = object.rotation.x;
        obj.ry = object.rotation.y;
        obj.rz = object.rotation.z;

        obj.scale = object.scale.x;

        return obj;
    }

    function initEditPanel() {
        editPanel = $('#edit-panel');
        var editContents = editPanel.find('.panel-content').children('div');
        editPanel.find('.head').children('div').on('click', function () {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var targetId = $(this).data('target');
            var target = $('#' + targetId);
            editContents.not(target).removeClass('active');
            target.addClass('active');
        });
    }

    /**
     *
     * @type {{px:*,py:*,pz:*,rx:*,ry:*,rz:*,sx:*,title:*}}
     */
    var transformInputs = {};

    function initTransformTab() {
        transformPanel = $('#transform-panel');
        transformPanel.find('input').each(function () {
            var name = $(this).attr('name');
            transformInputs[name] = $(this);
        });
        transformInputs.title = $('#settings-main-title-input');

        transformPanelBindInputUpdate();
        transformPanelBindSaveObject();

        transformPanel.find('form').on('submit', function (e) {
            e.preventDefault();
        });
        transformPanel.find('input').on('change', transformPanelOnInputChange);
        transformInputs.title.on('change', transformPanelOnInputChange);

        $('#edit-panel').drags({handle: '.head', margins: 10});
    }

    function transformPanelBindInputUpdate() {
        // update transform panel after 50ms
        var updateTimer;
        var updateInputs = function (event) {
            if (!selected) {
                return;
            }

            (function (obj) {
                if (updateTimer) {
                    clearTimeout(updateTimer);
                }
                updateTimer = setTimeout(function () {
                    updateTransformPanel(obj);
                }, transformPanelUpdateDelay);
            })(selected);
        };

        transformControl.addEventListener('objectChange', updateInputs);
        scaleControl.addEventListener('objectChange', updateInputs);
        scaleControl.addEventListener('mouseUp', updateInputs);
    }

    function transformPanelBindSaveObject() {
        var objectChanged = false;
        var initialState;
        var onBeforeTransform = function () {
            objectChanged = false;
            initialState = {};
            initialState.position = selected.position.toArray();
            initialState.rotation = selected.rotation.toArray();
            initialState.scale = selected.scale.toArray();
        };
        var onAfterTransform = function () {
            if (objectChanged) {
                onUpdateObjectState(selected);

                var action = {
                    name: 'transform',
                    target: selected,
                    value: initialState
                };
                self.addToUndo(action);
            }
        };
        var onTransform = function () {
            objectChanged = true;
        };

        transformControl.addEventListener('mouseDown', onBeforeTransform);
        transformControl.addEventListener('mouseUp', onAfterTransform);
        transformControl.addEventListener('objectChange', onTransform);

        scaleControl.addEventListener('mouseDown', onBeforeTransform);
        scaleControl.addEventListener('mouseUp', onAfterTransform);
        scaleControl.addEventListener('objectChange', onTransform);
    }

    function transformPanelOnInputChange() {
        var object = transformMode === 'scale' ? scaleControl.object : transformControl.object;

        if (!object) {
            if (!previousSelected) {
                console.error('Attempt to transform by inputs without selected object');
                return;
            }
            object = previousSelected;
        }

        if (this.name === 'title') {
            if (object.userData.object.title === this.value) {
                return;
            }

            if (sceneMode === 'object') {
                self.mediaLibrary.saveTitle(libraryObjectId, this.value);
            } else {
                object.userData.object.title = this.value;
                object.userData.titleChanged = true;
                onUpdateObjectState(object);
            }
            return;
        }

        var initialState = {};
        initialState.position = object.position.toArray();
        initialState.rotation = object.rotation.toArray();
        initialState.scale = object.scale.toArray();

        var value = parseFloat(this.value);
        if (isNaN(value)) {
            value = 0.0;
        }
        this.value = value;

        if (!object) {
            return;
        }

        var atr = this.name[0];
        var axis = this.name[1];
        var attributes = {
            'p': 'position',
            'r': 'rotation',
            's': 'scale'
        };
        var axisIndexes = {'x': 0, 'y': 1, 'z': 2};
        var axisIndex = axisIndexes[axis];

        var attribute = attributes[atr];

        if (attribute === 'rotation') {
            value = THREE.Math.degToRad(value);
        }

        var newTransformArray = object[attribute].toArray();
        newTransformArray[axisIndex] = value;
        if (attribute === 'scale') {
            newTransformArray = [value, value, value];
        }

        object[attribute].fromArray(newTransformArray);
        if (_applyLimitsOnTransform(object)) {
            updateTransformPanel(object);
        }

        // check if object still changed after limits applied
        if (object[attribute][axis] === initialState[attribute][axis]) {
            return;
        }

        if (transformMode === 'scale') {
            scaleControl.update();
        } else {
            transformControl.update();
        }

        selectionBox.update();

        var action = {
            name: 'transform',
            target: object,
            value: initialState
        };
        self.addToUndo(action);

        if (sceneMode !== 'object') {
            onUpdateObjectState(object);
        }
        self.requestRender();
    }

    function updateTransformPanel(obj) {
        var position = obj.position;
        var scale = obj.scale;
        if (transformMode === 'scale' && !(obj.parent instanceof THREE.Scene)) {
            obj.parent.updateMatrixWorld();

            position = new THREE.Vector3();
            position.setFromMatrixPosition(obj.matrixWorld);

            scale = new THREE.Vector3();
            scale.setFromMatrixScale(obj.matrixWorld);
        }

        transformInputs.px.val(position.x);
        transformInputs.py.val(position.y);
        transformInputs.pz.val(position.z);

        transformInputs.rx.val(THREE.Math.radToDeg(obj.rotation.x));
        transformInputs.ry.val(THREE.Math.radToDeg(obj.rotation.y));
        transformInputs.rz.val(THREE.Math.radToDeg(obj.rotation.z));

        transformInputs.sx.val(scale.x);
        // transformInputs.sy.val(scale.y);
        // transformInputs.sz.val(scale.z);

        transformInputs.title.val(obj.userData.object.title);
    }

    function initMediaPanels() {
        mediaPanel.video = $('#video-panel');
        mediaPanel.audio = $('#audio-panel');
    }

    function updateMediaPanel(type, previousSelected) {
        var panel = mediaPanel[type];

        if (previousSelected && typeof previousSelected[type] !== 'undefined') {
            $(previousSelected[type].getContainer()).hide();
        }

        if (!selected || (selected && typeof selected[type] === 'undefined')) {
            panel.hide();
            return;
        }

        var player = selected[type];
        $(player.getContainer()).show();

        panel.show();
    }

    function initAnimationsTab() {
        var i, animPair;
        
        animationsPanel = $('#animations-panel');
        //animations_panel_tab = $("#edit-panel").find(".head div[data-target='animations-panel']");
        animationsPanel.find('select').on('change', function () {
            var animationIndex = parseInt(this.value);
            if (animationIndex === -1) {
                for (i = 0; i < animations.length; i++) {
                    animationReset(animations[i].object, selected.currentAnimation);
                }
                if (animations[0]) {
                    if (animations[0].object.mixer.stopTimer)
                        window.clearTimeout(animations[0].object.mixer.stopTimer);

                    animations[0].object.mixer.stopTimer = timeoutAndMarkStopped(animations[0].object.mixer);
                }
            } else {
                animPair = animations[animationIndex];

                if (typeof animPair !== 'undefined') {
                    // console.log('anim switch from/to:', animPair.object.currentAnimation, animPair.animationName);

                    if (animPair.object.mixer.stopTimer)
                        window.clearTimeout(animPair.object.mixer.stopTimer);
                    animPair.object.mixer.stopped = false;

                    animationSwitchTo(animPair.object, animPair.animationName);
                }
            }

            selected.userData.object.options.animation = animationIndex;
        });
    }

    function timeoutAndMarkStopped(mixer) {
        return window.setTimeout(function(){
            mixer.stopped = true;
        }, 2000);
    }

    function animationReset(object, animationName) {
        object.currentAnimation = -1;
        return object.mixer.clipAction(animationName).setEffectiveWeight(0).reset();
    }

    function animationStopAll(object) {
        object.mixer.stopAllAction();
        object.currentAnimation = '';
    }

    function animationPlay(object, animationName) {
        return object.mixer.clipAction(animationName).setEffectiveWeight(1).play();
    }

    function animationSwitchTo(object, animationName) {
        object.mixer.stopAllAction();

        if (object.currentAnimation > -1) {
            var fromAction = animationPlay(object, object.currentAnimation);
            var toAction = animationPlay(object, animationName);
            fromAction.crossFadeTo(toAction, 0.5, false);
        } else {
            animationPlay(object, animationName);
        }

        object.currentAnimation = animationName;
    }

    function fillAnimationsList(object) {
        animations = [];
        var select = animationsPanel.find('select');
        select.empty();

        if (object.animations) {
            for (var ai = 0; ai < object.animations.length; ai++) {
                animations.push({object: object, animationName: object.animations[ai].name});
            }
        } else {
            object.traverse(function (child) {
                if (child.animations) {
                    for (var ai = 0; ai < child.animations.length; ai++) {
                        animations.push({object: child, animationName: child.animations[ai].name});
                    }
                } else if (child instanceof THREE.SkinnedMesh) {
                    if (child.geometry.animations !== undefined || child.geometry.morphAnimations !== undefined) {

                        for (var i = 0; i < child.geometry.animations.length; i++) {
                            animations.push({object: child, animationName: child.geometry.animations[i].name});
                        }

                    }
                }
            });
        }

        if (animations.length) {
            select.append('<option value="-1" ' + (object.userData.object.options.animation === -1 ? 'selected' : '') + '>No animation</option>');
            for (var i = 0; i < animations.length; i++) {
                select.append('<option value="' + i + '" ' + (object.userData.object.options.animation === i ? 'selected' : '') + '>' + animations[i].animationName + '</option>');
            }

            animationsPanel.show();
        } else {
            animationsPanel.hide();
        }
    }

    var viewModeControl;
    var isLoading = false;

    function initViewport3DSwitch() {
        viewModeControl = $('#view-mode');
        viewModeControl.on('click', function (e) {
            e.preventDefault();
            viewModeControl.closest('.view-mode-wrapper')
                .append('<img src="/img/tail-spin.svg" class="loader gray-background">');
            app.viewportSetMode(viewportMode === '3d' ? '2d' : '3d');
        });
    }

    function dollyzoom(targetDistance, duration, easing) {
        return new Promise(function (resolve) {
            var easing = easing || TWEEN.Easing.Linear.None; // TWEEN.Easing.Cubic.InOut
            var duration = duration || 1000;
            var target = cameraTarget;
            var tweenValues = getCameraYawPitchRadius(camera, target);
            var targetValues = {yaw: 0, pitch: Math.PI, radius: targetDistance};

            var x = Math.tan(THREE.Math.degToRad(camera.fov / 2)) * camera.position.distanceTo(target);

            var tween = new TWEEN.Tween(tweenValues);
            tween.to(targetValues, duration);
            tween.easing(easing);
            tween.onComplete(function () {
                resolve();
            });
            tween.onUpdate(function () {
                setCameraYawPitch(camera, target, tweenValues.yaw, tweenValues.pitch, tweenValues.radius);

                camera.fov = 2 * THREE.Math.radToDeg(Math.atan(x / tweenValues.radius));
                camera.updateProjectionMatrix();
            });
            tween.start();
        });
    }

    var transitionAnimating = false;

    var removeModeChangeLoader = function () {
        $('.view-mode-wrapper .loader').remove();
    };

    self.viewportSetMode = function (mode) {
        var title = '';
        if (viewportMode === mode) {
            return;
        }
        if (transitionAnimating)
            return;

        transitionAnimating = true;

        viewportMode = mode;

        var farRadius = 5000;
        var animationTime = 50;
        var cameraAnimation;
        if (mode === '2d') {
            cameraAnimation = new Promise(function (resolve) {
                viewcube.setView(viewcube.FACES.TOP).then(function () {
                    perspectiveCamera.far = farRadius + renderDistanceLimit;
                    dollyzoom(farRadius, animationTime, TWEEN.Easing.Quartic.In).then(function () {
                        resolve();
                        removeModeChangeLoader();
                    });
                });
            });

            cameraAnimation.then(function () {
                title = '2D';
                viewModeControl.removeClass('active');
                camera = orthographicCamera;

                viewportModeSwitchAxes('2d');
                if (transformMode === 'scale') {
                    selectionBox.material.color.set(bbox2dScaleColor);
                }

                viewModeControl.find('span').html(title);

                orbitControl.setObject(camera);
                orbitControl.enableRotate = false;
                orbitControl.enablePan = true;
                orbitControl.setZoom(zoom / 100);

                transformControl.setCamera(camera);
                scaleControl.setCamera(camera);

                viewcube.disable();

                transformControl.setRotationSnap(THREE.Math.degToRad(snapAngle2d));

                transitionAnimating = false;
                removeModeChangeLoader();
            });

        } else {
            cameraAnimation = new Promise(function (resolve) {
                var targetRadius = orbitControl.getInitialRadius() / (zoom / 100);

                // set camera and start animation
                camera = perspectiveCamera;
                orbitControl.setObject(perspectiveCamera);
                selectionBox.material.color.set(bboxDefaultColor);
                transformControl.setCamera(camera);
                scaleControl.setCamera(camera);
                viewportModeSwitchAxes('3d');

                dollyzoom(targetRadius, animationTime, TWEEN.Easing.Quartic.Out).then(function () {

                    viewcube.enable();

                    perspectiveCamera.far = renderDistanceLimit;
                    viewcube.setView(viewcube.FACES.TOP_FRONT_RIGHT_CORNER).then(function () {
                        resolve();
                    });
                });
            });

            cameraAnimation.then(function () {
                title = '3D';
                viewModeControl.addClass('active');

                orbitControl.enableRotate = true;
                orbitControl.enablePan = false;
                viewcube.updateOrientation();

                viewModeControl.find('span').html(title);

                orbitControl.setZoom(zoom / 100);

                transformControl.setRotationSnap(null);

                transitionAnimating = false;
                removeModeChangeLoader();
            });
        }


    };

    function viewportModeSwitchAxes(mode) {
        var visible = (mode !== '2d');
        var mode3dAxes = {
            'translate': ['Y', 'YZ', 'XY', 'XYZ'],
            'rotate': ['X', 'Z', 'Y', 'E', 'XYZE'] // 'E'
        };

        for (var _mode in mode3dAxes) {
            if (!mode3dAxes.hasOwnProperty(_mode))
                continue;
            var axes = mode3dAxes[_mode];

            for (var i = 0; i < axes.length; i++) {
                transformControl.setAxisVisible(_mode, axes[i], visible);
            }
        }

        // show full circle Y axis for 2d
        transformControl.setAxisVisible('rotate', 'DD', (mode === '2d'));
    }

    function initViewportLock() {
        $('#view-lock').on('click', function () {
            $(this).toggleClass('locked');
            app.viewportLock($(this).hasClass('locked'));
        });
    }

    self.viewportLock = function (lock) {
        if (lock) {
            viewcube.disable();
            orbitControl.dispose(); // TODO: implement enable/disable inside orbit control?
        } else {
            viewcube.enable();
            initTrackball(); // TODO: implement enable/disable inside orbit control?

            orbitControl.addEventListener('change', function () {
                if (viewportMode === '3d') {
                    viewcube.updateOrientation();
                }
            });
        }
    };

    function initViewcube() {
        // viewcube
        var viewcubeElement = document.getElementById('viewcube');
        viewcube = new FOUR.Viewcube({
            domElement: viewcubeElement,
            updateTween: false,
            viewport: {
                camera: perspectiveCamera
            }
        });
        viewcube.enable();

        orbitControl.addEventListener('change', function () {
            if (viewportMode === '3d') {
                viewcube.updateOrientation();
            }
        });

        viewcube.addEventListener(FOUR.EVENT.UPDATE, function (event) {
            //TODO: animate reposition
            var yawPitch = event.direction;
            // var view = event.view;
            rotateCameraAroundByYawPitch(perspectiveCamera, cameraTarget, yawPitch.yaw, yawPitch.pitch, true);
            // if (viewport_mode !== '3d') {
            //     self.viewport_set_mode('3d');
            // }
        });
    }

    function rotateCameraAroundByYawPitch(camera, target, yaw, pitch, animate) {
        if (typeof animate === 'undefined') {
            animate = false;
        }

        if (!animate) {
            setCameraYawPitch(camera, target, yaw, pitch);
        } else {
            tweenCamera(camera, target, yaw, pitch);
        }
    }

    function tweenCamera(camera, target, yaw, pitch) {

        var start = getCameraYawPitchRadius(camera, target);
        delete start.radius;
        var finish = {yaw: yaw, pitch: pitch};

        var tween = new TWEEN.Tween(start);
        tween.to(finish, 1000);
        tween.easing(TWEEN.Easing.Cubic.InOut);
        tween.onComplete(function () {
            setCameraYawPitch(camera, target, yaw, pitch);
            //render();
            //tween.stop();
        });
        tween.onUpdate(function () {
            setCameraYawPitch(camera, target, start.yaw, start.pitch);
            // render();
        });
        tween.start();
    }

    function getCameraYawPitchRadius(camera, target) {
        var spherical = new THREE.Spherical();
        var offset = new THREE.Vector3();

        // so camera.up is the orbit axis
        var quat = new THREE.Quaternion().setFromUnitVectors(camera.up, new THREE.Vector3(0, 1, 0));
        var position = camera.position;

        offset.copy(position).sub(target);

        // rotate offset to "y-axis-is-up" space
        offset.applyQuaternion(quat);

        // angle from z-axis around y-axis
        spherical.setFromVector3(offset);

        return {yaw: spherical.theta, pitch: Math.PI / 2 - spherical.phi, radius: spherical.radius};
    }

    self.setCameraYawPitch2 = function (yaw, pitch, radius) {
        setCameraYawPitch(perspectiveCamera, cameraTarget, yaw, pitch, radius);
    };

    function setCameraYawPitch(camera, target, yaw, pitch, radius) {
        var spherical = new THREE.Spherical();
        var offset = new THREE.Vector3();

        // so camera.up is the orbit axis
        var quat = new THREE.Quaternion().setFromUnitVectors(camera.up, new THREE.Vector3(0, 1, 0));
        var quatInverse = quat.clone().inverse();
        var position = camera.position;

        offset.copy(position).sub(target);

        // rotate offset to "y-axis-is-up" space
        offset.applyQuaternion(quat);

        // angle from z-axis around y-axis
        spherical.setFromVector3(offset);

        var thetaDelta = yaw - spherical.theta;
        var phiDelta = (-pitch + Math.PI / 2) - spherical.phi;

        spherical.theta += thetaDelta;
        spherical.phi += phiDelta;

        if (typeof radius !== 'undefined') {
            spherical.radius = radius;
        }

        spherical.makeSafe();
        offset.setFromSpherical(spherical);

        // rotate offset back to "camera-up-vector-is-up" space
        offset.applyQuaternion(quatInverse);

        camera.position.copy(target).add(offset);
        camera.lookAt(target);

        self.requestRender();
    }

    var exportRenderer;
    self.renderCover = function (cover, size) {
        var deferred = new $.Deferred();
        var debug = 0;

        // setup scene
        // 204x269
        var thumbWidth = size;
        var thumbHeight = size;

        var radius = 0; // camera viewport radius
        var lowestY = 0;

        if (!exportRenderer) {
            exportRenderer = new THREE.WebGLRenderer({antialias: true, preserveDrawingBuffer: true, alpha: true});
            // renderer.setClearColor(0xcccccc, 1);
            exportRenderer.setPixelRatio(1);

            if (debug) {
                $('body').append(exportRenderer.domElement);
                $(exportRenderer.domElement).css({
                    position: 'absolute',
                    border: '1px solid #ff0000',
                    left: 50,
                    top: 130
                });
            }
        }
        exportRenderer.setSize(thumbWidth, thumbHeight);

        var result = {};

        // setup scene, camera and lights
        var scene = new THREE.Scene();

        scene.add(new THREE.AmbientLight(0x444444));

        var spotLight = new THREE.SpotLight(0xffffff);
        spotLight.position.set(-250, 250, 250);
        scene.add(spotLight);

        var spotLight2 = new THREE.SpotLight(0x777777);
        spotLight2.position.set(250, -250, 250);
        scene.add(spotLight2);

        var render = function () {
            var camera = new THREE.OrthographicCamera(radius * -1, radius, radius, radius * -1, 1, cameraDistanceLimit);
            camera.position.set(0, cameraDistanceLimit, 0);
            camera.lookAt(new THREE.Vector3());

            camera.updateProjectionMatrix();
            scene.updateMatrixWorld();

            exportRenderer.render(scene, camera);
            exportRenderer.clear();
            exportRenderer.render(scene, camera); // i don't understand why it's not render by first call, two calls are working good

            result.image = exportRenderer.domElement.toDataURL('image/png');

            if (lowestY < 0) {
                camera.near = cameraDistanceLimit;
                camera.far = renderDistanceLimit;
                camera.updateProjectionMatrix();

                exportRenderer.clear();
                exportRenderer.render(scene, camera);
                result.image2 = exportRenderer.domElement.toDataURL('image/png');
            }

            result.radius = radius;
            deferred.resolve(result);
        };
        if (debug)
            window.renderAgain = render;

        var loadFinished = new $.Deferred();
        var materialsCollected = new $.Deferred();
        var materialsLoaded = new $.Deferred();
        var objects = [];
        var objectsLoadTotal = 1 + cover.objects.length; // cover image + objects
        var objectsLoadFinished = 0; // even with error

        var extendCameraViewport = function (bbox) {
            radius = Math.max(radius, Math.abs(bbox.min.x));
            radius = Math.max(radius, Math.abs(bbox.max.x));
            radius = Math.max(radius, Math.abs(bbox.max.z));
            radius = Math.max(radius, Math.abs(bbox.min.z));
        };

        result.labels = [];

        var loadObj = function (obj) {
            app.loadFile(obj, {fakeVideo: true, plainLoad: true}).then(function (object3d) {
                object3d.position.fromArray([obj.x, obj.y, obj.z, 'XYZ']);
                object3d.rotation.fromArray([obj.rx, obj.ry, obj.rz, defaultRotationOrder]);
                object3d.scale.fromArray([obj.scale, obj.scale, obj.scale, 'XYZ']);

                if (object3d.video && object3d.video.getMedia().canPlayType(obj.type)) {
                    object3d.video.pause();
                }

                scene.add(object3d);
                objects.push(object3d);

                var bbox = new THREE.Box3();
                bbox.setFromObject(object3d);
                extendCameraViewport(bbox);

                lowestY = Math.min(lowestY, bbox.min.y);

                var type = obj.type.split('/')[0];
                result.labels.push({type: type, x: bbox.max.x, y: bbox.max.z});

            }).always(function () {
                objectsLoadFinished++;

                if (objectsLoadFinished >= objectsLoadTotal) {
                    loadFinished.resolve();
                }
            });
        };

        app.loadImage(app.coversPath() + cover.filename).then(function (object3d) {
            object3d.position.set(0, -0.2, 0);
            //scene.add(object3d);
            objects.push(object3d);

            //var cover_outline = new THREE.BoxHelper(object3d, 0xAAAAAA);
            //scene.add(cover_outline);

            var bbox = new THREE.Box3();
            bbox.setFromObject(object3d);
            extendCameraViewport(bbox);

            result.cover = {
                width: bbox.max.x - bbox.min.x,
                height: bbox.max.z - bbox.min.z
            };

            // inc loaded count
            objectsLoadFinished++;
            if (objectsLoadFinished >= objectsLoadTotal) {
                loadFinished.resolve();
            }
        });

        for (var i = 0; i < cover.objects.length; i++) {
            loadObj(cover.objects[i]);
        }

        loadFinished.then(function () {
            // load_finished, go collect

            // check all textures are loaded
            var urls = [], images = [];

            function collectMaterial(m) {
                if (m.map) {
                    if (m.map.image) {
                        urls.push(m.map.image.src);
                        images.push(m.map.image);
                    }
                }
            }

            var traverseCallback = function (child) {
                if (child.material) {
                    if (child.material instanceof THREE.Material) {
                        collectMaterial(child.material);
                    } else if (typeof child.material.length !== 'undefined') {
                        for (var i = 0; i < child.material.length; i++) {
                            collectMaterial(child.material[i]);
                        }
                    }
                }
            };

            for (var oi = 0; oi < objects.length; oi++) {
                objects[oi].traverse(traverseCallback);
            }

            // signal that all materials textures are collected
            materialsCollected.resolve(urls);
        });

        materialsCollected.then(function (urls) {
            // materials collected, wait till all are loaded

            var loaded = 0;
            var onLoadFinished = function () {
                loaded++;
                // console.log('tex loaded', loaded, urls.length);
                if (loaded >= urls.length) {
                    materialsLoaded.resolve();
                }
            };

            // wait for all are loaded
            urls.map(function(url){
                new THREE.TextureLoader().load(url, onLoadFinished, null, onLoadFinished);
            });
        });

        materialsLoaded.then(function () {
            render();
        });

        return deferred.promise();
    };

    self.undo = function () {
        if (!undoStack.length)
            return;

        var action = undoStack.pop();

        self.updateUndoRedoUI();

        if (typeof action === 'undefined')
            return;

        var sceneObjects, i, id;
        switch (action.name) {
            case 'transform':
                var saveValue = {
                    position: action.target.position.toArray(),
                    rotation: action.target.rotation.toArray(),
                    scale: action.target.scale.toArray()
                };

                action.target.position.fromArray(action.value.position);
                action.target.rotation.fromArray(action.value.rotation);
                action.target.scale.fromArray(action.value.scale);

                selectionBox.update();
                scaleControl.update();

                action.value = saveValue;

                onUpdateObjectState(action.target);
                updateTransformPanel(action.target);
                self.addToRedo(action);
                break;
            case 'delete':
                var serverObject = action.target.userData.object;
                if (typeof serverObject === 'undefined') {
                    console.warn('Try to revert object delete with no associated serverObject');
                    return;
                }

                sceneObjects = getCover(action.coverId).objects;
                for (i = 0; i < sceneObjects.length; i++) {
                    if (sceneObjects[i].id === serverObject.id) {
                        sceneObjects[i].deleted = 0;
                        break;
                    }
                }

                if (action.coverId == coverId) {
                    self.addToScene(action.target);
                }

                self.addToRedo(action);
                break;
            case 'add':
                var serverIds = [];
                for (i = 0; i < action.target.length; i++) {
                    id = action.target[i].id;
                    serverIds.push(id);
                    convertationArray.splice(convertationArray.indexOf(id), 1);
                }

                if (action.sceneObjects)
                    sceneObjects = action.sceneObjects;
                else {
                    sceneObjects = [];
                    for (i = 0; i < raytraceObjects.length; i++) {
                        if (typeof raytraceObjects[i].userData !== 'undefined' && serverIds.indexOf(raytraceObjects[i].userData.object.id) !== -1) {
                            sceneObjects.push(raytraceObjects[i]);
                        }
                    }
                }

                // delete scene_objects
                for (i = 0; i < sceneObjects.length; i++) {
                    _deleteObject(sceneObjects[i]);
                    // delete from cover
                    if (sceneObjects[i].userData.object) {
                        _deleteServerObjectFromCover(coverId, sceneObjects[i].userData.object.id);
                    }
                }
                for (i = 0; i < action.target.length; i++) {
                    id = action.target[i].id;
                    self.objectsList.removeObject(id);
                }

                action.sceneObjects = sceneObjects;
                self.addToRedo(action);
                break;
            // case "select":
            // 	var current_selection = selected;
            // 	if (action.cover_id == cover_id) {
            // 		self.on_select(action.target);
            // 	}
            // 	action.target = current_selection;
            // 	self.addToRedo(action);
            // 	break;
            case 'deleteCover':
                for (i = 0; i < covers.length; i++) {
                    if (covers[i].id == action.target) {
                        covers[i].status = COVER_STATUS_NORMAL;
                        self.coversList.setDeleted(action.target, false);

                        self.addToRedo(action);
                        break;
                    }
                }
                break;
            case 'add_covers':
                for (i = 0; i < action.target.length; i++) {
                    action.target.status = COVER_STATUS_DELETED;
                    self.coversList.setDeleted(action.target[i].id, true);
                }
                self.addToRedo(action);
                break;
        }
        self.requestRender();
    };

    self.redo = function () {
        var action = redoStack.pop();
        self.updateUndoRedoUI();

        if (typeof action === 'undefined')
            return;

        var i;
        switch (action.name) {
            case 'transform':
                var saveValue = {
                    position: action.target.position.toArray(),
                    rotation: action.target.rotation.toArray(),
                    scale: action.target.scale.toArray()
                };

                action.target.position.fromArray(action.value.position);
                action.target.rotation.fromArray(action.value.rotation);
                action.target.scale.fromArray(action.value.scale);

                selectionBox.update();
                scaleControl.update();

                action.value = saveValue;

                onUpdateObjectState(action.target);
                updateTransformPanel(action.target);
                self.addToUndo(action, true);
                break;
            case 'delete':
                if (action.coverId == coverId) {
                    // add to undo will be inside delete_object call
                    self.deleteObject(action.target, true);
                } else {
                    // TODO: refactor this, same code in _delete_object
                    var sceneObjects = getCover(action.coverId).objects;
                    var sceneObjectIndex = sceneObjects.indexOf(action.target.userData.object);
                    if (sceneObjectIndex > -1) {
                        sceneObjects.splice(sceneObjectIndex, 1);
                    }
                    self.addToUndo(action, true); // add to undo
                }
                break;
            case 'add':

                for (i = 0; i < action.sceneObjects.length; i++) {
                    // self.import_to_scene(action.target[i]);
                    self.addToScene(action.sceneObjects[i]);
                }

                self.addToUndo(action, true);
                break;
            // case "select":
            // 	var current_selection = selected;
            // 	self.on_select(action.target);
            // 	action.target = current_selection;
            // 	self.addToUndo(action, true);
            // 	break;
            case 'deleteCover':
                // add to undo will be inside deleteCover call
                self.deleteCover(action.target);
                break;
            case 'add_covers':
                for (i = 0; i < action.target.length; i++) {
                    action.target.status = COVER_STATUS_TMP;
                    self.coversList.setDeleted(action.target[i].id, false);
                }
                self.addToUndo(action, true);
                break;
        }
        self.requestRender();
    };

    self.addToUndo = function (action, byRedo) {
        window._undoStack = undoStack;
        window._redoStack = redoStack;

        undoStack.push(action);

        if (!byRedo) {
            // flush redoStack
            redoStack = [];
        }

        self.updateUndoRedoUI();
        app.setUnsavedState(true);
    };
    self.addToRedo = function (action) {
        redoStack.push(action);

        self.updateUndoRedoUI();
        app.setUnsavedState(true);
    };

    self.updateUndoRedoUI = function () {
        if (redoStack.length)
            self.toolsPanel.enable('forward');
        else
            self.toolsPanel.disable('forward');

        if (undoStack.length)
            self.toolsPanel.enable('back');
        else
            self.toolsPanel.disable('back');
    };

    var blocker = $('.blocker');
    self.displayBlocker = function (display) {
        blocker[display ? 'show' : 'hide']();
    };

    self.displayMessage = function (html) {
        var div = $('<div>').addClass('message-error').html(html).appendTo('body');
        var okButton = $('<button>Ok</button>').appendTo(div);
        okButton.on('click', function () {
            div.remove();
            self.displayBlocker(false);
        });
    };

    self.displayLoadingProgress = function (current, total, label) {
        self.loadingControl.displayLoadingProgress(current, total, label);
    };

    self.hideLoadingProgress = function () {
        self.loadingControl.hideLoadingProgress();
    };

    self.humanizeFilesize = function (size) {
        var units = 'KB';
        var value = Math.round(size / 1024);
        if (value > 1024) {
            units = 'MB';
            value = Math.round(value / 1024);
        }
        if (value > 1024) {
            units = 'GB';
            value = Math.round(value / 1024);
        }

        return value + ' ' + units;
    };

    self.projectId = function () {
        return projectId;
    };

    self.editionId = function () {
        return editionId;
    };

    self.coverId = function () {
        return coverId;
    };

    self.test = {};

    self.test.sceneflood = function (libraryId, number) {
        var ids = [];
        ids.length = number;
        for (var i=0; i<number; i++) {
            ids[i] = libraryId;
        }
        app.addLibraryObject(ids);
    };

    self.test.sceneLineup = function () {
        var step = 30, scale = 1, inRow = 4, xs=-45, zs=-100;
        for (var i=0, xi=0, zi=0, x=xs, z=zs; i<app.scene.children.length;i++) {

            if (app.scene.children[i].userData.object) {
                app.scene.children[i].scale.x=scale;
                app.scene.children[i].scale.y=scale;
                app.scene.children[i].scale.z=scale;
                app.scene.children[i].position.x = x;
                app.scene.children[i].position.z = z;

                app.scene.children[i].userData.object.scale = scale;
                app.scene.children[i].userData.object.x = x;
                app.scene.children[i].userData.object.z = z;

                x += step;
                xi += 1;
                if (xi>=inRow) { x=xs; xi=0; z+=step; zi+=1; }
            }
        }
    };



    return this;
}

window.app = new App();
