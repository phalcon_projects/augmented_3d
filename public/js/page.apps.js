'use strict';
$(function () {

    initApp();
    initAppKeyDelete();
    initSearch();
    initAppKeysView();

    function initApp() {
        var app = $('form.app');

        //app.find('.version').niceSelect();

        $('.actions .action.save').on('click', function (e) {
            e.preventDefault();

            var data = {};
            data.name = app.find('input[name=name]').val();
            data.web_platform_id = app.find('input[name=web_platform_id]').val();
            data.android_platform_id = app.find('input[name=android_platform_id]').val();
            data.ios_platform_id = app.find('input[name=ios_platform_id]').val();
            data.version = app.find('input[name=version]').val();

            var nameRegExp =new RegExp("^[0-9A-Za-z_.-]+$");
            var error = false;
            var validateFields = [
                'name',
                'version',
                'web_platform_id',
                'android_platform_id',
                'ios_platform_id'
            ];

            $.each(validateFields, function (index, item) {
                var itemId = item.replace(new RegExp('_', 'g'), '-');

                if (!data[item].length) {
                    var itemName = item.replace(new RegExp('_', 'g'), ' ');
                    $('#' + itemId + '-help').html('Correct ' + itemName + '.').removeClass('hidden');
                    error = true;
                } else if (!nameRegExp.test(data[item])) {
                    $('#' + itemId + '-help')
                        .html('Only numbers, letters, hyphens and underscores are allowed.')
                        .removeClass('hidden');
                    error = true;
                } else {
                    $('#' + itemId + '-help').html('').addClass('hidden');
                }
            });

            if (error) {
                return false;
            }

            $.ajax({
                url: 'apps/save',
                data: data,
                method: 'post'
            }).then(function (response) {
                if (typeof response === 'object') {
                    if (response.status === 'ok') {
                        showMessage('saved', 'ok', 3000);
                        window.location.href = '/apps';
                    } else {
                        console.error(response);
                        showError(response.errors.join('<br>'));
                    }
                } else {
                    console.error(response);
                    showError(response);
                }
            });
        });
    }

    function initAppKeyDelete() {
        var appKeyDeleteModal = $('#app-delete-modal');

        $('.btn.action.delete').on('click', function (e) {
            e.preventDefault();

            appKeyDeleteModal.data('id', $(this).data('id'));
            appKeyDeleteModal.find('.description-app-name').html($(this).data('app-name'));
            appKeyDeleteModal.find('.description-key').html($(this).data('key'));
            appKeyDeleteModal.modal('show');
        });

        appKeyDeleteModal.find('.action.confirm-delete').on('click', function (e) {
            e.preventDefault();

            $.ajax({
                url: '/apps/delete/',
                data: {id: appKeyDeleteModal.data('id')},
                method: 'post'
            }).then(function (response) {
                if (typeof response === 'object') {
                    if (response.status === 'ok') {
                        if (response.redirect) {
                            window.location = response.redirect;
                        } else {
                            $('.apps .list tr[data-id="' + appKeyDeleteModal.data('id') + '"]').remove();
                            showMessage('The app key deleted', 'ok', 3000);
                            appKeyDeleteModal.modal('hide');
                        }
                    } else {
                        console.error(response);
                        showError(response.errors.join('<br>'));
                    }
                } else {
                    console.error(response);
                    showError(response);
                }
            });
        });
    }

    function initAppKeysView() {
        var appKeysViewModal = $('#app-keys-view-modal');

        $('.action.view-keys').on('click', function (event) {
            event.preventDefault();

            appKeysViewModal.data('id', $(this).data('id'));
            appKeysViewModal.find('.description-app-name').html($(this).data('app-name'));
            appKeysViewModal.find('.app-web-platform-id').html($(this).data('web-platform-id'));
            appKeysViewModal.find('.app-android-platform-id').html($(this).data('android-platform-id'));
            appKeysViewModal.find('.app-ios-platform-id').html($(this).data('ios-platform-id'));
            appKeysViewModal.find('.app-web-platform-key').html($(this).data('web-key'));
            appKeysViewModal.find('.app-android-platform-key').html($(this).data('android-key'));
            appKeysViewModal.find('.app-ios-platform-key').html($(this).data('ios-key'));
            appKeysViewModal.modal('show');
        });
    }

    function initSearch() {
        var formSearch = $('#form-search'),
            inputSearch = formSearch.find('input[name="search"]');

        formSearch.on('submit', function (event) {
            event.preventDefault();
        });

        inputSearch.on('keyup', function (event) {
            event.preventDefault();

            if (event.keyCode === 13) {
                var $url = (inputSearch.val() === '') ? 'apps/' : 'apps/?search=';
                window.location = $url + inputSearch.val();
            }
        });
    }

});
