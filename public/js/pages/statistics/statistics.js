'use strict';
$(function () {
    var chartSystemData = [['Platform', 'Views']];
    var chartViewsData = [['Date', 'iOS', 'Android']];
    var editionId = $('.statistics').data('edition-id');

    $(document).ready(function () {
        var dateFrom = $('input[name="date-from"]').val();
        var dateTo = $('input[name="date-to"]').val();

        $.ajax({
            url: 'statistics/get',
            data: {
                'edition_id': editionId,
                'date_from': dateFrom,
                'date_to': dateTo
            },
            method: 'post'
        }).then(function (response) {
            if (typeof response === 'object') {
                if (response.status === 'ok') {
                    chartSystemData = [['Platform', 'Views']];
                    $.each(response.systems, function (key, item) {
                        chartSystemData.push([key, item]);
                    });
                    chartViewsData = [['Date', 'iOS', 'Android']];
                    if (response.views.length > 0) {
                        $.each(response.views, function (key, item) {
                            chartViewsData.push(item);
                        });
                    } else {
                        chartViewsData.push([dateFrom, 0, 0]);
                    }
                    drawChartSystem();
                    drawChartViews();
                } else {
                    if (response.error) {
                        console.log(response.error);
                    } else {
                        console.error(response);
                    }

                    if (response.message) {
                        showError(response.message);
                    }
                }
            }
        });
    });

    $('.input-daterange').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('.action.period-select').on('click', function () {
        var dateFrom = $('input[name="date-from"]').val();
        var dateTo = $('input[name="date-to"]').val();

        if (dateFrom && dateTo) {
            window.location.href = '/statistics/' + editionId + '/?'
                + 'date_from=' + dateFrom + '&date_to=' + dateTo;
        } else {
            alert('Error. Correct date filter fields.');
        }
    });

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChartSystem);
    google.charts.setOnLoadCallback(drawChartViews);

    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawChartSystem() {
        // Create the data table.
        var data = google.visualization.arrayToDataTable(chartSystemData);

        // Set chart options
        var options = {
            pieHole: 0.4,
            colors: ['#7366b6', '#03a9f4'],
            legend: 'bottom',
            height: 400
        };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart-system'));
        chart.draw(data, options);
    }

    function drawChartViews() {
        var chartData = google.visualization.arrayToDataTable(chartViewsData);

        var options = {
            hAxis: {},
            vAxis: {minValue: 0},
            colors: ['#7366b6', '#03a9f4'],
            legend: 'bottom',
            height: 400
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart-views'));
        chart.draw(chartData, options);
    }

    $(window).resize(function(){
        drawChartSystem();
        drawChartViews();
    });
});