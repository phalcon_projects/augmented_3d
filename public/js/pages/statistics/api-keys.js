'use strict';
$(function () {
    var $modalStatisticAPIKeyRemove = $('#modal-statistic-api-key-remove');

    $('.statistic-api-keys .action.add').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: 'statistics/add-api-key',
            method: 'post'
        }).then(function (response) {
            if (typeof response === 'object') {
                if (response.status === 'ok' && response.reload) {
                    window.location.reload();
                }
            }
        });;
    });

    $('.statistic-api-keys .action.remove').on('click', function (e) {
        e.preventDefault();

        $modalStatisticAPIKeyRemove.find('.confirm-remove').attr('data-key-id', $(this).data('key-id'));
        $modalStatisticAPIKeyRemove.find('.description-key').html($(this).data('key-value'));
        $modalStatisticAPIKeyRemove.modal('show');
    });

    $('.confirm-remove').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url: 'statistics/remove-api-key',
            data: {
                key_id: $(this).data('key-id')
            },
            method: 'post'
        }).then(function (response) {
            if (typeof response === 'object') {
                if (response.status === 'ok') {
                    window.location.reload();
                }
            }
        });
    });
});