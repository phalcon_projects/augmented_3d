'use strict';
$(function () {
    var $youTubeDownloadAccessListInviteList = $('#youtube-download-access-list-invite-list');
    var $youTubeDownloadAccessListInviteMenu = $('#youtube-download-access-list-invite-menu');
    var $youTubeDownloadAccessInviteInput = $('input[name="youtube-download-user-invite"]');

    $('#youtube-download-user-invite').on('keyup', function (e) {
        e.preventDefault();

        if ($(this).val().length) {
            getYouTubeDownloadAccessInviteList($(this).val());
        } else {
            $youTubeDownloadAccessListInviteMenu.html('');
            $youTubeDownloadAccessListInviteMenu.removeClass('show');
            $youTubeDownloadAccessListInviteList.removeClass('show');
        }
    });

    $('.youtube-download-access-list .action.remove').on('click', function (e) {
        e.preventDefault();
        youTubeDownloadAccessListRemove($(this).closest('.list-group-item').data('id'));
        $(this).attr('disabled', true);
    });

    function youTubeDownloadAccessListAdd(userId, content) {
        $youTubeDownloadAccessInviteInput.attr('disabled', true);
        $.ajax({
            url: 'settings/youtube-download-access-list-add',
            method: 'post',
            data: {
                user_id: userId
            }
        }).then(function (response) {
            if (typeof response === 'object') {
                if (response.status === 'ok') {
                    $('.youtube-download-access-list').append(
                        '<div class="list-group-item" data-id="' + userId + '">' +
                        '<span>' + content + '</span>' +
                        '<a class="btn btn-danger action remove"><img src="/img/del.png" alt="Remove"></a>' +
                        '</div>'
                    );
                    $youTubeDownloadAccessListInviteMenu.html('');
                    $youTubeDownloadAccessListInviteMenu.removeClass('show');
                    $youTubeDownloadAccessListInviteList.removeClass('show');
                    $youTubeDownloadAccessInviteInput.attr('disabled', false).val('');

                    $('.youtube-download-access-list .action.remove').bind('click', function (e) {
                        e.preventDefault();
                        youTubeDownloadAccessListRemove($(this).closest('.list-group-item').data('id'));
                        $(this).attr('disabled', true);
                    });
                }
            } else {
                showError('User add error.');
            }
        });
    }

    function youTubeDownloadAccessListRemove(userId) {
        $youTubeDownloadAccessInviteInput.attr('disabled', true);
        $.ajax({
            url: 'settings/youtube-download-access-list-remove',
            method: 'post',
            data: {
                user_id: userId
            }
        }).then(function (response) {
            if (typeof response === 'object') {
                if (response.status === 'ok') {
                    $('.youtube-download-access-list').find('.list-group-item[data-id="' + userId + '"]').remove();
                    $youTubeDownloadAccessInviteInput.attr('disabled', false);
                }
            } else {
                showError('User removing error.');
            }
        });
    }

    function getYouTubeDownloadAccessInviteList(search) {
        $.ajax({
            url: 'settings/youtube-download-access-list-get-to-invite',
            method: 'get',
            data: {
                search: search
            }
        }).then(function (response) {
            if (typeof response === 'object') {
                if (response.status === 'ok' && response.list) {
                    $youTubeDownloadAccessListInviteMenu.html('');
                    $.each(response.list, function (index, item) {
                        var $newItem = $('<a class="dropdown-item youtube-download-access-list-new-user"></a>');
                        $newItem.attr('data-id', item.id);
                        $newItem.html(item.first_name + ' ' + item.surname + ' ' + item.email);
                        $youTubeDownloadAccessListInviteMenu.append($newItem);
                    });

                    $('.youtube-download-access-list-new-user').bind('click', function (e) {
                        e.preventDefault();
                        youTubeDownloadAccessListAdd($(this).data('id'), $(this).html());
                    });

                    $('.youtube-download-access-list .action.remove').bind('click', function (e) {
                        e.preventDefault();

                        youTubeDownloadAccessListRemove($(this).closest('.list-group-item').data('id'));
                        $(this).attr('disabled', true);
                    });

                    $youTubeDownloadAccessListInviteMenu.addClass('show');
                    $youTubeDownloadAccessListInviteList.addClass('show');
                }
            } else {
                $youTubeDownloadAccessListInviteMenu.html('');
                $youTubeDownloadAccessListInviteMenu.removeClass('show');
                $youTubeDownloadAccessListInviteList.removeClass('show');
                console.error('YouTube download access list invitation list fetching error.');
            }
        });
    }
});