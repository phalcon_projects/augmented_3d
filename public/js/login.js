/* global FB:false, alert:false, console:false */

$(document).ready(function () {
    'use strict';

    initLogin();
    initFacebookLogin();

    function initLogin() {
        // init login form submit
        var loginForm = $('.form-signin');
        loginForm.on('submit', function (e) {
            e.preventDefault();

            $.ajax({
                url: loginForm.attr('action'),
                type: 'post',
                data: loginForm.serialize()
            }).then(
                function (result) {
                    if (result && typeof result === 'object') {
                        if (result.continue) {
                            document.location = result.continue;
                        } else {
                            alert(result.error);
                        }
                    } else {
                        alert(result);
                    }
                }
            );

            return false;
        });
    }

    function initFacebookLogin() {
        var permissionsModal = $('#password_modal');
        permissionsModal.find('.btn.revise').on('click', function (e) {
            e.preventDefault();

            permissionsModal.modal('hide');

            FB.login(function (response) {
                window.fbLoginStatus = response.status;

                if (response.status === 'connected') {
                    // check if email permission is granted
                    var permissions = response.authResponse.grantedScopes.split(',');

                    if (permissions.indexOf('email') !== -1) {
                        // do our login
                        localLoginWithFB();
                    }
                }
            }, {scope: 'public_profile,email', return_scopes: true, auth_type: 'rerequest'});
        });

        $('.btn-sign-in-facebook').on('click', function (e) {
            e.preventDefault();

            FB.login(function (response) {
                window.fbLoginStatus = response.status;

                if (response.status === 'connected') {
                    // check if email permission is granted
                    var permissions = response.authResponse.grantedScopes.split(',');

                    if (permissions.indexOf('email') !== -1) {
                        // do our login
                        localLoginWithFB();
                    } else {
                        // Show Dialog with button to change permission for email
                        permissionsModal.modal('show');
                    }
                }
            }, {scope: 'public_profile,email', return_scopes: true});
        });

        function localLoginWithFB() {
            $.ajax({url: 'session/facebook', method: 'post'}).then(function (result) {

                if (result && typeof result === 'object') {
                    if (result.continue) {
                        document.location = result.continue;
                    } else {
                        alert(result.error);
                    }
                } else {
                    alert(result);
                }
            }, function (e) {
                console.error(e);
            });
        }
    }

});
