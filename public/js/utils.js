function showError(message) {
    showMessage('<strong>Error!</strong> ' + message, 'error');
}

function showMessage(message, type, timeout) {
    'use strict';

    var classByType = {'info': 'info', 'ok': 'success', 'error': 'danger'};
    var alertClass = classByType[type] || 'info';
    var messageContainer = $('#messages-container');

    if (!messageContainer.length) {
        messageContainer = $('<div>')
            .addClass('messages-container')
            .attr('id', 'messages-container')
            .appendTo('body');
    } else {
        messageContainer.html('');
    }

    var divClass = 'alert fade-in alert-' + alertClass;
    if (timeout)
        divClass += ' alert-dismissible';

    var div = $('<div class="' + divClass + '" style="display: none;">'
        + '<button class="close" data-dismiss="alert">&times;</button>'
        + message
        + '</div>');

    if (timeout) {
        window.setTimeout(function () {
            div.fadeOut(1000, function () {
                div.alert('close');
            });
        }, timeout);
    }

    div.appendTo(messageContainer);
    messageContainer.find('.alert').fadeIn();
}

/**
 * @typedef {Object} confirmOptions
 * @property {String} [title]
 * @property {String} [labelConfirm]
 * @property {function} [onConfirm]
 * @property {function} [onCancel]
 */
/**
 *
 * @param {string} message
 * @param {confirmOptions} options
 */
function modalConfirm(message, options) {
    'use strict';

    options = $.extend({}, options);
    var modal = $('#ui-confirm-modal');
    var buttonConfirm = modal.find('.btn.confirm');
    var buttonConfirmText = modal.find('.ui-message-btn-text-confirm');

    modal.find('form').off('submit').on('submit', function (e) {
        e.preventDefault();
        modal.find('.btn.confirm').trigger('click');
    });

    if (options.title) {
        $('#ui-confirm-modal-label').html(options.title);
    }

    modal.find('.ui-message-text').html(message);


    if (options.labelConfirm) {
        buttonConfirmText.text(options.labelConfirm);
    } else {
        buttonConfirmText.text('ok');
    }

    buttonConfirm.off('click').on('click', function (e) {
        e.preventDefault();
        if (typeof options.onConfirm === 'function')
            options.onConfirm();
        modal.modal('hide');
    });

    modal.find('.btn.cancel,close').off('click').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        if (typeof options.onCancel === 'function')
            options.onCancel();
        modal.modal('hide');
    });


    modal.modal('show');
    buttonConfirm.focus();
}