/* globals console: false, modalConfirm: false, showError: false, showMessage: true */
// jshint trailingcomma: false
$(function () {
    'use strict';
    var content = $('.content-wrapper');

    initEdit();
    initDelete();
    initPagination();
    initProjectAccessDialog();

    function loadPage(num) {
        $.ajax({
            url: 'projects',
            data: {page: num},
            target: 'div.projects',
            method: 'get'
        }).then(function (html) {
            $('.projects').replaceWith(html);
        });
    }

    function doAction(action, data) {
        var process = new $.Deferred();

        $.ajax({
            url: 'projects/' + action,
            data: data,
            method: 'post'
        }).then(function (response) {
            if (response && !!response.success) {
                // reload active page
                var page = parseInt(response.page);
                var activePage = parseInt($('.pagination .page.active').html());

                if (!isNaN(page)) {
                    activePage = page;
                } else {
                    if (isNaN(activePage) || activePage <= 0) {
                        activePage = 1;
                    }
                }

                loadPage(activePage);
                var message = action === 'delete' ? 'Project is deleted.' : 'Project is saved.';
                showMessage(message, 'ok', 5000);

                process.resolve(response);
            } else {
                console.error(response);

                if (response.errors.join) {
                    showError(response.errors.join('<br />'));
                } else {
                    showError(response.errors);
                }

                process.reject(response);
            }
        }, function (error) {
            console.error(error);
            var errorMessage = '';
            if (error.statusText) {
                errorMessage += 'Server error: ' + error.statusText;
            }
            showError(errorMessage);
        });

        return process.promise();
    }

    function initEdit() {
        var $modalProject = $('#modal-project');
        var $titleInput = $modalProject.find('input[name="project-title"]');
        var $uuidInput = $modalProject.find('input[name="project-uuid"]');
        var $idInput = $modalProject.find('input[name="project-id"]');
        var $modalTitle = $modalProject.find('.modal-project-title');
        var $saveButton = $('#modal-action-save');

        $modalProject.find('form').off('submit').on('submit', function () {
            $saveButton.trigger('click');
        });

        content.on('click', '.action.add, .action.edit', function (e) {
            e.preventDefault();

            var title = $(this).closest('.list-item').data('project-title');
            var uuid = $(this).closest('.list-item').data('project-uuid');
            var id = $(this).data('project-id');

            if (id) {
                $idInput.val(id);
            } else {
                $idInput.val('');
            }

            $modalTitle.html((id ? 'Edit project' : 'Add project'));
            $titleInput.val(title);
            $uuidInput.val(uuid);

            $modalProject.modal('show');
            $titleInput.focus();
        });

        $saveButton.bind('click', function () {
            var data = {};
            var title = $titleInput.val();
            var uuid = $uuidInput.val();
            var projectId = $idInput.val();

            if (!title || title.trim() === '') {
                return;
            }

            if (projectId) {
                data.id = projectId;
            }

            var uuidRegExp =new RegExp("^[0-9A-Za-z_.-]+$");

            if (!uuidRegExp.test(uuid)) {
                $('#project-uuid-help')
                    .html('Only numbers, letters, hyphens and underscores are allowed.')
                    .removeClass('hidden');
                return false;
            }

            data.title = title;
            data.uuid = uuid;

            doAction('save', data).then(function (response) {
                if (!response.errors) {
                    $modalProject.modal('hide');
                }
            });
        });
    }

    function initDelete() {
        content.on('click', '.list-item .delete', function (e) {
            e.preventDefault();
            var id = $(this).data('id');

            modalConfirm('Delete project and all it\'s pages?', {
                onConfirm: function () {
                    doAction('delete', {
                        id: id,
                        page: $('.pagination .page.active').html()
                    });
                }
            });
        });
    }

    function initPagination() {
        content.on('click', '.pagination .page', function (e) {
            e.preventDefault();

            loadPage($(this).text());
        });
    }

    function initProjectAccessDialog() {
        var $modalProjectAccess = $('#modal-project-access');

        $('.action.access').on('click', function (e) {
            e.preventDefault();

            var projectId = $(this).data('project-id');

            $.ajax({
                url: 'projects/access-teams',
                data: {
                    project_id: projectId
                },
                method: 'post'
            }).then(function (response) {
                $modalProjectAccess.find('.teams-list').html(response);
                $modalProjectAccess.data('project-id', projectId);
            });

            $modalProjectAccess.modal('show');
        });

        $modalProjectAccess.on('change', '.team-access .form-check-input', function (e) {
            e.preventDefault();

            $.ajax({
                url: 'projects/access-team',
                data: {
                    project_id: $modalProjectAccess.data('project-id'),
                    team_id: $(this).closest('.team-access').data('team-id'),
                    accessed: $(this).prop('checked')
                },
                method: 'post'
            }).then(function (response) {
                if (response.status === 'ok') {
                    showMessage(response.message, 'success', 2000);
                } else {
                    showError('Cannot save team access. Reload page and try again.');
                }
            });

            $modalProjectAccess.modal('show');
        });
    }

});