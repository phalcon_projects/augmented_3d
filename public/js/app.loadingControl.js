(function(app){
    /**
     * @param htmlBase
     * @param {boolean?} blocker
     * @param {boolean?} isFileSize
     * @returns {LoadingControl}
     * @constructor
     */
    function LoadingControl(htmlBase, blocker, isFileSize) {
        'use strict';

        var self = this;

        var loadingControl = htmlBase;
        var loadingProgress = loadingControl.find('.progress-fill');
        var loadingText = loadingControl.find('.progress-text .text');
        var loadingTotalText = loadingControl.find('.progress-total');
        var labelText = loadingControl.find('.progress-text-info');

        isFileSize = (typeof isFileSize === 'undefined')? true : isFileSize;
        blocker = (typeof blocker === 'undefined')? true : blocker;

        self.displayLoadingProgress = function (current, total, label) {

            if (blocker)
                app.displayBlocker(true);

            if (total === Infinity) {
                loadingControl.addClass('infinite-loading').show();
                return;
            } else {
                loadingControl.removeClass('infinite-loading');
            }

            if (label) {
                loadingControl.addClass('text-info-on');
                labelText.html(label).show();
            } else {
                loadingControl.removeClass('text-info-on');
                labelText.html('').hide();
            }

            if (current === total) {
                loadingText.hide();
                loadingProgress.css({width: '100%'});
            }

            var perc = ((current / total) * 100).toFixed(2);

            if (isFileSize) {
                loadingText.html(app.humanizeFilesize(current)).show();
                loadingTotalText.html(app.humanizeFilesize(total)).show();
            } else {
                loadingText.html(current).show();
                loadingTotalText.html(total).show();
            }
            loadingProgress.css({width: perc + '%'});

            loadingControl.show();
        };

        self.hideLoadingProgress = function() {
            loadingControl.hide();

            if (blocker)
                app.displayBlocker(false);
        };

        return this;
    }

    app.LoadingControl = LoadingControl;
})(window.app);
