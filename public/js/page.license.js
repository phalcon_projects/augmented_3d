"use strict";
$(function () {
    $('#payment-paypal-submit').on('click', function (event) {
        event.preventDefault();

        $.ajax({
            url: '/license/order',
            data: {},
            method: 'post'
        }).then(function (response) {
            if (typeof response === 'object') {
                if (response.status === 'ok') {
                    if (response.redirect) {
                        window.location = response.redirect;
                    } else if (response.order_id) {
                        $('input[name="item_number"]').val(response.order_id);
                        $('#form-payment-paypal').submit();
                    }
                } else {
                    console.error(response);
                    showError(response.errors.join('<br>'));
                }
            } else {
                console.error(response);
                showError(response);
            }
        });
    });
});