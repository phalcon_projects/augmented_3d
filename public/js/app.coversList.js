/* globals console: false, PDFJS: false, Sortable: false, modalConfirm: false, showError: false, showMessage: true */
// jshint esversion: 6
// jshint trailingcomma: false
(function (app){
    'use strict';

    /**
     *
     * @returns {CoversList}
     * @constructor
     */
    function CoversList() {
        var self = this;
        var coversList = $('.covers-list');
        var pdfDialog = $('#pdf-pages-dialog');
        var pdfDialogCenter = pdfDialog.find('.center');
        var pdfLoadingControl;
        var cropDialog;
        var cropInputs = {};
        var fileInput, uploaderInput;
        var droppedFiles = false;
        var newCoversUploadCount = 0;
        var editionId;
        var coversPath;

        var uploadNext = function(index, files, coverId) {
            if (index >= files.length) {
                droppedFiles = false;
                newCoversUploadCount = 0;
                return;
            }

            var label;
            if (files.length > 1) {
                label = (index+1) + ' of ' + files.length + ': ' + files[index].name;
            }

            uploadFromFile(files[index], label, coverId).then(function(){
                uploadNext(index+1, files, coverId);
            });
        };

        self.init = function() {
            coversPath = app.coversPath();
            editionId = app.editionId();
            self.initPdfPagesDialog();

            fileInput = $('#add-cover-input');
            uploaderInput = $('#cover-uploader-input');

            /* region drag and drop upload */

            var isAdvancedUpload = function () {
                var div = document.createElement('div');
                return (('draggable' in div) || ('ondragstart' in div && 'ondrop' in div))
                    && 'FormData' in window
                    && 'FileReader' in window;
            }();

            var $form = $(".drag-and-drop"),
                $coversListWrapper = $('.covers-list-wrapper'),
                dragging = 0;

            if (isAdvancedUpload) {
                $(window).on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
                    if (e.originalEvent.dataTransfer.types
                        && (e.originalEvent.dataTransfer.types.indexOf
                                ? e.originalEvent.dataTransfer.types.indexOf('Files') != -1
                                : e.originalEvent.dataTransfer.types.contains('Files')
                        )
                    ) {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                }).on('dragover dragenter', function (e) {
                    if (e.originalEvent.dataTransfer.types
                        && (e.originalEvent.dataTransfer.types.indexOf
                            ? e.originalEvent.dataTransfer.types.indexOf('Files') != -1
                            : e.originalEvent.dataTransfer.types.contains('Files')
                        )
                    ) {
                        $coversListWrapper.addClass('is-dragging');
                    }
                }).on('dragend drop', function (e) {
                    $coversListWrapper.removeClass('is-dragging');
                }).on('dragenter', function () {
                    dragging++;
                }).on('dragleave', function () {
                    dragging--;

                    if (dragging === 0) {
                        $coversListWrapper.removeClass('is-dragging');
                    }
                });

                $coversListWrapper.on('dragover dragenter', function () {
                    //$(this).addClass('is-dragging');
                }).on('dragleave dragend drop', function () {
                    //$(this).removeClass('is-dragging');
                });

                $form.on('dragleave dragend drop', function () {
                    //$coversListWrapper.removeClass('is-dragging');
                }).on('drop', function(e) {
                    droppedFiles = e.originalEvent.dataTransfer.files;
                    newCoversUploadCount = droppedFiles.length;

                    for (var i = 0; i < droppedFiles.length; i++) {
                        $('.covers-list').append('<div class="cover template" draggable="false"><div class="image"><div class="placeholder"></div></div>\n' +
                            '<div class="loader-wrapper-small"><div class="loader-small"><img src="/img/logo-main-small.svg"></div></div><div class="load-progress"><span></span></div></div>');
                    }

                    $(this).trigger('submit');
                }).addClass('advanced-upload');

                $('.covers-list').on('drag dragstart dragend dragover dragenter dragleave drop', '.cover', function (e) {
                    //e.preventDefault();
                    //e.stopPropagation();
                }).on('dragover dragenter', '.cover', function () {
                    $(this).addClass('is-dragover');
                }).on('dragleave dragend drop', '.cover', function () {
                    $(this).removeClass('is-dragover');
                }).on('drop', '.cover', function(e) {
                    //$coversListWrapper.removeClass('is-dragging');
                    $(this).removeClass('is-dragover');
                    droppedFiles = e.originalEvent.dataTransfer.files;

                    var coverId = $(this).data('id');

                    if (typeof coverId !== 'undefined') {
                        uploadNext(0, droppedFiles, coverId);
                    }
                });
            }


            var processUpload = function (index) {
                if (index >= droppedFiles.length) {
                    $form.removeClass('is-uploading');
                    return;
                }

                var label;

                if (droppedFiles.length > 1) {
                    label = (index + 1) + ' of ' + droppedFiles.length + ': ' + droppedFiles[index].name;
                }

                uploadFromFile(droppedFiles[index], label).then(function () {
                    processUpload(index + 1);
                });
            };

            $form.on('submit', function (e) {
                e.preventDefault();
                if ($form.hasClass('is-uploading')) return false;

                $form.addClass('is-uploading').removeClass('is-error');

                if (droppedFiles) {
                    processUpload(0);
                }
            });
            /* endregion drag and drop upload */

            $('.covers-list-action-add').on('click', function(){
                self.loadCoverDialog();
            });

            $('.main-bar .nav-link').not('[id="object-library-tab"]').bind('click', function () {
                $('#bar-object-library').removeClass('show active');
            });

            coversList.delegate('.delete', 'click', function (e) {
                e.stopPropagation();

                var cover = $(this).closest('.cover');
                modalConfirm('Delete?', {
                    onConfirm: function(){
                        app.deleteCover(cover.data('id'));
                    }
                });

            });

            coversList.delegate('.crop', 'click', function (e) {
                e.stopPropagation();
                var cover = $(this).closest('.cover');

                cropCoverDialog(cover);
            });

            coversList.delegate('.replace', 'click', function (e) {
                e.stopPropagation();
                var cover = $(this).closest('.cover');

                self.loadCoverDialog(cover.data('id'));
            });

            coversList.delegate('.set-main', 'click', function (e) {
                e.stopPropagation();
                var cover = $(this).closest('.cover');

                makeCoverMain(cover.data('id'));
            });

            coversList.delegate('.cover', 'click', onCoverClick);
            //add_cover.on('click', on_add_cover_click);
            var uploaderConfig = {
                dataType: 'json',
                dropZone: null,
                done: function (e, data) {
                    app.hideLoadingProgress();
                },
                progressall: function (e, data) {
                    //app.display_loading_progress(data.loaded, data.total);
                },
                fail: function (e, data) {
                    app.hideLoadingProgress();
                    showError('unexpected response from server:' + data.textStatus);
                }
            };
            uploaderInput.fileupload(uploaderConfig);
            fileInput.data('cover_id', 0);
            fileInput.on('change', function() {
                var coverId = fileInput.data('cover_id');

                if (typeof coverId !== 'undefined') {
                    uploadNext(0, this.files, coverId);
                    fileInput.data('cover_id', 0); // reset cover_id
                }
            });

            // covers_bar.find(".add-object").on("click", function(){
            // 	var add_to_scene = covers_bar.find(".active").is(".view1, .view2, .view3");
            // 	app.load_object_dialog(add_to_scene);
            // });
            //
            // covers_bar.find(".view1").on("click", function(){
            // 	covers_bar.removeClass("short-list objects-list library-list");
            // 	$(this).addClass("active").siblings().removeClass("active");
            // });
            // covers_bar.find(".view2").on("click", function(){
            // 	covers_bar.removeClass("objects-list library-list");
            // 	covers_bar.addClass("short-list");
            // 	$(this).addClass("active").siblings().removeClass("active");
            // });

            // self.load_covers().then(function(){
            // 	var cover_id = parseInt(document.location.hash.slice(1));
            // 	var current_cover;
            // 	if (!isNaN(cover_id)) {
            // 		current_cover = covers_list.find(".cover").filter(function(index){
            // 			return $(this).data('id') == cover_id;
            // 		});
            // 	} else {
            // 		current_cover = covers_list.find(".cover:first");
            // 	}
            //
            // 	current_cover.trigger("click");
            // });
        };

        self.loadCoverDialog = function (coverId) {
            fileInput.val(null);
            if (typeof coverId !== 'undefined') {
                fileInput.data('cover_id', coverId);
            }
            fileInput.trigger('click');
        };

        function makeCoverMain (coverId) {
            app.makeCoverMain(coverId);
        }

        self.resetActive = function() {
            coversList.find('.cover').removeClass('.active');
        };

        self.setCoverMain = function(coverId) {
            var cover = coversList.find('.cover').filter(function(index){
                return $(this).data('id') == coverId;
            });
            cover.siblings().removeClass('mainCover');
            cover.addClass('mainCover');
        };

        self.setActive = function(coverId) {
            var cover = coversList.find('.cover').filter(function(index){
                return $(this).data('id') == coverId;
            });
            cover.siblings().removeClass('active');
            cover.addClass('active');
        };

        self.activeCoverId = function() {
            return coversList.find('.cover.active').data('id');
        };

        /**
         * hide or show cover in list
         * @param coverId number
         * @param deleted boolean
         */
        self.setDeleted = function(coverId, deleted) {
            var coverUi = coversList.find('.cover').filter(function(index){
                return $(this).data('id') == coverId;
            });

            if (deleted)
                coverUi.addClass('deleted');
            else
                coverUi.removeClass('deleted');

            reindexCovers();
        };

        self.setCovers = function(data) {
            coversList.empty();

            for (var i = 0; i < data.length; i++) {
                var cover = self.buildCoverHtml(data[i]);

                coversList.append(cover);
            }
            reindexCovers();

            Sortable.create(coversList[0], {
                filter: '.add-cover',
                onEnd: function (/**Event*/evt) {
                    // var data = {
                    // 	id: $(evt.item).data('id'),
                    // 	from: evt.oldIndex+1,
                    // 	to: evt.newIndex+1
                    // };
                    // $.ajax({url: "covers/reposition", data: data, type: "POST"});
                    app.repositionCover(evt.oldIndex, evt.newIndex);
                    reindexCovers();
                }
            });

            //covers_list.find(".cover:first").trigger("click");
        };

        function onCoverClick() {
            var cover = $(this);
            //var filename = cover.data("filename");

            // cover.siblings().removeClass("active");
            // cover.addClass("active");

            //app.reload_cover(covers_path + filename);
            app.switchToCover(cover.data('id'));
        }

        function onAddCoverClick() {
            var fileInput = $('<input type="file" name="covers[]" multiple="multiple" />');
            fileInput.val(null);

            fileInput.on('change', function () {
                if (!this.files.length) {
                    return;
                }

                uploadNext(0, this.files);
            });

            fileInput.trigger('click');
        }

        function uploadFromFile(file, label, coverId) {
            var progress = $.Deferred();

            var ext = file.name.split('.').pop().toLowerCase();

            if (ext === 'jpg' || ext === 'jpeg' || ext === 'png') {
                var url = 'covers/?edition_id=' + app.editionId();
                if (coverId) {
                    url += '&id=' + coverId;
                }

                var jqXHR = uploaderInput.fileupload('send', {
                    url: url,
                    files: [file]
                });
                uploaderInput.bind('fileuploadprogress', function (e, data) {
                    $('.covers-list .cover.template').first().find('span').css('width', ((data.loaded / data.total) * 100).toFixed(2));
                });

                jqXHR.then(function (response) {
                    if (response && response.covers && response.covers.length) {
                        app.addUploadedCovers(response.covers);
                        var $pageListCount = $('.covers-list-count .count-title');
                        $pageListCount.html($('.covers-list .cover:not(.template)').length);
                        progress.resolve();
                        $('.covers-list .cover.template').first().remove();
                        var $coversTemplates = $('.covers-list .cover.template');

                        if ($coversTemplates.length > 0) {
                            $('.covers-list').append($coversTemplates);
                        }
                    } else {
                        if (response.errors && response.errors.join) {
                            var errorText = 'Filed to create cover:\n' + response.errors.join(',\n');
                            console.error(errorText);
                            window.alert(errorText);
                            showMessage(errorText, 'error');
                            progress.reject(response.errors);
                        } else {
                            console.error('unexpected response from server:', response);
                            showMessage('unexpected response from server:' + response, 'error');
                            progress.reject(response);
                        }
                    }

                    app.hideLoadingProgress();
                }, function(error){
                    console.error('unexpected response from server:', error);
                    //window.alert('unexpected response from server:' + error);
                    var errorMessage = 'Upload failed.';
                    if (error.statusText) {
                        errorMessage += ' Server error: ' + error.statusText;
                    }
                    showMessage(errorMessage, 'error');
                    progress.reject(error);
                    app.hideLoadingProgress();
                }, function(event){
                    if (newCoversUploadCount === 0) {
                        app.displayLoadingProgress(event.loaded, event.total, label);
                    }
                });
            } else if (ext === 'pdf') {
                console.time('read file');
                // app.load_from_file(file, function (content) {
                app.loadFromFileUint8Array(file, function (content) {
                    console.timeEnd('read file');
                    app.displayLoadingProgress(0, Infinity);
                    PDFJS.getDocument({data: content}).then(function (pdf) {
                            self.pdfPagesDialog(pdf, file.name).then(function () {
                                progress.resolve();
                                app.hideLoadingProgress();
                            });
                        },
                        function (e) {
                            console.error(e);
                            showError('Failed to read PDF');
                            progress.reject(e);
                            app.hideLoadingProgress();
                        }
                    );
                });
            }
            else {
                console.error('Unsupported file format', ext);
                showError('Unsupported file format \'' + ext + '\'');
                progress.reject('Unsupported file format \'' + ext + '\'');
            }

            /*app.load_from_file(file, function (content) {
                var ext = file.name.split(".").pop().toLowerCase();

                if (ext === 'jpg' || ext === 'jpeg' || ext === 'png') {
                    var ext2type = {
                        jpg: "image/jpeg", jpeg: "image/jpeg",
                        png: "image/png"
                    };
                    var encodedData = window.btoa(content);
                    var dataURI = "data:" + ext2type[ext] + ";base64," + encodedData;
                    //var cover = make_cover_html(file.name, dataURI, 0);
                    //// cover.insertBefore(add_cover);
                    //covers_list.append(cover);

                    uploadDataUrlImage(dataURI, file.name).then(function (response) {

                        if (response && response.covers && response.covers.length) {
                            for (var i=0;i<response.covers.length;i++) {
                                add_cover(response.covers[i]);
                            }
                            progress.resolve();
                        } else {
                            if (response.errors && response.errors.join) {
                                var err_text = "Filed to create cover:\n" + response.errors.join(",\n");
                                console.error(err_text);
                                window.alert(err_text);
                                progress.reject(response.errors);
                            } else {
                                console.error("unexpected response from server:", response);
                                window.alert("unexpected response from server:" + response);
                                progress.reject(response);
                            }
                        }

                        app.hide_loading_progress();
                    }, function(e){
                        console.error("unexpected response from server:", e);
                        window.alert("unexpected response from server:" + e);
                        progress.reject(e);
                        app.hide_loading_progress();
                    }, function(event){
                        //console.log("cover upload", event.loaded, event.total, label);

                        app.display_loading_progress(event.loaded, event.total, label);
                    });
                } else if (ext === 'pdf') {
                    app.display_loading_progress(0, Infinity);
                    PDFJS.getDocument({data: content}).then(function (pdf) {
                            self.pdf_pages_dialog(pdf, file.name).then(function(){
                                progress.resolve();
                                app.hide_loading_progress();
                            });
                        },
                        function (e) {
                            console.error(e);
                            window.alert("unable to load PDF");
                            progress.reject(e);
                            app.hide_loading_progress();
                        });

                }
                else {
                    console.error("Unsupported file format", ext);
                    window.alert("Unsupported file format '" + ext + "'");
                    progress.reject(e);
                }
            });*/

            return progress.promise();
        }

        this.addCover = function(cover) {
            var coverHtml = self.buildCoverHtml(cover);
            coversList.append(coverHtml);

            reindexCovers();
        };

        function reindexCovers() {
            coversList.find('.cover').not('.deleted').find('.index').each(function(index){
                $(this).html(index + 1);
            });
        }

        this.buildCoverHtml = function(row) {
            var cover = $('<div class="cover">')
                .data('id', row.id)
                .data('title', row.title)
                .data('filename', row.filename);

            if (String(row.main) === '1') {
                cover.addClass('mainCover');
            }

            var head = $('<div class="head">').appendTo(cover);
            $('<span>').addClass('index').attr('title', row.title).appendTo(head);
            $('<span>').addClass('title').attr('title', row.title).html(row.title).appendTo(head);
            $('<img src="/img/scene/icons/remove.svg">').addClass('action').addClass('delete').attr('title', 'Delete').appendTo(cover);
            $('<div>').addClass('action crop').appendTo(cover).attr('title', 'Crop');
            $('<div>').addClass('action replace').appendTo(cover).attr('title', 'Replace');
            $('<div>').addClass('action set-main').appendTo(cover)
                .append('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 13"><defs><style>.a{fill:none;stroke-linecap:round;stroke-linejoin:round}</style></defs><path class="a" d="M5.18 2.5l2.18-2h3.28l2.18 2H16a1.67 1.67 0 0 1 1.5 1.68V11a1.5 1.5 0 0 1-1.5 1.5H2A1.5 1.5 0 0 1 .5 11V4.18A1.67 1.67 0 0 1 2 2.5z"/><circle class="a" cx="9" cy="6.75" r="2.75"/></svg>')
                .attr('title', 'Set as main');

            var imageBlock = $('<div>').addClass('image').appendTo(cover);
            $('<img src="' + coversPath + row.filename + '">').appendTo(imageBlock);

            var ratingBlock = $('<div>').addClass('rating').appendTo(cover);
            var stars = $('<div class="stars"></div>').appendTo(ratingBlock);
            var starsValue = $('<div class="value"></div>').appendTo(stars);
            buildStarsHtml(starsValue, row.rating);

            return cover;
        };

        function buildStarsHtml($target, rating) {
            var i;
            if (isNaN(rating))
                rating = 0;

            $target.empty();

            var fullStars = Math.floor(rating);
            var halfStars = Math.round(rating - fullStars);
            var emptyStars = Math.min(3, 3 - (fullStars + halfStars));
            for (i = 0; i < fullStars; i++) {
                $target.append('<img src="/img/scene/icons/star-filled.svg">');
            }
            for (i = 0; i < halfStars; i++) {
                $target.append('<img src="/img/scene/icons/star-filled.svg">');
            }
            for (i = 0; i < emptyStars; i++) {
                $target.append('<img src="/img/scene/icons/star.svg">');
            }
        }


        // Crop
        function initCropImageDialog() {
            cropDialog = $('#image-crop-dialog');

            cropDialog.find('button.cancel, .close').on('click', function () {
                var img = cropDialog.find('img.target');
                img.cropper('destroy');
                cropDialog.hide();
                img.remove();
                app.displayBlocker(false);
            });

            cropDialog.find('button.submit').on('click', function () {
                var compressionQuality = 0.8;
                var img = cropDialog.find('img.target');
                var cover = img.data('cover');
                var filename = croppedFilename(cover.data('filename'));
                var dataUrl = img.cropper('getCroppedCanvas').toDataURL('image/jpeg', compressionQuality);

                uploadDataUrlImage(dataUrl, filename, cover.data('id')).then(function (result) {
                    if (result.errors && result.errors.length) {
                        showError(result.errors.join('<br>'));
                        return;
                    } else if (typeof result !== 'object') {
                        showError(result);
                        return;
                    }

                    cover.data('rating', parseInt(result.covers[0].rating, 10));
                    cover.data('filename', filename);
                    //self.update_cover_html(result.covers[0]);
                    app.addUploadedCovers([result.covers[0]]);

                    img.cropper('destroy');
                    cropDialog.hide();
                    img.remove();
                    app.displayBlocker(false);
                }, function(error){
                    var errorMessage = 'File upload failed.';
                    if (error.statusText) {
                        errorMessage += 'Server error: ' + error.statusText;
                    }
                    showError(errorMessage);
                });

            });

            // precise crop control
            var cropParams = cropDialog.find('.precise form');
            cropInputs.x = cropParams.find('.crop-x input');
            cropInputs.y = cropParams.find('.crop-y input');
            cropInputs.width = cropParams.find('.crop-width input');
            cropInputs.height = cropParams.find('.crop-height input');

            cropParams.on('submit', function(e){
                e.preventDefault();
                setCropperByInputs();
            });
            cropParams.find('input').on('change', function(e){
                setCropperByInputs();
            });


            var reinitTimeout;
            var cropData = null;
            $(window).on('resize', function(){
                if (cropDialog.is(':visible')) {
                    if (reinitTimeout)
                        window.clearTimeout(reinitTimeout);

                    var img = cropDialog.find('img.target');
                    if (!cropData)
                        cropData = img.cropper('getData');
                    img.cropper('destroy');
                    reinitTimeout = window.setTimeout(function() {
                        copperInit(img, cropData);
                        cropData = null;
                    }, 500);
                }
            });
        }

        function getCoverUi(coverId) {
            return coversList.find('.cover').filter(function(){
                return $(this).data('id') == coverId;
            });
        }

        self.updateCoverHtml = function (cover) {
            var coverUi = getCoverUi(cover.id);

            var filename = cover.filename;
            var rating = cover.rating;

            var coverImg = coverUi.find('.image img');
            coverImg.attr('src', coversPath + filename);

            buildStarsHtml(coverUi.find('.stars .value'), rating);

            // reload scene ...
            // TODO: reload and update just cover texture
            if (coverUi.is('.active'))
                coverUi.trigger('click');
        };

        /**
         * @returns {{x: number, y: number, width: number, height: number, rotate: number, scaleX: number, scaleY: number }}
         */
        function getCropperInputsValues() {
            var data = {x:0, y:0, width:0, height:0, rotate: 0, scaleX: 1, scaleY: 1 };
            for (var key in cropInputs) {
                if (!cropInputs.hasOwnProperty(key))
                    continue;

                var val = parseFloat(cropInputs[key].val());
                if (isNaN(val)) {
                    val = data[key];
                    cropInputs[key].val(val);
                }
                data[key] = val;
            }

            return data;
        }

        function setCropperByInputs() {
            var img = cropDialog.find('img.target');
            var data = getCropperInputsValues(); // inputs data
            img.cropper('setData', data);
        }
        function setInputsByCropper(e) {
            cropInputs.x.val(Math.round(e.x));
            cropInputs.y.val(Math.round(e.y));
            cropInputs.width.val(Math.round(e.width));
            cropInputs.height.val(Math.round(e.height));
        }

        function copperInit(img, data) {
            var params = {
                viewMode: 1,
                responsive: true,
                // dragMode: 'move',
                movable: false,
                zoomable: false,
                background: false,
                highlight: false,
                rotatable: false,
                scalable: false,
                crop: function (e) {
                    // console.log(e);
                    setInputsByCropper(e);
                }
            };
            if (data) {
                params.data = data;
            }
            img.cropper(params);
        }

        function cropCoverDialog(cover) {
            app.displayBlocker(true);

            if (!cropDialog) {
                initCropImageDialog();
            }
            cropDialog.find('img.target').remove();

            var imageSrc = coversPath + cover.data('filename');
            var img = $('<img>').addClass('target')
                .data('cover', cover);
            img.attr('src', imageSrc).appendTo(cropDialog.find('.center'));
            img.on('load', function(){
                copperInit(img);
            });


            cropDialog.show();
        }

        function croppedFilename(filename) {
            var add = 'crop' + new Date().getTime() + '_';
            if (filename.match(/^crop\d*_/)) {
                return filename.replace(/^crop(\d*)_/, add);
            } else {
                return (add + filename);
            }
        }

        // PDF
        var pdfDialogProgress;
        self.pdfPagesDialog = function(pdf, filename) {
            pdfDialogProgress = new $.Deferred();
            app.displayBlocker(true);

            var pages = pdf.numPages;

            if (typeof filename !== 'undefined') {
                pdfDialog.find('.head .filename').html(filename);
            }
            pdfDialog.find('.head .selection .total').html(pages);
            pdfDialogCenter.html('');

            pdfDialog.show();

            for (var i = 1; i <= pages; i++) {
                addPage(i);
            }

            function addPage(num) {
                var htmlPage = $('<div>').addClass('page');
                var htmlImg = $('<img>').appendTo(htmlPage).hide();
                $('<div>').addClass('check').appendTo(htmlPage);

                pdfDialogCenter.append(htmlPage);

                pdf.getPage(num).then(function (page) {
                    // you can now use *page* here
                    var desiredWidth = 125;
                    var desiredHeight = 125;
                    var viewport = page.getViewport(1);
                    var scaleWidth = desiredWidth / viewport.width;
                    var scaleHeight = desiredHeight / viewport.height;
                    var scale = Math.min(scaleWidth, scaleHeight);

                    viewport = page.getViewport(scale);

                    var canvas = document.createElement('canvas');
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;

                    var renderContext = {
                        canvasContext: context,
                        viewport: viewport
                    };
                    page.render(renderContext).then(function () {
                        var src = canvas.toDataURL('image/jpeg');
                        htmlImg.attr('src', src).show();
                    });

                    htmlPage.data('filename', filename + '_page' + num);
                    htmlPage.data('pdfpage', page);
                    htmlImg.css({
                        top: '50%',
                        left: '50%',
                        'margin-left': '-' + canvas.width / 2 + 'px',
                        'margin-top': '-' + canvas.height / 2 + 'px',
                        position: 'absolute'
                    });
                });
            }

            return pdfDialogProgress.promise();
        };

        self.pdfPagesUpdateSelectedCount = function() {
            var num = pdfDialog.find('.center .page.checked').length;
            pdfDialog.find('.selection .selected').html(num);
        };

        self.initPdfPagesDialog = function() {
            pdfLoadingControl = new app.LoadingControl(pdfDialog.find('.head .progress-control'), false, false);

            pdfDialog.find('.close').on('click', function () {
                app.displayBlocker(false);
                pdfDialog.hide();
                pdfDialogCenter.html('');
                if (pdfDialogProgress)
                    pdfDialogProgress.resolve();
            });

            pdfDialog.find('.buttons .select-all').on('click', function () {
                var $availablePages = pdfDialogCenter.find('.page').not('.processed');
                var totalAvailable = $availablePages.length;
                var checked = $availablePages.filter('.checked').length;
                if (totalAvailable == checked) { // all checked - remove selection
                    $availablePages.removeClass('checked');
                } else {
                    $availablePages.addClass('checked');
                }
                self.pdfPagesUpdateSelectedCount();
            });
            pdfDialog.find('.buttons .insert').on('click', function () {
                self.pdfDialogDoInsert();
            });

            pdfDialog.delegate('.page', 'click', function () {
                if ($(this).is('.processed'))
                    return;

                $(this).toggleClass('checked');
                self.pdfPagesUpdateSelectedCount();
            });
        };

        self.pdfDialogDoInsert = function() {
            var $checkedPages = pdfDialogCenter.find('.page.checked');
            var $uncheckedPages = pdfDialogCenter.find('.page').not('.checked');
            $uncheckedPages.hide();

            var totalUploads = $checkedPages.length;
            var finishedUploads = 0;
            var uploads = [];

            pdfLoadingControl.displayLoadingProgress(finishedUploads, totalUploads);

            $checkedPages.each(function () {
                var pageHtml = $(this);
                var page = pageHtml.data('pdfpage');
                var filename = pageHtml.data('filename');
                var promise = self.uploadPdfPage(page, filename);
                promise.then(function(){
                    pageHtml.removeClass('checked');
                    pageHtml.addClass('processed');
                    self.pdfPagesUpdateSelectedCount();

                    finishedUploads++;
                    pdfLoadingControl.displayLoadingProgress(finishedUploads, totalUploads);
                });
                uploads.push(promise);
            });
            $.when.apply($, uploads).always(function(){
                pdfLoadingControl.hideLoadingProgress();
                $uncheckedPages.show();
            });
        };

        self.uploadPdfPage = function (page, filename) {
            var progress = new $.Deferred();
            var desiredWidth = 1024;
            var desiredHeight = 1024;
            var viewport = page.getViewport(1);
            var scaleWidth = desiredWidth / viewport.width;
            var scaleHeight = desiredHeight / viewport.height;
            var scale = Math.min(scaleWidth, scaleHeight);

            viewport = page.getViewport(scale);

            var canvas = document.createElement('canvas');
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            var renderContext = {
                canvasContext: context,
                viewport: viewport
            };
            page.render(renderContext).then(function () {
                var src = canvas.toDataURL('image/jpeg');

                uploadDataUrlImage(src, filename + '.jpg').then(function (result) {

                    if (typeof result.errors !== 'undefined' && result.errors.length) {
                        console.error(result.errors);
                        window.alert(result.errors.join('/n'));
                    }

                    if (result && result.covers) {
                        app.addUploadedCovers([result.covers[0]]);
                        progress.resolve();
                    } else {
                        progress.reject();
                    }

                }, function(){
                    progress.reject();
                });
            });

            return progress.promise();
        };

        /**
         * upload cover dataURL to server
         * @param {string} dataURL
         * @param {string} filename
         * @param {number?} id
         * @returns {*}
         */
        function uploadDataUrlImage(dataURL, filename, id) {
            var process = new $.Deferred();
            var dataBlob = dataURLtoBlob(dataURL);
            var formData = new FormData();
            formData.append('image', dataBlob, filename);
            formData.append('edition_id', editionId);
            if (id)
                formData.append('id', id);

            $.ajax({
                url: 'covers',
                type: 'POST',
                xhr: function () {  // custom xhr
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) { // if upload property exists
                        myXhr.upload.addEventListener('progress', function (e) {
                            process.notify(e);
                        }, false); // progressbar
                    }
                    return myXhr;
                },
                success: function (data) {
                    process.resolve(data);
                },
                error: function (e) {
                    process.reject(e);
                },
                data: formData,
                processData: false,
                contentType: false
            });

            return process.promise();
        }

        function dataURLtoBlob(dataURL) {
            //Decode the dataURL
            var parts = dataURL.split(',');
            var type = parts[0].match(/^data:([^;]*);/)[1];
            var binary = atob(parts[1]);
            // Create 8-bit unsigned array
            var array = [];
            var i = 0;
            while (i < binary.length) {
                array.push(binary.charCodeAt(i));
                i++;
            }
            // Return our Blob object
            return new Blob([new Uint8Array(array)], {type: type});
        }


        return this;
    }

    app.coversList = new CoversList();
})(window.app);
