"use strict";
$(function () {

	init_profile();
	init_changePassword();

	function init_profile() {
		var profile_block = $(".profile-block");

		profile_block.find(".gender-select").niceSelect();

		$(".bottom-buttons .btn-save").on("click", function(e){
			e.preventDefault();

			var data = {};
			data.login = profile_block.find("input[name=login]").val();
			data.first_name = profile_block.find("input[name=first_name]").val();
			data.surname = profile_block.find("input[name=surname]").val();
			data.gender = profile_block.find(".gender-select").val();
			data.email = profile_block.find("input[name=email]").val();

			$.ajax({url:"profile/save", data:data, method:"post"}).then(function(result){
				if (typeof result === 'object') {
					if (result.status === 'ok') {
						showMessage("saved", "ok", 5000);
					} else {
						console.error(result);
						showError(result.errors.join("<br>"));
					}
				} else {
					console.error(result);
					showError(result);
				}
			});
		});
	}

	function init_changePassword() {
		var change_password_modal = $("#password_modal");

		$(".change-password").on("click", function (e) {
			e.preventDefault();

			change_password_modal.find("form").trigger("reset");
			change_password_modal.modal('show');
		});

		change_password_modal.find(".save").on("click", function (e) {
			e.preventDefault();

			var data = {};

			data.password = change_password_modal.find("input[name=password]").val();
			data.password_confirm = change_password_modal.find("input[name=password_confirm]").val();

			$.ajax({url:"profile/changePassword", data:data, method:"post"}).then(function(result){
				if (typeof result === 'object') {
					if (result.status === 'ok') {
						showMessage("password updated", "ok", 3000);
						change_password_modal.modal('hide');
					} else {
						console.error(result);
						showError(result.errors.join("<br>"));
					}
				} else {
					console.error(result);
					showError(result);
				}
			});
		});
	}
	

});
