'use strict';
$(function () {

    init();

    function init() {
        $('.action.cloud-space-plan-change').on('click', function (e) {
            e.preventDefault();

            var cloudSpacePlanChangeModal = $('#cloud-space-plan-change-modal');

            cloudSpacePlanChangeModal.data('plan-id', $(this).data('plan-id'));
            cloudSpacePlanChangeModal.find('.description-plan-space').html($(this).data('plan-space'));
            cloudSpacePlanChangeModal.find('.description-plan-price').html($(this).data('plan-price'));
            cloudSpacePlanChangeModal.modal('show');

            cloudSpacePlanChangeModal.find('.action.confirm-change').on('click', function (e) {
                e.preventDefault();

                var data = {};
                data.plan_id = cloudSpacePlanChangeModal.data('plan-id');

                $.ajax({
                    url: 'license/cloud-space-plan-change',
                    data: data,
                    method: 'post'
                }).then(function (response) {
                    if (typeof response === 'object') {
                        if (response.status === 'ok') {
                            showMessage(response.message, 'ok', 5000);
                            cloudSpacePlanChangeModal.modal('hide');

                            if (response.redirect) {
                                setTimeout(function () {
                                    window.location = response.redirect;
                                }, 2000);
                            }
                        } else {
                            console.error(response);
                            showError(response.message);
                        }
                    } else {
                        console.error(response);
                        showError(response);
                    }
                });
            });
        });
    }

});
