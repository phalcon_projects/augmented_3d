# VisionPaper project setup #

## Environment setup ##

* Setup the PHP server (PHP 7+), MySQL database
* Install the [phalcon](https://phalconphp.com/en/) depending on OS
* Install the [phalcon development tools](https://phalconphp.com/en/download/tools)
* Install the latest stable [node.js](https://nodejs.org/en/) release
* Install the [composer](https://getcomposer.org/)

## Project setup ##

* Install the required dependencies for the project by running following commands in the console in the project root directory:
`composer update` and `npm install`
* Create the `config.ini.dev` file at the `/app/config/` folder (as the example there is can be used `config.ini` file)
 and specify the DB and the other environment variables according to the server settings
* In the console run the command `phalcon migration run` in the project root directory
* Generate stylesheets by running in the console in phe project root directory command `npm run scss`


Navigate to the project website and login with followed credentials:
* username: `admin`
* password: `12345`

Create something amazing!