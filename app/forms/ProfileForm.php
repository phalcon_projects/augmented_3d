<?php

use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class ProfileForm extends Form
{
    /**
     * Initialize the profile form
     */
    public function initialize($entity = null, $options = array())
    {

        if (!isset($options['edit'])) {
            $element = new Text("id");
            $this->add($element->setLabel("Id"));
        } else {
            $this->add(new Hidden("id"));
        }

        $login = new Text("login");
		$login->setLabel("login");
		$login->setFilters(array('striptags', 'string'));
		$login->addValidators(array(
			new Uniqueness(array(
				"message" => "this login is already registered"
			)),
            new PresenceOf(array(
                'message' => 'login is required'
            )),
			new StringLength(array(
				'message' => 'login is too long',
				'max' => 255
			))
        ));
        $this->add($login);

		$field = new Text("first_name");
		$field->setLabel("first name");
		$field->setFilters(array('striptags', 'string'));
		$field->addValidators(array(
			new PresenceOf(array(
				'message' => 'name is required'
			)),
			new StringLength(array(
				'message' => 'name is too long',
				'max' => 255
			))
		));
		$this->add($field);

		$field = new Text("surname");
		$field->setLabel("surname");
		$field->setFilters(array('striptags', 'string'));
		$field->addValidators(array(
			new StringLength(array(
				'message' => 'surname is too long',
				'max' => 255
			))
		));
		$this->add($field);

		$field = new Select(
			"gender",
			[
				"" => "n/a",
				"male" => "male",
				"female" => "female",
			]
		);
		$this->add($field);


		$field = new Phalcon\Forms\Element\Email("email");
		$field->setLabel("email");
		$field->setFilters(array('email', 'striptags', 'string'));
		$field->addValidators(array(
			new Uniqueness(array(
				"message" => "this email is already registered"
			)),
			new Email(array(
				'message' => 'Not a valid email'
			))
		));
		$this->add($field);
    }
}