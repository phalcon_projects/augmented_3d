<?php

use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class ProjectEditForm extends Form
{
    /**
     * Initialize the app form
     */
    public function initialize($entity = null, $options = array())
    {
        if (!isset($options['edit'])) {
            $element = new Text('id');
            $this->add($element->setLabel('Id'));
        } else {
            $this->add(new Hidden('id'));
        }

        $title = new Text('title');
		$title->setLabel('title');
		$title->setFilters(array('striptags', 'string'));
		$title->addValidators(array(
            new PresenceOf(array(
                'message' => 'title is required'
            )),
			new StringLength(array(
				'message' => 'title is too long',
				'max' => 255
			))
        ));
        $this->add($title);

        $uuid = new Text('uuid');
        $uuid->setLabel('uuid');
        $uuid->setFilters(array('striptags', 'string'));
        $uuid->addValidators(array(
            new Uniqueness(array(
                'message' => 'the project with specified uuid is already registered'
            )),
            new PresenceOf(array(
                'message' => 'uuid is required'
            )),
            new StringLength(array(
                'message' => 'uuid is too long',
                'max' => 255
            ))
        ));
        $this->add($uuid);
    }
}