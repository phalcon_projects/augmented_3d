<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;

class ChangePasswordForm extends Form
{
	public function initialize()
	{
		// Password
		$password = new Password('password');
		$password->addValidators([
			new PresenceOf([
				'message' => 'Password is required'
			]),
//			new StringLength([
//				'min' => 8,
//				'messageMinimum' => 'Password is too short. Minimum 8 characters'
//			]),
			new Confirmation([
				'message' => 'Password doesn\'t match confirmation',
				'with' => 'password_confirm'
			])
		]);
		$this->add($password);
		// Confirm Password
		$confirmPassword = new Password('password_confirm');
		$confirmPassword->addValidators([
			new PresenceOf([
				'message' => 'The confirmation password is required'
			])
		]);
		$this->add($confirmPassword);
	}
}