<?php

use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class AppForm extends Form
{
    /**
     * Initialize the app form
     */
    public function initialize($entity = null, $options = array())
    {
        if (!isset($options['edit'])) {
            $element = new Text('id');
            $this->add($element->setLabel('Id'));
        } else {
            $this->add(new Hidden('id'));
        }

        $name = new Text('name');
		$name->setLabel('name');
		$name->setFilters(array('striptags', 'string', 'trim'));
		$name->addValidators(array(
			new Uniqueness(array(
				'message' => 'the app with specified name is already registered'
			)),
            new PresenceOf(array(
                'message' => 'name is required'
            )),
			new StringLength(array(
				'message' => 'name is too long',
				'max' => 255
			))
        ));
        $this->add($name);

		$version = new Text('version');
		$version->setLabel('version');
		$version->setFilters(array('striptags', 'string'));
		$version->setDefault('1.0');
		$version->addValidators(array(
			new StringLength(array(
				'message' => 'version is too long',
				'max' => 45
			))
		));
		$this->add($version);
    }
}