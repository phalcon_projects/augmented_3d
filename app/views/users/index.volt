<div class="content ui_block users">
    <div class="row block-header">
        <div class="col-sm-8">
            <h5>{{ 'Users' }}</h5>
        </div>
        <div class="col-sm-4 pull-right">
            <a class="btn primary action" href="/teams/add">add team</a>
        </div>
    </div>

    <div class="row block-content">
        <table class="list">
            <thead>
            <tr>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {% for item in page.items %}
            <tr>
                <td>{{ item.first_name ~ ' ' ~item.surname}}</td>
                <td>{{ item.login }}</td>
                <td>{{ item.email }}</td>
                <td><a href="/teams/edit/"{{ item.id }} class="btn action edit">edit</a></td>
            </tr>
            {% endfor %}
            </tbody>
        </table>
    </div>
</div>