<br>
{% for teamItem in teams %}
    <div class="row team-access" data-team-id="{{ teamItem.id }}">
        <div class="col-8">
            <div class="title">{{ teamItem.title }}</div>
        </div>
        <div class="col-4">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" value=""{{ teamItem.access >= 1 ? 'checked' : '' }}>
                </label>
            </div>
        </div>
    </div>
    <br>
{% endfor %}