{%- macro format_filesize(size) %}
    {% if not size %}
        {% return "0 mb" %}
    {% endif %}

    {% set kb = 1024 %}
    {% set mb = kb*1024 %}
    {% set gb = mb*1024 %}

    {% set val = size %}

    {% if size < kb %}
        {% return val ~ " b" %}
    {% elseif size < mb %}
        {% set val = size/kb %}
        {% set units = "kb" %}
    {% elseif size < gb %}
        {% set val = size/mb %}
        {% set units = "mb" %}
    {% else %}
        {% set val = size/gb %}
        {% set units = "gb" %}
    {% endif %}

    {% return "%01.1f"|format(val) ~ " " ~ units %}
{%- endmacro %}

<svg style="display:none;">
    <symbol id="pencil-icon" viewBox="0 0 23.65 23.65" xmlns="http://www.w3.org/2000/svg"><defs><style>.axcls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>pencil-icon</title><path class="axcls-1" d="M104.29 262.67l-6.47 1.52 1.52-6.46 15.6-15.6a2 2 0 0 1 2.83 0l2.12 2.12a2 2 0 0 1 0 2.83z" transform="translate(-97.33 -241.04)"/><path class="axcls-1" d="M21.86 6.75l-4.95-4.96m3.53 6.38L15.5 3.21M7.22 21.39l-4.95-4.95"/></symbol>
    <symbol id="lock-mode-white" viewBox="0 0 18 24" xmlns="http://www.w3.org/2000/svg"><defs><style>.aucls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>lock-mode-white</title><circle class="aucls-1" cx="9" cy="16" r="2"/><rect class="aucls-1" x=".5" y="9.5" width="17" height="14" rx="4" ry="4"/><path class="aucls-1" d="M1039.5 1975a5.5 5.5 0 0 1 11 0v3.63" transform="translate(-1036 -1969)"/></symbol>
    <symbol id="delete-icon" viewBox="0 0 23 24" xmlns="http://www.w3.org/2000/svg"><defs><style>.apcls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>delete-icon</title><path class="apcls-1" d="M3 3.5h16v20H3zm4-3h8v3H7zm-6.5 3h22M7 7v12m4-12v12m4-12v12"/></symbol>
</svg>

<div class="content-list projects">
    <div class="list-title">projects</div>
    <div class="row list-items">
        {% for item in page.items %}
            {% set totalSize = item.getTotalFileSize() %}
            <div class="col col-12 list-item"
                 data-project-id="{{ item.id }}"
                 data-project-title="{{ item.title }}"
                 data-project-uuid="{{ item.uuid }}"
                >
                <div class="row">
                    <div class="col-3">
                        <div class="image">
                            {% if item.image %}
                                <img src="{{ item.image }}" alt="">
                            {% else %}
                                <div></div>
                            {% endif %}
                        </div>
                    </div>

                    <div class="col info">
                        <a href="{{ url('projects/' ~ item.id) }}" class="title">{{ item.title }}</a>
                        <div class="pages">UUID: {{ item.uuid }}</div>
                        <div class="pages">{{ item.getEditionsCount() }} editions</div>
                        <div class="memory">
                            <img src="img/projects/icon-h.png" alt="">
                            <span>{{ format_filesize(totalSize) }}</span>
                        </div>
                    </div>
                    <div class="col col-12 actions">
                        <div class="row justify-content-between">
                            <div class="col col-3">
                                <div class="row icon-wrapper">
                                    <div class="col icon-action">
                                        <a href="#" data-project-id="{{ item.id }}" title="edit" class="action edit">
                                            {#<img src="/img/icons/pencil-icon.png">#}
                                            <svg class="svg-pencil-icon-dims" >
                                                <use xlink:href="#pencil-icon"></use>
                                            </svg>
                                        </a>
                                    </div>
                                    <div class="col icon-action">
                                        <a href="#" title="access" class="action access" data-project-id="{{ item.id }}">
                                            {#<img src="/img/icons/lock-mode.png">#}
                                            <svg class="svg-lock-mode-white-dims">
                                                <use xlink:href="#lock-mode-white"></use>
                                            </svg>
                                        </a>
                                    </div>
                                    <div class="col icon-action">
                                        <a href="#" data-id="{{ item.id }}" title="delete" class="action delete">
                                            {#<img src="/img/icons/delete-icon.png">#}
                                            <svg class="svg-delete-icon-dims">
                                                <use xlink:href="#delete-icon"></use>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-4">
                                <a href="{{ url('projects/' ~ item.id) }}" class="btn btn-primary action editions">
                                    <img src="img/projects/icon-edition.png" alt="">
                                    <span>editions</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {% endfor %}
    </div>

    <div class="list-bottom">
        <a href="" class="btn btn-primary action add">add project</a>
        {% if page.total_pages>1 %}
            <ul class="pagination">
                {% for i in 1..page.total_pages %}
                    <li class="page-wrapper">
                        <a href="{{ url('/projects/?page=' ~ i) }}"
                           class="page{% if page.current == i %} active{% endif %}">{{ i }}</a>
                    </li>
                {% endfor %}
            </ul>
        {% endif %}
    </div>
</div>

<div class="modal" id="modal-project">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="modal-project-title">Add project</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="/img/scene/icons/plus.png"
                             style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                    </span>
                </button>
            </div>
            <form action="">
                <input type="hidden" name="project-id">
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <label for="project-title"></label>
                                <input class="form-control" id="project-title" placeholder="Project title" name="project-title">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="project-uuid"></label>
                                <input class="form-control" id="project-uuid" name="project-uuid"
                                       placeholder="Project UUID" aria-describedby="project-uuid-help">
                                <small id="project-uuid-help" class="text-danger form-text hidden"></small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-actions">
                    <div class="row">
                        <div class="col-sm-12">
                            <button id="modal-action-save" type="button" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal-project-access">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="modal-project-title">Manage project access</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="/img/scene/icons/plus.png"
                             style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h4>Select teams to allow access to the project</h4>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col teams-list">

                        </div>
                    </div>
                </div>
            </div>
                <div class="modal-footer modal-actions">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            {#</div>#}
        </div>
    </div>
</div>