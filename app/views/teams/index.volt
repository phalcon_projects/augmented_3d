<div class="ui_block teams">
    <div class="row block-header">
        <div class="col-sm-8">
            <h5>{{ 'Teams' }}</h5>
        </div>
        <div class="col-sm-4 pull-right text-center">
            <a id="team-add" class="btn btn-primary action" href="#">create team</a>
        </div>
    </div>

    <div class="list-items">
        {% for item in page.items %}
            <div class="list-item team row"
                 data-team-id="{{ item.id }}"
                 data-team-title="{{ item.title }}">
                <div class="team-title col">
                    <span>{{ item.title }}</span>
                </div>
                <div class="col col-4">
                    <a href="#" class="members-detail">members: {{ item.members | length }}</a>
                </div>
                <div class="col actions text-center">
                    <div class="row">
                        {% if isCurrentUserAdmin %}
                        <div class="col col-12">
                            <a href="#" class="btn btn-success action invite">invite</a>
                        </div>
                        <div class="col col-12">
                            <a href="#" class="btn btn-default action add">add</a>
                        </div>
                        <div class="col col-12">
                            <a href="#" class="btn btn-default action edit">edit</a>
                        </div>
                        <div class="col col-12">
                            <a href="#" class="btn btn-danger action remove delete">delete</a>
                        </div>
                        {% endif %}
                    </div>
                </div>
                <div class="team-members container" data-team-id="{{ item.id }}">
                    {% for member in item.members %}
                        <div class="team-member row">
                            <div class="col col-8 team-member-title-wrapper">
                            <span class="team-member-title">{{ member.user.first_name
                                ? (member.user.first_name ~ ' ' ~ member.user.surname)
                                : member.user.login }}</span>
                            </div>
                            <div class="col col-4">
                                {% if (isCurrentUserAdmin) %}
                                    <a class="action remove remove-member"
                                       data-user-id="{{ member.user.id }}"><img src="/img/icons/delete-icon.png"></a>
                                {% endif %}
                            </div>
                        </div>
                    {% endfor %}
                </div>
            </div>
        {% endfor %}
    </div>
</div>

<div class="modal fade" id="modal-team-edit" tabindex="-1" role="dialog" aria-labelledby="modal-team-edit-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title" id="modal-team-edit-label"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <img src="/img/scene/icons/plus.png"
                         style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                </span></button>
            </div>

            {#<div class="modal-title">#}
                {#<div class="title" id="modal-team-edit-label"></div>#}
            {#</div>#}
            <div class="modal-body">
                <div class="container">
                    <form class="form team" id="team-form" autocomplete="off">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="title">title</label>
                                <div class="input-col">
                                    <input id="title" name="title" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h6 class="sub-title">Parameters</h6>
                            </div>
                        </div>

                        <div class="row parameters" id="parameters">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox"
                                               id="permission_projects_management"
                                               name="permission_projects_management">
                                        <span>Can manage projects</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox"
                                               id="permission_apps_management"
                                               name="permission_apps_management">
                                        <span>Can manage apps</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value=""
                                               id="permission_editions_management"
                                               name="permission_editions_management">
                                        <span>Can manage editions</span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value=""
                                               id="permission_pages_management"
                                               name="permission_pages_management">
                                        <span>Can manage pages</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer modal-actions text-center">
                <a href="#" class="btn btn-secondary action cancel" data-dismiss="modal">Cancel</a>
                <a href="#" class="btn btn-primary action save" data-team-id="">Save</a>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="modal-team-members" tabindex="-1" role="dialog" aria-labelledby="modal-team-members-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-team-members-label">Manage "<span id="modal-team-members-title"></span>"
                    team</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <img src="/img/scene/icons/plus.png"
                         style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                </span></button>
            </div>
            {#<button type="button" class="close" data-dismiss="modal" aria-label="Close">#}
                {#<img src="img/close.png" alt="Close">#}
            {#</button>#}

            {#<div class="modal-title">#}
                {#<div class="title" id="modal-team-members-label">Manage "<span id="modal-team-members-title"></span>"#}
                    {#team#}
                {#</div>#}
            {#</div>#}

            <div class="list-content"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-team-invite" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Invite to the &quot;<span class="title" id="modal-invite-team-title"></span>&quot;
                    team</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <img src="/img/scene/icons/plus.png"
                         style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                </span></button>
            </div>

            {#<div class="modal-title">#}
                {#<div class="title" id="modalLabel">Invite to the &quot;<span id="modal-invite-team-title"></span>&quot;#}
                    {#team#}
                {#</div>#}
            {#</div>#}
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="email">Enter email</label>
                            <div class="input-col email">
                                <input id="email" name="email" placeholder="example@company.com">
                                <span class="error danger" style="color: red;">Please specify a valid email address</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer modal-actions text-center">
                <a href="#" class="btn btn-secondary action cancel" data-dismiss="modal">cancel</a>
                <a href="#" class="btn btn-primary action invite">invite</a>
            </div>
        </div>

    </div>
</div>

<div class="modal fade team-add" id="modal-team-add" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title title" id="modalLabel">Add user to the "<span id="modal-add-team-title"></span>" team</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <img src="/img/scene/icons/plus.png"
                         style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                </span></button>
            </div>

            {#<div class="modal-title">#}
                {#<div class="title"#}
                     {#id="modalLabel">Add user to the &quot;<span id="modal-add-team-title"></span>&quot; team#}
                {#</div>#}
            {#</div>#}
            <div class="modal-body">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                            <label for="user">Select user to add</label>
                            <div class="input-col">
                                <select id="user-for-add" name="user">
                                    <option value="" disabled selected>Select user</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer modal-actions">
                <a href="#" class="btn btn-secondary action cancel" data-dismiss="modal">cancel</a>
                <a href="#" class="btn btn-primary action add" data-team-id="">add</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-team-member-remove-confirm" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title title" id="modalLabel">Are you sure you want to remove &quot;<span id="modal-team-title"></span>&quot; team member?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <img src="/img/scene/icons/plus.png"
                         style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                </span></button>
            </div>
            {#<div class="modal-title">#}
                {#<div class="title"#}
                     {#id="modalLabel"#}
                {#>Are you sure you want to remove &quot;<span id="modal-team-title"></span>&quot; team member?#}
                {#</div>#}
            {#</div>#}
            <div class="modal-body"></div>
            <div class="modal-footer modal-actions text-center">
                <a href="#" class="btn btn-secondary action cancel" data-dismiss="modal">cancel</a>
                <a href="#" class="btn btn-primary action remove-confirm" data-team-id="">confirm</a>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="modal-team-delete-confirm" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">Are you sure you want to delete &quot;<span id="modal-delete-team-title"></span>&quot; team??</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <img src="/img/scene/icons/plus.png"
                         style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                </span></button>
            </div>
            <div class="modal-body">

            </div>

            {#<div class="modal-title">#}
                {#<div class="title" id="modalLabel">Are you sure you want to delete &quot;<span id="modal-delete-team-title"></span>&quot; team?</div>#}
            {#</div>#}
            <div class="modal-footer modal-actions text-center">
                <a href="#" class="btn btn-secondary action cancel" data-dismiss="modal">cancel</a>
                <a href="#" class="btn btn-primary action delete-confirm" data-team-id="">confirm</a>
            </div>
        </div>

    </div>
</div>