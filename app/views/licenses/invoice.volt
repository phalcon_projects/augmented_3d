<style type="text/css">
    @import url("https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700");

    body {
        position: relative;
        min-height: 100vh;
        font-family: "Open Sans", sans-serif;
        font-size: 14px;
        overflow: visible;
    }

    .header-info {
        position: absolute;
        right: 50px;
    }

    .header-info table td {
        padding: 2px 0;
    }

    .main {
        position: relative;
        margin: 170px 35px;
    }

    .text-right {
        text-align: right;
    }

    .text-left {
        text-align: left;
    }

    .text-bold {
        font-weight: bold;
    }

    .header-info .title {
        font-size: 18px;
        font-weight: 600;
    }

    table {
        width: 100%;
        border-spacing: 0;
        border-collapse: collapse;
    }

    img.logo {
        position: absolute;
        left: 70px;
        width: 280px;
    }

    .decoration-left {
        position: absolute;
        top: 2px;
        left: -79px;
    }

    .decoration-left img {
        width: 55px;
        height: 180px;
    }

    .decoration-right {
        position: absolute;
        width: 31px;
        height: 106%;
        top: -26px;
        right: -26px;
        background-color: #f9c322;
    }

    .main .info .delivery {
        margin-top: 90px;
    }

    .main .order {
        width: 100%;
        margin-top: 30px;
    }

    .main .order .text-bold {
        font-weight: 600;
    }

    .main .order th {
        padding: 5px 0;
    }

    .main .order .calculation td,
    .main .order .product td {
        border-bottom: 1px #000 solid;
    }

    .main .order .calculation td,
    .main .order .product td,
    .main .order .total td {
        padding: 10px 0;
    }

    .main .order .total td {
        font-weight: 600;
    }

    .main .order p {
        margin: 5px 0;
    }

    .additional-info .row .col {
        margin: 10px 0 2px 0;
    }

    .additional-info p {
        padding: 2px 0;
        margin: 0;
    }

    .greetings {
        margin: 10px 0;
    }

    .footer-company-info {
        margin-left: 70px;
    }

    .footer-company-info .content-wrapper {
        border-left: 1px #000 solid;
        padding: 0 5px;
    }

    .footer-company-info .content-wrapper p {
        font-size: 12px;
        margin: 2px;
    }
</style>
<page backtop="7mm" backbottom="7mm" backleft="10mm" backright="5mm">
    <page_header>

        <img class="logo" src="{{ appPath }}/public/img/invoice/logo.gif" alt="Cosomedia GmbH & Co. KG">

        <div class="header-info">
            <table>
                <thead>
                <tr>
                    <th colspan="2" class="text-right" style="padding: 10px 0;"><span class="title"><?php echo $t->_('invoice') ?></span></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text-right"><?php echo $t->_('invoice'); echo $t->_('no') ?>.:</td>
                    <td class="text-right" style="padding-left: 20px;">{{ order.number }}</td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo $t->_('customer'); echo $t->_('no') ?>.:</td>
                    <td class="text-right" style="padding-left: 20px;">{{ orderCustomer.client_id }}</td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo $t->_('customer VAT ID'); echo $t->_('no') ?>.:</td>
                    <td class="text-right" style="padding-left: 20px;">{{ orderCustomer.customer_vat_id }}</td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo $t->_('date') ?>:</td>
                    <td class="text-right" style="padding-left: 20px;"><?php echo date('d.m.Y', strtotime($order->created_at)) ?></td>
                </tr>
                <tr>
                    <td class="text-right"><?php echo $t->_('delivery date') ?>:</td>
                    <td class="text-right" style="padding-left: 20px;"><?php echo date('d.m.Y', strtotime($order->created_at)) ?></td>
                </tr>
                </tbody>
            </table>
        </div>

    </page_header>
    <page_footer>
        <div class="footer-company-info container">
            <table style="width: 100%;">
                <tbody>
                <tr>
                    <td style="width: 30%;">
                        <div class="content-wrapper">
                            <p>cosomedia GmbH & Co. KG</p>
                            <p>Am Langen Hahn 21</p>
                            <p>33100 Paderborn</p>
                            <p>Tel.: 0 52 93 / 93 27 6 - 0</p>
                            <p>Fax: 0 52 93 / 93 27 6 - 29</p>
                            <p><a href="mailto:info@cosomedia.de">info@cosomedia.de</a></p>
                            <p><a href="http://www.cosomedia.de">www.cosomedia.de</a></p>
                        </div>
                    </td>
                    <td style="width: 30%;">
                        <div class="content-wrapper">
                            <p>USt-IdNr.: DE272801978</p>
                            <p>Amtsgericht Paderborn HRA 5941</p>
                            <p>Geschäftsführer: Björn Wedler</p>
                        </div>
                    </td>
                    <td style="width: 33%;">
                        <div class="content-wrapper">
                            <p>COSOMEDIA GMBH & CO.KG</p>
                            <p>Postbank</p>
                            <p>IBAN: DE25 2501 0030 0011 1303 00</p>
                            <p>BIC: PBNKDEFF</p>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </page_footer>

    <div class="decoration-right">
    </div>

    <div class="main">
        <div class="decoration-left">
            <img src="{{ appPath }}/public/img/invoice/invoice-side-image.png" alt="">
        </div>

        <div class="container info">
            <div class="row company">
                <div class="col">
                    <table class="table table-condensed">
                        <tbody>
                        <tr>
                            <td>Stoffwelten GmbH</td>
                        </tr>
                        <tr>
                            <td>Herrn Björn Wegge</td>
                        </tr>
                        <tr>
                            <td>Johannes-Höschen-Straße 18</td>
                        </tr>
                        <tr>
                            <td>33165 Lichtenau</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row delivery">
                <div class="col">
                    <span><?php echo $t->_('invoice-delivery-info') ?></span>
                </div>
            </div>
        </div>

        <div class="container order">
            <table style="width: 100%;">
                <thead>
                <tr>
                    <th class="text-bold text-left" style="width: 70%;"><b><?php echo $t->_('title') ?></b></th>
                    <th class="text-bold text-right" style="width: 30%;"><b><?php echo $t->_('total') ?> €</b></th>
                </tr>
                </thead>
                <tbody>
                <?php $total = 0;?>
                {% for orderItem in orderItems %}
                <?php $total += $orderItem->price; ?>
                <tr class="product">
                    <td>
                        <p class="text-bold"><b>{{ orderItem.title }}</b></p>
                        <p><?php echo $t->_('charges-rent-monthly') ?>*</p>
                    </td>
                    <td class="text-right">
                        <p>{{ orderItem.price }}</p>
                    </td>
                </tr>
                {% endfor %}
                <tr class="calculation">
                    <td>
                        <p><?php echo $t->_('subtotal') ?> (<?php echo $t->_('net') ?>)</p>
                        <p><?php echo $t->_('value added tax') ?> 19 %</p>
                    </td>
                    <td class="text-right">
                        <p>{{ total }}</p>
                        <p>{{ total * 0.19 }}</p>
                    </td>
                </tr>
                <tr class="total">
                    <td>
                        <p><b><?php echo $t->_('total amount') ?></b></p>
                    </td>
                    <td class="text-right">
                        <p><b>{{ total + total * 0.19 }}</b></p>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="container additional-info">
            <div class="row">
                <div class="col">
                    <p><?php echo $t->_('payable immediately, net') ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col grades">
                    <p>* <?php echo $t->_('additional-info-product-usage') ?></p>
                    <p><?php echo $t->_('up to') ?> 5.000 <?php echo $t->_('users') ?> – 49 €</p>
                    <p><?php echo $t->_('up to') ?> 10.000 <?php echo $t->_('users') ?> – 99 €</p>
                    <p><?php echo $t->_('up to') ?> 25.000 <?php echo $t->_('users') ?> – 199 €</p>
                    <p><?php echo $t->_('up to') ?> 50.000 <?php echo $t->_('users') ?> – 299 €</p>
                    <p><?php echo $t->_('up to') ?> 100.000 <?php echo $t->_('users') ?> – 499 €</p>
                    <p><?php echo $t->_('from') ?> 100.000 <?php echo $t->_('users') ?> – <?php echo $t->_('recalculation') ?></p>
                </div>
            </div>
        </div>

        <div class="container greetings">
            <div class="row">
                <div class="col">
                    <p><?php echo $t->_('many thanks') ?></p>
                </div>
            </div>
        </div>

    </div>
</page>