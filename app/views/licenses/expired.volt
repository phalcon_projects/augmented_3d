<div class="license center-block" style='margin-top: 200px'>
    <div class=" text-center">
        <h1 class="display-4">Your have license no active license</h1>
        <br>
        <p class="lead">To get full access to the service you need the license</p>
        <br>
        <a href="/licenses" class="btn btn-primary">Buy license</a>
        <hr class="my-4">
        <div class="title">
            <img src="img/logo.svg" class="company-logo" style='height: 45px'>
        </div>
    </div>
</div>