<div class="content ui_block plans cloud-storage">
    <section>

        <div class="row block-header">
            <h5 class="block-title">Cloud space plans</h5>
        </div>

        <div class="row block-content">

            {% for planItem in planItems %}
                <div class="col-sm-12 col-md-4">
                    <div class="plan card" data-id="{{ planItem['id'] }}">
                        <div class="card-body">
                            <div class="card-content">
                                <div class="section title">
                                    <h1 class="title">{{ planItem['space'] }}</h1>
                                </div>
                                <div class="section price">
                                    <h4 class="price">{% if planItem['price'] === 0 %}Free{% else %}{{ planItem['price'] }}{% endif %}{% if planItem['price'] !== 0 %} €{% endif %}</h4>
                                </div>
                                <div class="section content">
                                    <p class="content">
                                        <span class="state{% if currentPlanId !== planItem['id'] %} hidden{% endif %}"
                                        >Current plan</span>
                                        <a href="#"
                                           class="btn primary action cloud-space-plan-change
                                            {% if currentPlanId == planItem['id'] %}hidden{% endif %}"
                                           data-plan-space="{{ planItem['space'] }}"
                                           data-plan-price="{{ planItem['price'] }}"
                                           data-plan-id="{{ planItem['id'] }}">Choose plan</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {% endfor %}

        </div>

    </section>
</div>

<div class="modal fade" id="cloud-space-plan-change-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="img/close.png" alt="">
            </button>

            <div class="modal-title">
                <div class="title" id="modalLabel">Change cloud plan</div>
            </div>

            <div class="content">
                <p>Confirm cloud space plan changing to <span class="description-plan-space text-highlighted"></span>
                </p>
                <p>The new plan price is <span class="description-plan-price text-highlighted"></span> for month</p>
            </div>
            <div class="modal-actions text-center">
                <a href="#" class="btn action cancel" data-dismiss="modal">cancel</a>
                <a href="#" class="btn primary action confirm-change">change</a>
            </div>
        </div>

    </div>
</div>