<div class="content ui_block license">
    <section>
        <div class="row block-header">
            <h5 class="block-title">Your license</h5>
        </div>

        <div class="row block-content">
            <table class="list">
                <tbody>
                {% for licenseItem in license_items %}
                    <tr>
                        <td class="sdk">{{ 'goOnAr 1.0 SDK' }}</td>
                        <td class="name">{{ 'unlimited apps' }}</td>
                        <td class="price"><span>{{ '4.999' }} ,-</span></td>
                        <td class="action pull-right">
                            <a class="btn primary action upgrade"
                               href="{{ url('/license/upgrade/?id=' ~ licenseItem.id) }}">Upgrade 2.0</a>
                        </td>
                    </tr>
                {% endfor %}
                </tbody>
            </table>
        </div>
        <div class="row block-footer">
            <div class="row payment paypal">
                <form id="form-payment-paypal" action="{{ paypalUrl }}" method="post">
                    <input type="hidden" name="business" value="{{ paypalId }}">

                    <input type="hidden" name="cmd" value="_xclick">

                    <input type="hidden" name="item_name" value="{{ licenseTitle }}">
                    <input type="hidden" name="item_number" value="{{ '1' }}">
                    <input type="hidden" name="amount" value="1">
                    <input type="hidden" name="currency_code" value="EUR">

                    <input type="hidden" name="cancel_return" value="{{ paymentCancelUrl }}">
                    <input type="hidden" name="return" value="{{ paymentSuccessUrl }}">

                    <div class="col pull-left vertical-align-middle">
                        <button id="payment-paypal-submit" type="submit" class="btn primary action pay">pay with paypal</button>
                    </div>
                    <div class="col pull-right payment-image">
                        <img src="img/payment/paypal.png"/>
                    </div>

                </form>
            </div>
        </div>
    </section>

    <section>
        <div class="row block-header">
            <h5 class="block-title">Invoices</h5>
        </div>

        <div class="row block-content">
            <table class="list">
                <tbody>
                <tr>
                    <td class="date">{{ '31.07.2017' }}</td>
                    <td class="number">Invoice #{{ order.number }}</td>
                    <td class="type"><span>{{ orderType }}</span></td>
                    <td class="action pull-right">
                        <a class="btn primary action download-pdf"
                           target="_blank"
                           href="{{ url('/license/invoice/' ~ order.id) }}">Download PDF</a>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </section>
</div>

<div class="row traffic-block">
    <div class="col-sm-6">
        <div class="ui_block traffic">
            <div class="row block-header">
                <div class="col pull-left title">
                    <h5 class="block-title">Traffic</h5>
                </div>
                <div class="col pull-right date">
                    <h5 class="date-range">01.06.2017-31.06.2017</h5>
                </div>
            </div>
            <div class="block-content">
                <div class="row">
                    <div class="col pull-left total">
                        <h1>1239</h1>
                    </div>
                    <div class="col pull-right image">
                        <img src="img/traffic.png"/>
                    </div>
                </div>
                <div class="row info">
                    <div class="col">
                        <h5>10.000 cloud view</h5>
                    </div>
                </div>
                <div class="row actions">
                    <div class="col action">
                        <a href="{{ url('license/traffic-plan/') }}"
                           class="btn primary action change">change package</a>
                    </div>
                    <div class="col description">
                        <h5>per month 199,- Euro</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="ui_block cloud-space">
            <div class="block-header">
                <h5>Cloud space</h5>
            </div>
            <div class="block-content">
                <div class="row space">
                    <div class="col-sm-12 col-md-10">
                        <div class="row space-content">
                            <div class="col-sm-12">
                                <div class="progress">
                                    <span>56 %</span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 pull-left">
                                <h5 class="storage-used">1200 mb</h5>
                            </div>
                            <div class="col-sm-12 col-md-6 pull-right">
                                <h5 class="storage-total text-right">2000 mb</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-2">
                        <img src="img/cloud-storage.png"/>
                    </div>
                </div>
            </div>
            <div class="row actions">
                <div class="col action">
                    <a href="{{ url('license/cloud-space-plan/') }}"
                       class="btn primary action change">buy more space</a>
                </div>
                <div class="col description">
                    <h5>per month 299,- Euro</h5>
                </div>
            </div>
        </div>
    </div>
</div>