<form class="profile-block" autocomplete="off">
    <div class="ui_block">
        <div class="h5">login</div>
        <div class="row_col">
            <label>username</label><div class="input-col">{{ form.render('login') }}</div>
        </div>
        <div class="row_col">
            <button class="btn action change-password">change password</button>
        </div>
    </div>
    <div class="ui_block user-data">

        <div class="h5">user data</div>
        <div class="row_col">
            <label>first name</label><div class="input-col">{{ form.render('first_name') }}</div>
        </div>
        <div class="row_col">
            <label>surname</label><div class="input-col">{{ form.render('surname') }}</div>
        </div>
        <div class="row_col">
            <label for="gender">gender</label><div class="input-col">{{ form.render('gender', ['class':'gender-select']) }}</div>
        </div>
        <div class="row_col">
            <label>email</label><div class="input-col">{{ form.render('email') }}</div>
        </div>

    </div>

    <div class="bottom-buttons">
        <button class="btn btn-save primary action save">
            <img src="img/save.png" />
            <span>save</span>
        </button>
    </div>
</form>

<div class="modal fade password-modal" id="password_modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title" id="modalLabel">Change password</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <img src="/img/scene/icons/plus.png"
                         style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                </span></button>
            </div>

            <div class="">
                <div class="modal-body">
                <form autocomplete="off">

                    {#<div class="title" id="modalLabel">Change password</div>#}

                    <div class="row_col">
                        <label for="password">new password</label><div class="input-col">{{ password_form.render('password') }}</div>
                    </div>
                    <div>
                        <label for="password_confirm">confirm new password</label><div class="input-col">{{ password_form.render('password_confirm') }}</div>
                    </div>
                </div>
                <div class="modal-footer buttons">
                        <a href="#" class="btn action abort" data-dismiss="modal">Abort</a>
                        <a href="#" class="btn action save">Save</a>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>