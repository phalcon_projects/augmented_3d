{%- macro format_filesize(size) %}
    {% if not size %}
        {% return "0 mb" %}
    {% endif %}

    {% set kb = 1024 %}
    {% set mb = kb*1024 %}
    {% set gb = mb*1024 %}

    {% set val = size %}

    {% if size < kb %}
        {% return val ~ " b" %}
    {% elseif size < mb %}
        {% set val = size/kb %}
        {% set units = "kb" %}
    {% elseif size < gb %}
        {% set val = size/mb %}
        {% set units = "mb" %}
    {% else %}
        {% set val = size/gb %}
        {% set units = "gb" %}
    {% endif %}

    {% return "%01.1f"|format(val) ~ " " ~ units %}
{%- endmacro %}

<svg style="display:none;">
    <symbol id="pencil-icon" viewBox="0 0 23.65 23.65" xmlns="http://www.w3.org/2000/svg"><defs><style>.axcls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>pencil-icon</title><path class="axcls-1" d="M104.29 262.67l-6.47 1.52 1.52-6.46 15.6-15.6a2 2 0 0 1 2.83 0l2.12 2.12a2 2 0 0 1 0 2.83z" transform="translate(-97.33 -241.04)"/><path class="axcls-1" d="M21.86 6.75l-4.95-4.96m3.53 6.38L15.5 3.21M7.22 21.39l-4.95-4.95"/></symbol>
    <symbol id="statistics-icon" viewBox="0 0 24 16" xmlns="http://www.w3.org/2000/svg"><defs><style>.bocls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>statistics-icon</title><path class="bocls-1" d="M.5 15.5h23m-19 0v-5h-3v5m9 0v-10h-3v10m9 0v-8h-3v8m9 0V.5h-3v15"/></symbol>
    <symbol id="delete-icon" viewBox="0 0 23 24" xmlns="http://www.w3.org/2000/svg"><defs><style>.apcls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>delete-icon</title><path class="apcls-1" d="M3 3.5h16v20H3zm4-3h8v3H7zm-6.5 3h22M7 7v12m4-12v12m4-12v12"/></symbol>
</svg>

<div class="content-list editions" data-project_id="{{ project.id }}">
    <div class="h4">editions</div>
    <div class="row list-items">
        {% for item in page.items %}
            {% set totalSize = item.getTotalFileSize() %}
            <div class="col-12 list-item"
                 data-id="{{ item.id }}"
                 data-title="{{ item.title }}">
                <div class="row">
                    <div class="col-3">
                        <div class="image">
                            {% if item.image %}
                                <img src="{{ item.image }}" alt="">
                            {% else %}
                                <div></div>
                            {% endif %}
                        </div>
                    </div>
                    <div class="col info">
                        <div class="title-wrapper">
                            <span class="title">{{ item.title }}</span>
                        </div>
                        <div class="pages">{{ item.getCoversCount() }} pages</div>
                        <div class="memory">
                            <img src="img/projects/icon-h.png" alt="">
                            <span>{{ format_filesize(totalSize) }}</span>
                        </div>
                    </div>
                    <div class="col col-12 actions">
                        <div class="row justify-content-between">
                            <div class="col col-3">
                                <div class="row icon-wrapper">
                                    <div class="col icon-action">
                                        <a href="#" title="edit" class="action edit">
                                            {#<img src="/img/icons/pencil-icon.png">#}
                                            <svg class="svg-pencil-icon-dims" >
                                                <use xlink:href="#pencil-icon"></use>
                                            </svg>
                                        </a>
                                    </div>
                                    <div class="col icon-action">
                                        <a href="{{ url('statistics/' ~ item.id) }}" title="statistics" class="action statistics">
                                            {#<img src="/img/icons/statistics-icon.png">#}
                                            <svg class="svg-pencil-icon-dims" >
                                                <use xlink:href="#statistics-icon"></use>
                                            </svg>
                                        </a>
                                    </div>
                                    <div class="col icon-action">
                                        <a href="#" data-id="{{ item.id }}" title="delete" class="action delete">
                                            {#<img src="/img/icons/delete-icon.png">#}
                                            <svg class="svg-delete-icon-dims">
                                                <use xlink:href="#delete-icon"></use>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-4">
                                <a href="{{ url('projects/' ~ item.project_id ~ '/' ~ item.id) }}"
                                   class="btn btn-primary action manage">
                                    <span>manage pages</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {% endfor %}
    </div>
    <div class="list-bottom">
        <a href="" class="btn btn-primary add">add edition</a>
        {% if page.total_pages>1 %}
            <ul class="pagination">
                {% for i in 1..page.total_pages %}
                    <li class="page-wrapper">
                        <a href="{{ url('/projects/' ~ project.id ~'/?page=' ~ i) }}"
                           class="page{% if page.current == i %} active{% endif %}">{{ i }}</a>
                    </li>
                {% endfor %}
            </ul>
        {% endif %}
    </div>
</div>

<div class="modal" id="modal-edition-title-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="loader-wrapper hidden">
                <div class="loader">
                    <img src="/img/logo-main.svg">
                </div>
            </div>
            <div class="modal-header">
                <h5 class="modal-title"><span class="modal-edition-title">Add edition</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <img src="/img/scene/icons/plus.png"
                         style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                </span></button>
            </div>
            <form action="">
                <input type="hidden" name="edition-id">
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <label for="edition-title"></label>
                                <input class="form-control" id="edition-title" name="edition-title">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-actions">
                    <div class="row">
                        <div class="col-sm-12">
                            <button id="modal-action-save" type="button" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>