<style>
    body {
        margin:0;
        padding: 0;
    }
    .page-export-view {
        background-color: #eee;
        position: relative;

        margin: 0;
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        text-align: left;
        width: 1410px;
        height: 980px;
        page-break-after: always;
    }

    .page-export-view.last {
        page-break-after: avoid;
    }

    .page-export-view .content {
        padding: 5% 0;
        height: 100%;
        display: table;
    }

    .logo-image {
        width: 107px;
        height: 77px;
        position: absolute;
        bottom: 3%;
        right: 1.5%;
    }

    .page-image-wrapper {
        overflow: hidden;
        max-width: 68%;
        margin: -4px auto 0;
        padding: 14px 14px 18px;
        text-align: center;
    }

    .page-image-wrapper .page-image {
        max-width: 100%;
        max-height: 100%;
        box-shadow: 1px 4px 14px 0px #888;
    }

    .page-count .number {
        margin: 0 auto;
        margin-top: 17%;
        width: 2em;
        height: 2em;
        background-color: #0098d7;
        border-radius: 50%;
        color: #fff;
        font-size: 32px;
        line-height: 2em;
    }

    .page-count .title {
        font-size: 24px;
    }

    .page-scene-screen {
        display: inline-block;
        position: relative;
        height: 100%;
        /*padding: 10px;*/
    }

    .page-scene-screen .shadow-container {

    }

    .page-scene-screen img, .page-scene-screen .shadow-container, .labels-box {
        position: absolute;
        top: 0;
        left: 0;
        width: 800px;
        height: 800px;
    }

    .page-scene-screen img.underground {
        opacity: 0.75;
    }

    .labels-box div {
        padding: 0 7px;
        position: absolute;
        background-color: #0098d7;
        color: #ffffff;
    }

    .cover-shadow {
        position: absolute;
        box-shadow: 1px 4px 14px 0px rgba(0,0,0,0.5);
    }
    .cover-shadow img {
        width: 100%;
    }

    .page-export-view .row {
        display: table-row;
        margin-right: -15px;
        margin-left: -15px;
    }

    .page-export-view .col, .page-export-view .col-3, .page-export-view .col-9 {
        display: table-cell;
        vertical-align: top;
        max-width: 100%;

        position: relative;
        width: 100%;
        min-height: 1px;
        padding-right: 15px;
        padding-left: 15px;
    }

    .page-export-view .col-3 {
        width: 25%;
    }

    .page-export-view .col-9 {
        width: 75%;
    }

    .page-export-view .text-center {
        text-align: center!important;
    }

    *, ::after, ::before {
        box-sizing: border-box;
    }
</style>

<div class="page-export-view">
    <div class="content">
        <div class="row" style="height: 100%;">
            <div class="col col-3">
                <div class="row">
                    <div class="col">
                        <div class="page-image-wrapper">
                            <img class="page-image" src="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="page-count text-center">
                            <div class="number">1</div>
                            <div class="title">page</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col col-9">
                <div class="page-scene-screen">
                    <img class="underground" src="">
                    <div class="shadow-container"><img class="cover-shadow" src="" /></div>
                    <img class="main" src="">
                    <div class="labels-box"></div>
                </div>
            </div>
        </div>
    </div>

    <img class="logo-image" src="/img/pdf/logo-main.svg">
</div>
{#</div>#}