<svg style="display:none;">
    <symbol id="arrow-left" viewBox="0 0 18 9.5" xmlns="http://www.w3.org/2000/svg"><defs><style>.aicls-1{fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>arrow-left</title><path class="aicls-1" d="M17.5.5L9 9 .5.5"/></symbol>
    <symbol id="action-undo" viewBox="0 0 12.39 9.5" xmlns="http://www.w3.org/2000/svg"><defs><style>.afcls-1{fill:none;stroke:#999;stroke-linecap:round;stroke-linejoin:round;stroke-width:.5px}</style></defs><title>action-undo</title><path class="afcls-1" d="M1049 546.5c1 0 3-6-1-6h-9.5" transform="translate(-1038.25 -537.25)"/><path class="afcls-1" d="M3.25 6.25l-3-3 3-3"/></symbol>
    <symbol id="action-redo" viewBox="0 0 12.39 9.5" xmlns="http://www.w3.org/2000/svg"><defs><style>.aecls-1{fill:none;stroke:#999;stroke-linecap:round;stroke-linejoin:round;stroke-width:.5px}</style></defs><title>action-redo</title><path class="aecls-1" d="M1049 546.5c1 0 3-6-1-6h-9.5" transform="translate(-1038.25 -537.25)"/><path class="aecls-1" d="M3.25 6.25l-3-3 3-3"/></symbol>
    <symbol id="export-arrow" viewBox="0 0 17 24" xmlns="http://www.w3.org/2000/svg"><defs><style>.arcls-1{fill:none;stroke:#0097d6;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>export-arrow</title><path class="arcls-1" d="M8.5 23.5V6.88m4 5l-4-5-4 5"/><path class="arcls-1" d="M6 16.5H.5V.5h16v16H11"/></symbol>
    <symbol id="cloud-publish" viewBox="0 0 23 19.89" xmlns="http://www.w3.org/2000/svg"><defs><style>.aocls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>cloud-publish</title><path class="aocls-1" d="M1048.8 3089.5h2.4s4.3-.56 4.3-4.78a4.77 4.77 0 0 0-5-4.77 6.69 6.69 0 0 0-12.72 2.46 3.59 3.59 0 0 0-4.32 3.51c0 3.64 3.83 3.59 3.83 3.59h2.86" transform="translate(-1033 -3075.61)"/><path class="aocls-1" d="M11.5 8.39v11m3-8l-3-3-3 3"/></symbol>
    <symbol id="save-icon" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><defs><style>.bgcls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>save-icon</title><path class="bgcls-1" d="M23.5.5L9 18.5l-5-5"/><path class="bgcls-1" d="M355.27 637.49a9 9 0 1 1-4.77-6.06" transform="translate(-337 -625)"/></symbol>
    <symbol id="plus-blue" viewBox="0 0 23 23" xmlns="http://www.w3.org/2000/svg"><defs><style>{fill:none;stroke:#0097d6;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>plus-blue</title><path class="drag-and-drop-add" d="M11.5.5v22m11-11H.5"/></symbol>
    <symbol id="arrow-down" viewBox="0 0 18 9.5" xmlns="http://www.w3.org/2000/svg"><defs><style>.ahcls-1{fill:none;stroke:#000;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>arrow-down</title><path class="ahcls-1" d="M17.5.5L9 9 .5.5"/></symbol>
    <symbol id="drag-item-image" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><defs><style>.aqcls-1{fill:none;stroke:#999;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>drag-item-image</title><path d="M113.5 833a.5.5 0 1 0 .5.5.5.5 0 0 0-.5-.5z" transform="translate(-97 -817)"/><path class="aqcls-1" d="M8.5 23.5h-1v-1m15 1h1v-1m-16-2v-1m0-2v-1m4 7h-1m4 0h-1m-2-7h-1m4 0h-1m4 7h-1m4 0h-1m1-16h-1m-2 0h-1m6 0h1v1m0 2v1m0 2v1m-7-4v1m0 2v1m7 2v1m0 2v1m-15-4h-8V.5h16v8"/></symbol>
    <symbol id="lock-mode-white" viewBox="0 0 18 24" xmlns="http://www.w3.org/2000/svg"><defs><style>.aucls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>lock-mode-white</title><circle class="aucls-1" cx="9" cy="16" r="2"/><rect class="aucls-1" x=".5" y="9.5" width="17" height="14" rx="4" ry="4"/><path class="aucls-1" d="M1039.5 1975a5.5 5.5 0 0 1 11 0v3.63" transform="translate(-1036 -1969)"/></symbol>
    <symbol id="search" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><defs><style>.bkcls-1,.bkcls-2{fill:none;stroke:#999;stroke-linejoin:round}.bkcls-2{stroke-linecap:round}</style></defs><title>search</title><circle class="bkcls-1" cx="8.5" cy="8.5" r="8"/><path class="bkcls-2" d="M14.16 14.16l9.34 9.34"/></symbol>
    <symbol id="minus" viewBox="0 0 13 1" xmlns="http://www.w3.org/2000/svg"><defs><style>.avcls-1{fill:none;stroke:#999;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>minus</title><path class="avcls-1" d="M12.5.5H.5"/></symbol>
    <symbol id="plus" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg"><defs><style>.aycls-1{fill:none;stroke:#999;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>plus</title><path class="aycls-1" d="M6.5.5v12m6-6H.5"/></symbol>
    <symbol id="arrow-left-white" viewBox="0 0 18 9.5" xmlns="http://www.w3.org/2000/svg"><defs></defs><title>arrow-left-white</title><path d="M17.5.5L9 9 .5.5"/></symbol>
    <symbol id="settings" viewBox="0 0 24 23" xmlns="http://www.w3.org/2000/svg"><defs><style>.blcls-1{fill:none;stroke:#999;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>settings</title><rect class="blcls-1" x=".5" y=".5" width="23" height="22" rx="1" ry="1"/><circle class="blcls-1" cx="12" cy="5.5" r="2"/><path class="blcls-1" d="M14 5.5h6.52m-17 0H10"/><circle class="blcls-1" cx="8" cy="11.5" r="2"/><path class="blcls-1" d="M10 11.5h10.52m-17 0H6"/><circle class="blcls-1" cx="10" cy="17.5" r="2"/><path class="blcls-1" d="M12 17.5h8.52m-17 0H8"/></symbol>
    <symbol id="layers" viewBox="0 0 24 20" xmlns="http://www.w3.org/2000/svg"><defs><style>.atcls-1{fill:none;stroke:#999;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>layers</title><path class="atcls-1" d="M23.5 8.5l-11.5 5-11.5-5"/><path class="atcls-1" d="M23.5 11.5l-11.5 5-11.5-5"/><path class="atcls-1" d="M23.5 14.5l-11.5 5-11.5-5m11.5-4L.5 5.5 12 .5l11.5 5-11.5 5z"/></symbol>
    <symbol id="upload" viewBox="0 0 23 23" xmlns="http://www.w3.org/2000/svg"><defs><style>.bpcls-1{fill:none;stroke:#999;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>upload</title><path class="bpcls-1" d="M1007.5 2754.72v1.91a2.87 2.87 0 0 1-2.87 2.87h-16.26a2.87 2.87 0 0 1-2.87-2.87v-1.91" transform="translate(-985 -2737)"/><path class="bpcls-1" d="M11.5 17.61V.5m7 10.11l-7 7-7-7"/></symbol>
    <symbol id="warn" viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg" ><path d="M32 6L2 58h60L32 6zm0 4.002L58.537 56H5.463L32 10.002z"/><path d="M30 28v4l1 10h2l1-10v-4z"/><circle cx="32" cy="46" r="2"/></symbol>
    <symbol id="arrow-up-white" viewBox="0 0 18 9.5" xmlns="http://www.w3.org/2000/svg"><defs><style>.ajcls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>arrow-up-white</title><path class="ajcls-1" d="M17.5.5L9 9 .5.5"/></symbol>
    <symbol id="scale-gray" viewBox="0 0 17 17" xmlns="http://www.w3.org/2000/svg"><defs><style>.bhcls-1{fill:none;stroke:#999;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>scale-gray</title><path class="bhcls-1" d="M16.5 4.5v-4h-4m-12 16l16-16m-12 16h-4v-4"/></symbol>
    <symbol id="action-3d-rotate-x" viewBox="0 0 48.85 48.03" xmlns="http://www.w3.org/2000/svg"><defs><style>.aacls-1{fill:#e3121b}</style></defs><title>action-3d-rotate-x</title><path class="aacls-1" d="M273.13 40.7c-3.11-2.87-8.48-2.9-14.58-.63a.69.69 0 0 0 .45 1.31c4.65-1.46 8.64-1.27 11 .92 5.2 4.78-.27 17.77-10.11 27.61s-23.12 15.6-28.32 10.82c-2.75-2.53-2.81-7.23-.68-12.66a.67.67 0 0 0-1.24-.51c-3.06 6.91-3.19 13 .32 16.28 6 5.58 20.6 2.42 33.55-10.52s15.66-27.04 9.61-32.62z" transform="translate(-227.36 -38.45)"/></symbol>
    <symbol id="action-3d-rotate-y" viewBox="0 0 48.85 48.03" xmlns="http://www.w3.org/2000/svg"><defs><style>.abcls-1{fill:#00ff00}</style></defs><title>action-3d-rotate-y</title><path class="abcls-1" d="M273.13 40.7c-3.11-2.87-8.48-2.9-14.58-.63a.69.69 0 0 0 .45 1.31c4.65-1.46 8.64-1.27 11 .92 5.2 4.78-.27 17.77-10.11 27.61s-23.12 15.6-28.32 10.82c-2.75-2.53-2.81-7.23-.68-12.66a.67.67 0 0 0-1.24-.51c-3.06 6.91-3.19 13 .32 16.28 6 5.58 20.6 2.42 33.55-10.52s15.66-27.04 9.61-32.62z" transform="translate(-227.36 -38.45)"/></symbol>
    <symbol id="action-3d-rotate-z" viewBox="0 0 61.05 48.38" xmlns="http://www.w3.org/2000/svg"><defs><style>.accls-1{fill:#0600ff}</style></defs><title>action-3d-rotate-z</title><path class="accls-1" d="M87.75 62.8c-.29-7.33-4.16-13.89-10.15-18.5-.46-.36-1 .23-1 1.11a1.48 1.48 0 0 0 .4 1.06A20.48 20.48 0 0 1 84.39 61c.49 12.23-11.29 25.85-26.34 25.88s-27.52-12.8-28-25c-.26-6.48 2.93-12.36 8.21-16.5a1.37 1.37 0 0 0 .38-1.07c0-.82-.5-1.37-.93-1.07-7 4.83-11.29 12.26-11 20.53C27.3 78 40 91.79 58.34 91.5S88.31 77 87.75 62.8z" transform="translate(-26.72 -43.12)"/></symbol>
    <symbol id="action-link" viewBox="0 0 18.31 18.31" xmlns="http://www.w3.org/2000/svg"><defs><style>.adcls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>action-link</title><path class="adcls-1" d="M61.35 401.6l-3.18 3.18a3 3 0 0 1-4.24 0l-.71-.71a3 3 0 0 1 0-4.24l4.6-4.6a3 3 0 0 1 4.24 0l.71.71" transform="translate(-51.84 -387.84)"/><path class="adcls-1" d="M60.65 392.4l3.18-3.18a3 3 0 0 1 4.24 0l.71.71a3 3 0 0 1 0 4.24l-4.6 4.6a3 3 0 0 1-4.24 0" transform="translate(-51.84 -387.84)"/></symbol>
    <symbol id="action-video" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><defs><style>.agcls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>action-video</title><path class="agcls-1" d="M.5.5l23 11.5-23 11.5V.5z"/></symbol>
    <symbol id="valid" viewBox="0 0 25 25" xmlns="http://www.w3.org/2000/svg"><defs><style></style></defs><title>valid</title><path d="M24 1L7.5 24 1 17.5"/></symbol>
    <symbol id="invalid" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg"><defs><style></style></defs><title>invalid</title><path d="M6.5.5v12m6-6H.5"/></symbol>
    <symbol id="close-button" viewBox="0 0 9.12 9.12" xmlns="http://www.w3.org/2000/svg"><defs><style>.ancls-1{fill:#fff}</style></defs><title>close-button</title><path class="ancls-1" d="M5.17 0H3.94v3.94H0v1.23h3.94v3.95h1.23V5.17h3.95V3.94H5.17V0z"/></symbol>
    <symbol id="close-plus" viewBox="0 0 9.12 9.12" xmlns="http://www.w3.org/2000/svg"><defs><style></style></defs><title>close-plus</title><path d="M5.17 0H3.94v3.94H0v1.23h3.94v3.95h1.23V5.17h3.95V3.94H5.17V0z"/></symbol>
    <symbol id="cinema" viewBox="0 0 24 18" xmlns="http://www.w3.org/2000/svg"><defs><style>.alcls-1{fill:none;stroke:#ccc;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>cinema</title><path class="alcls-1" d="M.5.5h23v17H.5zm0 6h23m-23-3h23"/><path class="alcls-1" d="M5 .5l3 3-3 3m6-6l3 3-3 3m6-6l3 3-3 3m-13.5 8h17m-8-2v-2"/></symbol>
    <symbol id="hdd" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><defs><style>.ascls-1{fill:none;stroke:#ccc;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>hdd</title><path class="ascls-1" d="M11.5 12.5V.5m-4 8l4 4 4-4"/><path class="ascls-1" d="M6.5 11.5h-1l-5 6h23l-5-6H17m3 9h-4"/><path class="ascls-1" d="M793.5 2706.5v4a2 2 0 0 0 2 2h19a2 2 0 0 0 2-2v-4" transform="translate(-793 -2689)"/></symbol>
    <symbol id="clock" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><defs><style>.amcls-1{fill:none;stroke:#ccc;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>clock</title><circle class="amcls-1" cx="12" cy="12" r="11.5"/><path class="amcls-1" d="M11.5 6.5V12l6 5.5"/></symbol>
    <symbol id="context-menu-replace-object" viewBox="0 0 12 18" xmlns="http://www.w3.org/2000/svg"><defs><style></style></defs><title>context-menu-replace-object</title><path  d="M3.5.5v10m5-3v10m-2-14l-3-3-3 3m11 11l-3 3-3-3"/></symbol>
    <symbol id="delete-icon" viewBox="0 0 23 24" xmlns="http://www.w3.org/2000/svg"><defs><style>.apcls-1{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round}</style></defs><title>delete-icon</title><path class="apcls-1" d="M3 3.5h16v20H3zm4-3h8v3H7zm-6.5 3h22M7 7v12m4-12v12m4-12v12"/></symbol>

</svg>

<div class="scene-loader loader-wrapper">
    <div class="loader">
        <img src="/img/logo-main.svg">
    </div>
</div>
<div id="main" class="scene-view no-select main">

    <nav class="navbar navbar-expand-md fixed-top navbar-top">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link action back-to-editions-list" href="{{ url('projects/' ~ project.id) }}">
                        {#<img src="/img/scene/icons/arrow-left.png">#}
                        <svg class="svg-arrow-left-dims">
                            <use xlink:href="#arrow-left"></use>
                        </svg>
                        <span class="edition-title">{{ project.title }}</span>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav mt-6 mt-md-0 actions">
                <li class="nav-item">
                    <a href="#" class="action scene-action scene-action-undo">
                        {#<img src="/img/scene/icons/action-undo.png">#}
                        <svg class="svg-action-undo-dims">
                            <use xlink:href="#action-undo"></use>
                        </svg>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="action scene-action scene-action-redo">
                        {#<img src="/img/scene/icons/action-redo.png">#}
                        <svg class="svg-action-redo-dims">
                            <use xlink:href="#action-redo"></use>
                        </svg>
                    </a>
                </li>
                <li class="nav-item">
                    <button type="submit" class="btn btn-default scene-action-pdf-export">
                        {#<img src="/img/scene/icons/export-arrow.png" alt="" class="img-pdf-export">#}
                        <svg class="svg-export-arrow-dims">
                            <use xlink:href="#export-arrow"></use>
                        </svg>
                        Export
                    </button>
                </li>
                <li class="nav-item">
                    <button type="submit" class="btn btn-success scene-action-bundle-generate">
                        {#<img src="/img/scene/icons/cloud-publish.png" alt="">#}
                        <svg class="svg-cloud-publish-dims">
                            <use xlink:href="#cloud-publish"></use>
                        </svg>
                        Publish
                    </button>
                </li>
                <li class="nav-item">
                    <button type="submit" class="btn btn-primary action scene-action-save">
                        {#<img src="/img/scene/icons/save-icon.png" alt="">#}
                        <svg class="svg-save-icon-dims">
                            <use xlink:href="#save-icon"></use>
                        </svg>
                        Save
                    </button>
                </li>
            </ul>
        </div>
    </nav>

    <div class="collapse" id="navbarToggleExternalContent">
        <div class="bg-dark">
            <h4 class="text-white">Collapsed content</h4>
            <span class="text-muted">Toggleable via the navbar brand.</span>
        </div>
    </div>

    <div class="navbar-top-info no-select">
        <div class="covers-select collapse" id="covers-collapse">
            <div class="row">
                <div class="col">
                    <div class="covers-list-count"><span class="count-title"></span> pages</div>
                </div>
                <div class="col text-right">
                    <div class="covers-list-pagination">
                        show page
                        <input type="text" class="covers-list-page" id="covers-list-page-number"
                               min="1" max="1" step="1">
                    </div>
                </div>
            </div>
            <div class="card card-body covers-list-wrapper">
                <div class="covers-list">
                </div>
                <form method="post" enctype="multipart/form-data" class="drag-and-drop" novalidate>
                    <input id="add-cover-input" style="display: none;" type="file" name="add-cover" multiple>
                    <input id="cover-uploader-input" style="display: none;" type="file" name="cover" multiple>
                    <div class="text-center">
                        <div class="drag-and-drop-add">
                            {#<img src="/img/scene/icons/plus-blue.png" class="drag-and-drop-add">#}
                            <svg class="svg-plus-blue-dims">
                                <use xlink:href="#plus-blue"></use>
                            </svg>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <nav class="navbar covers-list-toggle">
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#covers-collapse"
                    aria-controls="covers-collapse" aria-expanded="false" aria-label="Toggle navigation">
                {#<img src="/img/scene/icons/arrow-down.png">#}
                <svg class="svg-arrow-down-dims">
                    <use xlink:href="#arrow-down"></use>
                </svg>
                <span class="edition-title">{{ edition.title
                    }}</span>
            </button>
            <div class="covers-list-actions">
                <div class="instruction">
                    {#<img src="/img/scene/icons/drag-item-image.png">#}
                    <svg class="svg-drag-item-image-dims">
                        <use xlink:href="#drag-item-image"></use>
                    </svg>
                    <span>drag a new page inside</span>
                </div>
                <button class="btn btn-primary covers-list-action-add">Upload</button>
            </div>
        </nav>
    </div>

    <div class="viewport" id="viewport">

        <div class="view-cube-wrapper">
            <div class="viewcube" id="viewcube"></div>
        </div>

        <div class="view-mode-wrapper">
            <div id="view-mode" class="view-mode active">
                <span>3D</span>
            </div>
        </div>

        <div class="view-lock-wrapper">
            <div id="view-lock" class="view-lock">
                {#<img src="/img/scene/icons/lock-mode-white.png">#}
                <svg class="svg-lock-mode-white-dims">
                    <use xlink:href="#lock-mode-white"></use>
                </svg>
            </div>
        </div>

        <div class="info">
            {#<img src="/img/scene/icons/drag-item-image.png">#}
            <svg class="svg-drag-item-image-dims">
                <use xlink:href="#drag-item-image"></use>
            </svg>
            <span>Drag new object here</span>
        </div>

    </div>

    <!--
    <div class="edit-panel" id="edit-panel">
        <div class="head">
            <div class="active" data-target="transform-panel">Settings</div>
            <div class="hidden" data-target="animations-panel">Animation</div>
            <div class="hidden" data-target="video-panel">Video</div>
            <div class="hidden" data-target="audio-panel">Audio</div>
        </div>
        <div class="panel-content">
            <div class="active">
                <form>
                    <div class="axis-labels"><span>X:</span><span>Y:</span><span>Z:</span></div>
                    <div class="controls"><span class="row-label">Position</span><span class="control"><input
                                    type="text" name="px"/></span><span class="control"><input type="text"
                                                                                               name="py"/></span><span
                                class="control"><input type="text" name="pz"/></span></div>
                    <div class="controls"><span class="row-label">Rotation</span><span class="control"><input
                                    type="text" name="rx"/></span><span class="control"><input type="text"
                                                                                               name="ry"/></span><span
                                class="control"><input type="text" name="rz"/></span></div>
                    <div class="controls"><span class="row-label">Scale</span><span class="control"><input type="text"
                                                                                                           name="sx"/></span><span
                                class="control"><input type="text" disabled name="sy"/></span><span
                                class="control"><input type="text" disabled name="sz"/></span></div>
                    <div class="controls"><span class="row-label">Title</span><span class="control-full"><input
                                    type="text" name="title"/></span></div>
                </form>
            </div>
            <div>
                Animations: <select size="15"></select>
            </div>
            <div class="video-panel" id="video-panel">
                Video:
                <span class="filename"></span>
                <div></div>
            </div>
            <div class="audio-panel" id="audio-panel">
                Audio:
                <span class="filename"></span>
                <div></div>
            </div>
        </div>
    </div>
    -->

    <footer class="footer no-select">
        <div class="content">
            <div class="title">
                <img src="/img/logo.svg" class="company-logo">
            </div>
            <div class="scene-scale-wrapper">
                <div class="icon-wrapper">
                    {#<img src="/img/scene/icons/search.png">#}
                    <svg class="svg-search-dims">
                        <use xlink:href="#search"></use>
                    </svg>
                </div>
                <div class="icon-wrapper scale-control zoom-out">
                    {#<img src="/img/scene/icons/minus.png">#}
                    <svg class="svg-minus-dims">
                        <use xlink:href="#minus"></use>
                    </svg>
                </div>
                <input class="scale-control-input" id="scene-scale" name="scene-scale" value="100%" autocomplete="off">
                <div class="icon-wrapper scale-control zoom-in">
                    {#<img src="/img/scene/icons/plus.png">#}
                    <svg class="svg-plus-dims">
                        <use xlink:href="#plus"></use>
                    </svg>
                </div>
            </div>
        </div>
    </footer>

</div>

<div id="tools-panel" class="tools-panel"></div>

<div class="settings-bar-toggle-wrapper">
    <a class="btn settings-bar-toggle" id="settings-bar-toggle" href="#">
        {#<img src="/img/scene/icons/arrow-left-white.png">#}
        {#<img src="/img/scene/icons/arrow-left-white.png">#}
        <svg class="svg-arrow-left-dims">
            <use xlink:href="#arrow-left-white"></use>
        </svg>
        <svg class="svg-arrow-left-dims">
            <use xlink:href="#arrow-left-white"></use>
        </svg>
    </a>
</div>

<div id="right-side-panel" class="right-side-panel no-select">
    <div class="settings-bar-wrapper">
        <ul class="nav nav-tabs main-bar" id="main-bar" role="tabList">
            <li class="nav-item">
                <a class="nav-link active" id="settings-tab"
                   data-toggle="tab" href="#bar-settings" role="tab" aria-controls="settings" aria-expanded="true">
                    {#<img src="/img/scene/icons/settings.png">#}
                    <svg class="svg-settings-dims">
                        <use xlink:href="#settings"></use>
                    </svg>
                    <span>Settings</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="layers-tab"
                   data-toggle="tab" href="#bar-layers" role="tab" aria-controls="layers">
                    {#<img src="/img/scene/icons/layers.png">#}
                    <svg class="svg-layers-dims">
                        <use xlink:href="#layers"></use>
                    </svg>
                    <span>Layers</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="object-library-tab"
                   data-toggle="tab" href="#bar-object-library" role="tab" aria-controls="upload">
                    {#<img src="/img/scene/icons/upload.png">#}
                    <svg class="svg-upload-dims">
                        <use xlink:href="#upload"></use>
                    </svg>
                    <span>Upload</span>
                </a>
            </li>
        </ul>
        <div class="tab-content settings-tab" id="settings-bar-content">
            <div class="tab-pane fade show active" id="bar-settings" role="tabpanel" aria-labelledby="settings-tab">

                <div class="settings-bar-no-object-selected">
                    <div class="title text-center">
                        {#<img class="icon" src="/img/scene/icons/warn.png">#}
                        <svg class="svg-warn-dims">
                            <use xlink:href="#warn"></use>
                        </svg>
                    </div>
                    <div class="message text-center">
                        <span>no objects selected in 3d scene</span>
                    </div>
                </div>

                <div id="settings-bar-content-accordion" role="tablist" class="content-accordion">

                    <div class="card" id="main-panel">
                        <div class="card-header" role="tab" id="settings-main-title">
                            <a class="collapsed" data-toggle="collapse" href="#settings-main"
                               aria-expanded="true" aria-controls="settings-main">
                                <h5 class="mb-0">
                                    <span>Main</span>
                                    {#<img src="/img/scene/icons/arrow-up-white.png">#}
                                    <svg class="svg-arrow-up-white-dims">
                                        <use xlink:href="#arrow-up-white"></use>
                                    </svg>
                                </h5>
                            </a>
                        </div>
                        <div id="settings-main" class="collapse show" role="tabpanel"
                             aria-labelledby="settings-main-title"
                             data-parent="#settings-bar-content-accordion">
                            <div class="card-body">
                                <div class="container-fluid">
                                    <div class="row settings-main">
                                        <div class="col col-12">
                                            <label for="settings-main-title-input">Title</label>
                                            <input id="settings-main-title-input" name="title">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card transform-panel" id="transform-panel">
                        <div class="card-header" role="tab" id="settings-layout-title">
                            <a data-toggle="collapse" href="#settings-layout"
                               aria-expanded="true" aria-controls="settings-layout">
                                <h5 class="mb-0">
                                    <span>Layout</span>
                                    {#<img src="/img/scene/icons/arrow-up-white.png">#}
                                    <svg class="svg-arrow-up-white-dims">
                                        <use xlink:href="#arrow-up-white"></use>
                                    </svg>
                                </h5>
                            </a>

                        </div>

                        <div id="settings-layout" class="collapse show settings-layout" role="tabpanel"
                             aria-labelledby="settings-layout-title" data-parent="#settings-bar-content-accordion">
                            <div class="card-body">
                                <form>
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col settings-scale">
                                                <div class="input-wrapper">
                                                    <label for="settings-scale-all">Scale</label>
                                                    <input id="settings-scale-all" name="sx"
                                                           value="1.0" min="0.1" step="0.1">
                                                </div>
                                                <div class="settings-input-image scale-all"
                                                     data-property="scale"
                                                     data-axis="all">
                                                    {#<img src="/img/scene/icons/scale-gray.png">#}
                                                    <svg class="svg-scale-gray-dims">
                                                        <use xlink:href="#scale-gray"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col col-12">
                                                <label>Position</label>
                                            </div>
                                            <div class="col settings-position-wrapper">
                                                <div class="settings-position">
                                                    <div class="settings-input-image move move-x"
                                                         data-property="position"
                                                         data-axis="x">
                                                        <img src="/img/scene/icons/action-3d-move-x.png">
                                                        <span>X</span>
                                                    </div>
                                                    <div class="input-wrapper">
                                                        <input id="settings-position-x" name="px" value="1.0" min="0.1"
                                                               step="0.1">
                                                    </div>
                                                </div>
                                                <div class="settings-position">
                                                    <div class="settings-input-image move move-y"
                                                         data-property="position"
                                                         data-axis="y">
                                                        <img src="/img/scene/icons/action-3d-move-y.png">
                                                        <span>Y</span>
                                                    </div>
                                                    <div class="input-wrapper">
                                                        <input id="settings-position-y" name="py" value="1.0" min="0.1"
                                                               step="0.1">
                                                    </div>
                                                </div>
                                                <div class="settings-position">
                                                    <div class="settings-input-image move move-z"
                                                         data-property="position"
                                                         data-axis="z">
                                                        <img src="/img/scene/icons/action-3d-move-z.png">
                                                        <span>Z</span>
                                                    </div>
                                                    <div class="input-wrapper">
                                                        <input id="settings-position-z" name="pz" value="1.0" min="0.1"
                                                               step="0.1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col settings-rotate">
                                                <label>Rotate</label>
                                            </div>
                                            <div class="col settings-rotate-wrapper">
                                                <div class="settings-rotate">
                                                    <div class="settings-input-image rotate-x"
                                                         data-property="rotation"
                                                         data-axis="x">
                                                        {#<img src="/img/scene/icons/action-3d-rotate-x.png">#}
                                                        <svg class="svg-action-3d-rotate-x-dims">
                                                            <use xlink:href="#action-3d-rotate-x"></use>
                                                        </svg>
                                                        <span>X</span>
                                                    </div>
                                                    <div class="input-wrapper">
                                                        <input id="settings-rotation-x" name="rx" value="1.0" min="0.1"
                                                               step="0.1">
                                                    </div>
                                                </div>
                                                <div class="settings-rotate">
                                                    <div class="settings-input-image rotate-y"
                                                         data-property="rotation"
                                                         data-axis="y">
                                                        {#<img src="/img/scene/icons/action-3d-rotate-y.png">#}
                                                        <svg class="svg-action-3d-rotate-y-dims">
                                                            <use xlink:href="#action-3d-rotate-y"></use>
                                                        </svg>
                                                        <span>Y</span>
                                                    </div>
                                                    <div class="input-wrapper">
                                                        <input id="settings-rotation-y" name="ry" value="1.0" min="0.1"
                                                               step="0.1">
                                                    </div>
                                                </div>
                                                <div class="settings-rotate">
                                                    <div class="settings-input-image rotate-z"
                                                         data-property="rotation"
                                                         data-axis="z">
                                                        {#<img src="/img/scene/icons/action-3d-rotate-z.png">#}
                                                        <svg class="svg-action-3d-rotate-z-dims">
                                                            <use xlink:href="#action-3d-rotate-z"></use>
                                                        </svg>
                                                        <span>Z</span>
                                                    </div>
                                                    <div class="input-wrapper">
                                                        <input id="settings-rotation-z" name="rz" value="1.0" min="0.1"
                                                               step="0.1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="card" id="action-panel">
                        <div class="card-header" role="tab" id="settings-action-title">
                            <a class="collapsed" data-toggle="collapse" href="#settings-action"
                               aria-expanded="true" aria-controls="settings-action">
                                <h5 class="mb-0">
                                    <span>Action</span>
                                    {#<img src="/img/scene/icons/arrow-up-white.png">#}
                                    <svg class="svg-arrow-up-white-dims">
                                        <use xlink:href="#arrow-up-white"></use>
                                    </svg>
                                </h5>
                            </a>
                        </div>
                        <div id="settings-action" class="collapse show" role="tabpanel"
                             aria-labelledby="settings-action-title" data-parent="#settings-bar-content-accordion">
                            <div class="card-body">
                                <div class="container-fluid">
                                    <div class="row settings-action">
                                        <div class="col col-12">
                                            <div class="settings-action-wrapper">
                                                <div class="object-action-icon action-icon-link">
                                                     {#src="/img/scene/icons/action-link.png">#}
                                                    <svg class="svg-action-link-dims">
                                                        <use xlink:href="#action-link"></use>
                                                    </svg>
                                                </div>

                                                <div class="object-action-icon action-icon-video" data-toggle="popover">
                                                     {#src="/img/scene/icons/action-video.png" data-toggle="popover">#}
                                                    <svg class="svg-action-video-dims" data-toggle="popover">
                                                        <use xlink:href="#action-video"></use>
                                                    </svg>
                                                </div>
                                                <select id="settings-action-type">
                                                    <option value="" selected>None</option>
                                                    <option value="open-url">Open URL</option>
                                                    <option value="open-video">Open Video</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col col-12 inline action-url-block">
                                            <div class="input-wrapper">
                                                <input id="settings-open-url-link" name="settings-open-url-link"
                                                       placeholder="https://www.example.com">
                                            </div>
                                            <div class="url-validity-state">
                                                {#<img class="sign-valid" src="/img/scene/icons/valid.png">#}
                                                <div class="sign-valid">
                                                    <svg class="svg-valid-dims">
                                                        <use xlink:href="#valid"></use>
                                                    </svg>
                                                </div>

                                                {#<img class="sign-invalid" src="/img/scene/icons/remove.png">#}
                                                <div class="sign-invalid">
                                                    <svg class="svg-invalid-dims">
                                                        <use xlink:href="#invalid"></use>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-12 action-video-block">
                                            <div class="action-video-preview"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card animations-panel" id="animations-panel">
                        <div class="card-header" role="tab" id="settings-animation-title">
                            <a class="collapsed" data-toggle="collapse" href="#settings-animation"
                               aria-expanded="true" aria-controls="settings-animation">
                                <h5 class="mb-0">
                                    <span>Animation</span>
                                    {#<img src="/img/scene/icons/arrow-up-white.png">#}
                                    <svg class="svg-arrow-up-white-dims">
                                        <use xlink:href="#arrow-up-white"></use>
                                    </svg>

                                </h5>
                            </a>
                        </div>
                        <div id="settings-animation" class="collapse show" role="tabpanel"
                             aria-labelledby="settings-animation-title"
                             data-parent="#settings-bar-content-accordion">
                            <div class="card-body">
                                <div class="container-fluid">
                                    <div class="row settings-animation">
                                        <div class="col col-12">
                                            <select id="settings-animation-type" size="15">
                                                <option value="no-animation" selected>No animation</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card video-panel" id="video-panel">
                        <div class="card-header" role="tab" id="settings-video-title">
                            <a class="collapsed" data-toggle="collapse" href="#settings-video"
                               aria-expanded="true" aria-controls="settings-video">
                                <h5 class="mb-0">
                                    <span>Video</span>
                                    {#<img src="/img/scene/icons/arrow-up-white.png">#}
                                    <svg class="svg-arrow-up-white-dims">
                                        <use xlink:href="#arrow-up-white"></use>
                                    </svg>
                                </h5>
                            </a>
                        </div>
                        <div id="settings-video" class="collapse show" role="tabpanel"
                             aria-labelledby="settings-video-title"
                             data-parent="#settings-bar-content-accordion">
                            <div class="card-body">
                                <div class="container-fluid">
                                    <div class="row settings-video">
                                        <div class="col col-12">

                                            <span class="filename"></span>
                                            <div></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card audio-panel" id="audio-panel">
                        <div class="card-header" role="tab" id="settings-audio-title">
                            <a class="collapsed" data-toggle="collapse" href="#settings-audio"
                               aria-expanded="true" aria-controls="settings-audio">
                                <h5 class="mb-0">
                                    <span>Audio</span>
                                    {#<img src="/img/scene/icons/arrow-up-white.png">#}
                                    <svg class="svg-arrow-up-white-dims">
                                        <use xlink:href="#arrow-up-white"></use>
                                    </svg>
                                </h5>
                            </a>
                        </div>
                        <div id="settings-audio" class="collapse show" role="tabpanel"
                             aria-labelledby="settings-audio-title"
                             data-parent="#settings-bar-content-accordion">
                            <div class="card-body">
                                <div class="container-fluid">
                                    <div class="row settings-audio">
                                        <div class="col col-12">

                                            <span class="filename"></span>
                                            <div></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card custom-fields-panel" id="custom-fields-panel">
                        <div class="card-header" role="tab" id="settings-custom-fields-title">
                            <a class="collapsed" data-toggle="collapse" href="#settings-custom-fields"
                               aria-expanded="true" aria-controls="settings-custom-fields">
                                <h5 class="mb-0">
                                    <span>Custom fields</span>
                                    {#<img src="/img/scene/icons/arrow-up-white.png">#}
                                    <svg class="svg-arrow-up-white-dims">
                                        <use xlink:href="#arrow-up-white"></use>
                                    </svg>
                                </h5>
                            </a>
                        </div>
                        <div id="settings-custom-fields" class="collapse show" role="tabpanel"
                             aria-labelledby="settings-custom-fields-title"
                             data-parent="#settings-bar-content-accordion">
                            <div class="card-body">
                                <div class="container-fluid">
                                    <div class="row settings-custom-fields">
                                        <div class="col col-12 text-center">
                                            <a class="btn btn-default action manage-custom-fields" href="#">Manage</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="tab-pane fade" id="bar-layers" role="tabpanel" aria-labelledby="layers-tab">

                <div class="library-list-no-object-selected">
                    <div class="title text-center">
                        {#<img class="icon" src="/img/scene/icons/warn.png">#}
                        <svg class="svg-warn-dims">
                            <use xlink:href="#warn"></use>
                        </svg>
                    </div>
                    <div class="message text-center">
                        <span>no objects in 3d scene</span>
                    </div>
                </div>

                <div id="object-list-wrapper">
                    <div class="objects-list short-list"></div>
                    <div class="add-object">UPLOAD ELEMENT</div>
                    <input id="upload-object-input" style="display: none;" type="file" name="object"
                           data-url="{{ url("project/" ~ project.id ~ "/" ~ edition.id) }}"/>
                    <input id="file-upload-dialog-input" style="display: none;" type="file" name="object" value=""/>
                </div>
            </div>

            <div class="tab-pane fade" id="bar-object-library" role="tabpanel" aria-labelledby="object-library-tab">
                <div class="library-list">
                    <div class="filter-block">
                        <div class="subhead filter-type">
                            {#<i data-type="" class="active">all</i>#}
                            <i data-type="object">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     preserveAspectRatio="xMidYMid" width="24" height="24" viewBox="0 0 24 24">
                                    <image xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYBAMAAAASWSDLAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAALVBMVEX///////////////////////////////////////////////////////////+g0jAFAAAADnRSTlMAb/z2W/s9MHr6EFdzH1irgrEAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAAAlklEQVQY02NgYGAQegcBj4Fslrj0chCo0ANyPPsWMDCAaaDElDowB0gDBRwgHCANFGAAc0A0g2h7+T6QAScdgBw9qLlvGICc5wxgZVzPacO5tWrV2merVuWBOOeg9j4CcljmAF1T9rq8IhDI8XRgAOsB0s+BzgVzQPRzsASQA6JfgCUYuMD0o3ZwaJQ9BSmwg5qrCGQDAPNcaNwTHKbSAAAAAElFTkSuQmCC"
                                           width="24" height="24"/>
                                </svg>
                            </i>
                            <i data-type="image">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     preserveAspectRatio="xMidYMid" width="24" height="24" viewBox="0 0 24 24">
                                    <image xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3KAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAvVBMVEX////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////jSrqQAAAAPnRSTlMA9BBG2EckYhdg9SUMuv6jqHhtwBgKDv0Jb+848jZC8x4CyneYBorwtTdh2wSyxycdcOlMgQ8x7My3+DrtoE2HV/QAAAABYktHRACIBR1IAAAACXBIWXMAAAsSAAALEgHS3X78AAAAtElEQVQoz2NgtMMKGBnsmLACOwY7BqwALsHMYsfCikWCjZGdg5OLG12Ch5ePH0gJCKJLCAmLiOKQEANqw2qUONDpWC2XYGKSADMkGaU4UZwLBdIysnLyCpgSikrKDAwqKggJVTV1Lg1NBgYtbVUGBgVGBZiEjq6evoGgvKGYkTFIGUgLRMKExRRImplbWIINBGkBS7BZyYIFrG30IVYBtYAlbG3RghaoBSiBAxCMD5pK4Eo+AHGoJmlYbi5eAAAAAElFTkSuQmCC"
                                           width="24" height="24"/>
                                </svg>
                            </i>
                            <i data-type="video">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     preserveAspectRatio="xMidYMid" width="28" height="25" viewBox="0 0 28 25">
                                    <image xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAZCAMAAAAVHr4VAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABa1BMVEX///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8xWP+lAAAAeHRSTlMAPLb0/tyADGv8q1NDdeXTFTn9YxPQsSwxFF8u8lHRckLx6dmKOrqBC+uWBEr7TOMB4gmnn5QFVfoa7JmqqIUcWRnqNOScvgN7j+jICGy5kzdNqXYNIIaIYfkvQOBgMFbB8I6QNnz4LbB4MpEluD3tmHf3G2dP73AVsWOXAAAAAWJLR0QAiAUdSAAAAAlwSFlzAAALEgAACxIB0t1+/AAAATJJREFUKM9jYGBgZGJmYWVjZ8AGODi5uHl4+fgFsMgJCgmDKBFRJiySYlwQWrxCgoFBUopDGllSRhbKkJNnUFBUUq5QQZKsUIUy1NQ1NLW0GXR09RCS+sIQ2sBQWIfZCMgwNkFImoqagWlzC0tzKxDD2gYhaalpqwGkFBTtGOw5HRgYHJ04kCx1dlFyVXFj0XJnYPDgNPb08vZBdi+7L5effwCYGRjEFGyE4tOQUDQggpCT4qxAB2FwSf5wdJ0RkSDxKPVoBoaYWPQQjatg0I5PqGBNBEuGJiUjQApQMtUlzUQCqAYkmZ6BBHQYeCoMMyXBBmAxNqtCG2o6djtxSzpXsOOWjK6Qyc7BbaywKEuuThZOO/NsOPO9GBhEC6LRQGERSElxSToDQ2kZetAWlUONAAB+HF43JjZvlgAAAABJRU5ErkJggg=="
                                           width="28" height="25"/>
                                </svg>
                            </i>
                            <i data-type="audio">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     preserveAspectRatio="xMidYMid" width="24" height="21" viewBox="0 0 24 21">
                                    <image xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAVCAMAAABrN94UAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAnFBMVEX///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////9FaQsiAAAAM3RSTlMATu8J9BBsvAq/rAHkU+BdQ/I+4gZj1FVX7PeyyAfJZ3ukTQPCgmG9PdrjKxb8AmZSCEQF3Un8AAAAAWJLR0QAiAUdSAAAAAlwSFlzAAALEgAACxIB0t1+/AAAAKlJREFUKM91kekOgjAQhKfgIlrF+6gXihcqnn3/d7MVElqh+6Np5ku7M7tAWczzUVesIaleZwUImqGtowBhq80tPQedLijqWXoO+oMh2Ghs6hoEISaRz6czzIWUcvHTNViu1nwTY7uDSIio8K/A/nDE6Yz0AtO7vqdX3Dxk9wog+cikOt0vRMJNUPaoc/XUrtSvRC87x1vnqCT/mMnds3JP170P9wb/dv4FGV4RE9J9obEAAAAASUVORK5CYII="
                                           width="24" height="21"/>
                                </svg>
                            </i>
                        </div>
                        <div class="filter-name-block"><input type="text" placeholder="Search" class="filter-name"/>
                            <div class="reset">
                                {#<img src="/img/scene/icons/close-button.png">#}
                                <svg class="svg-close-button-dims">
                                    <use xlink:href="#close-button"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="items"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade object-library" id="modal-object-library" tabindex="-1" role="dialog"
     aria-labelledby="modal-object-library-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Object library</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        {#<img src="/img/scene/icons/plus.png"#}
                             {#style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">#}
                        <svg class="svg-close-plus-dims">
                            <use xlink:href="#close-plus"></use>
                        </svg>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="block-content">
                    <h5 class="title text-center">Select objects to add</h5>
                    <div class="library-list multiple-select">
                        <div class="row items"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-primary action objects-add-save">Add</button>
                        <button type="button" class="btn btn-secondary action cancel" data-dismiss="modal">Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-custom-fields-manage" tabindex="-1" role="dialog"
     aria-labelledby="modal-custom-fields-manage-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Custom fields</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        {#<img src="/img/scene/icons/plus-white.png"#}
                             {#style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">#}
                        <svg class="svg-close-plus-dims">
                            <use xlink:href="#close-plus"></use>
                        </svg>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="block-content">
                    <h5 class="title text-center">Set custom fields in JSON format</h5>
                    <textarea name="custom-fields" class="custom-fields" rows="8"></textarea>
                </div>
                <div class="row">
                    <div class="col-sm-6 text-center">
                        <button type="submit" class="btn btn-primary action custom-fields-save">Save</button>
                    </div>
                    <div class="col-sm-6 text-center">
                        <button type="button"
                                class="btn btn-secondary action cancel"
                                data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade youtube add-youtube-link" id="modal-add-youtube-link" tabindex="-1" role="dialog"
     aria-labelledby="modal-add-youtube-link-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-add-youtube-link-title">Upload by youtube link</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        {#<img src="/img/scene/icons/plus.png"#}
                             {#style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">#}
                        <svg class="svg-close-plus-dims">
                            <use xlink:href="#close-plus"></use>
                        </svg>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="block-content video-link-wrapper">
                    <div class="">
                        <input type="text" id="video-youtube-link" class="form-control"
                               name="video-youtube-link" value=""
                               placeholder="https://youtube.com/watch?v=example">
                        <span class="link-error">The link is incorrect or you cannot access this video.<br>You must be the video owner to add the video.</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-default action cancel" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary action youtube-link-preview">Preview</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade video-link-add" id="modal-video-link-add" tabindex="-1" role="dialog"
     aria-labelledby="modal-add-video-link-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-add-video-link-title">Add video link</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        {#<img src="/img/scene/icons/plus-white.png"#}
                             {#style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">#}
                        <svg class="svg-close-plus-dims">
                            <use xlink:href="#close-plus"></use>
                        </svg>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="block-content video-link-wrapper">
                    <div class="">
                        <input type="text" id="video-link" class="form-control" name="video-link" value=""
                               placeholder="https://example.com/example">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-default action cancel" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary action video-link-add">Add</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade youtube" id="modal-import-youtube-video" tabindex="-1" role="dialog"
     aria-labelledby="modal-import-youtube-video-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content youtube">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="content-youtube">
                            <img class="video-thumbnail" src="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="video-info-dimensions">
                            {#<img src="/img/scene/icons/cinema.png" style="padding-right: 20px">#}
                            <svg class="svg-cinema-dims">
                                <use xlink:href="#cinema"></use>
                            </svg>
                            <span><span class="video-width"></span> X <span class="video-height"></span> px</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="wrap">
                    <div class="hdd">
                        {#<img src="/img/scene/icons/hdd.png" style="padding-right: 10px">#}
                        <svg class="svg-hdd-dims">
                            <use xlink:href="#hdd"></use>
                        </svg>
                        <span>20 MB</span>
                    </div>
                    <div class="clock">
                        {#<img src="/img/scene/icons/clock.png" style="padding-right: 10px">#}
                        <svg class="svg-clock-dims">
                            <use xlink:href="#clock"></use>
                        </svg>
                        <span class="video-duration"></span>
                    </div>
                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal" style=" margin-right: 10px">Cancel
                </button>
                <button type="button" class="btn btn-primary action import" style=" margin-right: 0" data-link="">
                    Import
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade youtube" id="modal-import-youtube-video-error" tabindex="-1" role="dialog"
     aria-labelledby="modal-import-youtube-video-error-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content youtube">
            <div class="modal-header">
                <h5 class="modal-title">You have no access to the video by link you provided</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <p>You must be the owner of the video to upload it.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style=" margin-right: 10px">Ok
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade export-pdf" id="modal-export-pdf" tabindex="-1" role="dialog"
     aria-labelledby="modal-export-pdf" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-export-pdf-title">PDF export is done</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        {#<img src="/img/scene/icons/plus.png"#}
                             {#style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">#}
                        <svg class="svg-close-plus-dims">
                            <use xlink:href="#close-plus"></use>
                        </svg>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="block-content">
                    <a class="btn btn-primary action pdf-open" target="_blank">Open exported PDF</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade object-replace" id="modal-object-replace" tabindex="-1" role="dialog"
     aria-labelledby="modal-object-replace-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Object replace</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        {#<img src="/img/scene/icons/plus.png"#}
                             {#style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">#}
                        <svg class="svg-close-plus-dims">
                            <use xlink:href="#close-plus"></use>
                        </svg>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="block-content used-covers">
                    <h5 class="title text-center">Object will be replaced also on this covers:</h5>
                    <div class="object-covers-list">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <input id="object-replace-input" type="file" />
                        <button type="submit" class="btn btn-primary action object-replace">Replace...</button>
                        <button type="button" class="btn btn-secondary action cancel" data-dismiss="modal">Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="youtube-at">{{ google_at }}</div>

<div id="context-menu">
    <h1 class="title text-truncate"></h1>
    <div class="actions row justify-content-between">
        <div class="col">
            <a class="action object-replace" href="#">
                {#<img src="/img/scene/icons/context-menu-replace-object.png">#}
                <svg class="svg-context-menu-replace-object-dims">
                    <use xlink:href="#context-menu-replace-object"></use>
                </svg>
            </a>
            <span>replace</span>
        </div>
        <div class="col">
            <a class="action object-delete" href="#">
                {#<img src="/img/scene/icons/context-menu-delete-icon.png">#}
                <svg class="svg-context-menu-delete-icon-dims">
                    <use xlink:href="#delete-icon"></use>
                </svg>
            </a>
            <span>delete</span>
        </div>
    </div>
</div>

<script id="vertex_shader" type="x-shader/x-vertex">
varying vec2 vUv;

void main() {

    vUv = uv;

    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

}


</script>

<script id="fragment_shader" type="x-shader/x-fragment">
uniform vec3 color;
uniform sampler2D texture;

varying vec2 vUv;

void main() {

    vec4 tColor = texture2D( texture, vUv );

    gl_FragColor = vec4( mix( color, tColor.rgb, 1.0-tColor.a ), tColor.a );

}


</script>

<div class="blocker"></div>

{{ partial('partials/loading') }}

{{ partial('partials/dialog.pdf-pages') }}

{{ partial('partials/dialog.image-crop') }}
{{ partial('partials/dialog.thumbnail-crop') }}
