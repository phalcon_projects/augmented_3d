<div class="content ui_block apps">
    <div class="row block-header">
        <div class="col-sm-8">
            <h5>{{ 'Keys - unlimited' }}; {{ 'License expires: ' ~ (currentUserHasLicense ? date('d-m-Y', strtotime(license.date_to)) : 'already expired')}}</h5>
        </div>
        <div class="col-sm-4 pull-right">
            <form id="form-search" class="search" method="post" action="/apps">
                <div class="input-col search-filed-wrapper">
                    <img class="search-image-addon" src="img/search.png"/>
                    <input type="text" id="apps-search" name="search" autocomplete="off" placeholder="search"
                           value="{{ searchFilter }}">
                </div>
            </form>
        </div>
    </div>

    <div class="row block-content">
        <table class="list">
            <tbody>
            {% for item in page.items %}
            <tr class="item" data-id="{{ item.id }}">
                <td class="ver">v {{ item.version }}</td>
                <td class="name">{{ item.name }}</td>
                <td class="key">
                    <a class="action view-keys"
                       data-app-name="{{ item.name }}"
                       {% for item_key in item.keys %}
                            {{ item_key|json_encode }}
                       data-{% if item_key.platform_type == 1 %}web{% elseif item_key.platform_type == 2 %}android{% elseif item_key.platform_type == 3 %}ios{% endif %}-key="{{ item_key.key }}"
                       data-{% if item_key.platform_type == 1 %}web{% elseif item_key.platform_type == 2 %}android{% elseif item_key.platform_type == 3 %}ios{% endif %}-platform-id="{{ item_key.platform_id }}"
                       {% endfor %}><img src="img/key.png" alt="">&nbsp;View platforms keys</a>
                </td>
                <!--td><a class="btn action edit" href="{{ url('/apps/edit/' ~ item.id) }}">edit</a></td-->
                <td><a class="btn action delete" data-id="{{ item.id }}" data-app-name="{{ item.name }}">
                        <img src="/img/del.png">
                    </a></td>
            </tr>
            {% endfor %}
            </tbody>
        </table>
    </div>

    <div class="row block-footer">
        <div class="col-sm-3">
            <a href="{{ url('/apps/add') }}" class="btn primary action add">add</a>
        </div>
        <div class="col-sm-9 pull-right">
            <div class="items-info pull-left">
                <span>apps: {% if page.total_pages > 1 %}{{ ((page.current - 1) * pageLimit + 1) * page.current }} - {{ (page.current * pageLimit) * page.current }}{% else %}{{ page.total_items }}{% endif %}</span>
            </div>
            {% if page.total_pages > 1 %}
            <ul class="pagination pull-right">
                {% for i in 1..page.total_pages %}
                <li><a href="{{ url('/apps/?page=' ~ i) }}"
                       class="{% if page.current == i %}active{% endif %}">{{ i }}</a></li>
                {% endfor %}
            </ul>
            {% endif %}
        </div>
    </div>
</div>

<div class="modal fade modal-apps" id="app-keys-view-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">App "<span class="description-app-name"></span>" platform keys</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <img src="/img/scene/icons/plus.png"
                         style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                </span></button>
            </div>

            {#<div class="modal-title">#}
                {#<div class="title" id="modalLabel">App "<span class="description-app-name"></span>" platform keys</div>#}
            {#</div>#}
            <div class="modal-body">
                <div class="container">
                    <div class="row platform-info">
                        <div class="col">
                            <div class="row platform-title">
                                <div class="col">
                                    <div class="title text-center text-uppercase">WEB</div>
                                </div>
                            </div>
                            <div class="row platform-data">
                                <div class="col-sm-12">
                                    <p>Platform id: <span class="app-web-platform-id text-highlighted"></span></p>
                                </div>
                                <div class="col-sm-12">
                                    <p>Key: <span class="app-web-platform-key text-highlighted"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row platform-info">
                        <div class="col">
                            <div class="row platform-title">
                                <div class="col">
                                    <div class="title text-center text-uppercase">Android</div>
                                </div>
                            </div>
                            <div class="row platform-data">
                                <div class="col-sm-12">
                                    <p>Platform id: <span class="app-android-platform-id text-highlighted"></span></p>
                                </div>
                                <div class="col-sm-12">
                                    <p>Key: <span class="app-android-platform-key text-highlighted"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row platform-info">
                        <div class="col">
                            <div class="row platform-title">
                                <div class="col">
                                    <div class="title text-center text-uppercase">iOS</div>
                                </div>
                            </div>
                            <div class="row platform-data">
                                <div class="col-sm-12">
                                    <p>Platform id: <span class="app-ios-platform-id text-highlighted"></span></p>
                                </div>
                                <div class="col-sm-12">
                                    <p>Key: <span class="app-ios-platform-key text-highlighted"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer modal-actions text-center">
                <a href="#" class="btn primary action cancel" data-dismiss="modal">close</a>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="app-delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="title" id="modalLabel">Delete app</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <img src="/img/scene/icons/plus.png"
                         style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                </span></button>
            </div>

            {#<div class="modal-title">#}
                {#<div class="title" id="modalLabel">Delete app</div>#}
            {#</div>#}
            <div class="modal-body">
                <div class="content">
                    <p>Confirm deletion the key <span class="description-key text-highlighted"></span></p>
                    <p> of the app <span class="description-app-name text-highlighted"></span>?</p>
                </div>
            </div>
            {#<div class="content">#}
                {#<p>Confirm deletion the key <span class="description-key text-highlighted"></span></p>#}
                {#<p> of the app <span class="description-app-name text-highlighted"></span>?</p>#}
            {#</div>#}
            <div class="modal-footer modal-actions text-center">
                <a href="#" class="btn action cancel btn btn-default" data-dismiss="modal">cancel</a>
                <a href="#" class="btn btn-primary btn primary action confirm-delete">delete</a>
            </div>
        </div>

    </div>
</div>