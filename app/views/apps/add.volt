<form class="form app" autocomplete="off">
    <div class="ui_block app-data">

        <div class="form-group">
            <label for="name">name</label>
            {{ form.render('name', ['class': 'form-control']) }}
            <small id="name-help" class="text-danger form-text hidden"></small>
        </div>

        <div class="form-group">
            <label for="version">version</label>
            {{ form.render('version', ['class': 'form-control', 'disabled': 'true']) }}
            <small id="version-help" class="text-danger form-text hidden"></small>
        </div>

        <div class="form-group">
            <label for="web_platform_id">web platform id</label>
            <input type="text" id="web-platform-id" name="web_platform_id"
                   class="form-control" aria-describedby="web-platform-id-help">
            <small id="web-platform-id-help" class="text-danger form-text hidden"></small>
        </div>

        <div class="form-group">
            <label for="android_platform_id">android platform id</label>
            <input type="text" id="android-platform-id" name="android_platform_id"
                   class="form-control" aria-describedby="android-platform-id-help">
            <small id="android-platform-id-help" class="text-danger form-text hidden"></small>
        </div>

        <div class="form-group">
            <label for="ios_platform_id">ios platform id</label>
            <input type="text" id="ios-platform-id" name="ios_platform_id"
                   class="form-control" aria-describedby="ios-platform-id-help">
            <small id="ios-platform-id-help" class="text-danger form-text hidden"></small>
        </div>

        <div class="form-group actions">
            <button class="btn primary action save">
                <span>save</span>
            </button>
        </div>

    </div>
</form>