
{{ content() }}

<div class="jumbotron">
    <h1>Internal Error</h1>

    {% if error and (error.devError is defined) %}
        {% if error.devError is type('string') %}
            <p>{{ error.devError }}</p>
        {% else %}
            <p>{{ error.devError.string|nl2br }}</p>
        {% endif %}
    {% else %}
        <p>Something went wrong, if the error continue please contact us</p>
    {% endif %}
    <p>{{ link_to('index', 'Home', 'class': 'btn btn-primary') }}</p>
</div>