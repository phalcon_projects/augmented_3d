<div class="statistic-api-keys">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="title">Statistic access API keys</h1>
            </div>
            <div class="col">
                <a class="btn btn-primary action add" href="#">Add new API key</a>
            </div>
        </div>
        <div class="key-list">
            {% for keyItem in keys %}
                <div class="row key-wrapper">
                    <div class="col key">{{ keyItem.api_key }}</div>
                    <div class="col">
                        <a class="btn btn-outline-danger action remove" href="#"
                           data-key-id="{{ keyItem.id }}"
                           data-key-value="{{ keyItem.api_key }}">Remove</a>
                    </div>
                </div>
            {% endfor %}
        </div>
    </div>
</div>

<div class="modal fade" id="modal-statistic-api-key-remove"
     tabindex="-1" role="dialog" aria-labelledby="modal-statistic-api-key-remove-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="img/close.png" alt="">
            </button>

            <div class="modal-title">
                <div class="title" id="modal-statistic-api-key-remove-label">Remove statistic API key</div>
            </div>

            <div class="container">
                <p>Confirm remove statistic API key <span class="description-key text-highlighted"></span>?</p>
            </div>

            <div class="modal-actions text-center">
                <a href="#" class="btn action cancel" data-dismiss="modal">Cancel</a>
                <a href="#" class="btn btn-danger action confirm-remove">Confirm</a>
            </div>
        </div>
    </div>
</div>