<div class="statistics" data-edition-id="{{ editionId }}">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="filters pull-right">
                    <div class="input-daterange input-group" id="datepicker">
                        <input type="text" class="input-sm form-control" name="date-from" value="{{ periodFrom }}">
                        <span class="input-group-addon">to</span>
                        <input type="text" class="input-sm form-control" name="date-to" value="{{ periodTo }}">
                        <button class="btn btn-primary action period-select">Show</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col col-md-12">
                <div class="card">
                    <div class="card-header"><b>System</b></div>
                    <div class="card-body">
                        <div id="chart-system"></div>
                    </div>
                </div>
            </div>

            <div class="col col-md-12">
                <div class="card">
                    <div class="card-header"><b>Views</b></div>
                    <div class="card-body">
                        <div id="chart-views"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card assets">
                    <div class="card-header"><b>Assets</b></div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col"><b>Element</b></th>
                                <th scope="col"><b>Name</b></th>
                                <th scope="col"><b>Views</b></th>
                                <th scope="col"><b>Touches</b></th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for assetItem in assetsStatistic %}
                            <tr>
                                <th class="asset-image">
                                    <div class="image-wrapper">
                                        <img class="image"
                                             src="{{ url((assetItem.thumb ? 'files/thumbs/' ~ assetItem.thumb : '/img/object-icon.svg')) }}">
                                    </div>
                                </th>
                                <td class="element">{{ assetItem.entity }}</td>
                                <td class="name">{{ assetItem.name }}</td>
                                <td class="views">
                                    <div class="row">
                                        <div class="col">
                                            <div class="count">{{ assetItem.ios_views_count }}</div>
                                            <div class="platform"><img src="{{ url('/img/icons/apple-black.svg') }}">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="count">{{ assetItem.android_views_count }}</div>
                                            <div class="platform"><img src="{{ url('/img/icons/android.svg') }}"></div>
                                        </div>
                                    </div>
                                </td>
                                <td class="touches">
                                    <div class="row">
                                        <div class="col">
                                            <div class="count">{{ assetItem.ios_touch_count }}</div>
                                            <div class="platform"><img src="{{ url('/img/icons/apple-black.svg') }}">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="count">{{ assetItem.android_touch_count }}</div>
                                            <div class="platform"><img src="{{ url('/img/icons/android.svg') }}"></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col">
                <div class="card page">
                    <div class="card-header"><b>Usage</b></div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col"><b>Cover</b></th>
                                <th scope="col"><b>Title</b></th>
                                <th scope="col"><b>Events</b></th>
                                <th scope="col"><b>Downloads</b></th>
                            </tr>
                            </thead>
                            <tbody>
                            {% for pageItem in pagesStatistic %}
                                <tr>
                                    <th class="page-image">
                                        <div class="image-wrapper">
                                            <img class="image" src="{{ url('files/covers/' ~ pageItem.image) }}">
                                        </div>
                                    </th>
                                    <td class="title">{{ pageItem.title }}</td>
                                    <td class="events">
                                        <div class="row">
                                            <div class="col d-flex">
                                                <div class="count">{{ pageItem.page_ios_views }}</div>
                                                <div class="platform">
                                                    <img src="{{ url('/img/icons/apple-black.svg') }}">
                                                </div>
                                            </div>
                                            <div class="col d-flex">
                                                <div class="count">{{ pageItem.page_android_views }}</div>
                                                <div class="platform">
                                                    <img src="{{ url('/img/icons/android.svg') }}">
                                                </div>
                                            </div>
                                            <div class="col d-flex total-wrapper">
                                                <div class="total">{{ pageItem.page_ios_views + pageItem.page_android_views}}</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="touches">
                                        <div class="row">
                                            <div class="col d-flex">
                                                <div class="count">{{ pageItem.page_ios_downloads }}</div>
                                                <div class="platform">
                                                    <img src="{{ url('/img/icons/apple-black.svg') }}">
                                                </div>
                                            </div>
                                            <div class="col d-flex">
                                                <div class="count">{{ pageItem.page_android_downloads }}</div>
                                                <div class="platform"><img src="{{ url('/img/icons/android.svg') }}"></div>
                                            </div>
                                            <div class="col d-flex total-wrapper">
                                                <div class="total">{{ pageItem.page_ios_downloads + pageItem.page_android_downloads }}</div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            {% endfor %}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>