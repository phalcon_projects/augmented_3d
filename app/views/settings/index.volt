<section class="settings">
    <div class="row">
        <div class="col">
            <div id="accordion" class="accordion">
                <div class="card">
                    <div class="card-header" id="settings-youtube-download-label">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#settings-youtube-download"
                                    aria-expanded="true" aria-controls="settings-youtube-download">
                                YouTube video download users access
                            </button>
                        </h5>
                    </div>

                    <div id="settings-youtube-download" class="collapse show"
                         aria-labelledby="settings-youtube-download-label" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <form>
                                        <div class="form-group">
                                            <input class="form-control" type="text" id="youtube-download-user-invite"
                                                   name="youtube-download-user-invite"
                                                   placeholder="Enter the name or email to invite user to the YouTube download access list">
                                            <div class="dropdown youtube-download-access-list-invite-list">
                                                <div class="dropdown-menu" id="youtube-download-access-list-invite-menu"
                                                     aria-labelledby="dropdownMenuButton">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="list-group youtube-download-access-list">
                                        {% for accessListItem in youtubeDownloadAccessUserList %}
                                            <div class="list-group-item" data-id="{{ accessListItem.id }}">
                                                <span>{{ accessListItem.first_name }} {{ accessListItem.surname }} {{ accessListItem.email }}</span>
                                                {% if accessListItem.id != currentUser.id and accessListItem.role != 3 %}
                                                <a class="btn btn-danger action remove">
                                                    <img src="/img/del.png" alt="Remove">
                                                </a>
                                                {% endif %}
                                            </div>
                                        {% endfor %}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>