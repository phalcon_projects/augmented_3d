<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    {{ get_title() }}
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

    {{ assets.outputCss() }}
    <base href="{{ url('') }}">
</head>
<body>
    <div id="errors-container">
        {{ flash.output() }}</div>

    <div class="container">
        {{ content() }}
    </div>

    {{ assets.outputJs() }}
</body>
</html>