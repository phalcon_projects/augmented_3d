<!DOCTYPE html>
<html lang="en">
<head>
    {{ get_title() }}
    <meta charset="UTF-8">
    <meta name="Description" content="">
    <meta name="Keywords" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    {{ assets.outputCss() }}
    {{ assets.outputJs('head') }}
    <base href="{{ url('') }}">

</head>
<body>

<div class="wrapper admin-panel">
    {{ flash.output() }}

    <div class="main-sidebar">

        <div class="top text-center">
            <a href="" class="logo">
                {{ image("img/logo.svg") }}
            </a>
            <div class="name">
                {{ auth['first_name'] }} {{ auth['surname'] }}
            </div>
        </div>
        {{ partial('partials/navigation') }}
        <div class="bottom text-center copyright">cosomedia GmbH & Co. KG</div>
    </div>
    <div class="content-wrapper">
        <div class="title">
            <h2>
                {% if section_title is defined %}
                {{ section_title }}
                {% endif %}
            </h2>
        </div>

        {{ content() }}

        {{ partial('partials/message-boxes') }}
    </div>

    <div class="modal confirm" id="modal-confirm">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><span class="modal-confirm-title">Confirm</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="/img/scene/icons/plus.png"
                                 style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                        </span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer modal-actions">
                    <button id="modal-action-confirm" type="button" class="btn btn-primary">Confirm</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>

    {{ assets.outputJs() }}
</body>
</html>