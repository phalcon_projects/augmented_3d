<!DOCTYPE html>
<html lang="en">
<head>
    {{ get_title() }}
    <meta charset="UTF-8">
    <meta name="Description" content="">
    <meta name="Keywords" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    {{ assets.outputCss() }}
    {{ assets.outputJs('head') }}
    <base href="{{ url('') }}">

</head>
<body>

<div class="wrapper admin-panel error">
    {{ flash.output() }}

    <div class="content-wrapper">
        <div class="title">
            <h2>
                {% if section_title is defined %}
                {{ section_title }}
                {% endif %}
            </h2>
        </div>

        {{ content() }}

    </div>
</div>

</body>
</html>