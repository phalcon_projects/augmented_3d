<!DOCTYPE html>
<html lang="en">
<head>
    {{ get_title() }}
    <meta charset="UTF-8">
    <meta name="Description" content="">
    <meta name="Keywords" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    {{ assets.outputCss() }}
    {{ assets.outputJs('head') }}
    <base href="{{ url('') }}">

</head>
<body>

<div class="wrapper admin-panel-slim">
    {{ flash.output() }}

    <div class="main-sidebar">
        <div class="toggle-sidebar">
            <span class="hide-sidebar glyphicon glyphicon-triangle-left"></span>
            <span class="show-sidebar glyphicon glyphicon-triangle-right"></span>
        </div>

        <div class="top text-center">
            <a href="" class="logo">
                {{ image("img/logo.png") }}
            </a>
            <div class="name">
                {{ auth['first_name'] }} {{ auth['surname'] }}
            </div>
        </div>
        {{ partial('partials/navigation') }}
        <div class="bottom text-center copyright">cosomedia GmbH & Co. KG</div>
    </div>
    <div class="content-wrapper wrapper-full">
        
        {{ content() }}

        {{ partial('partials/message-boxes') }}
    </div>
</div>

    {{ assets.outputJs() }}
{{ assets.outputInlineJs() }}
</body>
</html>