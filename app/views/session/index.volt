{{ stylesheet_link('css/login.css') }}

{# Facebook JavaScript SDK #}
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '{{ FB_appId }}',
            status: true,
            cookie: true, // This is important, it's not enabled by default
            version: 'v{{ FB_apiVersion }}'
        });

        FB.Event.subscribe('auth.statusChange', function (response) {
            window.fbLoginStatus = response.status;
            $(".btn-sign-in-facebook").show();
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

{{ content() }}

<div class="card sign-in">
    <img class="card-image" src="img/logo.svg" alt="VisionPaper">
    <div class="card-body">
        <form action="session/start" class="form-signin" role="form" method="post">
            <div class="form-group">
                <input type="text" name="login" class="form-control" placeholder="login" required autofocus>
            </div>
            <div class="divider"></div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="password" required>
            </div>
            <div class="form-group actions">
                <button class="btn btn-primary btn-block action btn-sign-in" type="submit">sign in</button>
            </div>
            <div class="form-group">
                <button class="btn btn-lg btn-block action btn-sign-in-facebook" type="submit">sign in with facebook</button>
            </div>
            <div class="form-group">
                <a class="btn btn-lg btn-block action sign-in-google" href="{{ googleAuthUrl }}">
                    <img src="img/social/google.png" alt="Google">sign in with google</a>
            </div>
            <a href="#" class="pull-left help-link">forgotten password</a>
        </form>
    </div>
</div>

<div class="logo-bottom">
    <img src="img/vision_lab_logo.png">
</div>

{# facebook email permissions re-request #}
<div class="modal fade password-modal" id="password_modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <img src="img/close.png" alt="">
            </button>
            <div class="">
                <form autocomplete="off">
                    <div class="title" id="modalLabel">Facebook login</div>

                    <div class="row_col">Email is required to login with Facebook</div>

                    <div class="buttons">
                        <a href="#" class="btn revise">Revise permissions</a>
                        <a href="#" class="btn abort" data-dismiss="modal">abort</a>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<script src="//apis.google.com/js/client:plusone.js"></script>