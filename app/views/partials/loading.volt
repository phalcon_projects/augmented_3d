<div class="loading-control">
    <div class="load-icon"></div>
    <div class="load-content">
        <div class="cancel"></div>

        <div class="progress-infinite">Processing...</div>
        <div class="progress-text-info">cover.jpg</div>
        <div class="progress-control">
            <div class="progress-bar">
                <div class="progress-fill">
                    <div class="progress-text">
                        <div class="text">5 MB</div>
                        <div class="line"></div>
                    </div>
                </div>
            </div>
            <div class="progress-total">10 MB</div>
        </div>
    </div>
</div>