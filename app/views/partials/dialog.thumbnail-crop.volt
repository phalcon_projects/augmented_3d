<div class="thumbnail-crop-dialog" id="thumbnail-crop-dialog">
    <div class="close"></div>

    <div class="head"></div>
    <div class="center">
    </div>
    <div class="buttons">
        <button class="cancel">Cancel</button>
        <button class="submit">Save</button>
    </div>
    <div class="precise">
        <form>
            <label class="crop-x">x:<input name="crop-x" type="number" value="0" /></label>
            <label class="crop-y">y:<input name="crop-y" type="number" value="0" /></label>
            <label class="crop-width">w:<input name="crop-width" type="number" value="0" /></label>
            <label class="crop-height">h:<input name="crop-height" type="number" value="0" /></label>

            <input type="submit" style="display: none;">
        </form>
    </div>
</div>