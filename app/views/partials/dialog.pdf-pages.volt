<div class="pdf-pages-dialog" id="pdf-pages-dialog">
    <div class="close"></div>
    <div>
        <div class="head">
            <div class="icon"></div>
            <div class="content">

                <div class="progress-control">
                    <div class="progress-bar">
                        <div class="progress-fill">
                            <div class="progress-text">
                                <div class="text">? MB</div>
                                <div class="line"></div>
                            </div>
                        </div>
                    </div>
                    <div class="progress-total">? MB</div>
                </div>

                <div class="filename"></div>
                <div class="selection">Select <span class="selected">0</span> of <span class="total">0</span></div>
            </div>
        </div>
        <div class="center"></div>
        <div class="buttons">
            <button class="select-all">Select all</button>
            <button class="insert">Insert</button>
        </div>
    </div>
</div>