<div class="modal fade" id="ui-confirm-modal" tabindex="-1" role="dialog" aria-labelledby="ui-confirm-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title ui-message-title" id="ui-confirm-modal-label">Confirm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <img src="/img/scene/icons/plus.png"
                         style="-webkit-transform: rotate(-45deg);-moz-transform: rotate(-45deg);-ms-transform: rotate(-45deg);-o-transform: rotate(-45deg);transform: rotate(-45deg);">
                </span></button>
            </div>
            <div class="modal-body">
                <div class="block-content">
                    <form autocomplete="off">
                        <div class="form-group row">
                            <div class="col">
                                <div class="content ui-message-text"></div>
                            </div>
                        </div>
                </div>
            </div>
                        <div class="modal-footer">
                            {#<div class="row">#}
                                {#<div class="col col-sm-6">#}
                                    <button class="btn btn-confirm btn-primary action confirm">Ok</button>
                                {#</div>#}
                                {#<div class="col col-sm-6">#}
                                    <button class="btn btn-abort btn-primary action cancel"
                                            data-dismiss="modal">Cancel</button>
                                {#</div>#}
                            {#</div>#}
                        </div>
                    </form>
                {#</div>#}
            {#</div>#}
        </div>

    </div>
</div>