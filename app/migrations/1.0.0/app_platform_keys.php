<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class AppPlatformKeysMigration_100
 */
class AppPlatformKeysMigration_100 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('app_platform_keys', [
                'columns' => [
                    new Column(
                        'app_id',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 11,
                            'first' => true
                        ]
                    ),
                    new Column(
                        'platform_type',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'default' => "1",
                            'notNull' => true,
                            'size' => 1,
                            'after' => 'app_id'
                        ]
                    ),
                    new Column(
                        'platform_id',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'platform_type'
                        ]
                    ),
                    new Column(
                        'key',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 255,
                            'after' => 'platform_id'
                        ]
                    )
                ],
                'indexes' => [
                    new Index('PRIMARY', ['app_id', 'platform_type'], 'PRIMARY'),
                    new Index('app_platform_keys_key_uindex', ['key'], 'UNIQUE'),
                    new Index('app_platform_keys_app_id_index', ['app_id'], null),
                    new Index('app_platform_keys_platform_id_index', ['platform_type'], null)
                ],
                'references' => [
                    new Reference(
                        'app_platform_keys_apps_id_fk',
                        [
                            'referencedTable' => 'apps',
                            'columns' => ['app_id'],
                            'referencedColumns' => ['id'],
                            'onUpdate' => 'CASCADE',
                            'onDelete' => 'CASCADE'
                        ]
                    )
                ],
                'options' => [
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'latin1_swedish_ci'
                ],
            ]
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
