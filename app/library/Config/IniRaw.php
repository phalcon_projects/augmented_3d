<?php
/**
 * read ini file without plalcons autocast
 */

namespace Config;


class IniRaw extends \Phalcon\Config\Adapter\Ini
{
	protected $mode;

	public function __construct($filePath, $mode = null)
	{
		$this->mode = $mode;
		parent::__construct($filePath, $mode);
	}

	protected function _cast($ini) {
			return $ini;
	}
}