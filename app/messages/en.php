<?php

$messages = [
    'invoice' => 'Invoice',
    'no' => ' No',
    'customer' => 'Client',
    'customer VAT ID' => 'Customer VAT ID',
    'date' => 'Date',
    'delivery date' => 'Delivery date',
    'invoice-delivery-info' => 'Our deliveries / services are charged to you as follows.',
    'title' => 'Title',
    'total' => 'Total',
    'charges-rent-monthly' => 'Monthly rental charges',
    'subtotal' => 'Subtotal',
    'net' => 'net',
    'value added tax' => 'Value added tax',
    'total amount' => 'Total amount',
    'payable immediately, net' => 'Payable immediately, net',
    'additional-info-product-usage' => 'The usage package refers to the number of active users and is automatically adjusted.',
    'up to' => 'up to',
    'users' => 'users',
    'from' => 'from',
    'recalculation' => 'recalculation',
    'many thanks' => 'Many thanks for the good cooperation.'
];