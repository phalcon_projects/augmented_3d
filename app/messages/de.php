<?php

$messages = [
    'invoice' => 'Rechnungs',
    'no' => 'nr',
    'customer' => 'Kunden',
    'customer VAT ID' => 'Kunden-USt-Id',
    'date' => 'Datum',
    'delivery date' => 'Lieferdatum',
    'invoice-delivery-info' => 'Unsere Lieferungen/Leistungen stellen wir Ihnen wie folgt in Rechnung.',
    'title' => 'Bezeichnung',
    'total' => 'Gesamt',
    'charges-rent-monthly' => 'Monatliche Nutzungspauschale',
    'subtotal' => 'Zwischensumme',
    'net' => 'netto',
    'value added tax' => 'Umsatzsteuer',
    'total amount' => 'Gesamtbetrag',
    'payable immediately, net' => 'Zahlbar sofort, rein netto',
    'additional-info-product-usage' => 'Die Nutzungspauschale bezieht sich auf die Anzahl aktiver User und wird automatisch angepasst.',
    'up to' => 'bis',
    'users' => 'User',
    'from' => 'ab',
    'recalculation' => 'Neuberechnung',
    'many thanks' => 'Vielen Dank für die gute Zusammenarbeit.'
];