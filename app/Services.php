<?php

use Phalcon\Mvc\View;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaData;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Mvc\Router;

class Services extends \Base\Services
{

    /**
     * We register the events manager
     */
    protected function initDispatcher()
    {
        $eventsManager = new EventsManager;

        /**
         * Check if the user is allowed to access certain action using the SecurityPlugin
         */
        $eventsManager->attach('dispatch:beforeDispatch', new SecurityPlugin);

        /**
         * Handle exceptions and not-found exceptions using NotFoundPlugin
         */
        $eventsManager->attach('dispatch:beforeException', new NotFoundPlugin);

        $dispatcher = new Dispatcher;
        $dispatcher->setEventsManager($eventsManager);

        return $dispatcher;
    }

    /**
     * The URL component is used to generate all kind of urls in the application
     */
    protected function initUrl()
    {
        $url = new UrlProvider();
        $url->setBaseUri($this->get('config')->application->baseUri);
        return $url;
    }

    protected function initView()
    {
        $view = new View();
        $view->setViewsDir(APP_PATH . '/' . $this->get('config')->application->viewsDir);
        $view->registerEngines(['.volt' => 'volt']);

        return $view;
    }

    /**
     * Setting up volt
     * @param $view
     * @param $di
     * @return VoltEngine
     */
    protected function initSharedVolt($view, $di)
    {
        $volt = new VoltEngine($view, $di);

        $volt->setOptions([
            'compiledPath' => APP_PATH . '/cache/'
        ]);

        $compiler = $volt->getCompiler();
        $compiler->addFunction('is_a', 'is_a');
        $compiler->addFunction('number_format', 'number_format');
        $compiler->addFunction('strtotime', 'strtotime');
        /*$compiler->addFunction('format_filesize', function ($size) {
            $size = (int)$size;

            if (!$size) {
                return '0 mb';
            }

            $kb = 1024;
            $mb = $kb * 1024;
            $gb = $mb * 1024;
            $val = $size;

            if ($size < $kb) {
                $units = 'b';
            } elseif ($size < $mb) {
                $val = $size / $kb;
                $units = 'kb';
            } elseif ($size < $gb) {
                $val = $size / $mb;
                $units = 'mb';
            } else {
                $val = $size / $gb;
                $units = 'gb';
            }

            return number_format($val) . ' ' . $units;
        });*/

        return $volt;
    }

    /**
     * Database connection is created based in the parameters defined in the configuration file
     */
    protected function initDb()
    {
        $config = $this->get('config')->get('database')->toArray();
        $dbClass = 'Phalcon\Db\Adapter\Pdo\\' . $config['adapter'];
        unset($config['adapter']);

        return new $dbClass($config);
    }

    /**
     * If the configuration specify the use of metadata adapter use it or use memory otherwise
     */
    protected function initModelsMetadata()
    {
        return new MetaData();
    }

    /**
     * Start the session the first time some component request the session service
     */
    protected function initSession()
    {
        $session = new SessionAdapter();
        $session->start();
        return $session;
    }

    /**
     * Register the flash service with custom CSS classes
     */
    protected function initFlash()
    {
        return new FlashSession([
            'error' => 'alert alert-danger',
            'success' => 'alert alert-success',
            'notice' => 'alert alert-info',
            'warning' => 'alert alert-warning'
        ]);
    }

    /**
     * Register a user component
     */
    protected function initElements()
    {
        return new Elements();
    }

    /**
     * Register router
     */
    protected function initRouter()
    {
        // Create the router
        $router = new Router();

        // list of project
        $router->add(
            '/',
            [
                'controller' => 'projects',
                'action' => 'index',
                'name' => 'projects'
            ]
        );

        $router->add(
            '/index/bundle',
            [
                'controller' => 'index',
                'action' => 'bundle'
            ]
        );

        // list editions of project
        $router->add(
            '/projects/{project_id:[0-9]+}/?',
            [
                'controller' => 'editions',
                'action' => 'index',
                'name' => 'editions'
            ]
        );

        // project access
        $router->add(
            '/projects/access-teams',
            [
                'controller' => 'projects',
                'action' => 'getTeamsForAccess',
                'name' => 'accessTeam'
            ]
        );

        // project access change
        $router->add(
            '/projects/access-team',
            [
                'controller' => 'projects',
                'action' => 'setTeamAccess',
                'name' => 'setAccessTeam'
            ]
        );

        // view edition scene
        $router->add(
            '/projects/{project_id:[0-9]+}/{edition_id:[0-9]+}/?',
            [
                'controller' => 'editions',
                'action' => 'view',
                'name' => 'editionScene'
            ]
        );

        $router->add(
            '/editions/set-custom-fields',
            [
                'controller' => 'editions',
                'action' => 'saveCustomFields'
            ]
        );

        // edition media pool
        $router->add(
            '/library/usage/{id:[0-9]+}',
            [
                'controller' => 'library',
                'action' => 'usage'
            ]
        );

        $router->add(
            '/library/{edition_id:[0-9]+}',
            [
                'controller' => 'library',
                'action' => 'index'
            ]
        );

        $router->add(
            '/library/upload-object/:params?',
            [
                'controller' => 'library',
                'action' => 'upload'
            ]
        );

        $router->add(
            '/covers/:params?',
            [
                'controller' => 'covers',
                'action' => 'index'
            ]
        );

        $router->add(
            '/covers/save',
            [
                'controller' => 'covers',
                'action' => 'save'
            ]
        );

        $router->add(
            '/apps/{app_id:[0-9]+}',
            [
                'controller' => 'apps',
                'action' => 'edit'
            ]
        );

        /* region License */
        $router->add(
            '/license/',
            [
                'controller' => 'licenses',
                'action' => 'index'
            ]
        );
        $router->add(
            '/license/order',
            [
                'controller' => 'licenses',
                'action' => 'order'
            ]
        );
        $router->add(
            '/license/expired',
            [
                'controller' => 'licenses',
                'action' => 'expired'
            ]
        );
        $router->add(
            '/license/invoice/{invoice_id:[0-9]+}/?',
            [
                'controller' => 'licenses',
                'action' => 'invoice'
            ]
        );
        $router->add(
            '/license/payment-success',
            [
                'controller' => 'licenses',
                'action' => 'paymentSuccess'
            ]
        );
        $router->add(
            '/license/payment-cancel',
            [
                'controller' => 'licenses',
                'action' => 'paymentCancel'
            ]
        );
        $router->add(
            '/license/traffic-plan/',
            [
                'controller' => 'licenses',
                'action' => 'trafficPlanView'
            ]
        );
        $router->add(
            '/license/traffic-plan-change',
            [
                'controller' => 'licenses',
                'action' => 'trafficPlanChange'
            ]
        );
        $router->add(
            '/license/cloud-space-plan/',
            [
                'controller' => 'licenses',
                'action' => 'cloudSpacePlanView'
            ]
        );
        $router->add(
            '/license/cloud-space-plan-change',
            [
                'controller' => 'licenses',
                'action' => 'cloudSpacePlanChange'
            ]
        );
        /* endregion License */

        /* region Users */
        /* endregion Users */

        /* region Teams */
        $router->add(
            '/teams/',
            [
                'controller' => 'teams',
                'action' => 'index'
            ]
        );
        $router->add(
            '/teams/permissions',
            [
                'controller' => 'teams',
                'action' => 'getPermissions'
            ]
        );
        $router->add(
            '/teams/create',
            [
                'controller' => 'teams',
                'action' => 'create'
            ]
        );
        $router->add(
            '/teams/edit',
            [
                'controller' => 'teams',
                'action' => 'edit'
            ]
        );
        $router->add(
            '/teams/users-for-add',
            [
                'controller' => 'teams',
                'action' => 'getUsersForAdd'
            ]
        );
        $router->add(
            '/teams/add-user',
            [
                'controller' => 'teams',
                'action' => 'addUserToTeam'
            ]
        );
        $router->add(
            '/teams/invite-user',
            [
                'controller' => 'teams',
                'action' => 'inviteUser'
            ]
        );
        $router->add(
            '/teams/remove-member',
            [
                'controller' => 'teams',
                'action' => 'removeUserFromTeam'
            ]
        );
        $router->add(
            '/teams/delete',
            [
                'controller' => 'teams',
                'action' => 'delete'
            ]
        );
        /* endregion Teams */

        /* region Video */
        $router->add(
            '/video-link/save',
            [
                'controller' => 'library',
                'action' => 'videoLinkSave'
            ]
        );
        $router->add(
            '/video-link/add',
            [
                'controller' => 'library',
                'action' => 'videoLinkAdd'
            ]
        );
        /* endregion Video */

        /* region REST API */
        $router->add(
            '/api/validate',
            [
                'controller' => 'apps',
                'action' => 'apiValidate'
            ]
        );
        $router->add(
            '/api/covers',
            [
                'controller' => 'index',
                'action' => 'apiGetBundles'
            ]
        );
        $router->add(
            '/api/bundle',
            [
                'controller' => 'index',
                'action' => 'apiGetBundle'
            ]
        )->setName('api-bundle');
        $router->add(
            '/api/bundle/validate',
            [
                'controller' => 'index',
                'action' => 'apiBundleValidate'
            ]
        )->setName('api-bundle-validate');
        /* endregion REST API */

        /* region Google Authentication */
        $router->add('/oauth-callback-google/:params', [
            'controller' => 'session',
            'action' => 'auth',
            'params' => 1
        ]);
        /* endregion Google Authentication */

        /*$router->add(
            '/:controller/:action/:params',
            [
                //'namespace' => 'App\Controllers',
                'controller' => 1,
                'action' => 2,
                'params' => 3,
            ]
        );

        $router->add(
            '/:controller',
            [
                //'namespace' => 'App\Controllers',
                'controller' => 1
            ]
        );*/

        /* region Statistics */

        $router->add('/statistics', [
            'controller' => 'statistics',
            'action' => 'index'
        ]);

        $router->add('/statistics/{edition_id:[0-9]+}/:params', [
            'controller' => 'statistics',
            'action' => 'index',
            'params' => 'params'
        ]);

        $router->add('/statistics/get', [
            'controller' => 'statistics',
            'action' => 'getStatistic'
        ]);

        $router->add('/statistics/api-keys', [
            'controller' => 'statistics',
            'action' => 'manageAPIKeys'
        ]);

        $router->add('/statistics/add-api-key', [
            'controller' => 'statistics',
            'action' => 'addAPIKey'
        ]);

        $router->add('/statistics/remove-api-key', [
            'controller' => 'statistics',
            'action' => 'removeAPIKey'
        ]);

        /* region Statistics API v1 */
        $router->add('/api/v1/statistics', [
            'controller' => 'statistics',
            'action' => 'api'
        ]);
        /* endregion Statistics API v1 */

        /* endregion Statistics */

        // Super admin settings
        $router->add(
            '/settings',
            [
                'controller' => 'settings',
                'action' => 'index'
            ]
        );
        $router->add(
            '/settings/youtube-download-access-list-add',
            [
                'controller' => 'settings',
                'action' => 'youTubeAccessListAdd'
            ]
        );
        $router->add(
            '/settings/youtube-download-access-list-remove',
            [
                'controller' => 'settings',
                'action' => 'youTubeAccessListRemove'
            ]
        );
        $router->add(
            '/settings/youtube-download-access-list-get-to-invite',
            [
                'controller' => 'settings',
                'action' => 'youTubeAccessListGetToInvite'
            ]
        );

        $router->handle();

        /*$route = $router->getMatchedRoute();
        var_dump($route->getRouteId(), $route->getName(), $route);exit;*/

        return $router;
    }

}
