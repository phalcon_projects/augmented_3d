<?php

use Phalcon\Http\Response;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class StatisticsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Statistic');
        $this->view->setVar('section_title', 'Statistic');

        parent::initialize();
    }

    /**
     * Statistics overview page
     */
    public function indexAction()
    {
        $editionId = $this->dispatcher->getParam('edition_id');
        $edition = Editions::findFirst($editionId);

        if (!$this->currentUser->id || !Users::findFirst($this->currentUser->id) || !$edition) {
            return $this->response->redirect([
                'controller' => 'errors',
                'action' => 'show404'
            ]);
        }

        $pageNumber = 1;
        $pageSize = 8;
        $searchKeyQuery = null;

        if ($this->request->isPost()) {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, StatisticEdition::class, $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, StatisticEdition::class, $this->request->get());

            if ($this->request->has('page')) {
                $pageNumber = $this->request->getQuery('page', 'int');
            }

            if ($this->request->has('limit')) {
                $pageSize = $this->request->getQuery('limit', 'int');
            }

            if ($this->request->has('search')) {
                $searchKeyQuery = $this->request->getQuery('search', 'string');
            }
        }

        $query->orderBy('name');
        $parameters = $query->getParams();

        $dateFrom = date('Y-m-d', time() - 86400 * 10);
        $dateTo = date('Y-m-d', time());

        $parameters['edition_id'] = $editionId;
        $parameters['date_from'] = $dateFrom = $this->request->get('date_from') ?: $dateFrom;
        $dateTo = $this->request->get('date_to') ?: $dateTo;
        $dateToNext = new DateTime($dateTo);
        $dateToNext->add(new DateInterval('P1D'));
        $parameters['date_to'] = $dateTo = $dateToNext->format('Y-m-d');

        $items = StatisticEdition::find($parameters);

        if ($this->request->has('search') && count($items) == 0) {
            $this->flash->notice('The search did not find any.');
        }

        $pageNumber = max((int)$pageNumber, 1);
        $pageSize = max((int)$pageSize, 1);

        $data = [
            'data' => $items,
            'limit' => min($pageSize, 1000),
            'page' => $pageNumber,
        ];

        if (!$items || empty($items)) {
            $paginator = new PaginatorArray($data);
        } else {
            $paginator = new PaginatorModel($data);
        }

        /*$editions = $this->modelsManager->executeQuery(
            'SELECT * FROM augmented3d.Editions e
              LEFT JOIN augmented3d.Projects p ON e.project_id = p.id
              LEFT JOIN augmented3d.Users u ON p.user_id = u.id'
        );

        $editionIds = [];

        foreach ($editions as $editionItems) {
            foreach ($editionItems as $editionItem) {
                if (!in_array($editionItem->id, $editionIds)) {
                    $editionIds[] = $editionItem->id;
                }
            }
        }*/

        $editionLoads = $this->getEditionLoads($editionId, $dateFrom, $dateTo);
        $assetsStatistic = $this->getAssetsStatistics($editionId, $dateFrom, $dateTo);
        $pagesStatistic = $this->getPageStatistics($editionId, $dateFrom, $dateTo);

        $this->view->editionId = $editionId;
        $this->view->editionStatistic = $editionLoads;
        $this->view->assetsStatistic = $assetsStatistic;
        $this->view->pagesStatistic = $pagesStatistic;
        $this->view->page = $paginator->getPaginate();
        $this->view->pageLimit = $pageSize;
        $this->view->searchFilter = $searchKeyQuery;
        $this->view->periodFrom = $dateFrom;
        $this->view->periodTo = $dateTo;
        $this->assets->addCss('css/statistics.css');
        $this->assets->addCss('vendor/bootstrap-datepicker/css/bootstrap-datepicker.css');
        $this->assets->addJs('vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js');
        $this->assets->addJs('js/pages/statistics/statistics.js');
    }

    public function getStatisticAction()
    {
        if ($this->request->isPost()) {
            $editionId = $this->request->get('edition_id');
            $dateFrom = $this->request->get('date_from');
            $dateTo = $this->request->get('date_to');
            $statistic = $this->getEditionLoads($editionId, $dateFrom, $dateTo);
            $result = [
                'status' => 'ok',
                'systems' => [
                    'Android' => 0,
                    'iOS' => 0
                ],
                'views' => []
            ];

            foreach ($statistic as $statisticItem) {
                $result['systems']['Android'] += $statisticItem->android_count;
                $result['systems']['iOS'] += $statisticItem->ios_count;
                $result['views'][] = [
                    $statisticItem->date,
                    (int)$statisticItem->android_count,
                    (int)$statisticItem->ios_count
                ];
            }

            return $this->response->setJsonContent($result);
        }

        $this->show404();
    }

    protected function getEditionLoads($editionId, $dateFrom, $dateTo)
    {
        $statisticSQL = "
          SELECT
            DATE_FORMAT(s.date, '%Y-%m-%d') date,
            SUM(IF (d.os = 'ios', 1, 0)) ios_count,
            SUM(IF (d.os = 'android', 1, 0)) android_count
          FROM StatisticEdition s
            LEFT JOIN StatisticDevice d ON s.id = d.statistic_id
          WHERE edition_id = {$editionId} AND s.date >= '" . $dateFrom . "' AND s.date <= '" . $dateTo . "'
          GROUP BY date
          ORDER BY s.date
        ";
        return $this->modelsManager->executeQuery($statisticSQL);
        /*return $this->modelsManager->createBuilder()
            ->addFrom(StatisticEdition::class, 's')
            ->columns([
                'DATE_FORMAT(s.date, "%Y-%m-%d") date',
                'SUM(IF (d.os = "ios", 1, 0)) ios_count',
                'SUM(IF (d.os = "android", 1, 0)) android_count'
            ])
            ->join(StatisticDevice::class, 'StatisticEdition.id = StatisticDevice.statistic_id')
            ->where('StatisticEdition.edition_id = ' . $editionId)
            ->andWhere('StatisticEdition.date >= ' . $dateFrom)
            ->andWhere('StatisticEdition.date <= ' . $dateTo)
            ->groupBy('StatisticEdition.date')
            ->orderBy('StatisticEdition.date')
            ->getQuery()
            ->execute();*/
    }

    protected function getAssetsStatistics($editionId, $dateFrom, $dateTo)
    {
        $assetsStatisticSQL = "
          SELECT
            s.name,
            s.entity,
            o.thumb,
            SUM(IF(d.os = 'ios' AND s.parameter = 'view', value, 0))      AS ios_views_count,
            SUM(IF(d.os = 'ios' AND s.parameter = 'touch', value, 0))     AS ios_touch_count,
            SUM(IF(d.os = 'android' AND s.parameter = 'view', value, 0))  AS android_views_count,
            SUM(IF(d.os = 'android' AND s.parameter = 'touch', value, 0)) AS android_touch_count
          FROM StatisticEdition s
            LEFT JOIN StatisticDevice d ON s.id = d.statistic_id
            LEFT JOIN Library o ON o.id = s.entity_id
          WHERE s.edition_id = {$editionId} AND s.parameter != 'load' AND d.os IS NOT NULL
                AND s.date >= '{$dateFrom}'
                AND s.date <= '{$dateTo}'
          GROUP BY
                s.entity_id,
                s.name,
                s.entity
            ";
        return $this->modelsManager->executeQuery($assetsStatisticSQL);
    }

    protected function getPageStatistics($editionId, $dateFrom, $dateTo)
    {
        $pagesStatisticSQL = "
            SELECT
              s.edition_id,
              c.filename image,
              c.id,
              c.title,
              SUM(IF(d.os = 'ios' AND s.parameter != 'download', value, 0)) page_ios_views,
              SUM(IF(d.os = 'android' AND s.parameter != 'download', value, 0)) page_android_views,
              SUM(IF(d.os = 'ios' AND s.parameter = 'download', value, 0)) page_ios_downloads,
              SUM(IF(d.os = 'android' AND s.parameter = 'download', value, 0)) page_android_downloads
            FROM a3dstatistics.StatisticEdition s
              LEFT JOIN a3dstatistics.StatisticDevice d ON s.id = d.statistic_id
              LEFT JOIN augmented3d.Covers c ON s.cover_id = c.id
            WHERE s.edition_id = {$editionId}
              AND c.id IS NOT NULL
              AND s.cover_id != 0
              AND s.cover_id IS NOT NULL
              AND s.date >= '{$dateFrom}'
              AND s.date <= '{$dateTo}'
            GROUP BY
              s.cover_id,
              s.edition_id
            ";

        return $this->modelsManager->executeQuery($pagesStatisticSQL);
    }

    public function manageAPIKeysAction()
    {
        //$this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $apiKeys = StatisticAPIKey::find(['user_id = ' . $this->currentUser->id]);
        $apiKeys = $apiKeys ?: [];

        $this->assets->addCss('css/statistics.css');
        $this->assets->addJs('js/pages/statistics/api-keys.js');
        $this->view->render('statistics', 'api-keys', ['keys' => $apiKeys]);
    }

    public function addAPIKeyAction()
    {
        if ($this->request->isPost()) {
            $key = new StatisticAPIKey([
                'user_id' => $this->currentUser->id,
                'api_key' => Base::getToken()
            ]);
            $key->save();

            return $this->response->setJsonContent([
                'status' => 'ok',
                'reload' => true
            ]);
        }

        $this->show404();
    }

    public function removeAPIKeyAction()
    {
        if ($this->request->isPost()) {
            $key = StatisticAPIKey::findFirst($this->request->get('key_id'));

            $result = [];

            if ($key->user_id === $this->currentUser->id) {
                $result = [
                    'status' => ($key && $key->delete()) ? 'ok' : 'error'
                ];
            } else {
                $result = [
                    'status' => 'error',
                    'message' => 'Error. You have no permissions to process this operation.'
                ];
            }

            return $this->response->setJsonContent($result);
        }

        $this->show404();
    }

    public function apiAction()
    {
        if ($this->request->isPost()/*$this->currentUserHasLicense()->id*/) {
            $result = ['status' => 'ok'];
            $request = $this->request->getPost();

            if (empty($request)) {
                $request = $this->request->getJsonRawBody();
            }

            if (!$request || empty($request)) {
                $result['errors'] = 'Request data error.';
            } else {
                $apiKey = $request->{'X-VP-API-KEY'};
                if ($apiKey) {
                    $key = StatisticAPIKey::findFirst([
                        'api_key = :api_key:',
                        'bind' => [
                            'api_key' => $apiKey
                        ]
                    ]);

                    if (!$key) {
                        $result['errors'] = 'API key is not correct.';
                    } else {
                        $data = $request->data;
                        $editionId = $request->edition_id;
                        $user = Users::findFirst($key->user_id);

                        if ($data && $editionId) {
                            if ($user) {
                                $edition = Editions::findFirst($editionId);

                                if ($edition) {
                                    $project = Projects::findFirst($edition->project_id);

                                    if ($project->userHaveAccess($user->id)) {
                                        if (is_array($data)) {
                                            foreach ($data as $dataItem) {
                                                $result = $this->processAddEntry($editionId, $dataItem);
                                                if (isset($result['errors'])) {
                                                    break;
                                                }
                                            }
                                        } else {
                                            $result = $this->processAddEntry($editionId, $data);
                                        }
                                    } else {
                                        $result['errors'] = 'Project restriction. Access denied.';
                                    }
                                } else {
                                    $result['errors'] = 'Edition is not found.';
                                }
                            } else {
                                $result['errors'] = 'User not fount.';
                            }
                        } else {
                            $result['errors'] = 'Request parameters are not correct.';
                        }
                    }
                } else {
                    $result['errors'] = 'No API key is specified.';
                }
            }

            return $this->response->setJsonContent($result);
        }
    }

    protected function processAddEntry($editionId, $data)
    {
        if (isset($data->cover_id)
            && isset($data->entity_id)
            && isset($data->entity)
            && isset($data->parameter)
            && isset($data->value)
            && isset($data->name)
            && isset($data->device)) {
            $statisticEntry = new StatisticEdition([
                'edition_id' => $editionId,
                'cover_id' => $data->cover_id,
                'entity_id' => $data->entity_id,
                'entity' => $data->entity,
                'parameter' => $data->parameter,
                'value' => $data->value,
                'name' => $data->name
            ]);
            if (!$statisticEntry->save()) {
                $result['errors'] = 'Statistics entry add error.';
            } else {
                $statisticEntryDevice = new StatisticDevice([
                    'statistic_id' => $statisticEntry->id,
                    'type' => $data->device->type,
                    'os' => $data->device->os,
                    'os_version' => $data->device->os_version,
                    'country' => $data->device->country
                ]);
                if (!$statisticEntryDevice->save()) {
                    $statisticEntry->delete();
                    $result['errors'] = 'Statistics entry device add error.';
                } else {
                    $result = ['status' => 'ok'];
                }
            }
        } else {
            $result['errors'] = 'Request data is not correct.';
        }

        return $result;
    }
}