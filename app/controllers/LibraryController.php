<?php

use Phalcon\Http\Response;

class LibraryController extends ControllerBase
{
    protected $converterSocket;
    protected $converterConnected;
    protected $converterServer = '127.0.0.1';
    protected $converterPort = 8124;

    public function initialize()
    {
        parent::initialize();
    }

    public function indexAction()
    {
        $result = new stdClass();
        $errors = [];

        $editionId = $this->dispatcher->getParam('edition_id');

        $edition = Editions::findFirst($editionId);

        if (!$edition || !$edition->userHaveAccess($this->currentUser->id)) {
            $result->errors = 'Edition is not found.';

            $this->response->setJsonContent($result);
            return false;
        }

        $result->has_files = $this->request->hasFiles();

        // delete all softDeleted objects
		Library::find([
			'edition_id = :edition_id: AND user_id = :user_id: AND deleted = :deleted:',
			'bind' => [
				'edition_id' => $editionId,
				'user_id' => $this->currentUser->id,
				'deleted' => 1
			]
		])->delete();

        // list all objects for "media library"
        $objects = Library::find([
            'edition_id = :edition_id: AND user_id = :user_id: AND deleted = :deleted:',
            'bind' => [
                'edition_id' => $editionId,
                'user_id' => $this->currentUser->id,
                'deleted' => 0
            ]
        ]);

        $result->objects = $objects;
        $result->errors = $errors;

        $this->response->setJsonContent($result);

        return false;
    }

    public function usageAction()
    {
        $result = new stdClass();
        $errors = [];

        $objectId = (int)$this->dispatcher->getParam('id');

        $object = Library::findFirst($objectId);

        if (!$object) {
            $result->errors = 'Object is not found.';

            $this->response->setJsonContent($result);
            return false;
        }

        $edition = Editions::findFirst($object->edition_id);

        if (!$edition || !$edition->userHaveAccess($this->currentUser->id)) {
            $result->errors = 'Edition is not found.';

            $this->response->setJsonContent($result);
            return false;
        }

        $objects = Objects::find([
            'object_id = :object_id:',
            'bind' => [
                'object_id' => $objectId,
            ]
        ]);

        $covers = [];
        foreach ($objects as $coverObject) {
            if (!isset($covers[$coverObject->cover_id])) {
                $covers[$coverObject->cover_id] = $coverObject->getCovers();
            }
        }

        $result->covers = $covers;
        $result->errors = $errors;

        $this->response->setJsonContent($result);

        return false;
    }

    public function uploadAction()
    {
        $result = new stdClass();
        $errors = [];
        $objects = [];

        $editionId = (int)$this->request->get('editionId');
        $coverId = (int)$this->request->get('coverId');
        $objectId = (int)$this->request->get('objectId');

        $result->cover_id = $coverId;

        $edition = Editions::findFirst($editionId);

        if (!$edition || !$edition->userHaveAccess($this->currentUser->id)) {
            $result->errors = 'Edition is not found . ';

            $this->response->setJsonContent($result);
            return false;
        }

        $result->has_files = $this->request->hasFiles();

        if ($this->request->hasFiles()) {
            try {
                $dirLocal = '/public/' . Library::DIRECTORY_PATH;
                $dirFS = APP_PATH . $dirLocal;
                $path = APP_PATH;
                $dirs = explode('/', $dirLocal);

                foreach ($dirs as $dir) {
                    if ($dir) {
                        $path .= '/' . $dir;

                        if (!file_exists($path)) {
                            mkdir($path, 0755);
                        }
                    }
                }

                $objectsDirLength = strlen($dirFS);
                $files = $this->request->getUploadedFiles();

                foreach ($files as $file) {
                    $filename = $file->getName();
                    $title = $file->getName();
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $error = $file->getError();

                    if ($error) {
                        $errors[] = $title . ': ' . self::uploadErrorText($error);
                        continue;
                    }

                    if ($ext == 'zip') {
                        // get the absolute path to $file
                        $unpackDir = $filename . '.unp';
                        $path = $dirFS . $unpackDir;
                        $newFileName = $this->makeUniqueFileName($path);

                        //$result->path = $path;
                        //$result->new_filename = $newFileName;

                        if ($newFileName != $unpackDir) {
                            $unpackDir = $newFileName;
                            $path = $dirFS . $unpackDir;
                        }

                        $zip = new ZipArchive;
                        $response = $zip->open($file->getTempName());
                        if ($response === true) {
                            // extract it to the path we determined above
                            $zip->extractTo($path);
                            $zip->close();

                            $returnExtensions = ['fbx', 'stl', 'obj', 'json'];

                            $dirFiles = [];
                            $cleanDirFiles = [];
                            self::readDirRecursive($path, $dirFiles, true);
                            foreach ($dirFiles as $i => $dirFile) {
                                if (!file_exists($dirFile))
                                    continue;

                                $baseName = basename($dirFile);

                                if ($baseName[0] == '.') {
                                    unlink($dirFile);
                                } elseif (is_dir($dirFile) && $baseName == '__MACOSX') {
                                    Base::unlinkDirRecursive($dirFile);
                                } else {
                                    $cleanDirFiles[] = $dirFile;
                                }
                            }
                            $dirFiles = $cleanDirFiles;

                            $fileSize = 0;
                            $additionalFiles = [];
                            foreach ($dirFiles as $i => $dirFile) {
                                if (!is_dir($dirFile)) {
                                    $fileSize += filesize($dirFile);
                                }

                                $relativePath = substr($dirFile, $objectsDirLength);
                                $additionalFiles[] = $relativePath;
                            }


                            $dirFSLength = strlen($dirFS);
                            $result->files = [];

                            foreach ($dirFiles as $dirFile) {
                                //							$result->files[] = $dirFile;
                                $ext = pathinfo($dirFile, PATHINFO_EXTENSION);
                                if (in_array($ext, $returnExtensions)) {
                                    $unpackedTitle = $unpackedFileName = substr($dirFile, strlen($dirFS));

                                    $result->files[] = substr($dirFile, $dirFSLength);

                                    if ($objectId) {
                                        $object = Library::findFirst($objectId);
                                        // check possible hacks
                                        if ($object->edition_id != $editionId) {
                                            throw new Exception("File not found");
                                        }

                                        // cleanup files if it is replace upload (objectId is set)
                                        $object->_deleteFiles();

                                        // if title unchanged, update title
                                        if ($object->title === $object->filename) {
                                            $object->title = $unpackedTitle;
                                        }
                                        // reset object thumbnail to be updated
                                        $object->thumb = '';
                                    } else {
                                        $object = new Library();
                                        $object->edition_id = $editionId;
                                        $object->user_id = $this->currentUser->id;
                                        $object->title = $unpackedTitle;
                                    }
                                    $object->filename = $unpackedFileName;
                                    $object->filesize = $fileSize;
                                    $object->type = $this->get_object_type($dirFS . $unpackedFileName);

                                    $object->params['files'] = $additionalFiles;

                                    if ($object->save()) {
                                        $objects[] = Library::findFirst($object->id);
                                    } else {
                                        $errors[] = $unpackedTitle . ': Failed to save object . ';

                                        foreach ($object->getMessages() as $message) {
                                            $errors[] = $message->getMessage();
                                        }

                                        $object->_deleteFiles();
                                    }
                                }
                            }
                        } else {
                            $errors[] = $title . ': failed to unpack';
                        }
                    } else {
                        if (file_exists($file->getTempName())) {
                            $localFullName = $dirFS . $filename;
                            $newFileName = $this->makeUniqueFileName($localFullName);

                            if ($newFileName != $filename) {
                                $filename = $newFileName;
                                $localFullName = $dirFS . $filename;
                            }

                            $response = $file->moveTo($localFullName);

                            if ($response) {
                                if ($objectId) {
                                    $object = Library::findFirst($objectId);
                                    // check possible hacks
                                    if ($object->edition_id != $editionId) {
                                        throw new Exception("File not found");
                                    }

                                    // cleanup files if it is replace upload (objectId is set)
                                    $object->_deleteFiles();

                                    // if title unchanged, update title
                                    if ($object->title === $object->filename) {
                                        $object->title = $title;
                                    }
                                    // reset object thumbnail to be updated
                                    $object->thumb = '';
                                } else {
                                    $object = new Library();
                                    $object->edition_id = $editionId;
                                    $object->user_id = $this->currentUser->id;
                                    $object->title = $title;
                                }
                                $object->filename = $filename;
                                $object->filesize = filesize($dirFS . $filename);
                                $object->type = $this->get_object_type($dirFS . $filename);
                                $object->params = [];
                                $matches = [];

                                if (preg_match('@^(video|image|audio)@', $object->type, $matches)) {
                                    $type = $matches[0];
                                    $getID3 = new getID3;

                                    $info = $getID3->analyze($dirFS . $filename);

                                    if ($type == 'image' || $type == 'video') {
                                        $object->params['width'] = $info['video']['resolution_x'];
                                        $object->params['height'] = $info['video']['resolution_y'];
                                    } elseif ($type == 'audio') {
                                        if ($info['fileformat'] == 'mp3') {

                                            if (isset($info['tags']['id3v2'])) {
                                                $title = $info['tags']['id3v2']['title'][0];
                                            } else if (isset($info['tags']['id3v1'])) {
                                                $title = $info['tags']['id3v1']['title'][0];
                                            }

                                            if ($title) {
                                                $object->title = $title;
                                            }
                                        }
                                    }
                                    if (isset($info['playtime_string'])) {
                                        $object->params['playtime_string'] = $info['playtime_string'];
                                    }
                                }

                                if ($object->save()) {
                                    $objects[] = Library::findFirst($object->id);
                                } else {
                                    $errors[] = $title . ': Failed to save object . ';

                                    foreach ($object->getMessages() as $message) {
                                        $errors[] = $message->getMessage();
                                    }

                                    $object->_deleteFiles();
                                }
                            } else {
                                $errors[] = $title . ": Failed to move uploaded file.";
                            }
                        } else {
                            $errors[] = $title . ": Uploaded file does not exists.";
                        }
                    }
                }

                $libraryObjects = $objects;

                if ($coverId) {
                    $objects = [];

                    foreach ($libraryObjects as $object) {
                        $sceneObject = new Objects();
                        $sceneObject->cover_id = $coverId;
                        $sceneObject->object_id = $object->id;
                        //if ($sceneObject->save()) {
                        // adaptation to javascript version
                        $obj = $sceneObject->toPlainVersion();
                        $objects[] = $obj;
                        //					} else {
                        //						$errors[] = $object->title . ": Failed to save scene obj db record.";
                        //						foreach ($sceneObject->getMessages() as $message) {
                        //							$errors[] = $message->getMessage();
                        //						}
                        //					}
                    }
                }

                $result->libraryObjects = [];

                foreach ($libraryObjects as $object) {
                    $result->libraryObjects[] = [
                        'id' => $object->id,
                        'filename' => $object->filename,
                        'type' => $object->type
                    ];
                }
                /*if (count($objects)) {
                    $video_files = array();
                    foreach ($objects as $object) {
                        if ($object->type != 'video / mp4' && substr($object->type, 0, 5) == 'video') {
                            $video_files[] = $object->filename;
                        }
                    }

                    if (count($video_files)) {
                        foreach ($video_files as $filename)
                            $this->convertation_add($filename);
                    }
                }*/
            } catch (Exception $exception) {
                $result->errors = $exception->getMessage();
            }
        }

        $result->objects = $objects;
        $result->errors = $errors;

        $this->response->setJsonContent($result);

        return false;
    }

    public function videoLinkSaveAction()
    {
        if ($this->request->isPost()) {
            $result = [];
            $editionId = $this->request->get('edition_id');
            $coverId = $this->request->get('cover_id');
            $linkUrl = $this->request->get('link');

            $link = VideoLinks::findFirst([
                'conditions' => 'user_id =?1 AND link = ?2',
                'bind' => [
                    1 => $this->currentUser->id,
                    2 => $linkUrl
                ]
            ]);

            if ($link) {
                $object = Library::findFirst($link->object_id);
            } else {
                $objectName = time();

                $title = $this->request->get('title');
                $thumbURL = $this->request->get('thumb');
                $width = $this->request->get('width');
                $height = $this->request->get('height');

                $thumbsDirFS = APP_PATH . '/public/' . Library::DIRECTORY_PATH_THUMBS;
                $thumbFilename = 'thumb.' . $objectName . '.jpg';

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $thumbURL);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $data = curl_exec($ch);
                curl_close($ch);

                if ($data) {
                    file_put_contents($thumbsDirFS . $thumbFilename, $data);
                }

                if (!file_exists($thumbsDirFS . $thumbFilename)) {
                    $thumbFilename = '';
                }

                $object = new Library([
                    'edition_id' => $editionId,
                    'user_id' => $this->currentUser->id,
                    'title' => $title,
                    'filename' => $objectName,
                    'thumb' => $thumbFilename,
                    'params' => [
                        'width' => $width,
                        'height' => $height,
                    ],
                    'type' => 'video/youtube',
                    'filesize' => 0
                ]);

                if (!$object->save()) {
                    foreach ($object->getMessages() as $errorMessage) {
                        $result['errors']['object create'][] = $errorMessage->getMessage();
                    }
                }

                if (!isset($result['errors'])) {
                    $link = new VideoLinks([
                        'object_id' => $object->id,
                        'link' => $linkUrl,
                        'user_id' => $this->currentUser->id,
                        'file_name' => $objectName,
                        'status' => 'new'
                    ]);

                    if (!$link->save()) {
                        foreach ($link->getMessages() as $errorMessage) {
                            $result['errors']['link create'][] = $errorMessage->getMessage();
                        }
                    }
                }
            }

            if (!isset($result['errors']) && $coverId) {
                $result['lib_object'] = $object->id;
            }

            $result['status'] = !isset($result['errors']) ? 'ok' : 'failed';

            $this->response->setJsonContent($result);

            return false;
        }

        $this->show404();
    }

    public function videoLinkAddAction()
    {
        if ($this->request->isPost()) {
            $editionId = $this->request->get('edition_id');
            $coverId = $this->request->get('cover_id');
            $linkUrl = $this->request->get('link');
            $result = [];

            $result['status'] = !isset($result['errors']) ? 'ok' : 'failed';

            $this->response->setJsonContent($result);
        }

        $this->show404();
    }

    public function thumbAction()
    {
        $result = new stdClass();
        $result->errors = [];

        $id = (int)$this->request->getPost('id');
        $filename = $this->request->getPost('filename');
        $file_data = $this->request->getPost('data');
        $dirFS = APP_PATH . '/public/' . Library::DIRECTORY_PATH_THUMBS;
        $type = '';

        if (substr($file_data, 0, 22) == 'data:image/jpeg;base64') {
            $file_data = base64_decode(substr($file_data, 22));
            $type = 'jpg';
        } elseif (substr($file_data, 0, 21) == 'data:image/png;base64') {
            $file_data = base64_decode(substr($file_data, 21));
            $type = 'png';
        }

        $im = imagecreatefromstring($file_data); // check if this an image
        if ($im !== false) {
            $object = Library::findFirst($id);

            // remove old thumb
            if ($object->thumb && file_exists($dirFS . $object->thumb)) {
                unlink($dirFS . $object->thumb);
            }

            // save new
            $filename .= '.' . $type;
            $result->filename = $filename;
            file_put_contents($dirFS . $filename, $file_data);
            $object->thumb = $filename;
            if ($object->save()) {
                $errors[] = $id . ': Failed to update db record.';
                foreach ($object->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
            }
        } else {
            $result->errors[] = 'failed to parse image data';
        }

        $response = new Response();
        $response->setJsonContent($result);
        return $response;
    }

    public function saveAction()
    {
        $result = new stdClass();
        $result->errors = [];

        $id = (int)$this->request->getPost('id');
        $title = $this->request->getPost('title');

        if (!$id)
            $result->errors[] = 'id is missing';
        if (!$title)
            $result->errors[] = 'title should not be empty';

        if (!$result->errors) {
            $object = Library::findFirst($id);

            if (!$object) {
                $result->errors[] = $id . ': Not found.';
            } else {
                $object->title = $title;

                if (!$object->save()) {
                    $result->errors[] = $object->id . ': Failed to save db record.';
                    foreach ($object->getMessages() as $message) {
                        $result->errors[] = $message->getMessage();
                    }
                }
            }
        }

        $response = new Response();
        $response->setJsonContent($result);
        return $response;
    }

    public function checkConvertationAction()
    {
        $ids = $this->request->get('objects');

        $result = $this->checkConvertation($ids);

        $response = new Response();
        $response->setJsonContent($result);
        return $response;
    }

    public function scanTypesAction()
    {
        $objects = Library::find();
        foreach ($objects as $object) {
            $object->type = $this->get_object_type(Library::DIRECTORY_PATH . $object->filename);
//			echo $object->filename;
//			echo $object->type;
            $object->save();
        }
        return false;
    }

    public function softDeleteAction()
    {
        return $this->setSoftDeleteStateAction(1);
    }

    public function unDeleteAction()
    {
        return $this->setSoftDeleteStateAction(0);
    }

    public function checkConvertation($ids)
    {
        $result = [
            'statuses' => [],
            'finished' => []
        ];

        if ($ids && count($ids) > 0) {
            foreach ($ids as $id) {
                $object = Library::findFirst($id);
                if (!$object)
                    continue;

                if ($object->type === 'video/mp4') {
                    // if checking already converted object - report 'finished'
                    $result['finished'][$object->id] = $object;
                } elseif ($object->type === 'video/youtube') {
                    $link = VideoLinks::findFirst(['conditions' => 'object_id = ?1', 'bind' => [1 => $object->id]]);

                    $result['statuses'][$object->id] = $link->status;
                } else {
                    $status = $this->convertation_status($object->filename);
                    $result['statuses'][$object->id] = $status;
                    if ($status === '0' && file_exists(APP_PATH . '/public/' . Library::directory_path($object->user_id) . $object->filename . '.mp4')) {
                        // delete original file
                        unlink(APP_PATH . '/public/' . Library::DIRECTORY_PATH . $object->filename);
                        unlink(APP_PATH . '/public/' . Library::DIRECTORY_PATH . $object->filename . '.mp4_log.txt');

                        $object->filename .= '.mp4';
                        $object->type = 'video/mp4';
                        $object->save();
                        $result['finished'][$object->id] = $object;
                    } elseif ($status == 'not found') {
                        $status = $this->convertation_add(Library::DIRECTORY_PATH . $object->filename);
                        $result['statuses'][$object->id] = 're-add:' . $status;
                    }
                }
            }
        }

        return $result;
    }

    private function setSoftDeleteStateAction($delete)
    {
        $response = new Response();
        $ids = $this->request->get('id');
        if (!is_array($ids))
            $ids = [$ids];

        $objects = Library::query()->inWhere('id', $ids)->execute();

        $messages = [];
        if ($objects) {
            foreach ($objects as $object) {
                $result = $delete ? $object->softDelete() : $object->unDelete();
                if (!$result) {
                    $messages = array_merge($messages, $object->getMessages());
                }
            }
        }

        if (count($messages)) {
            $response->setJsonContent($messages);
        } else {
            $response->setJsonContent(count($objects));
        }

        return $response;
    }

    public function deleteAction()
    {
        $response = new Response();
        $id = (int)$this->request->get('id');
        $object = Library::findFirst($id);

        if ($object) {
            if ($object->delete()) {
                //unlink(APP_PATH . '/public/files/objects/' . $object->filename);
                $response->setJsonContent(1);
            } else {
                $response->setJsonContent($object->getMessages());
            }
        } else {

        }

        return $response;
    }

    public function get_object_type($filename)
    {
        $localFullName = $filename;
        $type = mime_content_type($localFullName);
        if ($type == 'application/octet-stream' || $type == 'text/plain') {
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $type = 'object/' . $ext;
        }
        return $type;
    }

    public static function readDirRecursive($path, &$collection, $includeDirectories = false)
    {
        if ($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {
                // do something with the file
                // note that ' . ' and ' ..' is returned even
                if (!is_dir($path . '/' . $file)) {
                    $collection[] = $path . '/' . $file;
                } else {
                    if ($file != '.' && $file != '..') {
                        if ($includeDirectories) {
                            $collection[] = $path . '/' . $file;
                        }
                        self::readDirRecursive($path . '/' . $file, $collection, $includeDirectories);
                    }
                }
            }
            closedir($handle);
        }
    }

    /**
     * @param integer $errNo
     * @return string
     */
    private static function uploadErrorText($errNo)
    {
        static $phpFileUploadErrors;
        if (!$phpFileUploadErrors) {
            $upload_max_size = ini_get('upload_max_filesize');
            $phpFileUploadErrors = [
                0 => 'There is no error, the file uploaded with success',
                1 => 'The uploaded file exceeds allowed file size . Maximum - ' . $upload_max_size . 'b',
                2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
                3 => 'The uploaded file was only partially uploaded',
                4 => 'No file was uploaded',
                6 => 'Missing a temporary folder',
                7 => 'Failed to write file to disk . ',
                8 => 'A PHP extension stopped the file upload . ',
            ];
        }
        return $phpFileUploadErrors[$errNo];
    }

    protected function convertation_send($data)
    {
        $response = '';
        $connected = $this->convertation_connect();
        if ($connected) {
            socket_write($this->converterSocket, $data);
            $response = trim(socket_read($this->converterSocket, 256));
        }

        return $response;
    }

    protected function convertation_add($filename)
    {
        return $this->convertation_send('+' . $filename);
    }

    protected function convertation_remove($filename)
    {
        return $this->convertation_send('-' . $filename);
    }

    protected function convertation_status($filename)
    {
        $status = 'not found';
        $connection_exists = $this->convertation_connect(true); // just check if server available
        if ($connection_exists) {
            $status = $this->convertation_send('?' . $filename);
        }

        if (!$connection_exists || $status == 'not found') {
            // check if there converted video and log
            $converted_filename = APP_PATH . '/public/' . Library::DIRECTORY_PATH . $filename . '.mp4';
            $converter_log = $converted_filename . '_log.txt';

            if ($converter_log && file_exists($converter_log)) {
                $fh = fopen($converter_log, 'r');
                fseek($fh, -100, SEEK_END);
                $part = fread($fh, 100);
                //if (preg_match('@\[(\d +)]$@', $part, $matches)) {
                if (preg_match('@ffmpeg process exited with code(\d +)\.$@', $part, $matches)) {
                    $status = $matches[1];
                }
            }
        }

        return $status;
    }

    protected function convertation_disconnect()
    {
        if ($this->converterSocket)
            socket_close($this->converterSocket);
    }

    protected function convertation_connect($autostart = true)
    {
        if ($this->converterConnected)
            return true;

        if (!$this->converterSocket) {
            if (!($this->converterSocket = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP))) {
                //echo 'Unable to create socket:' . socket_strerror(socket_last_error()).'<br />';
                return false;
            }
        }

        $this->converterConnected = @socket_connect($this->converterSocket, $this->converterServer, $this->converterPort);
        if ($this->converterConnected) {
            return true;
        } else {
            // failed to connect!
            if ($autostart) {
                // try autostart
                return $this->start_converter_process();
            }
        }
        return false;
    }

    protected function start_converter_process()
    {
        $command = $this->config->os->nodeBinaryFile . ' ' . APP_PATH . '/node/video_encoder2.js >/dev/null 2>/dev/null &';

        shell_exec($command);
        sleep(1);
        return $this->convertation_connect(false);
    }
}

