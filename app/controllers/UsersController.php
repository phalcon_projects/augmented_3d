<?php

use Phalcon\Http\Response;

class UsersController extends ControllerBase
{
    public function initialize()
    {
        $auth = $this->session->get('auth');
        $userId = isset($auth['id']) ? $auth['id'] : null;

        if (!$userId || !($this->currentUser = Users::findFirst($userId))) {
            $this->dispatcher->forward([
                'controller' => 'errors',
                'action' => 'show404'
            ]);

            return false;
        }

        $this->tag->setTitle('Users');
        $this->view->setVar("section_title", "users");

        return parent::initialize();
    }

    protected function prepareList($model, $parameters = [])
    {

    }

    public function indexAction()
    {
        $pageNumber = 1;
        $pageSize = 8;
        $searchKey = null;

        if ($this->request->isPost()) {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, Users::class, $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, Users::class, $this->request->get());
        }

        if ($this->request->has("page")) {
            $pageNumber = $this->request->getQuery("page", "int");
        }

        if ($this->request->has("limit")) {
            $pageSize = $this->request->getQuery("limit", "int");
        }

        if ($this->request->has("search")) {
            $searchKey = $this->request->getQuery("search", "string");
        }

        $query->orderBy("first_name");

        if ($searchKey) {
            $query->andWhere("key like '%" . $searchKey . "%'");
        }

        $parameters = $query->getParams();

        $list = Users::find($parameters);

        if ($this->request->has("search") && count($list) == 0) {
            $this->flash->notice("The search did not find any");
        }

        $pageNumber = max((int)$pageNumber, 1);
        $pageSize = max((int)$pageSize, 1);

        $paginator = new Paginator(array(
            "data" => $list,
            "limit" => min($pageSize, 1000),
            "page" => $pageNumber
        ));

        $this->view->page = $paginator->getPaginate();
        $this->view->pageLimit = $pageSize;
    }
}