<?php

use Spipu\Html2Pdf\Html2Pdf;

class LicensesController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Licenses');
        $this->view->setVar("section_title", "license");

        parent::initialize();
    }

    public function indexAction()
    {
        $this->assets->addCss("css/license.css");
        $this->assets->addJs("js/page.license.js");

        if ($this->request->isPost()) {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, Licenses::class, $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, Licenses::class, $this->request->get());
        }

        $query->orderBy("date_from");
        $query->andWhere("user_id='" . (int)$this->currentUser->id . "'");
        $query->andWhere("type='" . Licenses::TYPE_MAIN . "'");
        $parameters = $query->getParams();

        $this->view->license_items = Licenses::find($parameters);

        if ($this->request->isPost()) {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, "Order", $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, "Order", $this->request->get());
        }

        $query->orderBy("created_at");
        $query->andWhere("user_id='" . (int)$this->currentUser->id . "'");
        $parameters = $query->getParams();

        $this->view->licenseTitle = $this->getDi()->getConfig()->application->licenseTitle;
        $this->view->paypalUrl = $this->getDi()->getConfig()->services->paypalUrl;
        $this->view->paypalId = $this->getDi()->getConfig()->services->paypalId;
        $this->view->paymentSuccessUrl = $this->getDi()->getConfig()->application->siteUri . '/license/payment-success';
        $this->view->paymentCancelUrl = $this->getDi()->getConfig()->application->siteUri . '/license/payment-cancel';
        $this->view->order = Order::findFirst($parameters);
        $this->view->orderType = 'Main'/*(isset($this->view->order) ? Licenses::TYPES[$this->view->order->type]['title'] : null)*/
        ;
        $this->view->orderItems = OrderItem::find([
            'conditions' => 'order_id = ?1',
            'bind' => [
                1 => $this->view->order->id
            ]
        ]);
    }

    public function orderAction()
    {
        if ($this->request->isPost() && $this->currentUser) {
            $result = [];
            $orderCustomer = OrderCustomer::findFirst([
                'conditions' => 'user_id = :1:',
                'bind' => [1 => $this->currentUser->id]
            ]);

            if (!$orderCustomer) {
                $orderCustomer = new OrderCustomer([
                    'user_id' => $this->currentUser->id,
                    'client_id' => time(),
                    'customer_vat_id' => time()
                ]);

                if (!$orderCustomer->save()) {
                    $result['errors'][] = 'Order customer initialization error.';
                }
            }

            if (!isset($result['errors'])) {
                $newLicenseOrder = new Order([
                    'number' => time(),
                    'user_id' => $this->currentUser->id,
                    'state' => Order::STATE_PENDING
                ]);

                if ($newLicenseOrder->save()) {
                    $newOrderItem = new OrderItem([
                        'order_id' => $newLicenseOrder->id,
                        'type' => Licenses::TYPE_MAIN,
                        'title' => 'Augme license',
                        'price' => Licenses::getPriceByType(Licenses::TYPE_MAIN),
                        'quantity' => 1
                    ]);

                    if (!$newOrderItem->save()) {
                        $result['errors'][] = 'Order item initialization error.';
                    } else {
                        $result['order_id'] = $newLicenseOrder->id;
                    }
                } else {
                    $result['errors'][] = 'License order initialization error.';
                }
            }

            $result['status'] = !isset($result['errors']) ? 'ok' : 'failed';

            return $this->response->setJsonContent($result);
        }
    }

    public function expiredAction()
    {
        return;
    }

    public function paymentSuccessAction()
    {
        $state = $this->request->get('st');

        if (strtolower($state) === 'completed') {
            $item_number = $this->request->get('item_number');
            $transaction_id = $this->request->get('tx');
            $payment_gross = $this->request->get('amt');
            $currency_code = $this->request->get('cc');

            $orderItem = OrderItem::findFirst([
                'conditions' => 'order_id = ?1',
                'bind' => [
                    1 => $item_number
                ]
            ]);

            if (!empty($transaction_id) && $payment_gross == $orderItem->price) {
                $orderItem->state = OrderItem::STATE_PAYED;

                if (!$orderItem->save()) {
                    $result['errors'][] = 'Failed to save transaction data.
                     Please contact administrator to correct order processing.';
                } else {
                    $result['state'] = 'OK';
                }
            } else {
                $result['errors'][] = 'Transaction checking error.';
            }

            return $this->response->setJsonContent($result);
        }

        return $this->response->redirect('/license');
    }

    public function paymentCancelAction()
    {
        return $this->response->redirect('/license');
    }

    public function invoiceAction()
    {
        $order_id = (int)$this->dispatcher->getParam('invoice_id');
        $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, "Order", ['id' => $order_id]);
        $query->orderBy("created_at");
        $query->andWhere("user_id='" . (int)$this->currentUser->id . "'");
        $parameters = $query->getParams();

        $order = Order::findFirst($parameters);

        if (!$order || $order->user_id !== $this->currentUser->id) {
            return $this->response->redirect('/license');
        }

        $orderCustomer = OrderCustomer::findFirst([
            'conditions' => 'user_id = ?1',
            'bind' => [
                1 => $order->user_id
            ]
        ]);
        $orderItems = OrderItem::find([
            'conditions' => 'order_id = ?1',
            'bind' => [
                1 => $order->id
            ]
        ]);

        $this->view->order = $order;
        $this->view->orderItems = $orderItems;
        $this->view->orderCustomer = $orderCustomer;

        $this->response->setHeader("Content-Type", "application/pdf");
        $this->response->setContentType('application/pdf');
        $this->view->disable();
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->appPath = APP_PATH;
        $this->view->t = $this->getTranslation();
        $html = $this->view->getRender('Licenses', 'invoice');

        $html2pdf = new Html2Pdf('P', 'A4', 'en');
        $html2pdf->pdf->setCreator('COSOMEDIA GMBH & CO.KG');
        $html2pdf->pdf->setAuthor('COSOMEDIA GMBH & CO.KG');
        //$html2pdf->setModeDebug(new \Spipu\Html2Pdf\Debug\Debug());
        $html2pdf->writeHTML($html);
        $html2pdf->output();
        exit;
    }

    public function trafficPlanViewAction()
    {
        $this->assets->addCss('css/license-plan.css');
        $this->assets->addJs('js/page.traffic-plans.js');

        $currentPlan = Licenses::findFirst([
            'conditions' => 'user_id = ?1 AND type = ?2',
            'bind' => [
                1 => $this->currentUser->id,
                2 => Licenses::TYPE_TRAFFIC
            ]
        ]);
        $this->view->currentPlanId = $currentPlan ? (int)$currentPlan->value : null;
        $this->view->planItems = Licenses::PLANS_TRAFFIC;
        echo $this->view->render('Licenses', 'plans-traffic');
    }

    public function trafficPlanChangeAction()
    {
        if ($this->request->isPost()) {
            $result = [];

            $planId = (int)$this->request->getPost('plan_id', 'int');
            $item = null;

            if (!$order = $this->getOrder()) {
                $order = $this->createOrder();
            }

            if ($order) {
                if (!$this->addOrderItem([
                    'order_id' => $order->id,
                    'type' => Licenses::TYPE_TRAFFIC,
                    'title' => 'Traffic package',
                    'price' => Licenses::PLANS_TRAFFIC[$planId]['price']
                ])) {
                    $result['errors'][] = 'Failed to save order item';
                }
            } else {
                $result['errors'][] = 'Failed to place order';
            }

            /*if (in_array($planId, array_keys(License::PLANS_TRAFFIC))) {
                $license = License::findFirst([
                    'conditions' => 'user_id = ?1 AND type = ?2',
                    'bind' => [
                        1 => $this->currentUser->id,
                        2 => License::TYPE_TRAFFIC
                    ]
                ]);

                if (!$license) {
                    $license = new License([
                        'user_id' => $this->currentUser->id,
                        'type' => License::TYPE_TRAFFIC,
                        'value' => $planId
                    ]);

                    if (!$license->save()) {
                        $result['errors'][] = "Cannot update traffic plan";
                    }
                } else if ((int)$license->value == (int)$planId) {
                    $result['message'] = 'Unable to change traffic plan';
                    $result['errors'][] = 'Plan already selected';
                } else if (!$license->update(['value' => $planId])) {
                    $result['message'] = 'Unable to change traffic plan';

                    foreach ($license->getMessages() as $message) {
                        $result['errors'][] = $message->getMessage();
                    }
                } else {
                    if (isset(License::PLANS_TRAFFIC[$planId])) {

                    }
                }
            }*/

            if (!empty($result['errors'])) {
                $result['message'] = 'Unable to place traffic plan change order';
                $result['status'] = 'error';
                $result['redirect'] = false;
            } else {
                $result['status'] = 'ok';
                $result['message'] = 'Traffic plan change order has been placed';
                $result['redirect'] = '/license';
                $result['errors'] = [];
            }

            $this->response->setJsonContent($result);

            return false;
        }

        return $this->response->redirect('/license');
    }

    protected function getOrder()
    {
        return Order::findFirst([
            'conditions' => 'user_id = ?1 AND state = ?2',
            'bind' => [
                1 => $this->currentUser->id,
                2 => Order::STATE_PENDING
            ]
        ]);
    }

    protected function createOrder()
    {
        $lastOrder = Order::findFirst([
            'order' => 'id desc',
            'limit' => '0, 1'
        ]);
        $order = new Order([
            'number' => sprintf('%05d', ($lastOrder ? (int)$lastOrder->number + 1 : 1)),
            'user_id' => $this->currentUser->id
        ]);

        if (!$customer = OrderCustomer::findFirst(['user_id' => $this->currentUser->id])) {
            $lastCustomer = OrderCustomer::findFirst([
                'order' => 'id desc',
                'limit' => '0, 1'
            ]);
            $customer = new OrderCustomer([
                'user_id' => $this->currentUser->id,
                'client_id' => sprintf('%05d', ($lastCustomer ? (int)$lastCustomer->client_id + 1 : 1)),
                'customer_vat_id' => sprintf('%05d', ($lastCustomer ? (int)$lastCustomer->customer_vat_id + 1 : 1))
            ]);

            if (!$customer->save()) {
                return null;
            }
        }

        if (!$order->save()) {
            return null;
        }

        return $order;
    }

    protected function addOrderItem($parameters = [])
    {
        if (!isset($parameters['order_id']) || !isset($parameters['type']) || !isset($parameters['price'])) {
            return null;
        }

        if ($orderItem = OrderItem::findFirst([
            'conditions' => 'order_id = ?1 AND type = ?2',
            'bind' => [
                1 => $parameters['order_id'],
                2 => $parameters['type']
            ]
        ])) {
            $orderItem->price = $parameters['price'];
        } else {
            $orderItem = new OrderItem([
                'order_id' => $parameters['order_id'],
                'type' => $parameters['type'],
                'title' => $parameters['title'],
                'price' => $parameters['price'],
                'quantity' => 1
            ]);
        }

        if (!$orderItem->save()) {
            return null;
        }

        return $orderItem;
    }

    public function cloudSpacePlanViewAction()
    {
        $this->assets->addCss('css/license-plan.css');
        $this->assets->addJs('js/page.cloud-space-plans.js');

        $currentPlan = Licenses::findFirst([
            'conditions' => 'user_id = ?1 AND type = ?2',
            'bind' => [
                1 => $this->currentUser->id,
                2 => Licenses::TYPE_CLOUD_SPACE
            ]
        ]);
        $this->view->currentPlanId = $currentPlan ? (int)$currentPlan->value : null;
        $this->view->planItems = Licenses::PLANS_CLOUD_SPACE;
        echo $this->view->render('Licenses', 'plans-cloud-space');
    }

    public function cloudSpacePlanChangeAction()
    {
        if ($this->request->isPost()) {
            $result = [];

            $planId = (int)$this->request->getPost('plan_id', 'int');
            $item = null;

            if (!$order = $this->getOrder()) {
                $order = $this->createOrder();
            }

            if ($order && in_array($planId, array_keys(Licenses::PLANS_CLOUD_SPACE))) {
                if (!$this->addOrderItem([
                    'order_id' => $order->id,
                    'type' => Licenses::TYPE_CLOUD_SPACE,
                    'title' => 'Cloud space package',
                    'price' => Licenses::PLANS_CLOUD_SPACE[$planId]['price']
                ])) {
                    $result['errors'][] = 'Failed to save order item';
                }
            } else {
                $result['errors'][] = 'Failed to place order';
            }

            if (!empty($result['errors'])) {
                $result['message'] = 'Unable to place cloud space plan order';
                $result['status'] = 'error';
                $result['redirect'] = false;
            } else {
                $result['status'] = 'ok';
                $result['message'] = 'Cloud space plan change has been placed';
                $result['redirect'] = '/license';
                $result['errors'] = [];
            }

            $this->response->setJsonContent($result);

            return false;
        }

        return $this->response->redirect('/license');
    }

    protected function getFile()
    {
        $file = resolveIdToActualFilePath($_GET["id"]);

        set_time_limit(0);

        //Important: we catch that manually to determine transfered bytes.
        ignore_user_abort(true);

        ini_set('output_buffering', 0);
        ini_set('zlib.output_compression', 0);

        header('Content-Description: File Transfer');
        header('Content-Type: video/mp4'); //set depending on format.
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));

        // Repeat reading until EOF
        $chunk = 1024 * 1024; // bytes per chunk (1 MB)
        $fh = fopen($file, "rb");
        while (!feof($fh)) {
            echo fread($fh, $chunk);
            flush();
            ob_flush();

            //catch user abort manually.
            if (connection_status() != 0) {
                //abort or timeout. Store already transfered amount to database.
                //here an error of one time chunk size might appear, cause it has been read, but not delivered.
                file_put_contents("test.txt", "Aborted after: " . (ftell($fh) + 1) . " Bytes.");
                fclose($fh);
                exit;
            }
        }

        //pointer pos + 1 = actual bytes transfered - write to database.
        $bytesTransfered = ftell($fh) + 1;
        file_put_contents("test.txt", "Download complete after " . $bytesTransfered . " Bytes");
        fclose($fh);
        exit;
    }
}