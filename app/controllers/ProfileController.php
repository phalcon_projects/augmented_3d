<?php

class ProfileController extends ControllerBase
{
	public function initialize()
	{
		$this->tag->setTitle('My data');
		$this->view->setVar("section_title", "my data");

		parent::initialize();
	}

	public function indexAction()
	{
		$auth = $this->session->get('auth');
		$id = $auth['id'];

		$user = Users::findFirst($id);

		if (!$id || !$user) {
			$this->dispatcher->forward([
				'controller' => 'errors',
				'action'     => 'show404'
			]);

			return false;
		}

		// $form->render("name");
		$form = new ProfileForm($user, array('edit' => true));
		$password_form = new ChangePasswordForm(null, array('edit' => true));

		$this->assets->addJs('js/page.profile.js');
		$this->assets->addJs("vendor/jquery.nice-select.min.js");


		$this->view->setVar('form', $form);
		$this->view->setVar('password_form', $password_form);
		//$this->view->setVar('user', $user);
	}

	public function changePasswordAction() {
		$auth = $this->session->get('auth');
		$id = (int) $auth['id'];
		return $this->_changePasswordAction($id);
	}

	public function changePasswordUserAction() {
		$id = (int) $this->request->getPost('id', 'int');
		return $this->_changePasswordAction($id);
	}

	protected function _changePasswordAction($user_id)
	{
		$result = ['status' => 'ok', 'user_id' => $user_id, 'errors' => []];

		$form = new ChangePasswordForm();
		if ($this->request->isPost()) {
			if (!$form->isValid($this->request->getPost())) {
				foreach ($form->getMessages() as $message) {
					$result['errors'][] = $message->getMessage();
				}
			} else {
				$user = Users::findFirst($user_id);

				if (!$user) {
					$result['errors'][] = "User not found";
				} else {
					$user->password = $this->security->hash($this->request->getPost('password'));

					if (!$user->save()) {
						$result['errors'][] = "Failed to save db record.";
						foreach ($user->getMessages() as $message) {
							$result['errors'][] = $message->getMessage();
						}
					}
				}
			}
		}

		if ($result['errors'])
			$result['status'] = 'error';

		$this->response->setJsonContent($result);
		return false;
	}

	public function saveAction() {
		$auth = $this->session->get('auth');
		$id = $auth['id'];
		$res =  $this->_saveAction($id);

		// update user name in session
		$user = Users::findFirst($id);
		$auth['first_name'] = $user->first_name;
		$auth['surname'] = $user->surname;
		$this->session->set('auth', $auth);

		return $res;
	}

	public function saveUserAction() {
		$id = (int) $this->request->getPost('id', 'int');
		return $this->_saveAction($id);
	}

	protected function _saveAction($user_id) {
		$result = ['status' => 'ok', 'errors' => []];

		if ($this->request->isPost()) {
			$user = Users::findFirst($user_id);

			if (!$user) {
				$result['errors'][] = "User not found";
			} else {
				$form = new ProfileForm;
				$data = $this->request->getPost();
				if (!$form->isValid($data, $user)) {
					$result['errors'][] = "Validation errors.";
					foreach ($form->getMessages() as $message) {
						$result['errors'][] = $message->getMessage();
					}
				}

				if ($user->save() == false) {
					$result['errors'][] = "Failed to save db record.";
					foreach ($user->getMessages() as $message) {
						$result['errors'][] = $message->getMessage();
					}
				}
			}
		}

		if ($result['errors'])
			$result['status'] = 'error';

		$this->response->setJsonContent($result);
		return false;
	}
}