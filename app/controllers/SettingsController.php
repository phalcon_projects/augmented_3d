<?php

class SettingsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Statistic');
        $this->view->setVar('section_title', 'Statistic');

        parent::initialize();
    }

    public function indexAction()
    {
        $youtubeAccessIds = [];
        $settingsAccessList = SettingsYouTubeDownloadAccess::find();

        foreach ($settingsAccessList as $yaItem) {
            $youtubeAccessIds[] = $yaItem->user_id;
        }

        if (array_search($this->currentUser->id, $youtubeAccessIds) === false) {
            $youtubeAccessIds[] = $this->currentUser->id;
        }

        $youtubeDownloadAccessUserList = Users::find([
            'state = :1: AND (role = :2: OR id IN ('.implode(',', $youtubeAccessIds).'))',
            'bind' => [
                1 => Users::STATE_ACTIVE,
                2 => Users::ROLE_MEGA_ADMIN
            ]
        ]);

        $this->view->setVar('currentUser', $this->currentUser);
        $this->view->setVar('youtubeDownloadAccessUserList', $youtubeDownloadAccessUserList);
        $this->assets->addCss('css/settings.css');
        $this->assets->addJs('js/pages/settings/settings.js');
    }

    public function youTubeAccessListAddAction()
    {
        if (!$this->request->isPost()) {
            return false;
        }
        if (!$this->request->get('user_id')) {
            $result = [
                'status' => 'false',
                'error' => 'User id is should be set.'
            ];
        } else {
            if (!SettingsYouTubeDownloadAccess::findFirst('user_id = ' . $this->request->get('user_id'))) {
                $accessUser = new SettingsYouTubeDownloadAccess([
                    'user_id' => $this->request->get('user_id')
                ]);
                if ($accessUser->save()) {
                    $result = ['status' => 'ok'];
                } else {
                    $result = [
                        'status' => 'false',
                        'error' => 'Adding user from list error.'
                    ];
                }
            } else {
                $result = [
                    'status' => 'false',
                    'error' => 'The user is already added.'
                ];
            }
        }
        $this->response->setJsonContent($result);
        return false;
    }

    public function youTubeAccessListRemoveAction()
    {
        if (!$this->request->isPost()) {
            return false;
        }
        $userId = $this->request->get('user_id', 'int');
        if (!$userId) {
            $result = [
                'status' => 'false',
                'error' => 'User id is should be set.'
            ];
        } else {
            $accessUser = SettingsYouTubeDownloadAccess::findFirst('user_id = ' . $userId);
            if ($accessUser && $accessUser->delete()) {
                $result = ['status' => 'ok'];
            } else {
                $result = [
                    'status' => 'false',
                    'error' => 'Removing user from list error.'
                ];
            }
        }
        $this->response->setJsonContent($result);
        return false;
    }

    public function youTubeAccessListGetToInviteAction()
    {
        $search = $this->request->get('search');

        if ($search) {
            $youtubeAccessIds = [];
            $settingsAccessList = SettingsYouTubeDownloadAccess::find();

            foreach ($settingsAccessList as $yaItem) {
                $youtubeAccessIds[] = $yaItem->user_id;
            }

            if (!array_search($this->currentUser->id, $youtubeAccessIds)) {
                $youtubeAccessIds[] = $this->currentUser->id;
            }
            $userList = Users::find([
                '(email LIKE :1: OR first_name LIKE :1: OR surname LIKE :1:) AND state = :2: AND (role = :3: OR id NOT IN (:4:))',
                'bind' => [
                    1 => $search . '%',
                    2 => Users::STATE_ACTIVE,
                    3 => Users::ROLE_MEGA_ADMIN,
                    4 => implode(',', $youtubeAccessIds)
                ]
            ]);
            $responseContent = ['status' => 'ok', 'list' => $userList];
        } else {
            $responseContent = ['status' => 'false'];
        }

        $this->response->setJsonContent($responseContent);
        return false;
    }
}
