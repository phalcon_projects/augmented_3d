<?php

class IndexController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('AugMe');
        $this->view->setVar('section_title', 'AugMe');
        parent::initialize();
    }

    public function indexAction()
    {
        if ($this->request->getPost('exception')) {
            $e = $this->request->getPost('exception');
            $stack = $this->request->getPost('stack');

            mail($this->config->email->log, 'Error on Augmented3d', 'Message:' . $e . '\n\nStack:' . $stack);
            return '';
        }

        return $this->dispatcher->forward([
            'controller' => 'projects',
            'action' => 'index',
        ]);
    }

    public function bundleAction()
    {
        $result = [];

        if (!$this->request->get('edition_id')) {
            $result['error'] = ['Edition is not specified'];
        } else {
            $result = Bundle::generate($this->request->get('edition_id'));
        }

        $this->response->setJsonContent($result);

        return false;
    }

    public function defaultBundleAction()
    {
        $projectUUID = $this->request->get('project_id');

        if (!$projectUUID) {
            $this->response->setJsonContent('Project uuid is required');
            return false;
        }

        $project = Projects::findFirstByUUID($projectUUID);

        if (!$project) {
            $this->response->setJsonContent('Failed to find project');
            return false;
        }

        $this->response->setJsonContent(BundleDefault::generate($project->id));

        return false;
    }

    public function importBundleAction()
    {
        $result = [];
        $result['error'] = '';

        $projectId = $this->request->get('projectId');
        if (!$projectId) {
            $result['error'] = 'Missing projectId.';
        }

        if (!$this->request->hasFiles()) {
            $result['error'] = 'No file to import';
        }

        if (!$result['error']) {
            $files = $this->request->getUploadedFiles();

            try {
                $edition = $this->importBundle($projectId, $files[0]->getTempName(), $files[0]->getName());
                $result['editionId'] = $edition->id;
                $result['project'] = $edition;
            } catch (Exception $exception) {
                $result['error'] = $exception->getMessage();
            }
        }

        $this->response->setJsonContent($result);

        return false;
    }

    public function apiGetBundlesAction()
    {
        $responseContent = $this->validateAndGetBundle();
        $this->response->setJsonContent($responseContent);

        return false;
    }

    public function apiBundleValidateAction()
    {
        $responseContent = [];
        $editionId = $this->request->get('bundle_id');
        $projectUUID = $this->request->get('project_id');

        if ($editionId === null || $editionId == 0 && !$projectUUID) {
            $responseContent['error'] = 'Not all parameters are specified.';
        } else {
            $validate = $this->validateBundleRequest();

            if (!isset($validate['error'])) {
                $bundle = null;

                if ($editionId != 0) {
                    $bundle = $this->getBundle($editionId);
                } else if ($projectUUID) {
                    $bundle = $this->getBundleDefault($projectUUID);
                }

                if (!$bundle) {
                    $responseContent['error'] = 'Bundle fetching error.';
                } else {
                    $responseContent['version'] = $bundle->version;
                }
            } else {
                $responseContent['error'] = 'Application validation error.';
            }
        }

        $this->response->setJsonContent($responseContent);

        return false;
    }

    public function apiGetBundleAction()
    {
        $responseContent = [];
        $bundleId = $this->request->get('bundle_id');
        $projectUUID = $this->request->get('project_id');

        if ($bundleId === null || $bundleId == 0 && !$projectUUID) {
            $responseContent['error'] = 'Not all parameters are specified.';
        } else {
            $responseContent = $this->validateAndGetBundle($bundleId, $projectUUID);
        }

        $this->response->setJsonContent($responseContent);

        return false;
    }

    protected function validateAndGetBundle($editionId, $projectUUID = '')
    {
        $validate = $this->validateBundleRequest();

        if (isset($validate['error'])) {
            $result['error'] = $validate['error'];
        } else {
            $bundleFile = $this->getBundleFile($editionId, $projectUUID);

            if ($bundleFile) {
                $result['path'] = $bundleFile;
            } else {
                $result['error'] = 'Error fetching bundle file.';
            }
        }

        return $result;
    }

    protected function validateBundleRequest()
    {
        $parameters['app_id'] = $this->request->get('app_id');
        $parameters['key'] = $this->request->get('secret_key');
        $parameters['platform_id'] = $this->request->get('platform_id');
        $parameters['platform_type'] = AppPlatformKey::getPlatformIdByType(strtolower($this->request->get('platform')));

        if ($parameters['app_id'] === null) {
            $validate['error'] = 'app_id parameter is not specified';
        } else if ($parameters['key'] === null) {
            $validate['error'] = 'secret_key parameter is not specified';
        } else if ($parameters['platform_id'] === null) {
            $validate['error'] = 'platform_id parameter is not specified';
        } else if ($parameters['platform_type'] === null) {
            $validate['error'] = 'platform parameter is not specified';
        } else {
            $validate = $this->validateApplication($parameters);
        }

        return $validate;
    }

    public function getBundle($editionId)
    {
        if (!$editionId) {
            return null;
        }

        $result = Bundle::findFirst([
            'edition_id = :1:',
            'bind' => [1 => $editionId]
        ]);

        return $result ?: null;
    }

    /**
     * @param string $projectUUID
     * @return null
     */
    public function getBundleDefault($projectUUID)
    {
        if (!$projectUUID) {
            return null;
        }

        $project = Projects::findFirstByUUID($projectUUID);

        if (!$project) {
            return null;
        }

        $result = BundleDefault::findFirst([
            'project_id = :1:',
            'bind' => [1 => $project->id]
        ]);

        return $result ?: null;
    }

    public function getBundleFile($editionId, $projectUUID = '')
    {
        $result = null;
        $bundlePath = $this->getDi()->getConfig()->application->siteUri . '/files/bundles/';

        if ($editionId != 0) {
            $result = $this->getBundle($editionId);
            $bundlePath .= "{$editionId}/";
        } else if ($projectUUID != '') {
            $project = Projects::findFirstByUUID($projectUUID);

            if ($project) {
                $result = $this->getBundleDefault($projectUUID);
                $bundlePath .= "p{$project->id}/";
            }
        }

        return $result ? $bundlePath . "{$result->hash}.zip" : null;
    }

    function clamp($value, $min, $max)
    {
        return max($min, min($max, $value));
    }

    /**
     * @param $m
     * @param $order
     * @return array
     */
    function makeEulerFromRotationMatrix($m, $order)
    {
        // assumes the upper 3x3 of m is a pure rotation matrix (i.e, unscaled)
        $m11 = $m[0];
        $m12 = $m[4];
        $m13 = $m[8];
        $m21 = $m[1];
        $m22 = $m[5];
        $m23 = $m[9];
        $m31 = $m[2];
        $m32 = $m[6];
        $m33 = $m[10];

        if ($order === 'XYZ') {

            $y = asin($this->clamp($m13, -1, 1));

            if (abs($m13) < 0.99999) {

                $x = atan2(-$m23, $m33);
                $z = atan2(-$m12, $m11);

            } else {

                $x = atan2($m32, $m22);
                $z = 0;

            }

        } else if ($order === 'YXZ') {

            $x = asin(-$this->clamp($m23, -1, 1));

            if (abs($m23) < 0.99999) {

                $y = atan2($m13, $m33);
                $z = atan2($m21, $m22);

            } else {

                $y = atan2(-$m31, $m11);
                $z = 0;

            }

        } else if ($order === 'ZXY') {

            $x = asin($this->clamp($m32, -1, 1));

            if (abs($m32) < 0.99999) {

                $y = atan2(-$m31, $m33);
                $z = atan2(-$m12, $m22);

            } else {

                $y = 0;
                $z = atan2($m21, $m11);

            }

        } else if ($order === 'ZYX') {

            $y = asin(-$this->clamp($m31, -1, 1));

            if (abs($m31) < 0.99999) {

                $x = atan2($m32, $m33);
                $z = atan2($m21, $m11);

            } else {

                $x = 0;
                $z = atan2(-$m12, $m22);

            }

        } else if ($order === 'YZX') {

            $z = asin($this->clamp($m21, -1, 1));

            if (abs($m21) < 0.99999) {

                $x = atan2(-$m23, $m22);
                $y = atan2(-$m31, $m11);

            } else {

                $x = 0;
                $y = atan2($m13, $m33);

            }

        } else if ($order === 'XZY') {

            $z = asin(-$this->clamp($m12, -1, 1));

            if (abs($m12) < 0.99999) {

                $x = atan2($m32, $m22);
                $y = atan2($m13, $m11);

            } else {

                $x = atan2(-$m23, $m33);
                $y = 0;

            }

        }

        return [
            'x' => $x,
            'y' => $y,
            'z' => $z,
        ];
    }

    function arrayMatrixToMatrix($array, $size)
    {

        $matrix = [];
        for ($row = 0; $row < $size; $row++) {
            for ($col = 0; $col < $size; $col++) {
                $matrix[$row][$col] = $array[$row * $size + $col];
            }
        }

        return $matrix;
    }

    /**
     * Given by Kharkiv team
     * @deprecated use makeEulerFromRotationMatrix
     * @param $matrix
     * @return array
     */
    function matrixToEuler($matrix)
    {
        // float fi, theta, ksi; // fi is a rotation around X, theta is a rotation around Y, ksi is a rotation around Z
        if (($matrix[2][0] != 1) & ($matrix[2][0] != -1)) { // cos(theta)==0
            // the half of a plane where cos(theta)>=0 is chosen
            $theta = -asin($matrix[2][0]); // another: pi-theta
            $fi = atan2($matrix[2][1], $matrix[2][2]); // another: atan2(-rot.at<double>(3, 2), -rot.at<double>(3, 3));
            $ksi = atan2($matrix[1][0], $matrix[0][0]); // another: atan2(-rot.at<double>(2, 1), -rot.at<double>(1, 1));
        } else {
            $ksi = 0; // may be set to any value
            if ($matrix[2][0] == -1) {
                $theta = M_PI_2;
                $fi = $ksi + atan2($matrix[0][1], $matrix[0][2]);
            } else {
                $theta = -M_PI_2;
                $fi = -$ksi + atan2(-$matrix[0][1], $matrix[0][2]);
            }
        }

        // TODO: find out why it comes negative?
        $ang = [
            'x' => $fi * -1,
            'y' => $theta * -1,
            'z' => $ksi * -1
        ];
        return $ang;
    }

    /**
     * @param integer $projectId
     * @param string $bundleFileName
     * @param string $bundleName
     * @return Editions
     * @throws Exception
     */
    function importBundle($projectId, $bundleFileName, $bundleName)
    {
        $userId = $this->currentUser->id;
        $coversDir = APP_PATH . '/public/' . Covers::DIRECTORY_PATH;
        $objectsDir = APP_PATH . '/public/' . Library::DIRECTORY_PATH;

        $coverPlaneWidth = 100;

        $zip = new ZipArchive();
        $zip->open($bundleFileName);

        $tmp_dir = sys_get_temp_dir() . 'aug3d' . time() . '/';
        mkdir($tmp_dir, 0777);

        $zip->extractTo($tmp_dir);

        if (!file_exists($tmp_dir . 'Config.json')) {
            throw new Exception('Bundle format error. Config file is missing.');
        }

        $config = json_decode(file_get_contents($tmp_dir . 'Config.json'));

        if (!$config || !$config->marker_configuration) {
            throw new Exception('Bundle format error. Config file unrecognized format.');
        }

        if (!$config->marker_configuration->marker_count) {
            throw new Exception('Bundle is empty.');
        }

        // create project
        $project = Projects::findFirst($projectId);
        if (!$project || !$project->isOwnedBy($userId)) {
            throw new Exception('Project not found.');
        }

        // create edition
        $edition = new Editions();
        $edition->project_id = $project->id;
        $edition->title = $bundleName;
        $edition->state = 1;
        $saved = $edition->save();

        if (!$saved) {
            $errors = [];
            foreach ($edition->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            throw new Exception('Failed to create edition' . implode('<br>', $errors));
        }

        $markerIdToCover = [];

        // create covers
        $position = 0;

        foreach ($config->markers as $marker) {
            $cover = new Covers();
            $cover->edition_id = $edition->id;
            $cover->title = $marker->id;
            $cover->position = $position;
            $cover->status = Covers::STATUS_NORMAL;

            $targetFileName = $coversDir . 'bundle_' . $project->id . '_' . $edition->id . '_' . basename($marker->marker_path);
            $cover->filename = $this->makeUniqueFilename($targetFileName);

            copy($tmp_dir . $marker->marker_path, $coversDir . $cover->filename);

            $cover->rating = (int)$this->getArQuality($coversDir . $cover->filename);

            $saved = $cover->save();
            if (!$saved) {
                $errors = [];
                foreach ($cover->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                throw new Exception('Failed to create cover' . implode('<br>', $errors));
            }

            $markerIdToCover[$marker->id] = $cover;
            $position++;
        }

        // fill covers objects && media pool

        foreach ($config->models as $modelIndex => $model) {
            if (!is_string($model->data)) {
                $model->data = $model->data->zip_path;
            }
            $modelsNamePrefix = 'bundle_' . $project->id . '_' . $edition->id . '_' . $model->id . '_' . $modelIndex . '_';

            $tmpModelDir = $tmp_dir . 'models/' . $model->id . '/';
            $tmpFullFileName = $tmpModelDir . $model->data;

            // create objects
            $libraryObject = new Library();
            $libraryObject->user_id = $userId;
            $libraryObject->edition_id = $edition->id;
            $libraryObject->title = $model->data; // model->data is fileName

            $targetFileName = basename($model->data);
            $targetSubDir = dirname($model->data);
            if ($targetSubDir === '.') {
                $targetSubDir = '';
            } else {
                $targetSubDir .= '.unp/';
            }
            $libraryObject->filename = $modelsNamePrefix . $targetSubDir . $targetFileName; // model->data is fileName

            $type = mime_content_type($tmpFullFileName);
            if ($type == 'application/octet-stream' || $type == 'text/plain') {
                $ext = pathinfo($model->data, PATHINFO_EXTENSION);
                $type = 'object/' . $ext;
            }
            $libraryObject->type = $type;
            $libraryObject->filesize = filesize($tmpFullFileName);

            $libraryObject->params = [];

            // TODO: same code in LibraryController -> uploadAction
            $matches = [];
            if (preg_match('@^(video|image|audio)@', $libraryObject->type, $matches)) {
                $type = $matches[0];
                $getID3 = new getID3;

                $info = $getID3->analyze($tmpFullFileName);

                if ($type == 'image' || $type == 'video') {
                    $libraryObject->params['width'] = $info['video']['resolution_x'];
                    $libraryObject->params['height'] = $info['video']['resolution_y'];
                } elseif ($type == 'audio') {
                    if ($info['fileformat'] == 'mp3') {
                        $title = '';
                        if (isset($info['tags']['id3v2'])) {
                            $title = $info['tags']['id3v2']['title'][0];
                        } else if (isset($info['tags']['id3v1'])) {
                            $title = $info['tags']['id3v1']['title'][0];
                        }

                        if ($title) {
                            $libraryObject->title = $title;
                        }
                    }
                }
                if (isset($info['playtime_string'])) {
                    $libraryObject->params['playtime_string'] = $info['playtime_string'];
                }
            }

            $saved = $libraryObject->save();
            if (!$saved) {
                $errors = [];
                foreach ($libraryObject->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                throw new Exception('Failed to create LibraryObject' . implode('<br>', $errors));
            }

            // generate directories & copy files
            $targetFileName = $objectsDir . $libraryObject->filename;
            $targetDir = dirname($targetFileName) . '/';
            if (!file_exists($targetDir)) {
                if (!mkdir($targetDir, 0777, true)) {
                    throw new Exception('Failed to create files.');
                }
            }
            copy($tmpFullFileName, $targetFileName);

            if ($model->external_data) {
                foreach ($model->external_data as $extraFileName) {
                    if (!is_string($extraFileName)) {
                        $extraFileName = $extraFileName->zip_path;
                    }
                    $extraFileName = trim($extraFileName, '/');

                    $targetFileName = basename($extraFileName);
                    $targetSubDir = dirname($extraFileName);
                    if ($targetSubDir === '.') {
                        $targetSubDir = '';
                    } else {
                        $targetSubDir .= '/';
                    }

                    $targetFileName = $objectsDir . $modelsNamePrefix . $targetSubDir . $targetFileName;
                    $targetDir = dirname($targetFileName) . '/';

                    if (!file_exists($targetDir)) {
                        if (!mkdir($targetDir, 0777, true)) {
                            throw new Exception('Failed to create files.');
                        }
                    }

                    copy($tmpModelDir . $extraFileName, $targetFileName);
                }
            }

            $cover = $markerIdToCover[$model->marker_id];
            $sceneObject = new Objects();
            $sceneObject->object_id = $libraryObject->id;
            $sceneObject->cover_id = $cover->id;

            list($tstCoverWidth, $tstCoverHeight) = getimagesize($coversDir . $cover->filename);
            $tstCoverSDK = $this->sdk3dImageSize($tstCoverWidth, $tstCoverHeight);
            $unitToSdk2 = $tstCoverSDK['width'] / $coverPlaneWidth;

            $sceneObject->x = $model->translation_vector[0] / $unitToSdk2;
            $sceneObject->y = $model->translation_vector[1] / $unitToSdk2;
            $sceneObject->z = $model->translation_vector[2] / $unitToSdk2;
            $sceneObject->scale = $model->scale_matrix[0] / $unitToSdk2;

            $rotation_matrix = $this->rotateMatrixArray($model->rotation_matrix);
            $rotation_matrix = $this->arrayMatrixToMatrix($rotation_matrix, 4);
            $rotation = $this->matrixToEuler($rotation_matrix);

            $sceneObject->rx = $this->radiansToDegree($rotation['x']);
            $sceneObject->ry = $this->radiansToDegree($rotation['y']);
            $sceneObject->rz = $this->radiansToDegree($rotation['z']);

            // options
            $sceneObject->options = new stdClass();

            if (isset($model->custom_fields)) {
                if (isset($model->custom_fields->animation)) {
                    $sceneObject->options->animation = $model->custom_fields->animation;
                }

                if (isset($model->custom_fields->action)) {
                    $action = $model->custom_fields->action;
                    if ($action->type) {
                        $sceneObject->options->action = new stdClass();
                        $sceneObject->options->action->type = $action->type;
                        if ($action->type === 'url') {
                            $sceneObject->options->action->url = $model->custom_fields->action->url;
                        } elseif ($action->type === 'video') {
                            $sceneObject->options->action->video = $model->custom_fields->action->video;
                        }
                    }
                }
            }

            $saved = $sceneObject->save();
            if (!$saved) {
                $errors = [];
                foreach ($sceneObject->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                throw new Exception('Failed to create scene object' . implode('<br>', $errors));
            }
        }

        return $edition;
    }

    /**
     * @throws Exception
     * @param $filename
     * @return bool|string
     */
    private function getArQuality($filename)
    {
        // TODO: same function in CoversController, move it to Base? make utils object

        if ($this->config->artools->quality == 'fake') {
            return rand(1, 3);
        }

        $tmp_name = sys_get_temp_dir() . '/a3d_tmp_' . time() . '.jpg';

//		 segmentation check
        $call = $this->config->artools->segmentation . ' ' . escapeshellarg($filename) . ' ' . escapeshellarg($tmp_name) . ' 2>&1';
        system($call);

        if (!file_exists($tmp_name)) {
            throw new Exception('segmentation failed');
        }

        // quality check
        $qcall = $this->config->artools->quality . ' ' . escapeshellarg($tmp_name) . ' 2>&1';
        $stars = system($qcall);
        unlink($tmp_name);

        return $stars;
    }
}
