<?php

use Phalcon\Http\Response;

/**
 * SessionController
 *
 * Allows to authenticate users
 */
class SessionController extends ControllerBase
{
    /**
     * @var $oAuthClientGoogle Google_Client
     */
    protected $oAuthClientGoogle;

    public function initialize()
    {
        $this->tag->setTitle('Sign In');
        parent::initialize();

        $this->initGoogleClient();
    }

    protected function initGoogleClient()
    {
        $clientId = $this->config->services->googleClientId;
        $clientSecret = $this->config->services->googleClientSecret;
        $redirectURL = 'http://' . $_SERVER['HTTP_HOST'] . '/oauth-callback-google';

        $client = new Google_Client();
        $client->setApplicationName('VisionPaper');
        $client->setClientId($clientId);
        $client->setClientSecret($clientSecret);
        $client->setIncludeGrantedScopes(true);
        $client->addScope('email');
        $client->setRedirectUri($redirectURL);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $this->oAuthClientGoogle = $client;

        $this->view->setVar('googleAuthUrl', $client->createAuthUrl());
    }

    public function indexAction()
    {
        $this->view->setVar("FB_appId", $this->config->facebook->appId);
        $this->view->setVar("FB_apiVersion", $this->config->facebook->apiVersion);
        $this->assets->addCss("css/styles.css");
        $this->assets->addJs("js/login.js");

        if (!$this->request->isPost()) {
            /*$this->tag->setDefault('email', '');
            $this->tag->setDefault('password', '');*/
        }
    }

    /**
     * Register an authenticated user into session data
     *
     * @param Users $user
     */
    private function _registerSession(Users $user)
    {
        $this->session->set('auth', [
            'id' => $user->id,
            'login' => $user->login,
            'first_name' => $user->first_name,
            'surname' => $user->surname,
            'role' => $user->role
        ]);
    }

    /**
     * This action authenticate and logs an user into the application
     *
     */
    public function startAction()
    {
        if ($this->request->isPost()) {
            $response = new Response();
            $result = new stdClass();

            $login = $this->request->getPost('login');
            $password = $this->request->getPost('password');

            $user = Users::findFirst([
                'login = :login:',
                'bind' => ['login' => $login]
            ]);

            if ($user != false) {
                if ($this->security->checkHash($password, $user->password)) {
                    $this->_registerSession($user);

                    $result->continue = $this->url->get('');
                } else {
                    $result->error = 'Wrong email/password';
                }
            } else {
                $result->error = 'Wrong email/password';
            }

            $response->setJsonContent($result);

            return $response;
            //$this->flash->error('Wrong email/password');
        }

        return $this->dispatcher->forward([
            'controller' => 'session',
            'action' => 'index',
        ]);
    }

    /**
     * Finishes the active session redirecting to the index
     *
     * @return unknown
     */
    public function endAction()
    {
        if ($this->session->has('auth')) {
            $this->session->remove('auth');
            $this->flash->success('Goodbye!');
        }

        return $this->response->redirect('/');
    }

    public function authAction()
    {
        if ($this->session->get('auth')) {
            return $this->response->redirect($this->config->application->defaultPath);
        }

        if (!$this->request->get('code')) {
            $this->flash->error('The OAuth provider information is invalid.');

            return false;
        }

        $this->oAuthClientGoogle->fetchAccessTokenWithAuthCode($this->request->get('code'));
        $googleAccessToken = $this->oAuthClientGoogle->getAccessToken();
        $googleRefreshToken = $this->oAuthClientGoogle->getRefreshToken();
        //$this->session->set('google_access_token', $googleAccessToken['access_token']);

        try {
            $service = new Google_Service_Oauth2($this->oAuthClientGoogle);
            $googleUserInfo = $service->userinfo->get();

            if ($googleUserInfo->email) {
                $user = Users::findFirst([
                    'conditions' => 'email = :email:',
                    'bind' => ['email' => $googleUserInfo->email]
                ]);

                if ($user) {
                    $oauth = Oauth::findFirst([
                        'conditions' => 'user_id = :id:',
                        'bind' => ['id' => $user->id]
                    ]);

                    if ($oauth) {
                        $oauth->update([
                            'user_id' => $user->id,
                            'access_token' => $googleAccessToken['access_token'],
                            'refresh_token' => $googleRefreshToken
                        ]);
                    }
                } else {
                    $user = new Users([
                        'email' => $googleUserInfo->email,
                        'login' => $googleUserInfo->email,
                        'first_name' => isset($googleUserInfo->givenName) ? $googleUserInfo->givenName : '',
                        'surname' => isset($googleUserInfo->familyName) ? $googleUserInfo->familyName : ''
                    ]);
                    if (!$user->save()) {
                        throw new Error('Cannot save a new user.');
                    }

                    $oauth = new Oauth([
                        'user_id' => $user->id,
                        'provider' => 'google',
                        'access_token' => $googleAccessToken['access_token'],
                        'refresh_token' => $googleRefreshToken
                    ]);

                    if (!$oauth->save()) {
                        throw new Error('Error processing oauht.');
                    }
                }
                $this->_registerSession($user);
            }

            return $this->response->redirect($this->config->application->defaultPath);
        } catch (Exception $exception) {
            error_log($exception->getMessage());
            $this->flash->error('Failed OAuth authentication.');
        }

        return $this->response->redirect($this->config->application->defaultPath);
    }

    /**
     * Sign in with the OAuth callback redirected from the Oauth provider
     */
    public function signInOauthAction()
    {
        $provider = $this->dispatcher->getParam('provider');
        $code = $this->request->get('code');

        if (empty($code)) {
            $this->flash->error($this->translate('The OAuth provider information is invalid.'));

            return false;
        }

        try {
            $this->auth->checkOauth($provider, $code);

            return $this->response->redirect($this->config->application->defaultPath);
        } catch (Exception $e) {
            $this->flash->error($e->getMessage());
        }
    }

    /**
     * Redirects to the Oauth provider url
     */
    public function signInRedirectOauthAction()
    {
        $provider = $this->dispatcher->getParam('provider');
        $nonce = $this->dispatcher->getParam('nonce');

        if ($nonce !== $this->security->getSessionToken()) {
            $this->flash->error($this->translate->gettext('Security token is invalid.'));

            return $this->response->redirect([
                'controller' => 'session',
                'action' => 'signIn',
            ]);
        }

        $authUrl = $this->auth->getAuthorizationUrl($provider);
        $this->response->redirect($authUrl, true);
        $this->response->send();

        return false;
    }

    public function facebookAction()
    {
        $result = new stdClass();

        $fb = new Facebook\Facebook([
            'app_id' => $this->config->facebook->appId,
            'app_secret' => $this->config->facebook->appSecret,
            'default_graph_version' => 'v2.2',
        ]);

        $helper = $fb->getJavaScriptHelper();

        try {
            $accessToken = $helper->getAccessToken();

            // Logged in

            // get user
            $response = $fb->get('/me?fields=id,name,email', $accessToken);
            $result->res = $response;

            $fb_user = $response->getGraphUser();
            $result->fb_user = $fb_user->asArray();

            if ($fb_user['email']) {
                $user = Users::findFirst(array(
                    "email = :email:",
                    'bind' => array('email' => $fb_user['email'])
                ));
                if ($user) {
                    $this->_registerSession($user);
                    $result->continue = $this->url->get("");
                } else {
                    $result->error = 'Sorry, you are not registered user here.';
                }
            }

        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            $result->error = 'Graph returned an error: ' . $e->getMessage();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            $result->error = 'Facebook SDK returned an error: ' . $e->getMessage();
        }

//		if (!isset($accessToken)) {
//			$result->error = 'No cookie set or OAuth data is not valid.';
//		}

        $this->response->setJsonContent($result);
        return false;
    }
}
