<?php

use Phalcon\Http\Response;

class ObjectsController extends ControllerBase
{
	public function indexAction()
	{
		$result = new stdClass();
		$errors = [];
		$objects = [];

		if ((int) $this->request->getPost('id')) {
			$id = (int)$this->request->getPost('id');

			// TODO: error handling, 404?

			$object = Objects::findFirst($id);
			$object->title = $this->request->getPost('title')? $this->request->getPost('title') : '';

			$object->x = $this->request->getPost('x');
			$object->y = $this->request->getPost('y');
			$object->z = $this->request->getPost('z');

			$object->rx = $this->request->getPost('rx');
			$object->ry = $this->request->getPost('ry');
			$object->rz = $this->request->getPost('rz');

			$object->scale = $this->request->getPost('scale');

			if (!$object->save()) {
				$errors[] = 'Failed to save db record.';
				foreach ($object->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
			}
		} else {
			// list all objects for 'media library'

			$objects = Objects::find();
		}

		$result->objects = $objects;
		$result->errors = $errors;

		$response = new Response();
		$response->setJsonContent($result);

		return $response;
	}

	public function addAction() {
		$result = new stdClass();
		$errors = [];
		$object = [];

		$cover_id = (int) $this->request->getPost('cover_id');
		$library_id = (int) $this->request->getPost('library_id');

		if ($cover_id && $library_id) {

			$object = new Objects();
			$object->object_id = $library_id;
			$object->cover_id = $cover_id;

			if ($object->save()) {
				$object = $object->toPlainVersion();
			} else {
				$errors[] = 'Failed to save db record.';
				foreach ($object->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
			}
		} else {
			if (!$cover_id)
				$errors[] = 'Cover id missing';
			if (!$library_id)
				$errors[] = 'Library id missing';
		}

		$result->object = $object;
		$result->errors = $errors;

		$response = new Response();
		$response->setJsonContent($result);

		return $response;
	}

	public function softDeleteAction() {
		return $this->setSoftDeleteStateAction(1);
	}

	public function unDeleteAction() {
		return $this->setSoftDeleteStateAction(0);
	}

	private function setSoftDeleteStateAction($delete) {
		$response = new Response();
		$ids = $this->request->get('id');
		if (!is_array($ids))
			$ids = array($ids);

		$objects = Objects::query()->inWhere('id', $ids)->execute();

		$messages = [];
		if ($objects) {
			foreach ($objects as $object) {
				$result = $delete? $object->softDelete() : $object->unDelete();
				if (!$result) {
					$messages = array_merge($messages, $object->getMessages());
				}
			}
		}

		if (count($messages)) {
			$response->setJsonContent($messages);
		} else {
			$response->setJsonContent(1);
		}

		return $response;
	}

	public function deleteAction() {
		$response = new Response();
		$id = (int)$this->request->get('id');
		$object = Objects::findFirst($id);

		if ($object) {
			if ($object->delete()) {
				$response->setJsonContent(1);
			} else {
				$response->setJsonContent($object->getMessages());
			}
		} else {

		}

		return $response;
	}

	public function checkConvertationAction() {
		$ids = $this->request->get('objects');

		$lib_ids = array();
		foreach ($ids as $id) {
			$object = Objects::findFirst($id);
			$lib_ids[] = $object->object_id;
		}

		$LibController = new LibraryController();
		$result = $LibController->checkConvertation($lib_ids);

		$response = new Response();
		$response->setJsonContent($result);
		return $response;
	}
}
