<?php

class ErrorsController extends ControllerBase
{
    private $result;

    public function initialize()
    {
//    	error_log("error init");
        $this->tag->setTitle('Oops!');
        $this->view->setVar('section_title', 'Oops!');

        parent::initialize();

        $this->result = new stdClass();
        if (isset($this->config->application->debug)) {
            if ($this->config->application->debug == 'true') {
                $message = $this->dispatcher->getParam(0);
                if ($message instanceof Exception) {
                    $this->result->devError = new stdClass();

                    $this->result->devError->code = $message->getCode();
                    $this->result->devError->message = $message->getMessage();
                    $this->result->devError->trace = $message->getTrace();
                    $this->result->devError->string = $message->__toString();
                } else {
                    $this->result->devError = $message;
                }
            }
        }

        //$this->view->setTemplateAfter('error');
    }

    public function show404Action()
    {
//		error_log("error 404");
		header('HTTP/1.0 404 Not Found');

        $this->result->error = 'Not found';

        if ($this->request->isAjax()) {
            $this->response->setJsonContent($this->result);
            return false;
        }

        $this->view->setVar('error', $this->result);
    }

    public function show401Action()
    {
//		error_log("error 401");

        $this->result->error = 'Unauthorized';

        if ($this->request->isAjax()) {
            header('HTTP/1.0 401 Unauthorized');
            $this->response->setJsonContent($this->result);
            return false;
        }

        $this->view->setVar('error', $this->result);
    }

    public function show500Action()
    {
//		error_log("error 500");

        $this->result->error = 'Internal Server Error';

        if ($this->request->isAjax()) {
            header('HTTP/1.0 500 Internal Server Error');
            $this->response->setJsonContent($this->result);
            return false;
        }

        $this->view->setVar('error', $this->result);
    }
}
