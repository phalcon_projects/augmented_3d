<?php

use Phalcon\Http\Response;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class ProjectsController extends ControllerBase
{
    protected $pageNumber = 1;
    protected $pageLimit = 2;

    public function initialize()
    {
        $this->tag->setTitle('Projects');
        $this->view->setVar('section_title', 'Projects');

        parent::initialize();
    }

    public function indexAction()
    {
        $this->assets->addCss('css/list.css');
        $this->assets->addCss('css/projects.css');
        $this->assets->addJs('js/page.projects.js');
        $this->view->page = $this->prepareList();
    }

    public function getTeamsForAccessAction()
    {
        if ($this->request->isPost()) {
            $projectId = $this->request->get('project_id');

            if (isset($projectId) && Projects::findFirst($projectId)) {
                $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
                return $this->view->getRender(
                    'projects',
                    'project-access',
                    ['teams' => $this->getCurrentUserTeams($projectId)]
                );
            } else {
                $response = [
                    'status' => 'error',
                    'message' => 'Project id is required.'
                ];
            }

            return $this->response->setJsonContent($response);
        }

        return $this->show404();
    }

    public function setTeamAccessAction()
    {
        if ($this->request->isPost()) {
            $projectId = $this->request->get('project_id');
            $teamId = $this->request->get('team_id');
            $accessed = $this->request->get('accessed');

            if (isset($projectId) && Projects::findFirst($projectId) && isset($teamId) && Teams::findFirst($teamId)) {
                $projectAccess = ProjectAccess::findFirst([
                    'project_id = :1: AND team_id = :2:',
                    'bind' => [
                        1 => $projectId,
                        2 => $teamId
                    ]
                ]);

                if (!$projectAccess) {
                    $projectAccess = new ProjectAccess();
                    $projectAccess->project_id = $projectId;
                    $projectAccess->team_id = $teamId;
                }

                $projectAccess->access_level = $accessed === 'true'
                    ? ProjectAccess::ACCESS_LEVEL_VIEW
                    : ProjectAccess::ACCESS_LEVEL_RESTRICTED;

                if (!$projectAccess->save()) {
                    foreach ($projectAccess->getErrors() as $error) {
                        $response['errors'] = $error->getMessage();
                        $response['message'] = 'Project access saving error.';
                    }
                } else {
                    $response = [
                        'status' => 'ok',
                        'message' => 'Project access for team is saved.'
                    ];
                }
            } else {
                $response = [
                    'status' => 'error',
                    'message' => 'Project and team parameters are required.'
                ];
            }

            return $this->response->setJsonContent($response);
        }

        return $this->show404();
    }

    protected function prepareList()
    {
        if ($this->request->isPost()) {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, Projects::class, $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, Projects::class, $this->request->get());

            if ($this->request->has('page')) {
                $this->pageNumber = $this->request->getQuery('page', 'int');
            }

            if ($this->request->has('limit')) {
                $this->pageLimit = $this->request->getQuery('limit', 'int');
            }
        }

        $query->orderBy('title');
        $query->andWhere('user_id = "' . $this->currentUser->id . '"');

        $accessProjectIds = [];

        foreach ($this->getAccess() as $accessItem) {
            $accessProjectIds[] = $accessItem->project_id;
        }

        if (count($accessProjectIds) > 0) {
            $query->orWhere('id IN(' . implode(',', $accessProjectIds) . ')');
            $parameters = $query->getParams();
            $items = Projects::find($parameters);
        } else {
            $items = [];
        }

        $this->pageNumber = max($this->pageNumber, 1);
        $this->pageLimit = max($this->pageLimit, 1);

        $paginatorData = [
            'data' => $items,
            'limit' => min($this->pageLimit, 1000),
            'page' => $this->pageNumber
        ];
        if (empty($items)) {
            $paginator = new PaginatorArray($paginatorData);
        } else {
            $paginator = new PaginatorModel($paginatorData);
        }

        return $paginator->getPaginate();
    }

    protected function getCurrentUserTeams($projectId)
    {
        $teams = [];

        if ($projectId) {
            $teamsSQL = "
          SELECT
            DISTINCT t.id,
            t.title,
            IF (pa.access_level > 0 IS NOT NULL, pa.access_level, 0) access
          FROM augmented3d.Teams t
            LEFT JOIN augmented3d.TeamUsers tu ON tu.team_id = t.id
            LEFT JOIN augmented3d.ProjectAccess pa ON pa.team_id = t.id
          WHERE t.creator_id = " . $this->currentUser->id . "
                OR tu.user_id = " . $this->currentUser->id . "
                AND tu.role = " . TeamUsers::ROLE_ADMIN;

            $teams = $this->modelsManager->executeQuery($teamsSQL);
        }

        return $teams;
    }

    protected function getAccess($projectId = null)
    {
        $projectWhere = '';

        if ($projectId) {
            $project = Projects::findFirst($projectId);

            if ($project) {
                if ($project->user_id === $this->currentUser->id) {
                    return ProjectAccess::ACCESS_LEVEL_EDIT;
                } else {
                    $projectWhere = ' AND pa.project_id = ' . $projectId;
                }
            } else {
                return ProjectAccess::ACCESS_LEVEL_RESTRICTED;
            }
        }

        $userTeamAccessSQL = "
          SELECT
            p.id project_id,
            IF(p.user_id = " . $this->currentUser->id . ", 2, pa.access_level) access_level
          FROM augmented3d.Projects p
            LEFT JOIN augmented3d.ProjectAccess pa ON pa.project_id = p.id
            LEFT JOIN augmented3d.TeamUsers tu ON tu.team_id = pa.team_id
          WHERE p.user_id = " . $this->currentUser->id . "
                OR (tu.user_id = " . $this->currentUser->id . " AND pa.access_level != 0)" . $projectWhere;

        $userTeamAccess = $this->modelsManager->executeQuery($userTeamAccessSQL);

        return $userTeamAccess;
    }

    public function saveAction()
    {
        $id = $this->request->get('id');
        $title = $this->request->get('title');
        $uuid = $this->request->get('uuid');

        if (!preg_match('/^[A-Za-z0-9._-]*$/', $uuid)) {
            $this->response->setJsonContent([
                'status' => 'false',
                'errors' => ['The uuid is contain inappropriate characters.']
            ]);
            return false;
        }

        if (!$title) {
            $this->response->setJsonContent(['Project title is need to be specified.']);
            return false;
        }

        if ($id) {
            $project = Projects::findFirst($id);

            if (!$project->userHaveAccess($this->currentUser->id)) {
                $this->response->setJsonContent(['You have no access to manage the project']);
                return false;
            }
        } else {
            $project = new Projects();
            $project->user_id = $this->currentUser->id;
        }

        $form = new ProjectEditForm();
        $data = [
            'title' => $this->request->getPost('title'),
            'uuid' => $uuid
        ];

        if (!$form->isValid($data, $project)) {
            $result['success'] = false;
            $result['errors'][] = "Validation errors.";

            foreach ($form->getMessages() as $message) {
                $result['errors'][] = $message->getMessage();
            }

            $this->response->setJsonContent($result);
        } else {
            if (!$project->save()) {
                $errors = [];

                foreach ($project->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }

                $this->response->setJsonContent(['success' => false, 'errors' => $errors]);
            } else {
                $this->response->setJsonContent(['success' => true]);
            }
        }

        return false;
    }

    public function deleteAction()
    {
        $response = new Response();
        $project = Projects::findFirst($this->request->get('id'));

        if (!$project->userHaveAccess($this->currentUser->id)) {
            $response->setJsonContent([
                'success' => false,
                'errors' => ['You have no access to manage the project']
            ]);
            return false;
        }

        if ($project) {
            if ($project->delete()) {
                $currentPage = $this->request->get('page');
                $itemList = $this->prepareList();

                if ($currentPage > $itemList->total_pages) {
                    $currentPage = $itemList->total_pages;
                }

                if ($currentPage < 1) {
                    $currentPage = 1;
                }

                $response->setJsonContent([
                    'success' => true,
                    'page' => $currentPage
                ]);
            } else {
                $response->setJsonContent([
                    'success' => false,
                    'errors' => $project->getMessages()
                ]);
            }
        }

        return $response;
    }
}
