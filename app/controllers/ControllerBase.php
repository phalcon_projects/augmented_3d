<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    protected $currentUser = null;
    protected $license = null;

    public function initialize()
    {
        //var_dump($this->dispatcher->getControllerName(), $this->dispatcher->getActionName());
        //exit;

        if ($this->dispatcher->getControllerName() == 'apps' && $this->dispatcher->getActionName() == 'apiValidate'
            || $this->dispatcher->getControllerName() == 'statistics' && $this->dispatcher->getActionName() == 'api'
            || $this->dispatcher->getControllerName() == 'index' && $this->dispatcher->getActionName() == 'apiGetBundles'
            || $this->dispatcher->getControllerName() == 'index' && $this->dispatcher->getActionName() == 'apiGetBundle') {
            return true;
        }

        $logoutProcess = $this->dispatcher->getControllerName() == 'session'
            && $this->dispatcher->getActionName() == 'end';

        if ($this->request->isAjax()) {
            // disable layout here
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        }

        $this->assets->collection('head')->addJs('vendor/modernizr-2.8.3-respond-1.4.2.min.js');

        $auth = $this->session->get('auth');

        if ($auth) {
            $this->currentUser = Users::findFirst([
                'conditions' => 'id = :1: AND state = :2:',
                'bind' => [
                    1 => $auth['id'],
                    2 => Users::STATE_ACTIVE
                ]
            ]);

            if ($this->currentUser) {
                $this->license = Licenses::findFirst([
                    'conditions' => 'user_id = :1: AND type = :2:',
                    'bind' => [
                        1 => $this->currentUser->id,
                        2 => Licenses::TYPE_MAIN
                    ]
                ]);
            }

            if (!$this->currentUserHasLicense()
                && ($this->dispatcher->getControllerName() == 'apps'
                    || $this->dispatcher->getControllerName() == 'team')) {
                return $this->response->redirect('/license/expired');
            }

            $this->view->setTemplateAfter('main');
            $this->view->setVar('auth', $auth);
            $this->view->setVar('currentUserHasLicense', $this->currentUserHasLicense());
            $this->view->setVar('isSuperAdmin', $this->currentUser->role == Users::ROLE_MEGA_ADMIN);
            $this->view->setVar('license', $this->license);
        } else {
            $this->view->setTemplateAfter('login');
        }

        if (!isset($this->assets->mainAssetsIncluded)) {
            $this->assets->addCss('https://fonts.googleapis.com/icon?family=Material+Icons', false);
            $this->assets->addCss($this->getDi()->getConfig()->application->siteUri . '/vendor/bootstrap/css/bootstrap.min.css');
            $this->assets->addCss($this->getDi()->getConfig()->application->siteUri . '/vendor/font-awesome/css/font-awesome.min.css');

            $this->assets->addJs($this->getDi()->getConfig()->application->siteUri . '/vendor/jquery/jquery.min.js');
            $this->assets->addJs($this->getDi()->getConfig()->application->siteUri . '/vendor/popper.min.js');
            $this->assets->addJs($this->getDi()->getConfig()->application->siteUri . '/vendor/bootstrap/js/bootstrap.min.js');

            if ($auth & !$logoutProcess) {
                $this->assets->addCss("vendor/css/nice-select.css");
                $this->assets->addJs("vendor/jquery.nice-select.min.js");
                $this->assets->addCss('css/styles.css'); // redesign styles
                $this->assets->addJs("js/utils.js");
            }

            $this->assets->mainAssetsIncluded = true;
        }

        $this->view->setVar('siteUri', $this->getDi()->getConfig()->application->siteUri);
    }

    protected function validateApplication($parameters)
    {
        $error = null;
        $license = null;

        if ($app = App::findFirst([
            'conditions' => 'name = :1:',
            'bind' => [
                1 => $parameters['app_id']
            ]
        ])) {
            $user = Users::findFirst([
                'conditions' => 'id = :1: AND state = :2:',
                'bind' => [
                    1 => $app->user_id,
                    2 => Users::STATE_ACTIVE
                ]
            ]);
            $license = Licenses::findFirst([
                'conditions' => 'user_id = :1: AND type = :2: AND date_to > NOW()',
                'bind' => [
                    1 => $user->id,
                    2 => Licenses::TYPE_MAIN
                ]
            ]);

            if (!$error && !$license) {
                $error = 'License is expired.';
            } else {
                $appKey = AppPlatformKey::findFirst([
                    'conditions' => 'app_id = :1: AND platform_type = :2: AND platform_id = :3: AND key = :4:',
                    'bind' => [
                        1 => $app->id,
                        2 => $parameters['platform_type'],
                        3 => $parameters['platform_id'],
                        4 => $parameters['key']
                    ]
                ]);

                if (!$error && !$appKey) {
                    $error = 'Specified key, platform id or platform type is not valid.';
                }
            }
        } else {
            $error = 'Cannot find application.';
        }

        if (!$error && !$license && $app) {
            $error = 'License is expired.';
        }

        $result = [];

        if ($error) {
            $result['error'] = $error;
        }

        return $result;
    }

    protected function show404()
    {
        $this->dispatcher->forward([
            'controller' => 'errors',
            'action' => 'show404'
        ]);

        return false;
    }

    protected function showLicenseExpired()
    {
        $this->dispatcher->forward([
            'controller' => 'licenses',
            'action' => 'showExpired'
        ]);

        return false;
    }

    public function currentUserHasLicense()
    {
        return $this->license && time() < strtotime($this->license->date_to);
    }

    protected function getTranslation()
    {
        $language = $this->request->getBestLanguage();

        $translationFile = APP_PATH . '/app/messages/' . $language . '.php';

        if (file_exists($translationFile)) {
            require $translationFile;
        } else {
            require APP_PATH . '/app/messages/en.php';
        }

        return new \Phalcon\Translate\Adapter\NativeArray(
            [
                'content' => $messages,
            ]
        );
    }

    public function makeUniqueFilename($fullPath) {
        $fileName = basename($fullPath);
        $directory = dirname($fullPath).DIRECTORY_SEPARATOR;

        $i = 2;
        while (file_exists($directory.$fileName)) {
            $parts = explode('.', $fileName);
            // Remove any numbers in brackets in the file name
            $parts[0] = preg_replace('/\(([0-9]*)\)$/', '', $parts[0]);
            $parts[0] .= '('.$i.')';

            $newFileName = implode('.', $parts);
            if (!file_exists($newFileName)) {
                $fileName = $newFileName;
            }
            $i++;
        }
        return $fileName;
    }
}
