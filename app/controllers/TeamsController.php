<?php

use Phalcon\Paginator\Adapter\Model as Paginator;
use PHPMailer\PHPMailer\PHPMailer;

class TeamsController extends ControllerBase
{

    public function initialize()
    {
        $this->tag->setTitle('Teams');
        $this->view->setVar("section_title", "teams");

        parent::initialize();
    }

    public function indexAction()
    {
        if (!$this->currentUserHasLicense()) {
            return $this->show404();
        }

        $this->assets->addCss('css/users.css');
        $this->assets->addJs("vendor/jquery.nice-select.min.js");
        $this->assets->addJs('js/page.teams.js');

        $pageNumber = 1;
        $pageSize = 8;
        $searchKey = null;

        if ($this->request->isPost()) {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, Teams::class, $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, Teams::class, $this->request->get());
        }

        if ($this->request->has("page")) {
            $pageNumber = $this->request->getQuery("page", "int");
        }

        if ($this->request->has("limit")) {
            $pageSize = $this->request->getQuery("limit", "int");
        }

        if ($this->request->has("search")) {
            $searchKey = $this->request->getQuery("search", "string");
        }

        $query->leftJoin('TeamUsers', 'TeamUsers.team_id = Teams.id')
            ->where('creator_id = ' . $this->currentUser->id)
            ->orWhere('TeamUsers.user_id = ' . $this->currentUser->id)
            ->orderBy("title");

        if ($searchKey) {
            $query->andWhere("key like '%" . $searchKey . "%'");
        }

        $parameters = $query->getParams();

        $list = Teams::find($parameters);

        if ($this->request->has("search") && count($list) == 0) {
            $this->flash->notice("The search did not find any");
        }

        $pageNumber = max((int)$pageNumber, 1);
        $pageSize = max((int)$pageSize, 1);

        $paginator = new Paginator(array(
            "data" => $list,
            "limit" => min($pageSize, 1000),
            "page" => $pageNumber
        ));

        $this->view->page = $paginator->getPaginate();
        $this->view->pageLimit = $pageSize;
        $this->view->isCurrentUserAdmin = (int)$this->currentUser->role === Users::ROLE_ADMIN;
    }

    public function getPermissionsAction()
    {
        if ($this->request->isPost()) {
            $response = [];
            $permissions = [];

            if ($teamId = $this->request->getPost('team_id')) {
                if ($team = Teams::findFirst([
                    'conditions' => 'id = ?1',
                    'bind' => [
                        1 => $teamId
                    ]
                ])) {
                    foreach ($team->permissions as $teamPermissionItem) {
                        switch ($teamPermissionItem->type) {
                            case '1';
                                $permissions['projects'] = $teamPermissionItem->value == '1';
                                break;
                            case '2';
                                $permissions['apps'] = $teamPermissionItem->value == '1';
                                break;
                            case '3';
                                $permissions['editions'] = $teamPermissionItem->value == '1';
                                break;
                            case '4';
                                $permissions['pages'] = $teamPermissionItem->value == '1';
                                break;
                        }
                    }
                } else {
                    $response['error'] = 'Cannot find team';
                }
            } else {
                $response['error'] = 'Request error';
            }

            $response['status'] = !isset($response['error']) ? 'ok' : 'failed';
            $response['permissions'] = $permissions;

            $this->response->setJsonContent($response);

            return false;
        }

        return $this->response->redirect('/license');
    }

    public function addAction()
    {
        $this->view->setVar("section_title", "teams");
        $team = null;

        if ($id = (int)$this->request->get('id')) {
            $team = Teams::findFirst();
        }

        $team = $team ?: new Teams(['creator_id' => $this->currentUser->id]);

        $form = new TeamForm($team, array('add' => true));

        $this->assets->addJs("js/jquery.nice-select.min.js");

        $this->view->form = $form;
        $this->view->userId = $this->currentUser->id;
        echo $this->view->render('users', 'team-add');
    }

    public function createAction()
    {
        if ($this->request->isPost()) {
            $result = [];

            $teamId = (int)$this->request->getPost('team_id', 'int');
            $teamTitle = $this->request->getPost('title', 'string');
            $item = null;

            if ($teamId && $team = Teams::findFirst($teamId)) {
                $team->title = $teamTitle;
            } else {
                $team = new Teams([
                    'title' => $teamTitle,
                    'creator_id' => $this->currentUser->id
                ]);
            }

            if (!$team->save()) {
                $result['errors'][] = 'Failed to save team';

                foreach ($team->getMessages() as $message) {
                    $result['errors'][] = $message->getMessage();
                }
            } else {
                $teamCreator = new TeamUsers([
                    'team_id' => $team->id,
                    'user_id' => $this->currentUser->id
                ]);

                if (!$teamCreator->save()) {
                    $result['errors'][] = 'Failed to add team creator to the members';

                    foreach ($team->getMessages() as $message) {
                        $result['errors'][] = $message->getMessage();
                    }
                }
            }

            if (!empty($result['errors'])) {
                $result['message'] = 'Unable to save team';
                $result['status'] = 'error';
                $result['redirect'] = false;
            } else {
                $result['status'] = 'ok';
                $result['message'] = 'A new team was created';
                $result['redirect'] = '/teams/';
                $result['errors'] = [];
            }

            $this->response->setJsonContent($result);

            return false;
        }

        return $this->response->redirect('/license');
    }

    public function editAction()
    {
        if ($this->request->isPost()) {
            $result = [];

            $teamId = (int)$this->request->getPost('team_id', 'int');
            $teamTitle = $this->request->getPost('title', 'string');
            $item = null;

            if ($teamId && $team = Teams::findFirst($teamId)) {
                $team->title = $teamTitle;

                if (!$team->save()) {
                    $result['errors'][] = 'Failed to save team';

                    foreach ($team->getMessages() as $message) {
                        $result['errors'][] = $message->getMessage();
                    }
                } else {
                    if (!isset($result['errors'])
                        && null !== $this->request->getPost('permission_projects_management')) {
                        $permission = new TeamPermissions([
                            'team_id' => $teamId,
                            'type' => 1,
                            'value' => ($this->request->getPost('permission_projects_management') == 'on' ? 1 : 0)
                        ]);

                        if (!$permission->save()) {
                            $result['errors'][] = 'Cannot save team permission';
                        }
                    }

                    if (!isset($result['errors'])
                        && null !== $this->request->getPost('permission_apps_management')) {
                        $permission = new TeamPermissions([
                            'team_id' => $teamId,
                            'type' => 1,
                            'value' => ($this->request->getPost('permission_apps_management') == 'on' ? 1 : 0)
                        ]);

                        if (!$permission->save()) {
                            $result['errors'][] = 'Cannot save team permission';
                        }
                    }

                    if (!isset($result['errors'])
                        && null !== $this->request->getPost('permission_editions_management')) {
                        $permission = new TeamPermissions([
                            'team_id' => $teamId,
                            'type' => 1,
                            'value' => ($this->request->getPost('permission_editions_management') == 'on' ? 1 : 0)
                        ]);

                        if (!$permission->save()) {
                            $result['errors'][] = 'Cannot save team permission';
                        }
                    }

                    if (!isset($result['errors'])
                        && null !== $this->request->getPost('permission_pages_management')) {
                        $permission = new TeamPermissions([
                            'team_id' => $teamId,
                            'type' => 1,
                            'value' => ($this->request->getPost('permission_pages_management') == 'on' ? 1 : 0)
                        ]);

                        if (!$permission->save()) {
                            $result['errors'][] = 'Cannot save team permission';
                        }
                    }
                }
            } else {
                $result['errors'][] = 'Team not found';
            }

            if (!empty($result['errors'])) {
                $result['message'] = 'Unable to save team';
                $result['status'] = 'error';
                $result['redirect'] = false;
            } else {
                $result['status'] = 'ok';
                $result['message'] = 'The team was saved';
                $result['redirect'] = '/teams/';
                $result['errors'] = [];
            }

            $this->response->setJsonContent($result);

            return false;
        }

        return $this->response->redirect('/license');
    }

    public function getUsersForAddAction()
    {
        if ($this->request->isPost()) {
            $result = [];

            $teamId = (int)$this->request->getPost('team_id', 'int');
            $usersForInvite = null;

            if (!$team = Teams::findFirst($teamId)) {
                $result['errors'][] = 'Failed to get team';

                foreach ($team->getMessages() as $message) {
                    $result['errors'][] = $message->getMessage();
                }
            } else {
                $teamMemberIds = [];

                foreach ($team->members as $member) {
                    $teamMemberIds[] = $member->user_id;
                }

                $parameters = $teamMemberIds ? 'id NOT IN (' . implode(', ', $teamMemberIds) . ')' : '';

                $usersForInvite = Users::find($parameters);
            }

            if (!empty($result['errors'])) {
                $result['message'] = 'Unable to get users for invite';
                $result['status'] = 'error';
                $result['redirect'] = false;
            } else {
                $result['status'] = 'ok';
                $result['users'] = $usersForInvite;
                $result['errors'] = [];
            }

            $this->response->setJsonContent($result);

            return false;
        }

        $this->response->setJsonContent([]);
    }

    public function addUserToTeamAction()
    {
        if ($this->request->isPost()) {
            $result = [];

            $teamId = (int)$this->request->getPost('team_id', 'int');
            $userId = (int)$this->request->getPost('user_id', 'int');
            $teamMember = null;

            if ($teamId && $team = Teams::findFirst($teamId) && $userId && $user = Users::findFirst($userId)) {
                $teamMember = new TeamUsers([
                    'team_id' => $teamId,
                    'user_id' => $userId,
                    'role' => TeamUsers::ROLE_USER
                ]);
            }

            if (!$teamMember->save()) {
                $result['errors'][] = 'Failed to invite user to the team';

                foreach ($teamMember->getMessages() as $message) {
                    $result['errors'][] = $message->getMessage();
                }
            }

            if (!empty($result['errors'])) {
                $result['message'] = 'Unable to invite user to the team';
                $result['status'] = 'error';
                $result['redirect'] = false;
            } else {
                $result['status'] = 'ok';
                $result['message'] = 'User is invited to the team';
                $result['redirect'] = '/teams/';
                $result['errors'] = [];
            }

            $this->response->setJsonContent($result);

            return false;
        }

        return $this->response->redirect('/license');
    }

    public function removeUserFromTeamAction()
    {
        if ($this->request->isPost()) {
            $result = [];

            $teamId = (int)$this->request->getPost('team_id', 'int');
            $userId = (int)$this->request->getPost('user_id', 'int');
            $teamMember = null;

            if ($teamMember = TeamUsers::findFirst([
                'conditions' => 'team_id = ?1 AND user_id = ?2',
                'bind' => [
                    1 => $teamId,
                    2 => $userId
                ]
            ])) {
                if (!$teamMember->delete()) {
                    $result['errors'][] = 'Failed to remove user from the team';

                    foreach ($teamMember->getMessages() as $message) {
                        $result['errors'][] = $message->getMessage();
                    }
                }
            } else {
                $result['errors'][] = 'The user is not in the team member list';
            }

            if (!empty($result['errors'])) {
                $result['message'] = 'Unable to remove user from the team';
                $result['status'] = 'error';
                $result['redirect'] = false;
            } else {
                $result['status'] = 'ok';
                $result['message'] = 'The member is removed from the team';
                $result['redirect'] = '/teams/';
                $result['errors'] = [];
            }

            $this->response->setJsonContent($result);

            return false;
        }

        return $this->response->redirect('/teams');
    }

    public function inviteUserAction()
    {
        if ($this->request->isPost()) {
            $result = [];

            $teamId = (int)$this->request->getPost('team_id', 'int');
            $email = $this->request->getPost('email', 'string');
            $teamMember = null;

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $result['errors'][] = 'Not correct email address is specified';
            } else {
                if ($teamId && $team = Teams::findFirst($teamId)) {
                    if (Users::findFirst([
                        'conditions' => 'email = ?1',
                        'bind' => [1 => $email]
                    ])) {
                        $result['errors'][] = 'Cannot invite user by email. The specified email is already registered';
                    } else {
                        $newUserPassword = uniqid($email, true);
                        $user = new Users([
                            'email' => $email,
                            'login' => explode('@', $email)[0],
                            'state' => Users::STATE_PENDING,
                            'password' => $this->getDi()->getSecurity()->hash($newUserPassword)
                        ]);

                        if (!$user->save()) {
                            $result['errors'][] = 'Cannot create user';

                            foreach ($user->getMessages() as $message) {
                                $result['errors'][] = $message->getMessage();
                            }
                        } else {
                            $mail = new PHPMailer;
                            $siteTitle = $this->getDi()->getConfig()->application->siteTitle;
                            $mail->setFrom($this->getDi()->getConfig()->email->main, $siteTitle);
                            $mail->addAddress($email, '');
                            $mail->Subject = $siteTitle . ' invitation';
                            $mail->Body = 'Greetings!
                                            <br>You has invited to the ' . $siteTitle . ' team "' . $team->title . '".
                                            <br>Please login at the site <a target="_blank"
                                             href="' . $this->getDi()->getConfig()->application->siteUri . '"
                                             >' . $siteTitle . '</a> with parameters:
                                            <br>login: ' . $email . '
                                            <br>password: ' . $newUserPassword . '
                                            <br>
                                            <br>Thank you!';

                            if (!$mail->send()) {
                                if ($user && !$user->delete()) {
                                    $messages = $user->getMessages();

                                    foreach ($messages as $message) {
                                        $result['errors'][] = $message;
                                    }
                                }

                                $result['errors'][] = 'Invitation message was not sent.';
                                $result['errors'][] = 'Mailer error: ' . $mail->ErrorInfo;
                            }
                        }

                        $teamMember = new TeamUsers([
                            'team_id' => $teamId,
                            'user_id' => $user->id,
                            'role' => TeamUsers::ROLE_USER
                        ]);

                        if (!$teamMember->save()) {
                            $result['errors'][] = 'Failed to invite user to the team';

                            foreach ($teamMember->getMessages() as $message) {
                                $result['errors'][] = $message->getMessage();
                            }
                        }
                    }
                } else {
                    $result['errors'][] = 'Can not find specified team';
                }
            }

            if (!empty($result['errors'])) {
                $result['message'] = 'Unable to invite user to the team';
                $result['status'] = 'error';
                $result['redirect'] = false;
            } else {
                $result['status'] = 'ok';
                $result['message'] = 'The member is invited to the team';
                $result['redirect'] = '/teams/';
                $result['errors'] = [];
            }

            $this->response->setJsonContent($result);

            return false;
        }

        return $this->response->redirect('/teams');
    }

    public function deleteAction()
    {
        if ($this->request->isPost()) {
            $result = [];

            $teamId = (int)$this->request->getPost('team_id', 'int');

            if ($team = Teams::findFirst($teamId)) {
                if (!$team->delete()) {
                    $result['errors'][] = 'Failed to delete team';

                    foreach ($team->getMessages() as $message) {
                        $result['errors'][] = $message->getMessage();
                    }
                }
            } else {
                $result['errors'][] = 'The is no such team';
            }

            if (!empty($result['errors'])) {
                $result['message'] = 'Unable to delete team';
                $result['status'] = 'error';
                $result['redirect'] = false;
            } else {
                $result['status'] = 'ok';
                $result['message'] = 'The team is deleted';
                $result['redirect'] = '/teams/';
                $result['errors'] = [];
            }

            $this->response->setJsonContent($result);

            return false;
        }

        return $this->response->redirect('/teams');
    }
}