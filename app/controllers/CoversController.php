<?php

use Phalcon\Http\Response;

class CoversController extends ControllerBase
{

    public function indexAction()
    {
        if ($this->request->hasFiles()) {
            return $this->handleUpload();
        }

        // check ownership
        $editionId = $this->request->get('editionId');

        if (!Editions::findFirst($editionId)->userHaveAccess($this->currentUser->id)) {
            throw new Exception();
        }

        $res = [];
        $query = Covers::query();
        $query->andWhere('edition_id = :editionId:', [ 'editionId' => $editionId])
            ->andWhere('status = :status:', ['status' => Covers::STATUS_NORMAL])
            ->orderBy('position');
        $covers = Covers::find($query->getParams());

        foreach ($covers as $key => $cover) {
            $obj = $cover->toArray();
            $obj['objects'] = [];

            /* @var Objects[] $sceneObjects */
            $sceneObjects = $cover->getObjects();

            foreach ($sceneObjects as $sceneObject) {
                if ($sceneObject->deleted) {
                    $sceneObject->delete();
                    continue;
                }

                $object = $sceneObject->toPlainVersion();

                $obj['objects'][] = $object;
            }

            $res[$key] = $obj;
        }

        $response = new Response();
        $response->setJsonContent($res);

        return $response;
    }

    public function rateAction()
    {
        $response = new Response();
        $res = new stdClass();
        $auth = $this->session->get('auth');
        $user_id = $auth['id'];

        $cover = Covers::findFirst((int)$this->request->get('id'));
        if ($cover) {
            $cover->rating = $this->getArQuality(APP_PATH . '/public/' . Covers::directoryPath($user_id) . $cover->filename);
            $cover->save();

            $res->cover = $cover;
        }

        $response->setJsonContent($res);
        return $response;
    }

    public function deleteAction()
    {
        $response = new Response();
        $cover = Covers::findFirst((int)$this->request->get('id'));

        if ($cover) {
            // check ownership
            $auth = $this->session->get('auth');
            if (!$cover->userHaveAccess($auth['id'])) {
                return $response;
            }


            $cover_position = $cover->position;
            $objects = $cover->getObjects();
            /** @var Objects[] $objects */
            foreach ($objects as $object)
                $object->delete();

            if ($cover->delete()) {
                $this->modelsManager->executeQuery(
                    'UPDATE Covers SET position=position-1 WHERE position>=:oldPos:',
                    ['oldPos' => $cover_position]
                );
                $response->setJsonContent(1);
            } else {
                $response->setJsonContent($cover->getMessages());
            }
        }

        return $response;
    }

    public function repositionAction()
    {
        $response = new Response();

        $id = (int)$this->request->getPost('id');
        $newPos = $this->request->getPost('to');
        $cover = Covers::findFirst($id);

        if ($cover && $newPos > 0) {
            // check ownership
            $auth = $this->session->get('auth');
            if (!$cover->userHaveAccess($auth['id'])) {
                return $response;
            }

            $oldPos = $cover->position;

            if ($oldPos < $newPos) {
                $phql = 'UPDATE Covers SET position=position-1 WHERE edition_id=:edition_id: AND position>:oldPos: AND position<=:newPos:';
            } else if ($oldPos > $newPos) {
                $phql = 'UPDATE Covers SET position=position+1 WHERE edition_id=:edition_id: AND position>=:newPos: AND position<:oldPos:';
            } else
                return $response;

            $this->modelsManager->executeQuery(
                $phql,
                [
                    'edition_id' => $cover->edition_id,
                    'oldPos' => $oldPos,
                    'newPos' => $newPos,
                ]
            );

            $cover->position = $newPos;
            $cover->save();
        }

        return $response;
    }

    public function saveAction()
    {
        if ($this->request->isPost()) {
            $result = new stdClass();
            $errors = [];
            $newObjects = [];
//            $log = [];

            $editionId = (int)$this->request->getPost('editionId');
            $edition = Editions::findFirst($editionId);

            if (!$edition->userHaveAccess($this->currentUser->id)) {
                $result->errors = [ 'You have no access to manage the project' ];
                $this->response->setJsonContent($result);
                return false;
            }

            $covers = $this->request->getPost('covers');
            $covers = json_decode($covers, true);

            $needToUpdateDefaultBundle = false;
            $initialCoversCount = $edition->getCoversCount();

            foreach ($covers as $coverItem) {
                $coverObjectsAreChanged = false;

                if (isset($coverItem['objects'])) {
                    $saveData = $coverItem['objects'];
                } else {
                    $saveData = [];
                }

                $cover = Covers::findFirst($coverItem['id']);
                $cover->position = $coverItem['position'];

                if ($cover->main != $coverItem['main']) {
                    $needToUpdateDefaultBundle = true;
                }
                $cover->main = (int)$coverItem['main'];

                if ($coverItem['status'] == Covers::STATUS_TEMP) {
                    $cover->status = Covers::STATUS_NORMAL;
                } else {
                    $cover->status = $coverItem['status'];
                }

                if (!$cover->save()) {
                    $errors[] = 'Failed to save Cover.';
                    $messages = $cover->getMessages();
                    foreach ($messages as $message) {
                        $errors[] = $message->getMessage();
                    }
                }

                $savedIds = [];

                foreach ($saveData as $saveObject) {
                    if ($saveObject['id'] && !preg_match('/^new/', $saveObject['id'])) {
//                        $log[] = 'Search by id ' . $saveObject['id'];
                        $object = Objects::findFirst((int)$saveObject['id']);
                    } else {
                        // if it is not in db and already deleted, skip for now?
                        if ($saveObject['deleted']) {
                            continue;
                        }

//                        $log[] = 'Create new object';
                        $object = new Objects();
                        $object->object_id = $saveObject['object_id'];
                        $object->cover_id = $coverItem['id'];

                        $coverObjectsAreChanged = true;
                    }

                    foreach ( ['x', 'y', 'z', 'rx', 'ry', 'rz', 'scale', 'deleted'] as $field ) {
                        // our database precision is 4
                        if ( round($object->{$field}, 4) != round($saveObject[$field],4) ) {
                            // echo "Parameter [$field] changed from '{$object->{$field}}' to '{$saveObject[$field]}'\n";
                            $coverObjectsAreChanged = true;
                            break;
                        }
                    }

                    $object->title = '';

                    if (isset($saveObject['titleChanged']) && isset($saveObject['title'])) {
                        $object->title = (string)$saveObject['title'];
                    }

                    $object->x = $saveObject['x'];
                    $object->y = $saveObject['y'];
                    $object->z = $saveObject['z'];

                    $object->rx = $saveObject['rx'];
                    $object->ry = $saveObject['ry'];
                    $object->rz = $saveObject['rz'];

                    $object->scale = $saveObject['scale'];

                    $prev_options = $object->options;
                    $object->options = isset($saveObject['options']) ? $saveObject['options'] : new stdClass();
                    $object->deleted = isset($saveObject['deleted']) ? $saveObject['deleted'] : 0;

                    if ($prev_options && $prev_options instanceof stdClass) {
                        $prev_options_json = json_encode($prev_options);
                        $new_options_json = json_encode($object->options);
                        if ($prev_options_json != $new_options_json) {
                            $coverObjectsAreChanged = true;
                        }
                    }

                    // check if we need to restore library object
                    if (!$saveObject['deleted']) {
                        $libraryObject = Library::findFirst($object->object_id);
                        if ($libraryObject->deleted) {
//                            $log[] = 'Restore library object [' . $libraryObject->id . ']';
                            $libraryObject->unDelete();
                        }
                    }

//                    $log[] = 'Save [' . $object->id . ']';

                    if (!$object->save()) {
                        $recordId = $object->id ? $object->id : $saveObject['id'];
                        $errors[] = $recordId . ': Failed to save db record.';

                        foreach ($object->getMessages() as $message) {
                            $errors[] = $recordId . ':' . $message->getMessage();
                        }
                    } else {
                        if (!$saveObject['id'] || preg_match('/^new/', $saveObject['id'])) {
                            $newObjects[] = ['coverId' => (int) $object->cover_id, 'tmpId'  => $saveObject['id'], 'newId' => (int) $object->id];
                        }

                        $savedIds[] = $object->id;
                    }
                }

                if ($cover->main && $coverObjectsAreChanged) {
                    $needToUpdateDefaultBundle = true;
                }
            }

            if (!$needToUpdateDefaultBundle) {
                $currentCoversCount = $edition->getCoversCount();

                $mainNotAloneAnymore = $initialCoversCount == 1 && $currentCoversCount > 1;
                $mainBecameAlone = $initialCoversCount > 1 && $currentCoversCount == 1;

                $needToUpdateDefaultBundle = $mainNotAloneAnymore || $mainBecameAlone;
            }

//            $result->needToUpdateDefaultBundle = $needToUpdateDefaultBundle;
            if ($needToUpdateDefaultBundle) {
                $result->bundle = BundleDefault::generate($edition->project_id);
            }

            // mark for delete all not in save data
//			$cover = Covers::findFirst($cover_id);
//			$objects = $cover->getObjects(); /** @var Objects[] $objects */
//			$log[] = $savedIds;
//			foreach ($objects as $object) {
//				if (!in_array($object->id, $savedIds)) {
//					$log[] = "Delete [" .$object->id. "]";
//					$object->softDelete();
//				}
//			}

            $result->newObjects = $newObjects;
            $result->errors = $errors;
//            $result->log = $log;
            $this->response->setJsonContent($result);
        }

        return false;
    }

    /**
     * @throws Exception
     * @param $filename
     * @return bool|string
     */
    private function getArQuality($filename)
    {
        if ($this->config->artools->quality == 'fake') {
            return rand(1, 3);
        }

        $tmp_name = sys_get_temp_dir() . '/a3d_tmp_' . time() . '.jpg';

//		 segmentation check
        $call = $this->config->artools->segmentation . ' ' . escapeshellarg($filename) . ' ' . escapeshellarg($tmp_name) . ' 2>&1';
        system($call);

        if (!file_exists($tmp_name)) {
            throw new Exception('Segmentation failed');
        }

        // quality check
        $qualityCheckCommand = $this->config->artools->quality . ' ' . escapeshellarg($tmp_name) . ' 2>&1';
        $stars = system($qualityCheckCommand);
        unlink($tmp_name);

        return $stars;
    }

    private function handleUpload()
    {
        $needToUpdateDefaultBundle = false;
        $userId = $this->currentUser->id;

        $editionId = (int) $this->request->get('edition_id');
        $coverId = (int) $this->request->get('id');

        $cover = false;
        $result = new stdClass();
        $result->covers = [];
        $errors = [];
        $dirLocal = '/public/' . Covers::DIRECTORY_PATH;
        $dirFS = APP_PATH . $dirLocal;
        $path = APP_PATH;
        $dirs = explode('/', $dirLocal);

        foreach ($dirs as $dir) {
            if ($dir) {
                $path .= '/' . $dir;

                if (!file_exists($path)) {
                    mkdir($path, 0775);
                }
            }
        }

        if (!$editionId) {
            $this->response->setJsonContent(['errors' => ['Edition id is required']]);
            return false;
        }

        $edition = Editions::findFirst($editionId);

        if (!$edition->userHaveAccess($userId)) {
            $this->response->setJsonContent(['errors' => ['Edition not found']]);
            return false;
        }

        if ($coverId) {
            $cover = Covers::findFirst($coverId);
            if (!$cover) {
                // $this->response->setStatusCode(404);
                $this->response->setJsonContent(['errors' => ['Cover not found']]);
                return false;
            }
        }

        $files = $this->request->getUploadedFiles();

        foreach ($files as $file) {
            $fileName = $file->getName();
            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            $error = $file->getError();

            if ($error) {
                $errors[] = $fileName . ': ' . self::uploadErrorText($error);
                continue;
            }

            if (file_exists($file->getTempName())) {
                $localFullName = $dirFS . $fileName;

                $newFile = (!$coverId)? true : $cover->filename !== $fileName;

                if ($newFile) {
                    $newFileName = $this->make_unique_filename($localFullName);

                    if ($newFileName != $fileName) {
                        $fileName = $newFileName;
                        $localFullName = $dirFS . $fileName;
                    }
                }

                $moved = move_uploaded_file($file->getTempName(), $localFullName);

                if ($moved) {
                    if (!$coverId) {
                        $cover = new Covers();
                        $cover->edition_id = $editionId;
                        $cover->title = $fileName;
                        $cover->rating = 0;
                        $cover->status = Covers::STATUS_TEMP;
                        $cover->main = 0;
                        $cover->position = Covers::maximum([
                                'edition_id = "' . $editionId . '"',
                                'column' => 'position'
                            ]) + 1;
                    } else {
                        if ($newFile && file_exists($dirFS . $cover->filename)) {
                            unlink($dirFS . $cover->filename);
                        }
                    }

                    $cover->filename = $fileName;
                    $cover->rating = $this->getArQuality($dirFS . $fileName);

                    if ($cover->save()) {
                        $result->covers[] = $cover;

                        if ($coverId) {
                            $needToUpdateDefaultBundle = $cover->main == 1;
                        }
                    } else {
                        $errors[] = $fileName . ': Failed to save db record.';

                        foreach ($cover->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                    }
                } else {
                    $errors[] = $fileName . ': Failed to move uploaded file.';
                }
            } else {
                $errors[] = $fileName . ': Uploaded file does not exists.';
            }
        }

        if ($needToUpdateDefaultBundle) {
            $result->bundle = BundleDefault::generate($edition->project_id);
        }

        $result->errors = $errors;
        $response = new Response();
        $response->setJsonContent($result);

        return $response;
    }

    public function make_unique_filename($full_path)
    {
        $file_name = basename($full_path);
        $directory = dirname($full_path) . DIRECTORY_SEPARATOR;

        $i = 2;
        while (file_exists($directory . $file_name)) {
            $parts = explode('.', $file_name);
            // Remove any numbers in brackets in the file name
            $parts[0] = preg_replace('/\(([0-9]*)\)$/', '', $parts[0]);
            $parts[0] .= '(' . $i . ')';

            $new_file_name = implode('.', $parts);
            if (!file_exists($new_file_name)) {
                $file_name = $new_file_name;
            }
            $i++;
        }
        return $file_name;
    }


    /**
     * @param integer $errorNo
     * @return string
     */
    private static function uploadErrorText($errorNo)
    {
        $phpFileUploadErrors = [
            0 => 'There is no error, the file uploaded with success',
            1 => 'The uploaded file exceeds allowed file size. Maximum - ' . ini_get('upload_max_filesize') . 'b',
            2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
            3 => 'The uploaded file was only partially uploaded',
            4 => 'No file was uploaded',
            6 => 'Missing a temporary folder',
            7 => 'Failed to write file to disk.',
            8 => 'A PHP extension stopped the file upload.',
        ];
        return $phpFileUploadErrors[$errorNo];
    }

}

