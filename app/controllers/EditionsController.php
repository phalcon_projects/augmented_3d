<?php

use Phalcon\Http\Response;
use Phalcon\Paginator\Adapter\Model as Paginator;

class EditionsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Editions');
        $this->view->setVar('section_title', 'editions');

        parent::initialize();
    }

    public function indexAction()
    {
        $projectId = (int)$this->dispatcher->getParam('project_id');
        $project = Projects::findFirst($projectId);

        if (!$project || !$project->userHaveAccess($this->currentUser->id)) {
            $this->dispatcher->forward([
                'controller' => 'errors',
                'action' => 'show404'
            ]);

            return false;
        } else {
            $this->view->setVar('project', $project);
            $this->view->setVar('section_title', $project->title);
            $this->tag->setTitle($project->title . ' - Editions');

            $items = $project->getEditions(['order' => 'title']);
        }

        $this->assets->addCss('vendor/css/jquery.fileupload.css');

        $this->assets->addJs('vendor/jquery.ui.widget.js');
        $this->assets->addJs('vendor/jquery.iframe-transport.js');
        $this->assets->addJs('vendor/jquery.fileupload.js');
        $this->assets->addJs('js/page.editions.js');

        $numberPage = 1;
        $pageSize = 2;

        if ($this->request->has('page'))
            $numberPage = $this->request->getQuery('page', 'int');

        if ($this->request->has('limit'))
            $pageSize = $this->request->getQuery('limit', 'int');

        $numberPage = max((int)$numberPage, 1);
        $pageSize = max((int)$pageSize, 1);

        $paginator = new Paginator([
            'data' => $items,
            'limit' => min($pageSize, 1000),
            'page' => $numberPage
        ]);

        $this->assets->addCss('css/list.css');
        $this->assets->addJs('js/page.editions.js');
        $this->view->page = $paginator->getPaginate();

        $bundlesPath = 'files/bundles';
        $bundles = [];
        foreach ($this->view->page->items as $item) {
            $bundleFileName = $bundlesPath . '/bundle_' . $item->id . '.zip';
            if (file_exists(APP_PATH . '/public/' . $bundleFileName)) {
                $bundles[$item->id] = $this->config->application->siteUri . '/' . $bundleFileName;
            }
        }

        $this->view->bundles = $bundles;
    }

    public function viewAction()
    {
        $project = Projects::findFirst((int) $this->dispatcher->getParam('project_id'));
        $edition = Editions::findFirst((int) $this->dispatcher->getParam('edition_id'));

        if (!$edition) {
            $this->show404();
            return;
        }

        if (!$edition->userHaveAccess($this->currentUser->id)) {
            $this->show404();
            return;
        }

        $this->assets->addCss('vendor/css/cropper.min.css');
        $this->assets->addCss('vendor/css/dragula.css');
        $this->assets->addCss('https://cdn.plyr.io/2.0.13/plyr.css', false);
        $this->assets->addCss('vendor/css/jquery.fileupload.css');
        $this->assets->addCss('css/scene.css');

        $this->assets->addJs('vendor/moment.js/moment.min.js');
        $this->assets->addJs('vendor/Sortable.min.js');
        $this->assets->addJs('vendor/bluebird.min.js');
        $this->assets->addJs('vendor/dragula.min.js');
        $this->assets->addJs('vendor/inflate.min.js'); // for THREE.FBXLoader
        $this->assets->addJs('vendor/rasterizeHTML.allinone.js');
        $this->assets->addJs('vendor/Tween.js');

        /*
         * Modified files:
         * Three.DragControls.js
         * Three.OBJLoader.js - need to check if it updated version or OBJLoader2 *(it is NOT modified by me)
         * Three.OrbitControls.js
         * Three.TransformControls.js
         * Three.viewcube.js - can be loaded from github (my version)
        */

        $this->assets->addJs('vendor/threejs/three_r90dev.js');
        // <!-- for fallback render -->
        $this->assets->addJs('vendor/threejs/SoftwareRenderer.js');
        $this->assets->addJs('vendor/threejs/CanvasRenderer.js');
        $this->assets->addJs('vendor/threejs/Projector.js');
        // <!-- for fallback render -->
        $this->assets->addJs('vendor/threejs/OrbitControls.js');
        $this->assets->addJs('vendor/threejs/TransformControls.js');
        $this->assets->addJs('vendor/threejs/ScaleControl2.js');
        $this->assets->addJs('vendor/threejs/FBXLoader.js');
        $this->assets->addJs('vendor/threejs/OBJLoader.js');
        $this->assets->addJs('vendor/threejs/MTLLoader.js');
        $this->assets->addJs('vendor/threejs/DDSLoader.js');
        $this->assets->addJs('vendor/threejs/STLLoader.js');
        $this->assets->addJs('vendor/threejs/CombinedCamera.js');
        $this->assets->addJs('vendor/threejs/viewcube.js');
        $this->assets->addJs('vendor/threejs/DragControls.js');
        $this->assets->addJs('vendor/threejs/NURBSCurve.js');
        $this->assets->addJs('vendor/threejs/NURBSUtils.js');
        // $this->assets->addJs('vendor/threejs/stats.min.js');

        $this->assets->addJs('vendor/pdf.js');
        $this->assets->addJs('vendor/mousetrap.min.js');
        $this->assets->addJs('vendor/cropper.min.js');
        $this->assets->addJs('vendor/jquery.drags.js');
        $this->assets->addJs('https://cdn.plyr.io/2.0.13/plyr.js', false);
        $this->assets->addJs('vendor/pdfmake/pdfmake.min.js');
        $this->assets->addJs('vendor/pdfmake/vfs_fonts.js');


        $this->assets->addJs('vendor/jquery.ui.widget.js');
        $this->assets->addJs('vendor/jquery.iframe-transport.js');
        $this->assets->addJs('vendor/jquery.fileupload.js');

        $this->assets->addJs('js/app.js');
        $this->assets->addJs('js/app.loadingControl.js');
        $this->assets->addJs('js/app.coversList.js');
        $this->assets->addJs('js/app.mediaLibrary.js');
        $this->assets->addJs('js/app.objectsList.js');
        $this->assets->addJs('js/app.toolsPanel.js');

        $this->assets->addJs('js/index.js');

        $hasYouTubeDownloadAccess = $this->currentUser->role == Users::ROLE_MEGA_ADMIN
            || SettingsYouTubeDownloadAccess::findFirst('user_id = ' . $this->currentUser->id);

        $config = [
            'edition' => [
                'id' => $edition->id,
                'title' => $edition->title,
            ],
            'paths' => [
                'objects' => Library::DIRECTORY_PATH,
                'thumbs' => Library::DIRECTORY_PATH_THUMBS,
                'covers' => Covers::DIRECTORY_PATH
            ],
            'hasYouTubeDownloadAccess' => $hasYouTubeDownloadAccess !== false
        ];

        $this->assets->addInlineJs('$(function() {
		    var config = ' . json_encode($config) . ';
		    app.run(' . $project->id . ', ' . $edition->id . ', config);
	    });');

        $googleOauth = Oauth::findFirst([
            'user_id = :user_id: AND provider = "google"',
            'bind' => [
                'user_id' => $this->currentUser->id
            ]
        ]);

        $this->view->setTemplateAfter('3d');

        $this->view->setVar('project', $project);
        $this->view->setVar('edition', $edition);
        $this->view->setVar('google_at', $googleOauth ? $googleOauth->access_token : null);

        $this->tag->setTitle($project->title . ' - ' . $edition->title);
    }

    public function saveAction()
    {
        $id = (int)$this->request->get('id');
        $projectId = (int)$this->request->get('projectId');
        $title = $this->request->get('title');
        $state = $this->request->get('state');
        $errors = [];

        if (null === $state && !$title) {
            $errors[] = 'Title is required.';
        }

        if (!$id && !$projectId) {
            $errors[] = 'Project id is required.';
        }

        if ($id) {
            $edition = Editions::findFirst($id);
        } else {
            $edition = new Editions();
            $edition->project_id = $projectId;
        }

        if (!$edition->userHaveAccess($this->currentUser->id)) {
            $errors[] = 'You have no access to manage the project';
        }

        if ($errors) {
            $this->response->setJsonContent($errors);
            return false;
        }

        if ($title) {
            $edition->title = $title;
        }

        if (null !== $state) {
            $edition->state = $state;
        }

        if ($edition->save()) {
            $bundleResult = Bundle::generate($edition->id);

            $this->response->setJsonContent(
                (!isset($bundleResult['errors'])
                    ? ['success' => true, 'redirect' => 'active-page']
                    : $bundleResult)
            );
        } else {
            $errors = [];
            foreach ($edition->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            $this->response->setJsonContent($errors);
        }
        return false;
    }

    public function saveCustomFieldsAction()
    {
        if ($this->request->isPost()) {
            $result = [];

            $object = Objects::findFirst((int) $this->request->get('object_id'));

            if (!$object) {
                $result['status'] = 'error';
                $result['errors'] = [ 'Object not found.' ];
                $this->response->setJsonContent($result);
                return false;
            }

            if (!$object->userHaveAccess($this->currentUser->id)) {
                $result['status'] = 'error';
                $result['errors'] = [ 'Object not found.' ];
                $this->response->setJsonContent($result);
                return false;
            }

            $customFieldsData = json_decode($this->request->get('custom_fields'));

            if (json_last_error() !== JSON_ERROR_NONE) {
                $result['errors'] = 'Not valid JSON data is specified.';
            } else {
                if ($object) {
                    $object->options->custom_fields = $customFieldsData;

                    if (!$object->save()) {
                        $result['errors'] = 'Error object saving';
                    }
                } else {
                    $result['errors'] = 'Error saving custom fields';
                }
            }

            $result['status'] = !isset($result['errors']) ? 'ok' : 'error';

            return $this->response->setJsonContent($result);
        }

        $this->show404();
    }

    public function deleteAction()
    {
        $result = [];
        $edition = Editions::findFirst((int)$this->request->get('id'));

        if (!$edition) {
            $result['status'] = 'error';
            $result['errors'] = [ 'Edition not found.' ];
            $this->response->setJsonContent($result);
            return false;
        }

        if (!$edition->userHaveAccess($this->currentUser->id)) {
            $result['status'] = 'error';
            $result['errors'] = [ 'You have no access to manage the project' ];
            $this->response->setJsonContent($result);
            return false;
        }

        $covers = Covers::findFirst([
            'edition_id = :edition_id: AND status=:status:',
            'bind' => [
                'edition_id' => $edition->id,
                'status' => Covers::STATUS_NORMAL
            ]
        ]);

        if ($edition && $edition->delete()) {
            // delete edition bundle
            Base::unlinkDirRecursive(APP_PATH . "/public/files/bundles/{$edition->id}/");

            $anyProjectEdition = Editions::findFirst([
                'project_id = :projectId:',
                'bind' => [
                    'projectId' => $edition->project_id
                ]
            ]);

            // if no editions left - update default bundle
            if (!$anyProjectEdition) {
                BundleDefault::generate($edition->project_id);
//                $result['delete default bundle'] = "TRUE";
//
//                $defaultBundle = BundleDefault::findFirst([
//                    'project_id = :1:',
//                    'bind' => [1 => $item->project_id]
//                ]);
//
//                if ($defaultBundle) {
//                    $defaultBundlePath = APP_PATH . "/public/files/bundles/{$defaultBundle->hash}.zip";
//
//                    if (!$defaultBundle->delete()) {
//                        $result['errors'][] = 'Default bundle record deletion error';
//                    } else if (file_exists($defaultBundlePath)) {
//                        if (!unlink($defaultBundlePath)) {
//                            $result['errors'][] = 'Default bundle file deletion error';
//                        } else {
//                            $result['success'] = true;
//                            $result['redirect'] = 'active-page';
//                        }
//                    }
//                }
            } else {
                // if deleted edition had covers - we need to regenerate default bundle
                if ($covers) {
                    BundleDefault::generate($edition->project_id);
                }
            }

            $result['success'] = true;
            $result['redirect'] = 'active-page';

            $this->response->setJsonContent($result);
        } else {
            $this->response->setJsonContent($edition ? $edition->getMessages() : 'Delete error.');
        }

        return false;
    }

    public function pdfTemplateAction()
    {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
    }

    public function pdfAction()
    {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);

        if ($this->request->get('editionId') && $this->request->getPost('images')) {
            $debug = true;
            $edition_id = $this->request->get('editionId');
            $edition = Editions::findFirst($edition_id);

            if (!$edition) {
                $result = [];
                $result['status'] = 'error';
                $result['errors'] = [ 'Edition not found.' ];
                $this->response->setJsonContent($result);
                return false;
            }

            if (!$edition->userHaveAccess($this->currentUser->id)) {
                $result = [];
                $result['status'] = 'error';
                $result['errors'] = [ 'You have no access to manage the project' ];
                $this->response->setJsonContent($result);
                return false;
            }

            $filebasename = 'edition_' . $edition_id . '.pdf';
            //$filename = 'files/' .$this->currentUser->id. '/pdf/' .$filebasename;

            $this->view->disable();
            $images = $this->request->getPost('images');
            $html = '<html>
                        <head>
                            <title>' . $edition->title . '</title>
                            <style>
                            </style>
                        </head>
                        <body>';

            $webDir = '/files/pdf/';
            $fsDir = APP_PATH . '/public' . $webDir;
            $subDir = 'pdf' . time() . '/';

            if (!file_exists($fsDir)) {
                mkdir($fsDir, 0777);
            }

            $fsDir .= $subDir;
            mkdir($fsDir, 0777);
            chmod($fsDir, 0777);
            $webDir .= $subDir;

            for ($i = 0; $i < count($images); $i++) {
                $html .= $images[$i];
            }

            $html .= '</body></html>';

            $html_filename = $fsDir . 'index.html';
            $pdf_filename = $fsDir . $filebasename;

            file_put_contents($html_filename, $html);
            chmod($html_filename, 0777);

            ob_start();
            $command = $this->config->os->wkhtmltopdfFile . ' --page-size A4 --orientation Landscape --print-media-type --margin-bottom 5mm --margin-left 5mm --margin-right 5mm --margin-top 5mm ' . escapeshellarg($html_filename) . ' ' . escapeshellarg($pdf_filename);
            system($command, $code);
            $ret = ob_get_clean();


            $res = [
                'fileName' => $filebasename,
                'fileLink' => $webDir . $filebasename,
                'ret' => $ret,
            ];
            if ($debug) {
                $res['debug'] = $debug;
                $res['command'] = $command;
                $res['code'] = $code;
            }

            $this->response->setJsonContent($res);

        } else {
            $this->response->setJsonContent(['error' => 'input data error.']);
        }

        return false;
    }

    function saveEncodedImage(&$file_data, $filename)
    {
        $type = '';
        if (substr($file_data, 0, 22) == 'data:image/jpeg;base64') {
            $file_data = base64_decode(substr($file_data, 22));
            $type = 'jpg';
        } elseif (substr($file_data, 0, 21) == 'data:image/png;base64') {
            $file_data = base64_decode(substr($file_data, 21));
            $type = 'png';
        }

        $im = imagecreatefromstring($file_data); // check if this an image
        if ($im !== false) {
            // save new
            $filename .= '.' . $type;

            file_put_contents($filename, $file_data);

            return $filename;
        } else {
            throw new Exception('failed to parse image data');
        }
    }
}
