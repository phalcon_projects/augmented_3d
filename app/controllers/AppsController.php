<?php

use Phalcon\Http\Response;
use Phalcon\Paginator\Adapter\Model as Paginator;

class AppsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('App');
        $this->view->setVar('section_title', 'apps');

        parent::initialize();
    }

    public function indexAction()
    {
        $this->assets->addCss("css/apps.css");

        $auth = $this->session->get('auth');
        $user_id = isset($auth['id']) ? $auth['id'] : null;

        if (!$user_id || !Users::findFirst($user_id)) {
            return $this->dispatcher->forward([
                'controller' => 'errors',
                'action' => 'show404'
            ]);
        }

        $pageNumber = 1;
        $pageSize = 8;
        $searchKeyQuery = null;

        if ($this->request->isPost()) {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, App::class, $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $query = Phalcon\Mvc\Model\Criteria::fromInput($this->di, App::class, $this->request->get());

            if ($this->request->has('page')) {
                $pageNumber = $this->request->getQuery('page', 'int');
            }

            if ($this->request->has('limit')) {
                $pageSize = $this->request->getQuery('limit', 'int');
            }

            if ($this->request->has('search')) {
                $searchKeyQuery = $this->request->getQuery('search', 'string');
            }
        }

        $query->orderBy('name');
        $query->andWhere('user_id = "' . (int)$user_id . '"');

        if ($searchKeyQuery) {
            $searchKeyResult = AppPlatformKey::find([
                'condition' => 'key like "%:1:%"',
                'bind' => [
                    1 => $searchKeyQuery
                ]
            ]);

            if ($searchKeyResult) {
                $appIds = [];

                foreach ($searchKeyResult as $key) {
                    $appIds[] = $key->app_id;
                }

                $query->andWhereIn('id', $appIds);
            }
        }

        $parameters = $query->getParams();

        $items = App::find($parameters);

        if ($this->request->has('search') && count($items) == 0) {
            $this->flash->notice('The search did not find any');
        }

        $pageNumber = max((int)$pageNumber, 1);
        $pageSize = max((int)$pageSize, 1);

        $paginator = new Paginator(array(
            'data' => $items,
            'limit' => min($pageSize, 1000),
            'page' => $pageNumber,
        ));

        $this->view->page = $paginator->getPaginate();
        $this->view->pageLimit = $pageSize;
        $this->view->searchFilter = $searchKeyQuery;

        $this->assets->addJs('js/page.apps.js');
    }

    public function addAction()
    {
        if (!$this->currentUser->id) {
            return $this->dispatcher->forward([
                'controller' => 'errors',
                'action' => 'show404'
            ]);
        }

        $form = new AppForm($this->currentUser, ['edit' => true]);
        $this->assets->addJs('js/page.apps.js');
        $this->view->setVar('form', $form);
        $this->view->setVar('section_title', 'app add');
    }

    public function saveAction()
    {
        return $this->_saveAction();
    }

    protected function _saveAction()
    {
        $result = [
            'status' => 'ok',
            'errors' => []
        ];

        if ($this->request->isPost()) {
            $action = 'add';

            if (!preg_match('/^[A-Za-z0-9._-]*$/', $this->request->getPost('name'))) {
                $this->response->setJsonContent([
                    'status' => 'false',
                    'errors' => ['The name is contain inappropriate characters.']
                ]);
                return false;
            }

            if ($action === 'add') {
                $app = new App([
                    'user_id' => $this->currentUser->id,
                    'name' => $this->request->getPost('name'),
                    'version' => '1.0'
                ]);
            } else {
                $appId = 0;
                $app = App::findFirst($appId);
            }

            if (!$app) {
                $result['errors'][] = 'Application saving error.';
            } else {
                $form = new AppForm;
                $data = [
                    'name' => $this->request->getPost('name'),
                    'version' => $this->request->getPost('version')
                ];

                if (!$form->isValid($data, $app)) {
                    $result['errors'][] = 'Validation errors.';

                    foreach ($form->getMessages() as $message) {
                        $result['errors'][] = $message->getMessage();
                    }
                }

                if (!isset($result['errors']) || count($result['errors']) == 0) {
                    if (!$app->save()) {
                        $result['errors'][] = 'Failed to save app.';

                        foreach ($app->getMessages() as $message) {
                            $result['errors'][] = $message->getMessage();
                        }
                    } else if ($action === 'add') {
                        $resultPlatformKeys = $this->saveNewPlatformKeys([
                            'app_id' => $app->id,
                            'web_platform_id' => $this->request->getPost('web_platform_id'),
                            'android_platform_id' => $this->request->getPost('android_platform_id'),
                            'ios_platform_id' => $this->request->getPost('ios_platform_id')
                        ]);

                        if (count($resultPlatformKeys['errors']) > 0) {
                            $result['errors'] = $resultPlatformKeys['errors'];
                            $app->delete();
                        }
                    }
                }
            }
        }

        if ($result['errors']) {
            $result['status'] = 'error';
        }

        $this->response->setJsonContent($result);

        return false;
    }

    protected function saveNewPlatformKeys($parameters)
    {
        $result = [
            'errors' => []
        ];

        if (!isset($parameters['app_id'])
            || !isset($parameters['web_platform_id'])
            || !isset($parameters['android_platform_id'])
            || !isset($parameters['ios_platform_id'])) {
            return $result['errors'][] = 'Not all parameters are specified.';
        }

        if ($parameters['web_platform_id'] !== '' && AppPlatformKey::findFirst([
            'conditions' => 'platform_id = :1:',
            'bind' => [
                1 => $parameters['web_platform_id']
            ]
        ])) {
            $result['errors'][] = 'Specified web platform id is already registered.';
        } else {
            $appWebPlatformKey = new AppPlatformKey([
                'app_id' => $parameters['app_id'],
                'platform_type' => AppPlatformKey::PLATFORM_TYPE_WEB,
                'platform_id' => $parameters['web_platform_id'],
                'key' => md5(uniqid($this->currentUser->email, true))
            ]);
            $appWebPlatformKey->save();
        }

        if ($parameters['android_platform_id'] !== '' && AppPlatformKey::findFirst([
            'conditions' => 'platform_id = :1:',
            'bind' => [
                1 => $parameters['android_platform_id']
            ]
        ])) {
            $result['errors'][] = 'Specified android platform id is already registered.';
        } else {
            $appAndroidPlatformKey = new AppPlatformKey([
                'app_id' => $parameters['app_id'],
                'platform_type' => AppPlatformKey::PLATFORM_TYPE_ANDROID,
                'platform_id' => $parameters['android_platform_id'],
                'key' => md5(uniqid($this->currentUser->email, true))
            ]);
            $appAndroidPlatformKey->save();
        }

        if ($parameters['ios_platform_id'] !== '' && AppPlatformKey::findFirst([
            'conditions' => 'platform_id = :1:',
            'bind' => [
                1 => $parameters['ios_platform_id']
            ]
        ])) {
            $result['errors'][] = 'Specified ios platform id is already registered.';
        } else {
            $appIOSPlatformKey = new AppPlatformKey([
                'app_id' => $parameters['app_id'],
                'platform_type' => AppPlatformKey::PLATFORM_TYPE_IOS,
                'platform_id' => $parameters['ios_platform_id'],
                'key' => md5(uniqid($this->currentUser->email, true))
            ]);
            $appIOSPlatformKey->save();
        }

        return $result;
    }

    public function deleteAction()
    {
        $result = [
            'status' => 'ok',
            'message' => 'The app key deleted',
            'redirect' => '/apps',
            'errors' => []
        ];

        if ($this->request->isPost()) {
            $id = (int)$this->request->getPost('id', 'int');
            $item = App::findFirst($id);

            if (!$item) {
                $result['errors'][] = 'App key not found';
            } else if (!$item->delete()) {
                $result['message'] = 'Unable to delete app key';

                foreach ($item->getMessages() as $message) {
                    $result['errors'][] = $message->getMessage();
                }
            }

            if ($result['errors']) {
                $result['status'] = 'error';
                $result['redirect'] = false;
            }

            $this->response->setJsonContent($result);

            return false;
        } else {
            $id = (int)$this->request->get('id');

            return $this->response->redirect('/apps');
        }
    }

    public function apiValidateAction()
    {
        $parameters['app_id'] = $this->request->get('app_id');
        $parameters['key'] = $this->request->get('secret_key');
        $parameters['platform_id'] = $this->request->get('platform_id');
        $parameters['platform_type'] = AppPlatformKey::getPlatformIdByType(strtolower($this->request->get('platform')));

        if ($parameters['app_id'] === null) {
            $validate['error'] = 'app_id parameter is not specified';
        } else if ($parameters['key'] === null) {
            $validate['error'] = 'secret_key parameter is not specified';
        } else if ($parameters['platform_id'] === null) {
            $validate['error'] = 'platform_id parameter is not specified';
        } else if ($parameters['platform_type'] === null) {
            $validate['error'] = 'platform parameter is not specified';
        } else {
            $validate = $this->validateApplication($parameters);
        }

        $responseContent = [];

        if (isset($validate['error'])) {
            $responseContent['error'] = $validate['error'];
        } else {
            $responseContent['valid'] = true;
        }

        $this->response->setJsonContent($responseContent);

        return false;
    }

    protected function checkAuth($userId)
    {

        return true;
    }
}