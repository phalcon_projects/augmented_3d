<?php

class AppPlatformKey extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $app_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $platform_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $platform_type;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $key;

    const PLATFORM_TYPE_WEB = 1;
    const PLATFORM_TYPE_ANDROID = 2;
    const PLATFORM_TYPE_IOS = 3;

    const PLATFORM_TYPES = [
        self::PLATFORM_TYPE_WEB => 'web',
        self::PLATFORM_TYPE_ANDROID => 'android',
        self::PLATFORM_TYPE_IOS => 'ios'
    ];

    public static function getPlatformIdByType($type) {
        foreach (self::PLATFORM_TYPES as $id => $itemType) {
            if ($type == strtolower($itemType)) {
                return $id;
            }
        }

        return null;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->belongsTo('app_id', App::class, 'id');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'app_platform_keys';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return App[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return App
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @param integer $user_id
     * @return bool
     */
    public function isOwnedBy($user_id) {
        return (int)$user_id && (int)$this->user_id && $this->user_id == $user_id;
    }
}