<?php

class Order extends \Phalcon\Mvc\Model
{
    const STATE_PENDING = 0;
    const STATE_PAID = 1;
    const STATE_COMPLETED = 2;

    const STATES = [
        self::STATE_PENDING => [
            'id' => self::STATE_PENDING,
            'title' => 'Pending'
        ],
        self::STATE_PAID => [
            'id' => self::STATE_PAID,
            'title' => 'Paid'
        ],
        self::STATE_COMPLETED => [
            'id' => self::STATE_COMPLETED,
            'title' => 'Completed'
        ]
    ];

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $number;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=false)
     */
    public $state;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->hasMany('id', ObjectItem::class, 'order_id', ['alias' => 'orderItems']);
        $this->hasMany('id', ObjectCustomers::class, 'order_id', ['alias' => 'orderCustomers']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'orders';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Order[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Order
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}