<?php

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class Users extends Model
{
    const ROLE_USER = 1;
    const ROLE_ADMIN = 2;
    const ROLE_MEGA_ADMIN = 3;

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    const STATE_ACTIVE = 1;
    const STATE_PENDING = 2;
    const STATE_DELETED = 3;
    /**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return 'users';
	}

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->hasOne('id', Licenses::class, 'user_id', ['alias' => 'license']);
	}

//    public function validation()
//    {
//        $validator = new Validation();
//
//        $validator->add(
//            'login',
//            new UniquenessValidator([
//            'message' => 'Sorry, The login was registered by another user'
//        	])
//		);
//
//        return $this->validate($validator);
//    }
}
