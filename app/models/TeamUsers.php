<?php

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class TeamUsers extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $team_id;
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var integer
     * @Column(type="tinyint", length=4, nullable=false)
     */
    public $role;

    const ROLE_USER = 1;
    const ROLE_ADMIN = 2;

    /**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return 'team_users';
	}

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->hasMany('id', Teams::class, 'team_id');
        $this->hasMany('id', Users::class, 'user_id');
        $this->belongsTo('team_id', Teams::class, 'id', ['alias' => 'Team']);
        $this->belongsTo('user_id', Users::class, 'id', ['alias' => 'User']);
    }

}
