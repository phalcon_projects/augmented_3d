<?php

use Phalcon\Mvc\Model;
use Phalcon\Validation;

class TeamPermissions extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $team_id;

    /**
     *
     * @var integer
     * @Column(type="tinyint", length=1, nullable=false)
     */
    public $type;

    /**
     *
     * @var integer
     * @Column(type="tinyint", length=1, nullable=false)
     */
    public $value;

    /**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return 'team_permissions';
	}

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->belongsTo('team_id', Teams::class, 'id', ['alias' => 'Permissions']);
    }

}
