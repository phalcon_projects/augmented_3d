<?php

class Bundle extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $edition_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $version;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $hash;

    /**
     *
     * @var string
     * @Column(type="timestamp", nullable=false)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="timestamp", nullable=false)
     */
    public $updated_at;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'bundles';
    }

    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->hasOne('edition_id', Editions::class, 'id', ['alias' => 'edition']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Objects[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Objects
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function generate($parentId, $default = false)
    {
        if (!$parentId) {
            return new Error(($default ? 'Project' : 'Edition') . ' id is undefined');
        }

        $result = [];

        if ($default) {
            $bundle = BundleDefault::findFirst([
                'project_id = :1:',
                'bind' => [1 => $parentId]
            ]);
        } else {
            $bundle = Bundle::findFirst([
                'edition_id = :1:',
                'bind' => [1 => $parentId]
            ]);
        }

        if (!$bundle) {
            if ($default) {
                $bundle = new BundleDefault();
                $bundle->project_id = $parentId;
            } else {
                $bundle = new Bundle();
                $bundle->edition_id = $parentId;
            }

            $bundle->hash = Base::getToken();
            $bundle->version = 1;
        } else {
            $bundle->version = $bundle->version + 1;
        }

        $bundleName = $bundle->hash;
        $bundleFileName = $bundleName . '.zip';
        $bundlesPath = 'files/bundles/';

        if ($default) {
            $bundlesDir = APP_PATH . '/public/' . $bundlesPath . 'p' . $parentId . '/';
            $bundlesDirRelative = $bundlesPath . 'p' . $parentId . '/';
        } else {
            $bundlesDir = APP_PATH . '/public/' . $bundlesPath . $parentId . '/';
            $bundlesDirRelative = $bundlesPath . $parentId . '/';
        }

        $coversDir = APP_PATH . '/public/' . Covers::DIRECTORY_PATH;
        $objectsDir = APP_PATH . '/public/' . Library::DIRECTORY_PATH;
        $defaultRotationOrder = 'ZYX';
        $coverPlaneWidth = 100;

        if (!file_exists($bundlesDir)) {
            mkdir($bundlesDir, 0755);
        }

        /*
        Типы моделей поддерживаемых SDK
            Model3D       = 1,
            Sound         = 2,
            Image         = 3,
            Video         = 4,
            Video3G2      = 7,
            Bundle   	  = 18;
            UndefinedType = -1
        */

        $sdkModelTypeByMimeType = [
            'undefined' => -1,
            'object' => 1, // 3D model
            'audio' => 2, // Sound
            'image' => 3, // Image
            'video' => 4, // Video
        ];

        $reservedIds = [];
        $editionBundleCoversCount = [];

        if ($default) {
            $projectItem = Projects::findFirst($parentId);

            if (!$projectItem) {
                return new Exception('Failed to find project');
            }

            $bundleCovers = [];
            $editions = Editions::find([
                'conditions' => 'project_id = :1:',
                'bind' => [
                    1 => $projectItem->id
                ]
            ]);

            foreach ($editions as $edition) {
                $cover = Covers::findFirst([
                    'conditions' => 'edition_id = :1: and main = :2: and status = :3:',
                    'bind' => [
                        1 => $edition->id,
                        2 => 1,
                        3 => Covers::STATUS_NORMAL
                    ]
                ]);

                if ($cover) {
                    $reservedIds[] = $edition->id;

                    $normalCoversCount = Covers::count([
                        'edition_id = :editionId: AND status = :status: AND main=:main:',
                        'bind' => [
                            'editionId' => $edition->id,
                            'main' => 0,
                            'status' => Covers::STATUS_NORMAL
                        ]
                    ]);
                    $editionBundleCoversCount[$edition->id] = $normalCoversCount;

                    $cover->edition = $edition;
                    $bundleCovers[] = $cover;
                }
            }
        } else {
            $editionItem = Editions::findFirst($parentId);

            if (!$editionItem) {
                return new Exception('Failed to find edition');
            }

            $bundleCovers = Covers::find([
                'conditions' => 'edition_id = :1: and main = :2: and status = :3:',
                'bind' => [
                    1 => $editionItem->id,
                    2 => 0,
                    3 => Covers::STATUS_NORMAL
                ]
            ]);
        }

        // config json
        $config = new stdClass();
        $config->version = $bundle->version;
        $config->bundle_id = $default ? 0 : $parentId;
        $config->markerConfiguration = [
            'marker_count' => count($bundleCovers),
            'markers_per_frame' => 1
        ];
        if ($config->markerConfiguration['marker_count'] > 0) {
            $config->markerConfiguration['pyramid_filename'] = 'pyramid.bin';
            $config->markerConfiguration['database_filename'] = 'dbow.bin';
        }

        if (count($bundleCovers)) {
            if (!file_exists($bundlesDir . 'markers')) {
                mkdir($bundlesDir . 'markers', 0755);
            }
        }

        $config->markers = [];
        $config->models = [];

        try {
            $modelsFilesCache = [];
            $bundleModelsIds = [];
            $bundleMarkerId = 0;
            $modelIdAutoincrement = -1;
            $xScale = 1;

            foreach ($bundleCovers as $index => $cover) {
                $coverFilename = $cover->filename;
                $markerFileName = 'markers/' . $bundleMarkerId . '.jpg';

                if (file_exists($bundlesDir . $markerFileName)) {
                    unlink($bundlesDir . $markerFileName);
                }

                if (!file_exists($coversDir . $coverFilename)) {
                    throw new Exception('Cover image not found: [' . $bundleMarkerId . '(' . $cover->id . ')][' . $coverFilename . ']');
                }

                $output = self::markerSegmentation(
                    $coversDir . $coverFilename,
                    $bundlesDir . $markerFileName
                );

                if (!$output) {
                    throw new Exception('Marker segmentation failed:[' . $output . ']');
                }

                $markerInfo = explode(',', $output);

                if (count($markerInfo) < 4) {
                    throw new Exception(
                        'Marker segmentation error: expected \'x,y,w,h\' got [' . $output . ']'
                    );
                }

                $markerInfo = [
                    'x' => $markerInfo[0],
                    'y' => $markerInfo[1],
                    'width' => $markerInfo[2],
                    'height' => $markerInfo[3],
                ];

                if (!file_exists($bundlesDir . $markerFileName)) {
                    throw new Exception(
                        'Marker segmentation failed. [' . $cover->id . '][' . $coverFilename . ']'
                    );
                }

                // offset "cover center" based coordinates to "marker center"
                list($coverWidth, $coverHeight) = getimagesize($coversDir . $coverFilename);

                $markerSDK = self::sdk3dImageSize($markerInfo['width'], $markerInfo['height']);
                $pixel2SDK = $markerSDK['width'] / $markerInfo['width'];

                $coverSDK = [
                    'width' => $coverWidth * $pixel2SDK,
                    'height' => $coverHeight * $pixel2SDK
                ];

                if ($markerInfo['width'] < $markerInfo['height']) {
                    $xScale = $coverWidth / $markerInfo['width'];
                } else {
                    $xScale = $coverWidth / $markerInfo['height'];
                }

                // our coordinates
                $coverRatio = $coverHeight / $coverWidth;
                $cover3d = [
                    'width' => $coverPlaneWidth,
                    'height' => $coverPlaneWidth * $coverRatio
                ];

                $unitToSdk = $coverSDK['width'] / $cover3d['width'];

                $coverCenterPx = [
                    'x' => $coverWidth / 2,
                    'y' => $coverHeight / 2
                ];
                $markerCenterPx = [
                    'x' => $markerInfo['x'] + $markerInfo['width'] / 2,
                    'y' => $markerInfo['y'] + $markerInfo['height'] / 2
                ];
                $offsetPx = [
                    'x' => $coverCenterPx['x'] - $markerCenterPx['x'],
                    'y' => $coverCenterPx['y'] - $markerCenterPx['y']
                ];
                $offsetSdk = [
                    'x' => $offsetPx['x'] * $pixel2SDK,
                    'z' => $offsetPx['y'] * $pixel2SDK
                ];

                $marker = [
                    'id' => $bundleMarkerId,
                    'marker_path' => '/' . $markerFileName,
//                    'markerInfo' => $markerInfo
                    /*'width' => $markerInfo['width'],
                    'height' => $markerInfo['height']*/
                ];
                $config->markers[] = $marker;

                $objectsDirLength = strlen($objectsDir);
                $sceneObjects = $cover->getObjects();

                if ($default) {
                    $edition = $cover->edition;

                    if ($editionBundleCoversCount[$edition->id] > 0) {
                        $editionBundle = Bundle::findFirst([
                            'edition_id = :1:',
                            'bind' => [1 => $edition->id]
                        ]);

                        $bundleURL = \Phalcon\Di::getDefault()->getConfig()->application->siteUri
                            . "/{$bundlesPath}{$edition->id}/{$editionBundle->hash}.zip";

                        $model = [
                            'id' => (int)$edition->id,
                            'marker_id' => $bundleMarkerId,
                            'data' => $bundleURL,
                            'model_type' => 18,
                            'custom_fields' => []
                        ];

                        $config->models[] = $model;
                    }
                }

                /* @var Objects[] $sceneObjects */
                foreach ($sceneObjects as $sceneObject) {
                    if ($sceneObject->deleted) {
                        continue;
                    }

                    $externalData = [];

                    if (isset($bundleModelsIds[$sceneObject->object_id])) {
                        $bundleModelId = $bundleModelsIds[$sceneObject->object_id];
                    } else {
                        $modelIdAutoincrement++;

                        while (in_array($modelIdAutoincrement, $reservedIds)) {
                            $modelIdAutoincrement++;
                        }

                        $bundleModelId = $modelIdAutoincrement;

                        $bundleModelsIds[$sceneObject->object_id] = $bundleModelId;
                    }

                    if (isset($modelsFilesCache[$bundleModelId])) {
                        $modelFilename = $modelsFilesCache[$bundleModelId][0]['zipPath'];

                        for ($i = 1; $i < count($modelsFilesCache[$bundleModelId]); $i++) {
                            $externalData[] = $modelsFilesCache[$bundleModelId][$i]['zipPath'];
                        }
                    } else {
                        $modelsFilesCache[$bundleModelId] = [];
                        $modelFilename = $sceneObject->object->filename;

                        if (strpos($modelFilename, '.unp/')) {
                            $cutPosition = strpos($modelFilename, '.unp/') + 5;
                            $modelFilename = substr($modelFilename, $cutPosition);
                            $modelDir = substr($sceneObject->object->filename, 0, -1 * (strlen($modelFilename) + 1));
                            $modelsFilesCache[$bundleModelId][0] = [
                                'realPath' => $sceneObject->object->filename,
                                'zipPath' => $modelFilename
                            ];
                            $modelFiles = [];
                            LibraryController::readDirRecursive($objectsDir . $modelDir, $modelFiles);

                            foreach ($modelFiles as $modelFilePath) {
                                $realPath = substr($modelFilePath, $objectsDirLength);
                                $zipPath = substr($realPath, $cutPosition);

                                if ($sceneObject->object->filename === $realPath) {
                                    // skip main file
                                    continue;
                                }
                                $externalData[] = $zipPath;
                                $modelsFilesCache[$bundleModelId][] = [
                                    'realPath' => $realPath,
                                    'zipPath' => $zipPath
                                ];
                            }
                        } else {
                            $modelsFilesCache[$bundleModelId][0] = [
                                'realPath' => $sceneObject->object->filename,
                                'zipPath' => $modelFilename
                            ];
                        }
                    }

                    $type = explode('/', $sceneObject->object->type);
                    if ($sceneObject->object->type === 'video/youtube') {
                        // skip object?
                        continue;
                    } else {
                        $modelType = isset($sdkModelTypeByMimeType[$type[0]])
                            ? $sdkModelTypeByMimeType[$type[0]]
                            : null;
                        if (!$modelType) {
                            // skip object?
                            continue;
                        }
                    }

                    if ($type[0] == 'image' || $type[0] == 'video') {
                        $scale = $sceneObject->scale * $xScale;

                        $sceneObject->rz *= -1;
                    } else {
                        $scale = $sceneObject->scale * $unitToSdk;

                        $sceneObject->rx -= M_PI / 2;
                        $sceneObject->rz += M_PI;
                        $sceneObject->ry += M_PI;
                    }

                    $model = [
                        'id' => $bundleModelId,
                        'marker_id' => $bundleMarkerId,
                        'rotation_matrix' => self::rotateMatrixArray(
                            self::makeRotationFromEuler(
                                $sceneObject->rx,
                                $sceneObject->rz,
                                $sceneObject->ry,
                                $defaultRotationOrder
                            )
                        ),

                        'translation_vector' => [
                            $sceneObject->x * $unitToSdk + $offsetSdk['x'],
                            -1 * ($sceneObject->z * $unitToSdk + $offsetSdk['z']),
                            $sceneObject->y * $unitToSdk,
                        ],

                        'scale_matrix' => [
                            $scale, 0, 0, 0,
                            0, $scale, 0, 0,
                            0, 0, $scale, 0,
                            0, 0, 0, 1
                        ],
                        'data' => $modelFilename,
                        'external_data' => $externalData,
                        'model_type' => $modelType,
                        'custom_fields' => new stdClass()
                    ];

                    if ($model['model_type'] == $sdkModelTypeByMimeType['video']) {
                        $model['autostart'] = true;
                    }

                    if ($sceneObject->options && isset($sceneObject->options->custom_fields)) {
                        $model['custom_fields'] = $sceneObject->options->custom_fields;
                    }

                    if ($model['model_type'] == $sdkModelTypeByMimeType['object']) {
                        if (isset($sceneObject->options->animation) && $sceneObject->options->animation > -1) {
                            $model['custom_fields']->animation = $sceneObject->options->animation;
                        }
                    }

                    if ($sceneObject->options && isset($sceneObject->options->action) && isset($sceneObject->options->action->type)) {
                        $model['custom_fields']->action = [
                            'type' => $sceneObject->options->action->type
                        ];

                        if ($model['custom_fields']->action['type'] == 'url')
                            $model['custom_fields']->action['url'] = $sceneObject->options->action->url;

                        if ($model['custom_fields']->action['type'] == 'video')
                            $model['custom_fields']->action['video'] = $sceneObject->options->action->video;
                    }

                    $config->models[] = $model;
                }

                $bundleMarkerId++;
            }

            if ($config->markerConfiguration['marker_count'] > 0) {
                $output = self::generateBundleBinaries($bundlesDir . 'markers', $bundlesDir);
                if (!file_exists($bundlesDir . 'dbow.bin') || !file_exists($bundlesDir . 'pyramid.bin')) {
                    throw new Exception('Bundle binaries generation failed: ' . $output);
                }
            }

            /// generate zip
            $zip = new ZipArchive();
            $zipFileName = $bundleFileName;
            if (file_exists($bundlesDir . $zipFileName)) {
                unlink($bundlesDir . $zipFileName);
            }

            if ($zip->open(
                    $bundlesDir . $zipFileName,
                    ZipArchive::CREATE
                ) !== true) {
                throw new Exception('Failed to create <$zipFilename>\n');
            }

            $zip->addFromString('Config.json', json_encode($config, JSON_PRETTY_PRINT));

            if ($config->markerConfiguration['marker_count'] > 0) {
                $zip->addFile($bundlesDir . 'dbow.bin', 'dbow.bin');
                $zip->addFile($bundlesDir . 'pyramid.bin', 'pyramid.bin');
            }

            foreach ($config->markers as $marker) {
                if (file_exists($bundlesDir . $marker['marker_path'])) {
                    $zipMarkerFileName = substr($marker['marker_path'], 1);
                    $zip->addFile($bundlesDir . $marker['marker_path'], $zipMarkerFileName);
                } else {
                    throw new Exception('The marker file is not exists');
                }
            }

            foreach ($modelsFilesCache as $modelId => $modelsFiles) {
                // zip objects files
                foreach ($modelsFiles as $file) {
                    $zip->addFile(
                        APP_PATH . '/public/' . Library::DIRECTORY_PATH . $file['realPath'],
                        'models/' . $modelId . '/' . $file['zipPath']
                    );
                }
            }

            $zip->close();

            // cleanup
            if ($config->markerConfiguration['marker_count'] > 0) {
                unlink($bundlesDir . 'dbow.bin');
                unlink($bundlesDir . 'pyramid.bin');
                Base::unlinkDirRecursive($bundlesDir . 'markers');
            }

            $bundle->updated_at = date('Y-m-d H:i:s');

            if (!$bundle->save()) {
                $result['errors'] = 'Cannot save bundle.';
            }

            //$this->response->setJsonContent($config, JSON_PRETTY_PRINT);
            //$result = BundleDefault::generate($editionItem->project_id);

            if (!isset($result['errors'])) {
                $result['bundle'] = \Phalcon\Di::getDefault()->getConfig()->application->siteUri
                    . '/' . $bundlesDirRelative . $zipFileName;
            }
        } catch (Exception $e) {
            $result['errors'][] = $e->getMessage();
        }

        return $result;
    }

    public static function sdk3dImageSize($width, $height)
    {
        $ratio = $height / $width;

        if ($ratio >= 1) {
            $width3d = 1;
            $height3d = $ratio;
        } else {
            $width3d = $ratio;
            $height3d = 1;
        }

        return ['width' => $width3d, 'height' => $height3d];
    }

    /**
     * convert matrix array from column-major order to row-major
     *
     * @param $m
     * @return array
     */
    public static function rotateMatrixArray($m)
    {
        $a = [
            $m[0], $m[4], $m[8], $m[12],
            $m[1], $m[5], $m[9], $m[13],
            $m[2], $m[6], $m[10], $m[14],
            $m[3], $m[7], $m[11], $m[15]
        ];

        return $a;
    }

    public static function degreeToRadians($degree)
    {
        return $degree * M_PI / 180;
    }

    public static function radiansToDegree($radians)
    {
        return $radians * 180 / M_PI;
    }

    public static function makeRotationFromEuler($x, $y, $z, $order)
    {
        $te = [];

        $a = cos($x);
        $b = sin($x);
        $c = cos($y);
        $d = sin($y);
        $e = cos($z);
        $f = sin($z);

        if ($order === 'XYZ') {
            $ae = $a * $e;
            $af = $a * $f;
            $be = $b * $e;
            $bf = $b * $f;

            $te[0] = $c * $e;
            $te[4] = -$c * $f;
            $te[8] = $d;

            $te[1] = $af + $be * $d;
            $te[5] = $ae - $bf * $d;
            $te[9] = -$b * $c;

            $te[2] = $bf - $ae * $d;
            $te[6] = $be + $af * $d;
            $te[10] = $a * $c;
        } else if ($order === 'YXZ') {
            $ce = $c * $e;
            $cf = $c * $f;
            $de = $d * $e;
            $df = $d * $f;

            $te[0] = $ce + $df * $b;
            $te[4] = $de * $b - $cf;
            $te[8] = $a * $d;

            $te[1] = $a * $f;
            $te[5] = $a * $e;
            $te[9] = -$b;

            $te[2] = $cf * $b - $de;
            $te[6] = $df + $ce * $b;
            $te[10] = $a * $c;
        } else if ($order === 'ZXY') {
            $ce = $c * $e;
            $cf = $c * $f;
            $de = $d * $e;
            $df = $d * $f;

            $te[0] = $ce - $df * $b;
            $te[4] = -$a * $f;
            $te[8] = $de + $cf * $b;

            $te[1] = $cf + $de * $b;
            $te[5] = $a * $e;
            $te[9] = $df - $ce * $b;

            $te[2] = -$a * $d;
            $te[6] = $b;
            $te[10] = $a * $c;
        } else if ($order === 'ZYX') {
            $ae = $a * $e;
            $af = $a * $f;
            $be = $b * $e;
            $bf = $b * $f;

            $te[0] = $c * $e;
            $te[4] = $be * $d - $af;
            $te[8] = $ae * $d + $bf;

            $te[1] = $c * $f;
            $te[5] = $bf * $d + $ae;
            $te[9] = $af * $d - $be;

            $te[2] = -$d;
            $te[6] = $b * $c;
            $te[10] = $a * $c;
        } else if ($order === 'YZX') {
            $ac = $a * $c;
            $ad = $a * $d;
            $bc = $b * $c;
            $bd = $b * $d;

            $te[0] = $c * $e;
            $te[4] = $bd - $ac * $f;
            $te[8] = $bc * $f + $ad;

            $te[1] = $f;
            $te[5] = $a * $e;
            $te[9] = -$b * $e;

            $te[2] = -$d * $e;
            $te[6] = $ad * $f + $bc;
            $te[10] = $ac - $bd * $f;
        } else if ($order === 'XZY') {
            $ac = $a * $c;
            $ad = $a * $d;
            $bc = $b * $c;
            $bd = $b * $d;

            $te[0] = $c * $e;
            $te[4] = -$f;
            $te[8] = $d * $e;

            $te[1] = $ac * $f + $bd;
            $te[5] = $a * $e;
            $te[9] = $ad * $f - $bc;

            $te[2] = $bc * $f - $ad;
            $te[6] = $b * $e;
            $te[10] = $bd * $f + $ac;
        }

        // last column
        $te[3] = 0;
        $te[7] = 0;
        $te[11] = 0;

        // bottom row
        $te[12] = 0;
        $te[13] = 0;
        $te[14] = 0;
        $te[15] = 1;

        return $te;
    }

    public static function markerSegmentation($sourceFile, $targetFile)
    {
        if (\Phalcon\Di::getDefault()->getConfig()->artools->segmentation == 'fake') {
            // test mode... just copy random part 30% width or height
            list($srcWidth, $srcHeight) = getimagesize($sourceFile);

            $cut = false;

            if ($cut) {
                $destWidth = $srcWidth * 0.3;
                $destHeight = $srcHeight * 0.3;
                $x = round(rand(0, $srcWidth - $destWidth));
                $y = round(rand(0, $srcHeight - $destHeight));
            } else {
                $destWidth = $srcWidth;
                $destHeight = $srcHeight;
                $x = 0;
                $y = 0;
            }

            $type = exif_imagetype($sourceFile);

            switch ($type) {
                case  IMAGETYPE_JPEG:
                    $src = imagecreatefromjpeg($sourceFile);
                    break;
                case IMAGETYPE_PNG:
                    $src = imagecreatefrompng($sourceFile);
                    break;
                default:
                    die('Failed to load image ' . $sourceFile);
            }

            $dest = imagecreatetruecolor($destWidth, $destHeight);

            imagecopy($dest, $src, 0, 0, $x, $y, $destWidth, $destHeight);

            imagefilter($dest, IMG_FILTER_GRAYSCALE);
            imagejpeg($dest, $targetFile);
            imagedestroy($dest);
            imagedestroy($src);

            $ret = [$x, $y, $destWidth, $destHeight];

            return implode(',', $ret);
        }

        $call = \Phalcon\Di::getDefault()->getConfig()->artools->segmentation
            . ' ' . escapeshellarg($sourceFile)
            . ' ' . escapeshellarg($targetFile) . ' 2>&1';

        return exec($call);
    }

    public static function generateBundleBinaries($markersFolder, $targetFolder)
    {

        if (\Phalcon\Di::getDefault()->getConfig()->artools->bundlebinaries == 'fake') {

            file_put_contents($targetFolder . 'dbow.bin', 'fake');
            file_put_contents($targetFolder . 'pyramid.bin', 'fake');

            return 'Done.';
        }

        $call = \Phalcon\Di::getDefault()->getConfig()->artools->bundlebinaries
            . ' ' . escapeshellarg($markersFolder)
            . ' ' . escapeshellarg($targetFolder) . ' 2>&1';

        return exec($call);
    }

}