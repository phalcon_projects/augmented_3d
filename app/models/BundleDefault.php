<?php

class BundleDefault extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $project_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $version;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $hash;

    /**
     *
     * @var string
     * @Column(type="timestamp", nullable=false)
     */
    public $created_at;

    /**
     *
     * @var string
     * @Column(type="timestamp", nullable=false)
     */
    public $updated_at;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'bundles_default';
    }

    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->hasOne('edition_id', Editions::class, 'id', ['alias' => 'edition']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return BundleDefault[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return BundleDefault
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public static function generate($projectId)
    {
        return Bundle::generate($projectId, true);
    }
}