<?php

class StatisticAPIKey extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $api_key;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'statistic_api_keys';
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->hasOne('id', Users::class, 'user_id', ['alias' => 'user']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return App[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return App
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @param integer $user_id
     * @return bool
     */
    public function isOwnedBy($user_id) {
        return (int)$user_id && (int)$this->user_id && $this->user_id == $user_id;
    }
}