<?php

use Phalcon\Mvc\Model;

class Oauth extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var integer
     * @Column(type="string", length=255, nullable=false)
     */
    public $provider;

    /**
     *
     * @var integer
     * @Column(type="string", length=255, nullable=false)
     */
    public $auth_token;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'oauth';
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->hasOne('id', Users::class, 'user_id');
    }

}