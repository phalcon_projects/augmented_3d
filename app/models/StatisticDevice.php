<?php

//namespace App\Models\Statistics;

class StatisticDevice extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $statistic_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $type;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $os;

    /**
     *
     * @var string
     * @Column(type="sting", length=255, nullable=true)
     */
    public $os_version;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('a3dstatistics');
        $this->belongsTo('statistic_id', StatisticEdition::class, 'id', ['alias' => 'statistic']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'devices';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Editions[]|Editions
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Editions
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}