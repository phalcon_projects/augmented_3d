<?php

class OrderItem extends \Phalcon\Mvc\Model
{
    const STATE_PENDING = 1;
    const STATE_PAYED = 2;

    const STATES = [self::STATE_PENDING, self::STATE_PAYED];
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $order_id;

    /**
     *
     * @var string
     * @Column(type="string", length=11, nullable=false)
     */
    public $title;

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $quantity;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->belongsTo('order_id', Order::class, 'id');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'order_items';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Order[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Order
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}