<?php

class Covers extends \Phalcon\Mvc\Model
{
    const DIRECTORY_PATH = 'files/covers/';

    const STATUS_TEMP    = 0;
    const STATUS_NORMAL  = 1;
    const STATUS_DELETED = 2;

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

	/**
	 *
	 * @var integer
	 * @Column(type="integer", length=11, nullable=false)
	 */
	public $edition_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $title;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $filename;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=false)
     */
    public $rating;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $position;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $status;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $main;

	/**
	 * Initialize method for model.
	 */
	public function initialize()
	{
        $this->setSchema('augmented3d');
//		$this->hasMany("id", "Objects", "cover_id");
		$this->hasMany("id", "Objects", "cover_id");
        $this->belongsTo('edition_id', 'Editions', 'id', ['alias' => 'Edition']);

//		$this->hasManyToMany(
//			'id',
//			__NAMESPACE__ . '\SceneObjects',
//			'cover_id',
//			'object_id',
//			__NAMESPACE__ . '\Objects',
//			'id',
//			['alias' => 'objects']
//		);
	}

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'covers';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Covers[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Covers
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

	public function delete()
	{
		// delete all child elements
		// 1. Scene objects
		$objects = $this->getObjects(); /* @var Objects[] $objects */
		foreach ($objects as $object) {
			$object->delete();
		}

		// 2. own image
		$file = APP_PATH . '/public/' . self::DIRECTORY_PATH . $this->filename;
		if (file_exists($file) && !is_dir($file))
			unlink($file);

		// delete self
		return parent::delete();
	}

    /**
     * @param integer $userId
     * @return bool
     */
    public function userHaveAccess($userId)
    {
        return $userId && $this->getEdition()->userHaveAccess($userId);
    }

	/**
	 * @param integer $user_id
	 * @return bool
	 */
	public function isOwnedBy($user_id) {
		return $user_id && $this->getEdition()->isOwnedBy($user_id);
	}

	public function getOwnerId() {
		return $this->getEdition()->getOwnerId();
	}
}
