<?php

class Library extends \Phalcon\Mvc\Model
{
    const DIRECTORY_PATH        = 'files/objects/';
    const DIRECTORY_PATH_THUMBS = 'files/thumbs/';

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $edition_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $title;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $filename;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $thumb;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $type;

    /**
     *
     * @var integer
     * @Column(type="integer", length=1, nullable=false)
     */
    public $deleted;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $filesize;

    /**
     *
     * @var array
     * @Column(type="string", length=255, nullable=false)
     */
    public $params;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->belongsTo('edition_id', 'Editions', 'id', ['alias' => 'Edition']);
        $this->hasMany('id', 'Objects', 'object_id');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'objects';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Library[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

//	public function beforeValidationOnCreate()
//	{
//		if (!$this->thumb) {
//			$this->thumb = new \Phalcon\Db\RawValue('');
//		}
//	}


    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Library
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    public function softDelete()
    {
        $this->deleted = 1;

        // delete references from scenes
        foreach ($this->getObjects() as $object) {
            $object->softDelete();
        }

        return $this->save();
    }

    public function unDelete()
    {
        $this->deleted = 0;
        return $this->save();
    }

    public function afterFetch()
    {
        //Convert the string to an array
        $this->params = json_decode($this->params);
    }

    public function beforeSave()
    {
        //Convert the array into a string
        $this->params = json_encode($this->params);
    }

	/**
	 * @return array|bool
	 */
    public function _deleteFiles() {
        // delete self files
		$result = [];

        $base_file = explode('/', $this->filename);
        $base_file = $base_file[0];
        $file = APP_PATH . '/public/' . self::DIRECTORY_PATH . $base_file;

        if (file_exists($file)) {
            if (is_dir($file)) {
                $result = self::deleteDirectory($file);
            } else {
                $result[$base_file] = unlink($file);
            }
        }

        // delete self thumbnails
        if ($this->thumb) {
            $thumb_filename = APP_PATH . '/public/' . self::DIRECTORY_PATH_THUMBS . $this->thumb;
            if (file_exists($thumb_filename) && !is_dir($thumb_filename)) {
				$result[$this->thumb] = unlink($thumb_filename);
            }
        }

        return $result;
    }

    public function delete()
    {
        $this->_deleteFiles();

        // delete references from scenes
        $objects = Objects::find("object_id='{$this->id}'");

        foreach ($objects as $object) {
            $object->delete();
        }

        return parent::delete();
    }

    private static function deleteDirectory($directoryName)
    {
        if (!is_dir($directoryName))
            return false;

        $directoryHandle = opendir($directoryName);

        if (!$directoryHandle)
            return false;

        $result = [];

        while ($file = readdir($directoryHandle)) {
            if ($file != '.' && $file != '..') {
                if (!is_dir($directoryName . '/' . $file))
                    $result[$file] = unlink($directoryName . '/' . $file);
                else
                    $result[$file] = self::deleteDirectory($directoryName . '/' . $file);
            }
        }

        closedir($directoryHandle);
        rmdir($directoryName);
        return $result;
    }
}
