<?php

class Licenses extends \Phalcon\Mvc\Model
{
    const TYPE_MAIN = 1;
    const TYPE_TRAFFIC = 2;
    const TYPE_CLOUD_SPACE = 3;
    const TYPES = [
        self::TYPE_MAIN => [
            'id' => self::TYPE_MAIN,
            'title' => 'Main'
        ],
        self::TYPE_TRAFFIC => [
            'id' => self::TYPE_TRAFFIC,
            'title' => 'Traffic package'
        ],
        self::TYPE_CLOUD_SPACE => [
            'id' => self::TYPE_CLOUD_SPACE,
            'title' => 'Cloud space plan'
        ]
    ];

    const PLAN_TRAFFIC_FREE = 0;
    const PLAN_TRAFFIC_10_GB = 1;
    const PLAN_TRAFFIC_50_GB = 2;
    const PLAN_TRAFFIC_100_GB = 3;
    const PLANS_TRAFFIC = [
        self::PLAN_TRAFFIC_FREE => [
            'id' => self::PLAN_TRAFFIC_FREE,
            'space' => '5 GB',
            'price' => 0
        ],
        self::PLAN_TRAFFIC_10_GB => [
            'id' => self::PLAN_TRAFFIC_10_GB,
            'space' => '10 GB',
            'price' => 299
        ],
        self::PLAN_TRAFFIC_50_GB => [
            'id' => self::PLAN_TRAFFIC_50_GB,
            'space' => '50 GB',
            'price' => 399
        ],
        self::PLAN_TRAFFIC_100_GB => [
            'id' => self::PLAN_TRAFFIC_100_GB,
            'space' => '100 GB',
            'price' => 599
        ]
    ];

    const PLAN_CLOUD_SPACE_FREE = 0;
    const PLAN_CLOUD_SPACE_50_GB = 1;
    const PLAN_CLOUD_SPACE_100_GB = 2;
    const PLANS_CLOUD_SPACE = [
        self::PLAN_CLOUD_SPACE_FREE => [
            'id' => self::PLAN_CLOUD_SPACE_FREE,
            'space' => '15 GB',
            'price' => 0
        ],
        self::PLAN_CLOUD_SPACE_50_GB => [
            'id' => self::PLAN_CLOUD_SPACE_50_GB,
            'space' => '50 GB',
            'price' => 299
        ],
        self::PLAN_CLOUD_SPACE_100_GB => [
            'id' => self::PLAN_CLOUD_SPACE_100_GB,
            'space' => '100 GB',
            'price' => 599
        ]
    ];

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=255, nullable=false)
     */
    public $type;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->hasOne('id', Users::class, 'user_id');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'licenses';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Licenses[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Licenses
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @param integer $app_id
     * @return bool
     */
    public function belongsToApp($app_id) {
        return (int)$app_id && (int)$this->app_id && $this->app_id == $app_id;
    }

    public static function getPriceByType($type)
    {
        switch ($type) {
            case self::TYPE_MAIN:
                return 200;
                break;
            case self::TYPE_TRAFFIC:
                return 50;
                break;
            case self::TYPE_CLOUD_SPACE:
                return 100;
                break;
        }
    }
}