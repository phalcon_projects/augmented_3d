<?php

class Objects extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $cover_id;

	/**
	 *
	 * @var integer
	 * @Column(type="integer", length=11, nullable=false)
	 */
	public $object_id;

	/**
	 *
	 * @var string
	 * @Column(type="string", length=255, nullable=false)
	 */
	public $title;

    /**
     *
     * @var double
     * @Column(type="double", nullable=false)
     */
    public $x;

    /**
     *
     * @var double
     * @Column(type="double", nullable=false)
     */
    public $y;

    /**
     *
     * @var double
     * @Column(type="double", nullable=false)
     */
    public $z;

    /**
     *
     * @var double
     * @Column(type="double", nullable=false)
     */
    public $rx;

    /**
     *
     * @var double
     * @Column(type="double", nullable=false)
     */
    public $ry;

    /**
     *
     * @var double
     * @Column(type="double", nullable=false)
     */
    public $rz;

    /**
     *
     * @var double
     * @Column(type="double", nullable=false)
     */
    public $scale;

	/**
	 *
	 * @var integer
	 * @Column(type="integer", length=1, nullable=false)
	 */
	public $deleted;

    /**
     * Initialize method for model.
     */

	/**
	 *
	 * @var array
	 * @Column(type="string", length=255, nullable=false)
	 */
	public $options;

    public function initialize()
    {
        $this->setSchema('augmented3d');
    	$this->hasOne("object_id", Library::class, "id", ['alias' => 'object']);
        $this->belongsTo('cover_id', Covers::class, 'id', ['alias' => 'Covers']);
        //$this->belongsTo('object_id', 'Objects', 'id', ['alias' => 'Objects']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'scene_objects';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Objects[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Objects
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

	public function softDelete() {
    	$this->deleted = 1;
    	return $this->save();
	}
	public function unDelete() {
		$this->deleted = 0;
		return $this->save();
	}

	public function toPlainVersion() {
    	$object = $this->dump();
		unset($object['object']);

		if (!$object['title'])
			$object['title'] = $this->object->title;
		$object['filename'] = $this->object->filename;
		$object['type'] = $this->object->type;
		$object['thumb'] = $this->object->thumb? $this->object->thumb : "";

		foreach (array('x', 'y', 'z', 'rx', 'ry', 'rz') as $attr) {
			if (!is_numeric($object[$attr]))
				$object[$attr] = 0;
		}
		if (!is_numeric($object['scale']) || $object['scale']<=0)
			$object['scale'] = 1;

		$object['params'] = $this->object->params;

		return $object;
	}

	public function afterFetch()
	{
		//Convert the string to an array
		$this->options = json_decode($this->options);
	}

	public function beforeSave()
	{
		//Convert the array into a string
		$this->options = json_encode($this->options);
	}

    /**
     * @param integer $userId
     * @return bool
     */
    public function userHaveAccess($userId)
    {
        return $userId && $this->getCover()->userHaveAccess($userId);
    }


//	public function delete()
//	{
//		$file = APP_PATH . "/public/files/objects/" . $this->filename;
//		if (file_exists($file) && !is_dir($file))
//			unlink($file);
//
//		return parent::delete();
//	}
}
