<?php

//namespace App\Models\Statistics;

class StatisticEdition extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $edition_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $parameter;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=true)
     */
    public $value;

    /**
     *
     * @var string
     * @Column(type="timestamp", nullable=false)
     */
    public $date;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('a3dstatistics');
        $this->hasOne('statistic_id', StatisticDevice::class, 'id', ['alias' => 'devices']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'statistics';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Editions[]|Editions
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Editions
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
}