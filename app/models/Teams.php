<?php

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class Teams extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="varchar", length=255, nullable=false)
     */
    public $title;

    /**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return 'teams';
	}

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->hasMany('id', TeamUsers::class, 'team_id', ['alias' => 'Members']);
        $this->hasMany('id', TeamPermissions::class, 'team_id', ['alias' => 'permissions']);
        $this->belongsTo('project_id', 'Projects', 'id', ['alias' => 'Project']);
    }

}
