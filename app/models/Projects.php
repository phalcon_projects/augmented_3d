<?php

class Projects extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $user_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $title;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->hasMany('id', Editions::class, 'project_id');
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'projects';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Projects[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Projects
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Allows to query the first record that match the specified UUID
     *
     * @param string $uuid
     * @return Projects
     */
    public static function findFirstByUUID($uuid = null)
    {
        if (!$uuid) {
            return null;
        }

        return parent::findFirst([
            'uuid = :1:',
            'bind' => [1 => $uuid]
        ]);
    }


    /**
     * @return integer
     */
    public function getEditionsCount()
    {
        return $this->getEditions()->count();
    }

    /**
     * @param integer $user_id
     * @return bool
     */
    public function isOwnedBy($user_id)
    {
        return (int)$user_id && (int)$this->user_id && $this->user_id == $user_id;
    }

    /**
     * @return int
     */
    public function getOwnerId()
    {
        return $this->user_id;
    }

    public function getImage()
    {
        $edition = Editions::findFirst(["project_id='" . $this->id . "'", "order" => "title"]);
        if ($edition)
            return $edition->getImage();
        else
            return "";
    }

    /**
     * @return integer
     */
    public function getTotalFileSize()
    {
        $total = 0;

        $editions = $this->getEditions();
        foreach ($editions as $edition) {
            $total += $edition->getTotalFileSize();
        }

        return $total;
    }

    public function delete()
    {
        // delete all child elements

        $editions = $this->getEditions();
        /* @var Editions[] $editions */
        foreach ($editions as $edition) {
            $edition->delete();
        }

        // delete self
        return parent::delete();
    }

    public function userHaveAccess($userId)
    {
        $teams = TeamUsers::find([
            'columns' => 'team_id',
            'user_id = :user_id: AND role > :user_role:',
            'bind' => [
                'user_id' => $userId,
                'user_role' => Users::ROLE_USER
            ]
        ]);

        $userTeamsIds = array_map(function ($item) {
            return $item['team_id'];
        }, $teams->toArray());

        $access = ProjectAccess::find([
            'project_id = :project_id: AND team_id IN (:team_ids:) AND access_level >= :access_level:',
            'bind' => [
                'project_id' => $this->id,
                'team_ids' => implode(',', $userTeamsIds),
                'access_level' => ProjectAccess::ACCESS_LEVEL_VIEW
            ]
        ]);

        return $this->user_id === $userId || $access;
    }
}
