<?php

class VideoLinks extends \Phalcon\Mvc\Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $object_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $title;

    /**
     *
     * @var double
     * @Column(type="text", nullable=false)
     */
    public $link;

    /**
     *
     * @var double
     * @Column(type="string", nullable=false)
     */
    public $status;

    /**
     *
     * @var double
     * @Column(type="string", nullable=false)
     */
    public $file_name;

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'video_loader';
    }

    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->belongsTo('object_id', Objects::class, 'id', ['alias' => 'object']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Objects[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Objects
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /*public function delete()
	{
		$file = APP_PATH . '/public/files/objects/' . $this->filename;
		if (file_exists($file) && !is_dir($file))
			unlink($file);

		return parent::delete();
	}*/
}
