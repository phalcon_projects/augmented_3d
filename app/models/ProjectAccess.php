<?php

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class ProjectAccess extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $team_id;
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $project_id;

    /**
     *
     * @var integer
     * @Column(type="tinyint", length=4, nullable=false)
     */
    public $access_level;

    const ACCESS_LEVEL_RESTRICTED = 0;
    const ACCESS_LEVEL_VIEW = 1;
    const ACCESS_LEVEL_EDIT = 2;

    /**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource()
	{
		return 'project_access';
	}

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        /*$this->hasMany('id', Teams::class, 'team_id');
        $this->hasMany('id', Projects::class, 'project_id');*/
        $this->belongsTo('team_id', Teams::class, 'id', ['alias' => 'Team']);
        $this->belongsTo('user_id', Projects::class, 'id', ['alias' => 'Project']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Editions[]|Editions
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Editions
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
