<?php

class Editions extends \Phalcon\Mvc\Model
{
    const STATE_NOT_PUBLISHED = 0;
    const STATE_PUBLISHED = 1;

    const STATES = [
        self::STATE_NOT_PUBLISHED => 'Not published',
        self::STATE_PUBLISHED => 'Published'
    ];

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $project_id;

    /**
     *
     * @var string
     * @Column(type="string", length=255, nullable=false)
     */
    public $title;

    /**
     *
     * @var integer
     * @Column(type="integer", length=4, nullable=false)
     */
    public $state;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema('augmented3d');
        $this->hasMany("id", "Covers", "edition_id");
        $this->hasMany("id", "Library", "edition_id");
        $this->belongsTo('project_id', 'Projects', 'id', ['alias' => 'Project']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'editions';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Editions[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Editions
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @return integer
     */
    public function getCoversCount()
    {
        return $this->getCovers("status='" . Covers::STATUS_NORMAL . "'")->count();
    }

    /**
     * @param integer $userId
     * @return bool
     */
    public function userHaveAccess($userId)
    {
        return $userId && $this->getProject()->userHaveAccess($userId);
    }

    public function getOwnerId()
    {
        return $this->getProject()->getOwnerId();
    }

    public function getImage()
    {
        $mainCover = Covers::findFirst([
            'edition_id = :edition_id: AND main = :main: AND status = :status:',
            'bind'  => [
                'edition_id' => $this->id,
                'main' => 1,
                'status'     => Covers::STATUS_NORMAL
            ],
            'order' => 'position'
        ]);

        if ($mainCover)
            return Covers::DIRECTORY_PATH . $mainCover->filename;
        else
            return '';
    }

    /**
     * @return integer
     */
    public function getTotalFileSize()
    {
        $total = 0;

        $covers = $this->getCovers();
        /* @var Covers[] $covers */
        foreach ($covers as $cover) {
            if ($cover->filename) {
                $file = APP_PATH . '/public/files/covers/' . $cover->filename;
                if (file_exists($file) && !is_dir($file))
                    $total += filesize($file);
            }
        }

        /* @var Library[] $objects */
        $objects = $this->getLibrary();

        foreach ($objects as $object) {
            $total += $object->filesize;
        }

        return $total;
    }

    public function delete()
    {
        // delete all child elements
        // 1. Covers
        $covers = $this->getCovers();
        /* @var Covers[] $covers */
        foreach ($covers as $cover) {
            $cover->delete();
        }

        // 1. Shared media pool
        $library_objects = $this->getLibrary();
        /* @var Library[] $library_objects */
        foreach ($library_objects as $library_object) {
            $library_object->delete();
        }

        // delete self
        return parent::delete();
    }
}
