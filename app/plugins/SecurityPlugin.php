<?php

use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Adapter\Memory as AclList;

/**
 * SecurityPlugin
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class SecurityPlugin extends Plugin
{
    /**
     * Returns an existing or new access control list
     *
     * @returns AclList
     */
    public function getAcl()
    {
        $debug = false;
        $aclIniFile = APP_PATH . '/app/config/acl.ini';
        $aclMtime = filemtime($aclIniFile);

        $updateAcl = $debug || !isset($this->persistent->acl) || $this->persistent->acl_mtime !== $aclMtime;

        if ($updateAcl) {
            $acl = new AclList();

            $acl->setDefaultAction(Acl::DENY);

            // Register roles
            $roles = [
                'admins' => new Role(
                    'Admins',
                    'Admin privileges, granted after admin sign in.'
                ),
                'users' => new Role(
                    'Users',
                    'Member privileges, granted after sign in.'
                ),
                'guests' => new Role(
                    'Guests',
                    'Anyone browsing the site who is not signed in is considered to be a "Guest".'
                )
            ];

            foreach ($roles as $role) {
                $acl->addRole($role);
            }

            $aclConfig = new Phalcon\Config\Adapter\Ini($aclIniFile);

            foreach ($aclConfig as $rows) {
                foreach ($rows as $resource => $actions_str) {
                    $actions = explode(',', $actions_str);
                    $acl->addResource(new Resource($resource), $actions);
                }
            }

            //Grant access to public areas to both users and guests
            foreach ($roles as $role) {
                foreach ($aclConfig->public as $resource => $actions_str) {
                    $actions = explode(',', $actions_str);
                    foreach ($actions as $action) {
                        $acl->allow($role->getName(), $resource, $action);
                    }
                }
            }

            //Grant access to private area to role Users
            if (isset($aclConfig->users)) {
                foreach ($aclConfig->users as $resource => $actions_str) {
                    $actions = explode(",", $actions_str);
                    foreach ($actions as $action) {
                        $acl->allow('Admins', $resource, $action);
                        $acl->allow('Users', $resource, $action);
                    }
                }
            }

            //Grant access to admin area and actions to role Admins
            if (isset($aclConfig->admins)) {
                foreach ($aclConfig->admins as $resource => $actions_str) {
                    $actions = explode(",", $actions_str);
                    foreach ($actions as $action) {
                        $acl->allow('Admins', $resource, $action);
                    }
                }
            }

            //The acl is stored in session, APC would be useful here too
            $this->persistent->acl = $acl;
            $this->persistent->acl_mtime = $aclMtime;
        }

        return $this->persistent->acl;
    }

    /**
     * This action is executed before execute any action in the application
     *
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        $auth = $this->session->get('auth');

        if (!$auth) {
            $role = 'Guests';
        } else {
            if (isset($auth['role'])
                && ($auth['role'] == Users::ROLE_ADMIN || $auth['role'] == Users::ROLE_MEGA_ADMIN)) {
                $role = 'Admins';
            } else {
                $role = 'Users';
            }
        }

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        $acl = $this->getAcl();
        $allowed = $acl->isAllowed($role, $controller, $action);
        // var_dump($controller, $action, $role, $allowed);exit;

        if (!$allowed) {
            if ($role == 'Guests' && !$this->request->isAjax()) {
                // redirect guests to login page
                $dispatcher->forward([
                    'controller' => 'session',
                    'action' => 'index'
                ]);
            } else {
                if (!$acl->isResource($controller)) {
                    error_log("Security 404: [$controller] is not in ACL list");
                    $dispatcher->forward([
                        'controller' => 'errors',
                        'action' => 'show404',
                        'params' => [ "Security 404: [$controller] is not in ACL list" ]
                    ]);

                    return false;
                }

                $dispatcher->forward([
                    'controller' => 'errors',
                    'action' => 'show401',
                    'params' => [ "Security 404: [$controller,$action] is not allowed" ]
                ]);
                //$this->session->destroy();
            }

            return false;
        }
    }
}
