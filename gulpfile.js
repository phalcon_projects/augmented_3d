var gulp = require('gulp'),
	spritesmith = require('gulp.spritesmith');
//var stripDebug = require('gulp-strip-debug');

gulp.task('sprite', function() {
	var spriteData =
		gulp.src([
			'./public/css/buttons/*.*',
			'./public/css/ui/*.*'
		]) // путь, откуда берем картинки для спрайта
			.pipe(spritesmith({
				imgName: 'sprites.png',
				cssName: 'sprites.scss',
				cssVarMap: function(sprite) {
					sprite.name = 's-' + sprite.name
				}
			}));

	spriteData.img.pipe(gulp.dest('./public/css/')); // путь, куда сохраняем картинку
	spriteData.css.pipe(gulp.dest('./public/css/')); // путь, куда сохраняем стили
});